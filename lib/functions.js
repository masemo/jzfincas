/*------------------------------------------------------------ valida email --*/
function validamail(texto) {
    var mailres = true;
    var cadena = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890@._-";
    var arroba = texto.indexOf("@", 0);
    if ((texto.lastIndexOf("@")) !== arroba)
        arroba = -1;
    var punto = texto.lastIndexOf(".");
    for (var contador = 0; contador < texto.length; contador++) {
        if (cadena.indexOf(texto.substr(contador, 1), 0) === -1) {
            mailres = false;
            break;
        }
    }
    if ((arroba > 1) && (arroba + 1 < punto) && (punto + 2 < (texto.length)) && (mailres == true) && (texto.indexOf("..", 0) == -1))
        mailres = true;
    else
        mailres = false;

    return mailres;
}
/*---------------------------------------------------------- element exists --*/
function exists(target) {
    if ($(target).length > 0) {
        return true;
    }
    return false;
}
/*----------------------------------------------------------------- cookies --*/
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}
/*--------------------------------------------------------------- scroll to --*/
function scrollPageTo(target, speed) {
    speed = typeof speed !== 'undefined' ? speed : 500;
    $('html,body').animate({scrollTop: $(target).offset().top}, speed);
}