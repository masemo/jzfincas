<?
function generate_href($url) {
    if ($url != "") {
        if (strpos($url, 'http://') === false || strpos($url, 'https://') === false) {
            return 'http://' . $url;
        }
    }
    return $url;
}

function obtener_parametros_url($paramString) {
    $param = explode("&", $paramString);
    for ($i = 0; $i < count($param); $i++) {
        $exploded = explode("=", $param[$i]);
        for ($j = 0; $j < count($exploded[$i]) / 2; $j++) {
            $key = $exploded[$j];
            $value = "";
            if(isset($exploded[$j + 1])){
                $value = $exploded[$j + 1];
            }
            $param_array[$key] = $value;
        }
    }
    return $param_array;
}
//-- any copyright
function copyright_year($copy) {
    if (date("Y") > $copy) {
        $copy = $copy . " - " . date("Y");
    }
    return $copy;
}
