(function ($) {
    $.fn.cookieAlert = function (options) {
        var defaults = {
            "domain": "sollutia",
            "infoUrl": "/aviso-legal.php",
            "infoTxt": "Utilizamos cookies para ofrecer nuestros servicios y recoger informaci&oacute;n estad&iacute;stica. Si contin&uacute;a navegando, entendemos que acepta su instalaci&oacute;n y uso. Puede cambiar la configuraci&oacute;n u obtener m&aacute;s informaci&oacute;n en nuestra Pol&iacute;tica de Cookies.",
            "aceptarTxt": "De acuerdo",
            "masinfoTxt": "M&aacute;s informaci&oacute;n",
            "fontSize": "11px",
            "backgroundColor": "#FFFFFF",
            "backgroundColorRgba": "255,255,255,.9",
            "borderTopColor": "#CCCCCC",
            "textColor": "#27639C",
            "btnBackgroundColor": "#27639C",
            "btnBackgroundColorHover": "#19405F",
            "btnColor": "white",
            "linkColor": "#5496D4",
            "linkColorHover": "#19405F",
            "acceptOn": "#wrapperio",
            "callback": null,
            "viejuno": false
        };
        var parameters = $.extend(defaults, options);
        var cookieAlert;
        var acceptOnSelector;
        var cookieName = "cookieAlert_" + parameters.domain;

        function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else
                var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        }
        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
        function accept_cookie() {
            createCookie(cookieName, 1, 120);
            close_popper();
        }

        function close_popper() {
            cookieAlert.slideUp(200);
        }
        function show_popper() {
            cookieAlert.delay(400).slideDown(200);
        }

        if (!readCookie(cookieName)) {
            return this.each(function () {
                var target = $(this);
                var popper = '<div id="cookieAlert_popper"><div class="cookieAlert_container"><div  class="cookieAlert_text"><span>' + parameters.infoTxt + '</span></div><div class="cookieAlert_btn"><button type="button" class="cookieAlert_aceptar">' + parameters.aceptarTxt + '</button><a href="' + parameters.infoUrl + '" target="blank" class="cookieAlert_masinfo">' + parameters.masinfoTxt + '</a>\n\</div></div></div>';
                var style = '<style type="text/css">';
                style += '#cookieAlert_popper{ position:fixed;bottom:0;left:0;width:100%;background:' + parameters.backgroundColor + ';background:rgba(' + parameters.backgroundColorRgba + ');border-top:solid 3px ' + parameters.borderTopColor + ';z-index:1000;overflow:hidden;display: none;}';
                style += '.cookieAlert_container{padding: 5px 5px 5px 15px;}';
                style += '.cookieAlert_container strong, .cookieAlert_container span{ color:' + parameters.textColor + ' !important;}';
                style += '.cookieAlert_container a, .cookieAlert_container button{ display:inline-block;border:none; }';
                style += '.cookieAlert_text, .cookieAlert_btn{ display:inline-block;vertical-align:middle;width:23%;}';
                style += '.cookieAlert_btn{text-align:center;}';
                style += '.cookieAlert_text{ font-size:' + parameters.fontSize + ';width:75%;}';
                style += '.cookieAlert_text span{ padding-right:10px;}';
                style += 'button.cookieAlert_aceptar{ padding:6px 10px;margin-right:15px;background:' + parameters.btnBackgroundColor + ';color:' + parameters.btnColor + ';text-decoration:none;border-solid 1px white;font-size:' + parameters.fontSize + ';}';
                style += 'button.cookieAlert_aceptar:hover {background:' + parameters.btnBackgroundColorHover + ';}';
                style += '.cookieAlert_masinfo:link,.cookieAlert_masinfo:visited {color:' + parameters.linkColor + ';}';
                style += '.cookieAlert_masinfo:hover {color:' + parameters.linkColorHover + ';}';
                style += '</style>';

                target.append(popper);
                $("head").append(style);

                cookieAlert = $("#cookieAlert_popper");

                show_popper();

                switch (parameters.acceptOn) {
                    case "button":
                        acceptOnSelector = $(".cookieAlert_aceptar");
                        break;
                    default:
                        acceptOnSelector = $(parameters.acceptOn);
                        if (acceptOnSelector.length === 0) {
                            acceptOnSelector = $(".cookieAlert_aceptar");
                        }
                        break;
                }
                if (parameters.viejuno) {
                    acceptOnSelector.click(function (event) {
                        event.preventDefault();
                        accept_cookie();
                    });
                } else {
                    acceptOnSelector.on("click", function (event) {
                        event.preventDefault();
                        accept_cookie();
                    });
                }
            });
        }
    };
})(jQuery);