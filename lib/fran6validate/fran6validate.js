/*!
 fran6validate v2.2
 fpascual@sollutia.com
 Date: Mon Dec 16 10:58:00 2013
 Utilitza jQuery;

 --- Estructura del formulari ------------------------------------------------:
 - Obligatòria estructura amb <label>
 <label for="nombre">Nombre</label>
 <input type="text" name="nombre" id="nombre" value="" />

 - Els camps tipo PASSWORD han de tenir la seua confirmació fent referència al primer camp amb "confirm_"+id primer camp password
 <input type="password" name="password_admin" id="password_admin" />
 <input type="password" name="confirm_password" id="confirm_password_admin" />

 --- Atributs i Parámetres per a validar els camps ----------------------------:
 - Els paràmetres bàsics per al fran6validate s'afegeixen a l'atribut CLASS
 required = El camp no ha d'estar buit.
 email = El camp ha de ser un email vàlid.
 <input type="text" name="nombre" id="nombre" class="required" />
 <input type="text" name="email_user" id="email_user" class="email" />

 - Poden haver varios paràmetres.
 <input type="text" name="email_user" id="email_user" class="email required" />

 - Validacions que necessiten indicar valors.
 minlength = El camp ha de tenir un mínim de caracters.
 <input type="text" name="user" id="user" class="required" minlength="6" />

 - Validar tipo de dato
 email: class="email"
 numero: class="esnumero"
 fecha: class="esfecha"

 --- Crida del fran6validate -------------------------------------------------:
 <script type="text/javascript">
 $(document).ready(function() {
 $("form#form_contacto input[type='submit']").click(function(event){
 event.preventDefault();
 $().fran6validate("form#form_contacto", "submit");
 });
 });
 </script>

 --- Label per als alerts de camps incorrectes--------------------------------:
 Amb estructura normal: 1 label -> 1 camp
 $().fran6validate("form#form_contacto", "submit");

 Amb varios camps per a un label o pirules semblants, indiquem el selector (podem deixar en null el array de idioma)
 $().fran6validate("#admin_form", "submit", null, "$('#'+id).parent().parent().find('label').text()");

 --- Callback de la validació -----------------------------------------------:
 - Una vegada validat el form es pot indicar en el segon paràmetre una funció a executar o directament fer SUBMIT()
 $().fran6validate("form#form_contacto", "fran6validate_callback");  //Nom de la funció sense parèntesi i amb cometes
 $().fran6validate("form#form_contacto", "submit");

 --- millores próxima versió-------------------------------------------------:
 - Validar caràcters especials.
 - Implementar datepickers.
 - En compte de fer un alert, donar estil guapet.
 */
(function ($) {
    $.fn.fran6validate = function (selector_form_id, callback_function, fran6validate_lang, label_selector) {
        var selector_form = $(selector_form_id);
        //console.log("selector_form", selector_form);
        if (selector_form.length == 0) {
            alert("fran6validate:\n No encuentro el formulario '" + selector_form + "'");
        } else {
            /*-------------------------------------------------- find_label --*/
            function find_label(id) {
                if (!label_selector) {
                    var label = $("label[for='" + id + "']").text();
                } else {
                    var label = eval(label_selector);
                    console.log(label);
                }
                return label;
            }
            /*-------------------------------------------------- submit_btn --*/
            var submit_btn = $("input[type='submit']", selector_form);
            submit_btn.prop("disabled", true); // desactivem submit per evitar reclicks

            /*-------------------------------------------------------- lang --*/
            var lang = [];
            lang["no_valido"] = " no es válido.";
            lang["no_valida"] = " no es válida.";
            lang["requiere_al_menos"] = " requiere al menos ";
            lang["caracteres"] = " caracteres.";
            lang["no_numero"] = " no es un numero.";
            lang["llenar_campo"] = "Debe rellenar el campo ";
            lang["no_coincide"] = "No coincide la confirmación del campo ";
            lang["y_confirmacion"] = " y la confirmación.";
            lang["errores_encontrados"] = "Se han encontrado los siguientes errores:";

            for (i in fran6validate_lang) {
                if (fran6validate_lang[i] !== "") {
                    lang[i] = fran6validate_lang[i];
                }
            }
            /*----------------------------------------------- gestió errors --*/
            var error_fields = [];
            function clean_errors(){
                $(".not_valid_field", selector_form).removeClass("not_valid_field");
                $(".not_valid_label", selector_form).removeClass("not_valid_label");
                $(".pestanyera .highlight", selector_form).removeClass("highlight");
            }
            function check_invalid_lang_tab() {
                $(".lang_group", selector_form).each(function () {
                    var lang_group_with_error = $(".not_valid_label:first", this).parents(".lang_group");
                    var lang_group_lang = lang_group_with_error.attr("data-lang");
                    lang_group_with_error.closest("section").find(".pestanyera li[data-lang='" + lang_group_lang + "'] button").addClass("highlight");
                });
            }
            function add_error_fields(field_id) {
                error_fields.push(field_id);
            }
            function display_error(error) {
                clean_errors();
                alert(error);
                tag_error_fields();
            }
            function tag_error_fields() {
                for (i in error_fields) {
                    $("#" + error_fields[i]).addClass("not_valid_field");
                    $("label[for='" + error_fields[i] + "']").addClass("not_valid_label");
                }
                $(".not_valid_field").on("focus, keyup, blur", function () {
                    $(this).removeClass("not_valid_field");
                });
                check_invalid_lang_tab();
            }


            /*------------------------------------------------- validacions --*/
            var error = "";
            function check_email(content, label, id) {
                if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(content))) {
                    error += "- " + label + lang["no_valido"] + "\n";
                    add_error_fields(id);
                }
            }
            function check_minlength(minlength, content, label, id) {
                if (minlength != undefined) {
                    var total_char = content.length;
                    if (total_char < minlength) {
                        error += "- " + label + lang["requiere_al_menos"] + minlength + lang["caracteres"] + "\n";
                        add_error_fields(id);
                    }
                }
            }
            function check_number(content, label, id) {
                if (isNaN(content)) {
                    error += "- " + label + lang["no_numero"] + "\n";
                    add_error_fields(id);
                }
            }
            function check_fecha(Cadena, label, id) {
                var Fecha = new String(Cadena)   // Crea un string  
                var ok = true;

                // Cadena Año  
                var Ano = new String(Fecha.substring(Fecha.lastIndexOf("/") + 1, Fecha.length))
                // Cadena Mes  
                var Mes = new String(Fecha.substring(Fecha.indexOf("/") + 1, Fecha.lastIndexOf("/")))
                // Cadena Día  
                var Dia = new String(Fecha.substring(0, Fecha.indexOf("/")))

                // Valido el año  
                if (isNaN(Ano) || Ano.length < 4 || parseFloat(Ano) < 1900) {
                    ok = false;
                }
                // Valido el Mes  
                if (isNaN(Mes) || parseFloat(Mes) < 1 || parseFloat(Mes) > 12) {
                    ok = false;
                }
                // Valido el Dia  
                if (isNaN(Dia) || parseInt(Dia, 10) < 1 || parseInt(Dia, 10) > 31) {
                    ok = false;
                }
                if (Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11 || Mes == 2) {
                    if (Mes == 2 && Dia > 28 || Dia > 30) {
                        ok = false;
                    }
                }

                if (!ok) {
                    error += "- " + label + lang["no_valida"] + "\n";
                    add_error_fields(id);
                }
            }

            function check_url(content, label, id) {
                if (!(/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(content))) {
                    error += "-  " + label + lang["no_valida"] + "\n";
                    add_error_fields(id);
                }
            }
            /*-------------------------------------------------------- required --*/
            $("input[type='text'], input[type='file'], input[type='email'], input[type='number'], input[type='date'], select, textarea, select, input[type='password']:not(.confirmar)", selector_form).each(function () {
                var required = $(this).hasClass("required");
                var esEmail = $(this).hasClass("email");
                var esNumero = $(this).hasClass("esnumero");
                var esFecha = $(this).hasClass("esfecha");
                var esUrl = $(this).hasClass("url");

                var content = $(this).val();
                var id = $(this).attr("id");
                var label = find_label(id);

                console.log(required+"|"+content);
                if (required && content === "") {
                    error += "- " + lang["llenar_campo"] + label + ".\n";
                    add_error_fields(id);
                } else {
                    var minlength = $(this).attr("minlength");

                    /*-------------------------------------- altres validacions --*/
                    check_minlength(minlength, content, label, id);
                    if (esEmail) {
                        check_email(content, label, id);
                    }
                    if (esNumero) {
                        check_number(content, label, id);
                    }
                    if (esFecha) {
                        check_fecha(content, label, id);
                    }
                    if (esUrl) {
                        check_url(content, label, id);
                    }
                }

            });

            /*------------------------------------------------ password confirm --*/
            $("input[type='password'].confirmar", selector_form).not("input[id*='confirm_']").each(function () {
                if ($(this).attr("disabled") != "disabled") {
                    var content = $(this).val();
                    var id = $(this).attr("id");

                    var content_confirm = $("#confirm_" + id).val();
                    var label = find_label(id);

                    if (content != "" || content_confirm != "") {
                        if (content != content_confirm) {
                            error += "- " + lang["no_coincide"] + label + ".\n";
                        } else {
                            var minlength = $(this).attr("minlength");

                            /*------------------------------ altres validacions --*/
                            check_minlength(minlength, content, label);
                        }
                    }
                    /*else{ //no es necesari si estan els dos buits
                     error += "-  "+lang["llenar_campo"]+label+lang["y_confirmacion"]+"\n";
                     add_error_fields(id);
                     add_error_fields("confirm_"+id);
                     }*/
                }


            });
            /*---------------------------------------------------------- estils --*/
            var f6v_css_not_valid_field = selector_form_id + " .not_valid_field{box-shadow: 0px 0px 4px red;}"
            var f6v_css_not_valid_form = selector_form_id + " .not_valid_label{color:firebrick;font-weight:bold;}"
            var f6v_css = "<style type='text/css'>";
            f6v_css += f6v_css_not_valid_field + f6v_css_not_valid_form;
            f6v_css += "</style>";
            $(f6v_css).appendTo("head");

            if (error == "") {
                if (callback_function == "submit") {
                    selector_form.submit();
                } else {
                    eval(callback_function + "()");
                }
            } else {
                error = lang["errores_encontrados"] + "\n\n" + error;
                display_error(error);
                submit_btn.prop("disabled", false);
            }
            return false;
        }
    }
})(jQuery);