/*!
 * fran6gallery v6.2
 * fpascual@sollutia.com
 * Date: sat 12 12 19:36:00 2015
 * Utilitza jquery;
 *
 * --- Exemple de crida---------------------------------------------------------:
 *  <script type="text/javascript">
 *	$(document).ready(function(){
 *	   $("#gal6home").fran6gallery({
 *			duracio: 4000, //delay entre fades
 *			velocitat: 800, //velocitat fade
 *			paginacio: 0, //redolinets(0) numeros(1)  no res(2)
 *			fletxes: false, //fletxes-desplaçament
 *		});
 *	});
 *  </script>
 *
 *  --- Exemple d'estructura ---------------------------------------------------:
 *   <div id="qualsevolId">
 *	<ul class="gal6">
 *	    <li><img src="../images/timThumb.php?src=trabajos/imagen.jpg&w=700" alt="imagen" /></li>
 *	    <li><img src="../images/timThumb.php?src=trabajos/imagen.jpg&w=700" alt="imagen" /></li>
 *	    <li><img src="../images/timThumb.php?src=trabajos/imagen.jpg&w=700" alt="imagen" /></li>
 *	</ul>
 *   </div>
 *
 *  --- millores próxima versió-------------------------------------------------:
 *   - canviar el augment infinit de z-index per un "swapDepths()"
 *   - possibilitat canvi tipus transició.
 */

(function (jQuery) {
    jQuery.extend(jQuery.fn, {
        fran6gallery: function (options) {
            jQuery(this).each(function () {
                var settings = jQuery.extend({
                    duracio: 3000,
                    velocitat: 500,
                    paginacio: 0,
                    bulletSize: "16px",
                    bulletTextColor: "white",
                    bulletTextColor_selected: "black",
                    bulletBorder: "transparent",
                    bulletBackground: "gray",
                    bulletBackground_selected: "white",
                    fletxes: false,
                    autoplay: true,
                    fran6galleryUrl: "/lib/fran6gallery/",
                    timer: true,
                    timerBackground: "#e7e7e7",
                    timerColor: "#58585A",
                    paginacioDisplay: "default"
                }, options);

                var capaActual = "#" + $(this).attr("id");
                var elementActual = 1;
                var elementsTotal = 1;
                var elementAnterior = 1;
                var elementZindex = 1;
                var gal6interval;

                function posicioElements() {
                    elementActual++;
                    if (elementActual > elementsTotal) {
                        elementActual = 1;
                    }
                    elementAnterior++;
                    if (elementAnterior > elementsTotal) {
                        elementAnterior = 1;
                    }
                }

                function deselectarPaginacio() {
                    $(capaActual + " .gal6pag li").removeClass("gal6selected");
                }

                function animaTimer() {
                    $("#gal6timer div").css("width", "0%");
                    $("#gal6timer div").animate({
                        width: "100%"
                    }, (settings.duracio - settings.velocitat), "linear");
                }

                function fadeAllElements(except) {
                    $(capaActual + ' .gal6 li').removeClass("relative").css({
                        opacity: 0,
                        display: "none"
                    });
                    $(capaActual + ' .gal6 li:nth-child(' + except + ')').css("zIndex", elementZindex);
                    $(capaActual + ' .gal6pag').css("zIndex", ++elementZindex);
                    $(capaActual + ' .gal6 li:nth-child(' + except + ')')
                        .addClass("relative")
                        .css({
                            opacity: 0,
                            display: "block"
                        })
                        .animate({
                            opacity: 1
                        }, settings.velocitat);
                    elementActual = except;
                }

                function activaClick(num) {
                    $(capaActual + ' #gal6img_' + num).bind('click', {
                        numero: num
                    }, function (event) {
                        gotoAndStop(event.data.numero);
                    });
                }

                function gotoAndStop(img) {
                    if (typeof gal6interval !== 'undefined') {
                        clearInterval(gal6interval);
                    }
                    deselectarPaginacio();
                    fadeAllElements(img);
                    $(capaActual + ' #gal6img_' + img).addClass("gal6selected");
                }

                function calculaTarget(direccio) {
                    if (direccio == "prev") {
                        var actual = elementActual - 1;
                        if (actual <= 0) {
                            actual = elementsTotal;
                        }
                    }
                    if (direccio == "next") {
                        actual = elementActual + 1;
                        if (actual > elementsTotal) {
                            actual = 1;
                        }
                    }
                    actualitzaPaginacio(actual);
                    elementActual = actual;
                    return actual;
                }

                function actualitzaPaginacio(actual) {
                    $(capaActual + "_gal6counter").text(actual + "/" + elementsTotal)
                }

                function swapElements(primera) {
                    var slideAnterior = $(capaActual + ' .gal6 li:nth-child(' + elementAnterior + ')');
                    var slideActual = $(capaActual + ' .gal6 li:nth-child(' + elementActual + ')');
                    if (settings.paginacio != 2) {
                        deselectarPaginacio();
                        actualitzaPaginacio(elementActual);
                        $(capaActual + ' #gal6img_' + elementActual).addClass("gal6selected");
                    }
                    slideActual.css("zIndex", ++elementZindex);
                    slideAnterior.removeClass("relative");
                    slideActual.addClass("relative").css({
                        opacity: "0",
                        display: "block"
                    }).animate({
                        opacity: 1
                    }, settings.velocitat, function () {
                        if (!primera) {
                            slideAnterior.css({
                                opacity: "0",
                                display: "none"
                            });
                            if (settings.timer) {
                                animaTimer();
                            }
                        }

                    });
                    if (!primera) {
                        posicioElements();
                    }
                }

                function avant_la_entra() {
                    elementsTotal = $(capaActual + " .gal6 li").size();
                    elementAnterior = elementsTotal;
                    if (elementsTotal > 1) {
                        if (settings.paginacio != 2) {
                            $(capaActual).append('<div class="gal6pag"><ul></ul></div>');
                            for (i = 1; i <= elementsTotal; i++) {
                                $(capaActual + " .gal6pag ul").append('<li id="gal6img_' + i + '" class="gal6unselected"><span>' + i + '</span></li>');
                                activaClick(i);
                            }
                        }
                        if (settings.fletxes) {
                            $(capaActual).append('<div id="gal6left" class="fletxa"><span class="icon">&lt;</span></div><div id="gal6right" class="fletxa"><span class="icon">&gt;</span></div>');
                            $("#gal6left").click(function () {
                                gotoAndStop(calculaTarget("prev"));
                            });
                            $("#gal6right").click(function () {
                                gotoAndStop(calculaTarget("next"));
                            });
                        }
                        if (settings.timer) {
                            $('<div id="gal6timer"><div></div></div>').insertAfter($(".gal6", capaActual));
                        }
                        if (settings.autoplay) {
                            actualitzaPaginacio(elementActual);
                            if (elementsTotal > 1) {// si hi ha més d'una imatge enxufem l'automàtic, si nos, no cal.
                                swapElements();
                                gal6interval = setInterval(function () { // anxufem l'automàtic
                                    swapElements();
                                }, settings.duracio);
                            }
                        } else {
                            swapElements(true);
                        }
                    } else {
                        swapElements(true); //la primera
                    }
                    /*-------------- estils -----------------------------*/
                    var f6g_css_main = "";
                    f6g_css_main += capaActual + " .gal6{ margin:0; padding:0; overflow:hidden; z-index:1; background:url(" + settings.fran6galleryUrl + "loading.gif) no-repeat center center;list-style-type:none;height:auto;position:relative;}";
                    f6g_css_main += capaActual + " .gal6 li{ position:absolute; opacity:0;display:none;width:100%;top:0;left:0;}";
                    f6g_css_main += capaActual + " .gal6 li.relative{ position:relative;display:block;}";
                    f6g_css_main += capaActual + " .gal6 img{ display:block;}";


                    if (settings.paginacio != 2) {
                        switch (settings.paginacioDisplay) {
                            case "top-right":
                                f6g_css_main += capaActual + " .gal6pag{ position:absolute; right:20px; top:15px; z-index:3;}";
                                break;
                            case "bottom-right":
                                f6g_css_main += capaActual + " .gal6pag{ position:absolute; right:20px; bottom:15px;}";
                                break;
                            case "top-left":
                                f6g_css_main += capaActual + " .gal6pag{ position:absolute; left:20px; top:15px; z-index:3;}";
                                break;
                            case "bottom-left":
                                f6g_css_main += capaActual + " .gal6pag{ position:absolute; left:20px; bottom:15px;}";
                                break;
                            default:
                                f6g_css_main += capaActual + " .gal6pag{ width:100%; margin:8px 0;}";
                                break;
                        }
                        f6g_css_main += capaActual + " .gal6pag{ z-index:3;text-align: center;}";
                        f6g_css_main += capaActual + " .gal6pag ul li{display:inline-block; vertical-align:middle; height:" + settings.bulletSize + "; width:" + settings.bulletSize + "; border:solid 1px " + settings.bulletBorder + "; text-align: center; margin: 0 5px; cursor:pointer;}";
                        f6g_css_main += capaActual + " .gal6pag ul{ list-style-type: none; margin:0; padding:0; overflow: hidden;}";
                        f6g_css_main += capaActual + " .gal6pag ul li:not(.gal6selected):hover{opacity:.6}";
                        f6g_css_main += capaActual + " .gal6unselected{ background:" + settings.bulletBackground + "; color:" + settings.bulletTextColor + ";}";
                        f6g_css_main += capaActual + " .gal6selected{ background: " + settings.bulletBackground_selected + "; color:" + settings.bulletTextColor_selected + ";}";
                    }

                    //------- opcions extra ----
                    var f6g_css_opcions_extra = "";
                    if (settings.paginacio == 0) { // llevem números i posem redolinets
                        f6g_css_opcions_extra += capaActual + " .gal6pag ul li span{display:none;}";
                        f6g_css_opcions_extra += capaActual + " .gal6pag ul li {border-radius:100%;}";
                    } else {
                        if (settings.paginacio == 1) {
                            f6g_css_opcions_extra += capaActual + " .gal6pag ul li span{display:none;}";
                            f6g_css_opcions_extra += capaActual + " .gal6pag ul li {border:none; background-color: transparent;}";
                        }
                    }
                    if (settings.fletxes) {
                        f6g_css_opcions_extra += capaActual + " .fletxa{width: 15%;height:100%;position:absolute;top:0;left:0;cursor: pointer;z-index:2;}";
                        f6g_css_opcions_extra += capaActual + " #gal6right{left:auto;right:0;}";
                        f6g_css_opcions_extra += capaActual + " .fletxa .icon{background-size:auto;background-image: url(" + settings.fran6galleryUrl + "gal6sprite.png);width: 19px;height: 23px;position: absolute;top: 50%;margin-top: -12px;right: 15px;background-position: -19px 0;opacity:.5;text-indent:-9999px;}";
                        f6g_css_opcions_extra += capaActual + " #gal6left .icon{ left: 15px; background-position: 0 0;}"
                        f6g_css_opcions_extra += capaActual + " .fletxa:hover .icon{opacity:1;}"
                    }
                    if (settings.timer) {
                        f6g_css_opcions_extra += capaActual + " #gal6timer{height:1px;width:100%;background:" + settings.timerBackground + ";z-index:10}";
                        f6g_css_opcions_extra += capaActual + " #gal6timer div{background:" + settings.timerColor + ";width:1%;height:1px;}";
                    }

                    var f6g_css = "<style type='text/css'>";
                    f6g_css += f6g_css_main + f6g_css_opcions_extra;
                    f6g_css += "</style>";

                    $(f6g_css).appendTo("head");
                }

                avant_la_entra();
            })
        }
    })
})(jQuery);