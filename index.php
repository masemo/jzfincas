<?
session_start();
include("admin/system/vars.php");
include("admin/system/conexiondb.php");
include("admin/system/config-public.php");
include("admin/lib/funciones.php");
include("admin/lib/embed_video/embed_video.php");
include("admin/common/functions.php");
include("admin/funciones/url.php");
include("admin/funciones/funciones.php");
include("admin/includes/plugins/seo/functions.php");

include("functions/lang.php");
include("functions/main.php");
include("lib/functions.php");

include("admin/system/preload-public.php");
include("functions/listados.php");

include("controllers/nav.php");
$is_special_page = is_special_page($id_seccion, $seccion, $special_pages);
if (empty($is_special_page)) {
    include("controllers/" . $sec . ".php");
} else if ($id_seccion == 78) {
    include("controllers/comunidades-login.php");
}
?>
<!DOCTYPE html>
<html lang="<? echo $lang; ?>">
<head>
    <? include($themeIncludeUrl . "template/head.php"); ?>
    <? include("includes/template/head.php"); ?>
</head>
<body class="<? echo $sec; ?>">
<? //echo "sec: ".$sec." - sec_admin: ".$sec_admin." - id: ".$id;  ?>
<div id="wrapper">
    <? include($themeIncludeUrl . "template/header.php"); ?>
    <main>
        <div id="slidexou_portada">
            <ul class="gal6">
                <? foreach ($slidexouPortada as $id => $data) { ?>
                    <li style="background-image:url(<? echo $data["image"]; ?>);">
                        <? if ($data["link"] != "") { ?>
                            <a href="<? echo $data["link"]; ?>" class="cover"></a>
                        <? } ?>
                    </li>
                <? } ?>
            </ul>
        </div>
        <?
        if (!empty($is_special_page)) {
            include("includes/special-pages/" . $is_special_page["include"]);
        } else {
            include("section/" . $sec . ".php");
        }
        ?>
    </main>
    <? include($themeIncludeUrl . "template/footer.php"); ?>
</div>
<? include("includes/template/javascript.php"); ?>
<? include($themeIncludeUrl . "template/javascript.php"); ?>
</body>
</html>
