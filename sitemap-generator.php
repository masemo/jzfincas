<?php
header("Content-Type:text/xml");
echo '<?xml version="1.0" encoding="UTF-8"?>';

include("admin/system/vars.php");
include("admin/system/conexiondb.php");
include("admin/system/config.php");
include("admin/funciones/url.php");
include("admin/funciones/funciones.php");
include("admin/common/functions.php");
include("functions/main.php");
$idiomas = carga_idioma();
$moduls_contractats = cargar_modulos_contratados();

function check_modul_contractat($estatica, $sec_public) {
    if ((!$estatica) || ($estatica && modul_contractat($sec_public))) {
        return true;
    } else {
        return false;
    }
}

function seccion_online($id_page) {
    global $link;
    $query = "SELECT activo, sec_public, estatica FROM paginas WHERE id = $id_page";
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_assoc($result);
    if ($row["activo"] == 1 && check_modul_contractat($row["estatica"], $row["sec_public"])) {
        return true;
    }
    return false;
}

function fetch_array($id, $lang, $sec) {
    $aux = array();
    $aux["url"] = obtener_url_amigable($lang, $sec, $id);
    $aux["lastmod"] = date("Y-m-d");
    $aux["priority"] = "0.9";
    $aux["changefreq"] = "weekly";
    $aux["alternate_langs"] = obtener_alternate($lang, $id, $sec);
    return $aux;
}

function xml_url_layout($data, $lang_default) {
    $item = "";
    $url = $data["url"];
    $lastmod = $data["lastmod"];
    $priority = $data["priority"];
    $changefreq = $data["changefreq"];
    $alternate_langs = $data["alternate_langs"];

    $item .= "<url>\r\n";
    $item .= "<loc>$url</loc>\r\n";
    if (!empty($alternate_langs)) {
        foreach ($alternate_langs as $alternate) {
            $item .= "<xhtml:link rel='alternate' hreflang='" . $alternate["lang"] . "' href='" . $alternate["url"] . "' />\r\n";
        }
        $item .= "<xhtml:link rel='alternate' hreflang='" . $lang_default . "' href='" . $url . "' />\r\n";
    }
    $item .= "<lastmod>$lastmod</lastmod>\r\n";
    $item .= "<changefreq>$changefreq</changefreq>\r\n";
    $item .= "<priority>$priority</priority>\r\n";
    $item .= "</url>\r\n";
    return $item;
}

function obtener_alternate($lang, $id, $sec) {
    global $idiomas;
    $princ = array();
    if (count($idiomas["activos"]) > 1) {
        foreach ($idiomas["activos"] as $idioma) {
            $leng = $idioma["idioma"];
            if ($lang != $leng) {
                $aux = array();
                $aux["lang"] = $leng;
                $aux["url"] = obtener_alternate_url($id, $leng, $sec);
                $princ[] = $aux;
            }
        }
    }
    return $princ;
}

function obtener_alternate_url($id, $leng, $sec) {
    global $siteUrl;
    $fu = $siteUrl . $leng . "/";
    if ($sec != "" && $sec != "home") {
        $fu = obtener_url_amigable($leng, $sec, $id);
    }
    return $fu;
}

/* --------------------------------------------------------------- CONTENIDO -- */

function crea_xml_contenido($lang) {
    global $link;
    $sql = "SELECT id, estatica, sec_public, external_link FROM paginas WHERE activo = 1 AND oculto = 0 AND visible_menu = 1 ORDER BY orden ASC";
    $consulta = mysqli_query($link, $sql);
    $totals = mysqli_num_rows($consulta);
    $princ = array();
    if ($totals > 0) {
        while ($row = mysqli_fetch_assoc($consulta)) {
            if (seccion_online($row['id']) && !$row['external_link']) {
                $princ[] = fetch_array($row['id'], $lang, "paginas", "nombre_menu");
            }
        }
    }
    return $princ;
}

/* ---------------------------------------------------------------- NOTICIAS -- */

function crea_xml_noticias_categorias($lang, $activar_categorias_noticias) {
    global $link;
    global $id_pagina_noticias;
    $princ = array();
    if ($activar_categorias_noticias && seccion_online($id_pagina_noticias)) {
        $query = "SELECT id FROM noticias_categoria WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "noticias_categoria", "descripcion");
            }
        }
    }
    return $princ;
}

function crea_xml_noticias($lang) {
    global $link;
    global $id_pagina_noticias;
    $princ = array();
    if (seccion_online($id_pagina_noticias)) {
        $query = "SELECT id FROM noticias WHERE activo = 1 ORDER BY fecha_public DESC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "noticias", "titular");
            }
        }
    }
    return $princ;
}

/* ------------------------------------------------------------- PRODUCTOS -- */

function crea_xml_productos_categorias($lang) {
    global $link;
    global $id_pagina_productos;
    $princ = array();
    if (seccion_online($id_pagina_productos)) {
        $query = "SELECT id FROM productos_categoria WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "productos_categoria", "descripcion");
            }
        }
    }
    return $princ;
}

function crea_xml_productos($lang) {
    global $link;
    global $id_pagina_productos;
    $princ = array();
    if (seccion_online($id_pagina_productos)) {
        $query = "SELECT id FROM productos WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "productos", "nombre");
            }
        }
    }
    return $princ;
}

/* --------------------------------------------------------------- GALERIA -- */

function crea_xml_galeria($lang) {
    global $link;
    global $id_pagina_galeria_imagenes;
    $princ = array();
    if (seccion_online($id_pagina_galeria_imagenes)) {
        $query = "SELECT id FROM galeria_categoria WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 1) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "galeria_categoria", "descripcion");
            }
        }
    }
    return $princ;
}/* --------------------------------------------------------------- GALERIA -- */

function crea_xml_videos($lang) {
    global $link;
    global $id_pagina_videos;
    $princ = array();
    if (seccion_online($id_pagina_videos)) {
        $query = "SELECT id FROM videos_categoria WHERE activo = 1 ORDER BY id, orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 1) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "videos_categoria", "descripcion");
            }
        }
    }
    return $princ;
}

/* -------------------------------------------------------------------------- */
$lang_default = $idiomas["defecto"]["idioma"];

$xml = array(
    "home" => array(fetch_array(1, $lang_default, "home", "nombre_menu")),
    "contenido" => crea_xml_contenido($lang_default),
    "noticias_categorias" => crea_xml_noticias_categorias($lang_default, $activar_categorias_noticias),
    "noticias" => crea_xml_noticias($lang_default),
    "productos_categorias" => crea_xml_productos_categorias($lang_default),
    "productos" => crea_xml_productos($lang_default),
    "galeria_afotos" => crea_xml_galeria($lang_default),
    "galeria_videos" => crea_xml_videos($lang_default),
);

$home = fetch_array(1, $lang_default, "home", "nombre_menu");
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xhtml="http://www.w3.org/1999/xhtml">
            <?
            foreach ($xml as $key => $xmlItem) {
                foreach ($xmlItem as $item) {
                    echo xml_url_layout($item, $lang_default);
                }
            }
            ?>
</urlset>