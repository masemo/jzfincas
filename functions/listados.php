<?php

function banners_listado($posicion, $limit = "", $extra_clause = "") {
    global $link;
    global $siteUrl;
    if ($limit != "") {
        $limit = "LIMIT " . $limit;
    }
    $query = "SELECT * FROM banners WHERE activo = 1 AND (DATEDIFF(fecha_public, current_date) <= 0) AND posicion = $posicion $extra_clause ORDER BY orden ASC, id ASC $limit";
    $result = mysqli_query($link, $query);
    if ($result) {
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            $princ = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $aux = array();
                $aux["href"] = generate_href($row["url"]);
                $aux['imagen'] = $siteUrl . "lib/timThumb/timThumb.php?src=" . pintar_imagen(obtnener_imagen($row["imagen"]), true) . "&amp;w=640&amp;zc=2";
                $aux["title"] = $row["title"];
                $aux["alt"] = $row["alt"];
                $princ[] = $aux;
            }
            return $princ;
        }
    }
    return false;
}

function noticias_listado($parent = 0, $limit = "", $extra_clause = "") {
    global $link;
    global $lang;
    global $siteUrl;
    $parent_clause = "";
    
    if ($limit != "") {
        $limit = "LIMIT " . $limit;
    }
    if ($parent != "" && $parent != -1) {
        $parent_clause = " AND parent " . $parent;
    }
    
    $query = "SELECT * FROM noticias WHERE activo = 1 AND (DATEDIFF(fecha_public, current_date) <= 0) $parent_clause $extra_clause ORDER BY fecha_public DESC $limit";
    $result = mysqli_query($link, $query);
    if ($result) {
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            $princ = array();
            while ($row = mysqli_fetch_array($result)) {
                if (!fecha_caducada($row["fecha_caduca"])) {
                    $aux = array();
                    $month = lang("mes_" . date("m", strtotime($row['fecha_public'])));
                    $year = date("Y", strtotime($row['fecha_public']));

                    $aux['date'] = fecha_to_view($row['fecha_public']);
                    $aux['date_text'] = $month . " " . $year;
                    $aux['datetime'] = $row['fecha_public'];
                    $aux['link'] = obtener_url_amigable($lang, "noticias", $row['id']);
                    $aux['title'] = obtener_valor($lang, "noticias", "titular", $row['id']);
                    $aux['desc'] = extrae_cadena(strip_tags(obtener_valor($lang, "noticias", "contenido", $row['id'])), 150);
                    $aux['imagen'] = "";
                    if ($row["imagen"] > 0) {
                        $aux['imagen'] = $siteUrl . "lib/timThumb/timThumb.php?src=" . pintar_imagen(obtnener_imagen($row["imagen"]), true) . "&amp;w=640&amp;zc=2";
                    }
                    if ($parent == -1) {
                        $aux["categoria"] = obtener_valor($lang, "noticias_categoria", "descripcion", $row['parent']);
                    }
                    $princ[] = $aux;
                }
            }
            return $princ;
        }
    }
    return false;
}

function videos_listado($limit = "", $extra_clause = "") {
    global $link;
    global $lang;
    if ($limit != "") {
        $limit = "LIMIT " . $limit;
    }

    $query = "SELECT * FROM videos WHERE activo = 1 AND (DATEDIFF(fecha_public, current_date) <= 0) $extra_clause ORDER BY fecha_public DESC $limit";
    $result = mysqli_query($link, $query);
    if ($result) {
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            $princ = array();
            while ($row = mysqli_fetch_array($result)) {
                $aux = array();
                $aux['video'] = $row['video'];
                $aux['video_img'] = url_miniatura($row['video']);
                $aux['text'] = obtener_valor($lang, "videos", "descripcion", $row['id']);
                $princ[] = $aux;
            }
            return $princ;
        }
    }
    return false;
}

function categoria_imagenes_listado($limit = "", $extra_clause = "") {
    global $lang;
    global $link;
    global $siteUrl;
    if ($limit != "") {
        $limit = "LIMIT " . $limit;
    }
    $query = "SELECT * FROM galeria_categoria WHERE activo = 1 $extra_clause ORDER BY id DESC $limit";
    $result = mysqli_query($link, $query);
    if ($result) {
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            $princ = array();
            while ($row = mysqli_fetch_array($result)) {
                $aux = array();
                $aux['link'] = obtener_url_amigable($lang, "galeria_categoria", $row['id']);
                if ($row["imagen"] > 0) {
                    $aux['image'] = $siteUrl . "lib/timThumb/timThumb.php?src=" . pintar_imagen(obtnener_imagen($row["imagen"]), true) . "&amp;w=640";
                }
                $aux['text'] = obtener_valor($lang, "galeria_categoria", "descripcion", $row['id']);

                $princ[] = $aux;
            }

            return $princ;
        }
    }
    return false;
}
