<?php
function get_id_pagina_estatica($seccion)
{
    global $link;
    $query = "SELECT id FROM paginas WHERE estatica_sec = '$seccion'";
//    echo $query;
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $row = mysqli_fetch_array($result);
        return $row["id"];
    }
    return 0;
}

/* -- productos -- */

function primera_categoria_productos_activa($parent = 0)
{
    global $link;
    $query = "SELECT id FROM productos_categoria WHERE activo = 1 AND parent = $parent ORDER BY orden ASC LIMIT 1";
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_array($result);

    $nProds = tiene_productos($row["id"]);

    if ($nProds > 0) {
        $id = $row["id"];
    } else {
        if (tiene_subcategorias_productos($row["id"])) {
            $id = primera_categoria_productos_activa($row["id"]);
        } else {
            $id = 0;
        }
    }
    return $id;
}

/* -- galeria multi images -- */

function obtener_galeria_multi_imagen($item_id, $dataSec)
{
    global $link;
    global $lang;
    $query = "SELECT * FROM imagenes WHERE idsec = $item_id AND sec = '$dataSec' ORDER BY orden ASC, ID ASC";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $princ = array();
        while ($fila_imagenes = mysqli_fetch_array($result)) {
            $aux = array();
            $aux["imagen"] = $fila_imagenes['fichero'];
            $aux["alt"] = obtener_valor($lang, "imagenes", "img_alt", $fila_imagenes['id']);
            $princ[] = $aux;
        }
        return $princ;
    } else {
        return false;
    }
}

/* -- banners -- */

function pinta_banner_list($banners)
{
    $bannerGallery = "<ul>";
    foreach ($banners as $banner) {
        $bannerGallery .= "<li>";
        $bannerGallery .= "<a href='" . $banner["url"] . "' target='_blank'>";
        $bannerGallery .= "<img src='" . $banner["imagen"] . "' title='" . $banner["title"] . "' alt='" . $banner["alt"] . "' />";
        $bannerGallery .= "</a>";
        $bannerGallery .= "</li>";
    }
    $bannerGallery .= "</ul>";
    echo $bannerGallery;
}

function obtener_banner_posicion($idPosicion, $banners_list)
{
    if (!empty($banners_list)) {
        $princ = array();
        foreach ($banners_list as $banner) {
            if ($banner["posicion"] == $idPosicion) {
                $princ[] = $banner;
            }
        }
        if (!empty($princ)) {
            pinta_banner_list($princ);
        }
    }
    return false;
}

function migas_de_pan($id, $lan, $miga = "")
{
    global $link;
    if ($miga == "") {
        $miga = array();
    }

    if ($id > 0) {
        $sql = "SELECT * FROM paginas WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $parent = $fila['parent'];


        if ($parent > 0) {
            $miga = migas_de_pan($fila['parent'], $lan, $miga);
        }
        $aux = array();
        $aux["url"] = "";
        if ($fila['nodo'] == 0) {
            $aux["url"] = obtener_url_amigable($lan, "paginas", $id);
        }
        $aux["title"] = obtener_valor($lan, "paginas", "nombre_menu", $id);
        $miga[] = $aux;
        return $miga;
    }
}

function migas_de_pan_productos_public($id, $lan, $miga = "")
{
    global $link;
    if ($miga == "") {
        $miga = array();
    }

    if ($id > 0) {
        $sql = "SELECT * FROM productos_categoria WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $parent = $fila['parent'];


        if ($parent > 0) {
            $miga = migas_de_pan_productos_public($fila['parent'], $lan, $miga);
        }
        $aux = array();
        $aux["url"] = obtener_url_amigable($lan, "productos_categoria", $id);
        $aux["title"] = obtener_valor($lan, "productos_categoria", "descripcion", $id);
        $miga[] = $aux;
    }
    return $miga;
}

function migas_de_pan_videos($id, $lan, $miga = "")
{
    global $link;
    if ($miga == "") {
        $miga = array();
    }

    if ($id > 0) {
        $sql = "SELECT * FROM videos_categoria WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $parent = $fila['parent'];

        if ($parent > 0) {
            $miga = migas_de_pan_galeria($fila['parent'], $lan, $miga);
        }
        $aux = array();
        $aux["url"] = obtener_url_amigable($lan, "videos_categoria", $id);
        $aux["title"] = obtener_valor($lan, "videos_categoria", "descripcion", $id);
        $miga[] = $aux;
    }
    return $miga;
}

function migas_de_pan_galeria($id, $lan, $miga = "")
{
    global $link;
    if ($miga == "") {
        $miga = array();
    }

    if ($id > 0) {
        $sql = "SELECT * FROM galeria_categoria WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $parent = $fila['parent'];

        if ($parent > 0) {
            $miga = migas_de_pan_galeria($fila['parent'], $lan, $miga);
        }
        $aux = array();
        $aux["url"] = obtener_url_amigable($lan, "galeria_categoria", $id);
        $aux["title"] = obtener_valor($lan, "galeria_categoria", "descripcion", $id);
        $miga[] = $aux;
    }
    return $miga;
}

function obtener_id_articulo($novedad)
{

    $sql = "SELECT id FROM noticias WHERE url_friendly='$novedad'";
    $consulta = mysql_query($sql);
    $fila = mysql_fetch_array($consulta);

    return $fila['id'];
}

function obtener_id_pag($pagina)
{

    $sql = "SELECT id FROM paginas WHERE url_friendly_complet='$pagina'";
    $consulta = mysql_query($sql);
    $fila = mysql_fetch_array($consulta);

    return $fila['id'];
}

function add_attr($data)
{
    foreach ($data as $attr => $value) {
        return $attr . "='$value' ";
    }
}

function is_required($class)
{
    if (strpos($class, 'required') !== false) {
        return true;
    }
}

function create_input_password($id, $data, $attr, $required)
{
    $input = "<p>";
    $input .= "<label for='$id'>" . lang($id) . $required . "</label>";
    $input .= "<input type='password' id='$id' name='$id' class='" . $data["class"] . "' " . $attr . " />";
    $input .= "</p>";
    return $input;
}

function create_input_text($id, $data, $attr, $required)
{
    $input = "<p>";
    $input .= "<label for='$id'>" . lang($id) . $required . "</label>";
    $input .= "<input type='text' id='$id' name='$id' class='" . $data["class"] . "' " . $attr . " />";
    $input .= "</p>";
    return $input;
}

function create_input_number($id, $data, $attr, $required)
{
    $input = "<p>";
    $input .= "<label for='$id'>" . lang($id) . $required . "</label>";
    $input .= "<input type='number' id='$id' name='$id' class='" . $data["class"] . "' " . $attr . " />";
    $input .= "</p>";
    return $input;
}

function create_input_email($id, $data, $attr, $required)
{
    $input = "<p>";
    $input .= "<label for='$id'>" . lang($id) . $required . "</label>";
    $input .= "<input type='email' id='$id' name='$id' class='" . $data["class"] . "' " . $attr . " />";
    $input .= "</p>";
    return $input;
}

function create_input_textarea($id, $data, $attr, $required)
{
    $input = "<p>";
    $input .= "<label for='$id'>" . lang($id) . $required . "</label>";
    $input .= "<textarea id='$id' name='$id' class='" . $data["class"] . "' " . $attr . " ></textarea>";
    $input .= "</p>";
    return $input;
}

function create_input_checkbox($id, $data, $attr)
{
    $input = "<p class='checkbox'>";
    $input .= "<input type='checkbox' id='$id' name='$id' class='" . $data["class"] . "' " . $attr . " />";
    $input .= "<label for='$id'>" . lang($id) . "</label>";
    $input .= "</p>";
    return $input;
}

function create_input_submit($id, $data, $attr)
{
    $input = "<p>";
    $input .= "<input type='submit' id='$id' name='$id' value='" . lang($id) . "' class='" . $data["class"] . "' " . $attr . " />";
    $input .= "</p>";
    return $input;
}

function create_input_select($id, $data, $attr, $required)
{
    $input = "<p>";
    $input .= "<label for='$id'>" . lang($id) . $required . "</label>";
    $input .= "<select id='$id' name='$id' " . $attr . ">";
    foreach ($data["options"] as $optionId => $optionData) {

        if (isset($optionData["id"])) {
            $value = $optionData["id"];
        } else {
            $value = $optionData["value"];
        }
        
        $input .= "<option value='$value'>" . $optionData["value"] . "</option>";
    }
    $input .= "</select></p>";
    return $input;
}

function create_form($fields)
{
    $form = "";
    foreach ($fields as $id => $data) {
        $attr = "";
        $required = "";
        if (isset($data["attr"])) {
            $attr = add_attr($data["attr"]);
        }
        if (is_required($data["class"])) {
            $required = "*";
        }
        switch ($data["type"]) {
            case "text":
                $form .= create_input_text($id, $data, $attr, $required);
                break;
            case "number":
                $form .= create_input_number($id, $data, $attr, $required);
                break;
            case "email":
                $form .= create_input_email($id, $data, $attr, $required);
                break;
            case "textarea":
                $form .= create_input_textarea($id, $data, $attr, $required);
                break;
            case "checkbox":
                $form .= create_input_checkbox($id, $data, $attr);
                break;
            case "password":
                $form .= create_input_password($id, $data, $attr, $required);
                break;
            case "submit":
                $form .= create_input_submit($id, $data, $attr);
                break;
            case "select":
                $form .= create_input_select($id, $data, $attr, $required);
                break;
        }
    }
    echo $form;
}

function encryptIt($q)
{
    global $encrypt;

    $cryptKey = $encrypt;
    $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    return ($qEncoded);
}

function decryptIt($q)
{
    global $encrypt;

    $cryptKey = $encrypt;
    $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
    return ($qDecoded);
}

function f_date($date, $format, $isEmpty = null)
{
    if ($date != "" && $date != "0000-00-00") {
        $return = date($format, strtotime($date));
    } else if (!isset($return) && !is_null($isEmpty)) {
        $return = lang($isEmpty);
    } else {
        $return = "";
    }

    return $return;
}