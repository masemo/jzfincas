<?

function obtener_idioma_url($url)
{
    global $arrail;
    if ($arrail != "/") {
        $uriAux = str_replace($arrail, "/", $url);
    } else {
        $uriAux = $url;
    }
    $idioma = substr($uriAux, 1, 2);
    return $idioma;
}

function existe_idioma($lang, $idiomas_activos)
{
    foreach ($idiomas_activos as $data) {
        if ($lang == $data["idioma"]) {
            return true;
        }
    }
    return false;
}

function existe_url_idioma($url, $idiomas_activos)
{
    $idioma = obtener_idioma_url($url);
    return existe_idioma($idioma, $idiomas_activos);
}

function lang($nodename)
{
    global $idiomaXml;
    global $lang;

    if ($idiomaXml->$nodename == "") {
        return "!_" . $nodename;
    } else {
        return replace_links($idiomaXml->$nodename, $lang);
    }
}

function langUrl($langData)
{
    global $sec;
    global $id;
    $fu = obtener_url_amigable($langData["idioma"], $sec, $id);
    return $fu;
}

function langLink($langData, $lang, $abbr)
{
    $fu = langUrl($langData);
    $selected = "";
    $desc = $langData["desc"];
    if ($lang == $langData["idioma"]) {
        $selected = "class='selected'";
    }
    if ($abbr) {
        $desc = $langData["idioma"];
    }
    echo '<a href="' . $fu . '" rel="alternate" hreflang="' . $langData["idioma"] . '" ' . $selected . '>' . $desc . '</a>';
}

function generaLangNav($idiomas, $lang, $abbr = false)
{
    if (count($idiomas["activos"]) > 1) {
        foreach ($idiomas["activos"] as $idioma => $data) {
            if (count($idiomas["activos"]) > 2) {
                langLink($data, $lang, $abbr); // muestra todos los idiomas
            } else {
                if ((count($idiomas["activos"]) == 2) && ($data["idioma"] != $lang)) {
                    langLink($data, $lang, $abbr); // muestra el otro
                }
            }
        }
    }
}

function obtener_browser_lang($idiomas_activos)
{
    if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $lang_browser = strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',');
        return substr($lang_browser, 0, 2);
    }
    return false;
}

function define_lang($url, $arrail, $idiomas)
{
    $idiomas_activos = $idiomas["activos"];
    $lang = $idiomas["defecto"]["idioma"];

    if ($url == $arrail || $url == "") { // si estic en la home pelà pille del navegador
        $browser_lang = obtener_browser_lang($idiomas_activos);
        if (!empty($browser_lang) && existe_idioma($browser_lang, $idiomas_activos)) {
            $lang = $browser_lang;
        }
    } else {
        if (count($idiomas_activos) > 1) { // si hi han varios idiomes pille de la URL
            $url_lang = obtener_idioma_url($url);
            if (!empty($url_lang) && existe_idioma($url_lang, $idiomas_activos)) {
                $lang = $url_lang;
            }
        }
    }
    return $lang;
}

//echo $lang;
function lang_file($lang_default, $ruta = "")
{
    $file_lang = $ruta . "lang/" . $lang_default . ".xml";

    if (!file_exists($file_lang)) {
        echo "<p>Lang file not found.</p>";
    }
    return simplexml_load_file($file_lang);
}
