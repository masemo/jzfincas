<?
function pinta_menu($categorias, $id_categoria) {
    echo "<ul>";
    foreach ($categorias as $item) {
        $selected = "";
        if ($item["id"] == $id_categoria) {
            $selected = "class='selected'";
        }
        echo "<li $selected>";
        if (isset($item["sub"])) {
            echo "<button type='button'>" . $item["desc"] . "</button>";
            pinta_menu($item["sub"], $id_categoria);
        }else{
            echo "<a href='" . $item["url"] . "' rel='subsection'>" . $item["desc"] . "</a>";
        }
        echo "</li>";
    }
    echo "</ul>";
}
pinta_menu($categorias, $id_categoria);
