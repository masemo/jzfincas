<?php

if(!isset($_SESSION["login_usuario"])){
    header("location: " . obtener_url_amigable($lang, "paginas", 78));
} else {
    $logged_in = true;
    if ($id_seccion == 2 && $seccion == "comunidades"){
        session_destroy();
        $logged_in = false;
        header("location: " . obtener_url_amigable($lang, "paginas", 78));
    }
}

$fu_logout =  obtener_url_amigable($lang, "comunidades", 2);