<script type="text/javascript" src="<? echo $siteUrl; ?>lib/jQuery/jQuery-1.11.2.min.js"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>lib/imagesloaded/imagesloaded.min.js"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>lib/<? echo minify("functions", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>lib/cookieAlert.min.js"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>js/<? echo minify("main", "js", $development); ?>?ver=<? echo $siteVersion; ?>"></script>
<script type="text/javascript">
    var siteUrl = "<? echo $siteUrl;?>";
    var siteName = "<? echo $siteName;?>";
    var arrail = "<? echo $arrail; ?>";
    var cookieAlertDomain = siteName.replace(/ /g,'');
    var cookieAlertUrl = "<? echo $aviso_legal_fu; ?>";
    var cookieAlertInfoTxt = "<? echo lang("politica_cookies_info_lang"); ?>";
    var cookieAlertAceptarTxt = "<? echo lang("politica_cookies_aceptar_txt"); ?>";
    var cookieAlertMasinfoTxt = "<? echo lang("politica_cookies_masInfo_txt"); ?>";
</script>