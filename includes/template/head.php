<meta charset="utf-8" />
<link rel="icon" href="<? echo $siteUrl; ?>favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<? echo $siteUrl; ?>favicon.ico" type="image/x-icon" />
<meta name="author" content="Sollutia.com" />
<meta property="og:type" content="website" />
<?php
$Get_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if (count($idiomas["activos"]) > 1) {
    foreach ($idiomas["activos"] as $idioma => $value) {
        if ($value["idioma"] != $lang) {
            ?>
            <link rel="alternate" href="<? echo langUrl($value); ?>" hreflang="<? echo $value["idioma"]; ?>" />
            <?
        }
    }
    if ($Get_url == $siteUrl) {
        ?>
        <link rel="canonical" href="<? echo $siteUrl . $lang_default . "/"; ?>" />
        <?
    }
}
if ($sec == "error" && $Get_url != $siteUrl . $lang . "/error.html") {
    ?>
    <link rel="canonical" href="<? echo $siteUrl . $lang . "/error.html"; ?>" />
    <?
}
if ($Get_url == $productos_fu && !$product_folder_style && $first_cat_by_default) {
    $productos_default = obtener_url_amigable($lang, "productos_categoria", $id_categoria);
    ?>
    <link rel="canonical" href="<? echo $productos_default; ?>" />
<? } ?>

<title><? echo obtener_metas($lang, $sec, "title", $id); ?></title>
<meta name="description" content="<? echo obtener_metas($lang, $sec, "description", $id); ?>"/>

<?
if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    header('X-UA-Compatible: IE=edge,chrome=1');
}
?>
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!--[if IE]>
<script src="<? echo $siteUrl; ?>lib/html5-ie/trunk.js"></script>
<![endif]-->

<link rel="stylesheet" href="<? echo $themeUrl; ?>style/<? echo minify("main", "css", $development) ?>?ver=<? echo $siteVersion; ?>" type="text/css" />