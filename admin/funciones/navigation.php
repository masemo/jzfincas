<?php

function genera_menu($id, $secActive, $privileges, $user, $lang = "") {
    global $link;
    $subseccion = "";

    $navegacion = menu_navegacion($id, $user);
    $nItems = mysqli_num_rows($navegacion);

    if ($nItems > 0) {
        echo "<ul>";
        while ($filaItems = mysqli_fetch_array($navegacion)) {

            if (in_array($filaItems['sec'], $privileges)) {

                $selected = selectar_menu($filaItems['sec'], $secActive);

                $seccion = "?sec=" . $filaItems['sec'];

                if ($filaItems['parent'] > 0) {
                    if ($filaItems['sub'] != "listado" && $filaItems['sub'] != "anyadir" && $filaItems['sub'] != "") {
                        $subseccion = "&sub=" . $filaItems['sub'];
                        echo "<li rel='section' class='$selected'>";
                        echo "<a href='index.php" . $seccion . $subseccion . "' rel='section'>" . $filaItems['descripcion'] . "</a>";
                        echo "</li>";
                    }
                } else {

                    echo "<li class='$selected'>";
                    echo "<a href='index.php" . $seccion . $subseccion . "' rel='section' class='icontainer' title='" . $filaItems['descripcion'] . "'>";
                    echo "<span><span class='icon " . $filaItems['sec'] . "'></span></span>";
                    echo "<span><span>" . $filaItems['descripcion'] . "</span></span>";
                    echo "</a>";

                    $itemMenu = genera_menu($filaItems['id'], $secActive, $privileges, $user, $lang);

                    echo "</li>";
                }
            }
        }
        echo "</ul>";
    }

    return;
}
