<?
function lang($nodename, $lang_file_url = "") {
    global $idioma;
    if ($lang_file_url == "") {
        $lang_file = $idioma;
    } else {
        $lang_file = $lang_file_url;
    }
    if ($lang_file->$nodename == "") {
        return "!_" . $nodename;
    } else {
        return $lang_file->$nodename;
    }
}

function lang_file($lang_default, $ruta = "") {
//    $ruta = "";
//    if ($lang_file_url != "") {
//        for ($i = 0; $i < $lang_file_url; $i++) {
//            $ruta.="../";
//        }
//    }
    $file_lang = $ruta . "lang/" . $lang_default . ".xml";

    if (!file_exists($file_lang)) {
        echo "<p>Lang file not found.</p>";
    }
    return simplexml_load_file($file_lang);
}

function require_lang_field($leng, $addClassAttr = false) {
    global $require_all_langs;
    global $lang_default;
    $required = false;
    if ($require_all_langs) {
        $required = true;
    } else {
        if ($leng == $lang_default) {
            $required = true;
        }
    }
    if ($required) {
        if ($addClassAttr) {
            echo 'class="required"';
        } else {
            echo 'required';
        }
    }
}

/* ------------------------------------------------------------------ LANG -- */
if (!isset($lang_file_url)) {
    $lang_file_url = "";
}
$lang_default = $lenguajes["defecto"]["idioma"];
$idioma = lang_file($adminLang, $lang_file_url);
