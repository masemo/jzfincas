<?php

/* ------------------------------------------ truco para el register globals -- */
foreach ($_REQUEST as $variable => $valor) {
    $$variable = mysqli_real_escape_string($link, $valor);
}

function seccion_activa($seccion, $file) {
    if ($seccion == "") {
        $seccion = "defecto";
    }
    switch ($file) {
        case "controller":
            $include = "secciones/" . $seccion . "/controller.php";
            break;
        case "funciones":
            $include = "secciones/" . $seccion . "/funciones/funciones.php";
            break;
    }
    return $include;
}
