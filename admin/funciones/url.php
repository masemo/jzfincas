<?php

/* Funciones de url */

function format_url_fu($url, $lang, $idiomas_activos)
{
    if ($url == "/") {
        $url = "";
    }
    $friendly_url = $url;
    if (count($idiomas_activos) > 1) {
        $lang_aux = "/" . $lang . "/";
        $friendly_url = str_replace($lang_aux, "", $friendly_url);
    } else {
        $friendly_url = ltrim($friendly_url, "/");
    }

    return $friendly_url;
}

function seccion_publica($sec)
{
    global $link;

    $sql = "SELECT sec FROM config_secciones_public WHERE sec_admin = '$sec' LIMIT 1";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    return $fila['sec'];
}

function obtener_url_friendly_public($idioma, $seccion, $campo, $id_seccion, $valor_default)
{
    global $link;
    $sql = "SELECT url_friendly FROM idiomas WHERE idioma = '$idioma' AND seccion = '$seccion' AND id_seccion = $id_seccion AND url_friendly <> '' LIMIT 1";
    //echo $sql."<br/>"; 
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $texto = $fila['url_friendly'];

    return $texto;
}

function obtener_ruta($tabla, $cat, $lan)
{
    global $link;

    $aux = "";

    $sql = "SELECT parent FROM $tabla WHERE id=$cat";
    //echo $sql."<br/>";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($fila['parent'] > 0) {
        $aux = obtener_ruta($tabla, $fila['parent'], $lan);
    }

    $descripcion = obtener_url_friendly_public($lan, $tabla, "url_friendly_" . $lan, $cat, "");
    if ($descripcion != "") {
        return $aux . $descripcion . "/";
    }
}

function obtener_url_amigable($idioma, $seccion, $id_seccion, $absolute = true)
{
    global $link;
    global $siteUrl;
    global $idiomas;

    $query = "SELECT url FROM friendly_url WHERE idioma = '$idioma' AND seccion = '$seccion' AND id_seccion = $id_seccion";
    //echo $query."<br />";
    $result = mysqli_query($link, $query);
    if ($result) {
        $row = mysqli_fetch_assoc($result);
        $url = $row["url"];
        if (count($idiomas["activos"]) > 1) {
            $url = $idioma . "/" . $row["url"];
        }
        if ($absolute) {
            return $siteUrl . $url;
        }
        return $url;
    }
    return false;
}

function obtener_url_id($idioma, $seccion, $id_seccion)
{
    global $link;

    $query = "SELECT id FROM friendly_url WHERE idioma = '$idioma' AND seccion = '$seccion' AND id_seccion = $id_seccion";
    //echo $query."<br />";

    $result = mysqli_query($link, $query);
    if ($result) {
        $row = mysqli_fetch_assoc($result);
        return $row["id"];
    }

    return false;
}

function obtener_url_amigable_from_id($idioma, $id, $absolute = true)
{
    global $link;
    global $siteUrl;
    global $idiomas;

    $query = "SELECT * FROM friendly_url WHERE id = $id";
//    echo $query."<br />";
    $result = mysqli_query($link, $query);
    if ($result) {
        $row = mysqli_fetch_assoc($result);
        $url = $row["url"];
        if (count($idiomas["activos"]) > 1) {
            $url = $idioma . "/" . $row["url"];
        }
        if ($absolute) {
            return $siteUrl . $url;
        }
        return $url;
    }
    return false;
}

function encode_url_dinamic($friendly_url_id)
{
    global $tag_fu;

    return str_replace("#", $friendly_url_id, $tag_fu);
}

function decode_url_dinamic($friendly_url_id)
{
    global $tag_fu;
    $tag_fu_arr = split("#", $tag_fu);
    $start = $tag_fu_arr[0];
    $end = $tag_fu_arr[1];

    if (substr($friendly_url_id, 0, strlen($start)) == $start && substr($friendly_url_id, strlen($friendly_url_id) - strlen($end), strlen($end)) == $end) {
        $friendly_url_id = str_replace($start, "", $friendly_url_id);
        $friendly_url_id = str_replace($end, "", $friendly_url_id);

        return intval($friendly_url_id);
    }

    return false;
}

function clean_protocol_link($link)
{
    $protocols = array("https://", "http://", "ftp://", "news://");

    foreach ($protocols as $protocol) {
        $link = str_replace($protocol, "", $link);
    }
    return $link;
}

function get_protocol_link($texto, $pos_start = 0)
{
    $protocols = array("https://", "http://", "ftp://", "news://");
    $protocol_used = "";
    foreach ($protocols as $protocol) {
        $protocol_len = strlen($protocol);
        $link = substr($texto, $pos_start - $protocol_len, $protocol_len);
        if ($link == $protocol) {
            $protocol_used = $protocol;
        }
    }
    return $protocol_used;

}

function obtener_url_amigable_metas($idioma, $seccion, $campo, $id, $absolute = true)
{
    global $link;
    global $siteUrl;
    $sql = "SELECT url_friendly_complet FROM idiomas WHERE idioma = '$idioma' AND seccion_public = '$seccion' AND campo = '$campo' AND id_seccion = $id LIMIT 1";
//    echo $sql . "<br/>";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $url = "";
    if ($absolute) {
        $url .= $siteUrl;
    }
    $url .= $fila['url_friendly_complet'];

    return $url;
}

function paginas($id, $lan)
{
    global $link;
    global $nIdiomas;

    $categoria = "";

    $sql = "SELECT parent, noborrar  FROM paginas WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $pagina = obtener_url_friendly_public($lan, "paginas", "url_friendly_" . $lan, $id, "");

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    if ($fila['parent'] > 0) {
        $categoria = obtener_ruta("paginas", $fila['parent'], $lan);
    }
    $id_pagina = $id . "/";
    if ($fila['noborrar'] > 0) {
        $id_pagina = "";
    }
    $url = $idioma_aux . $categoria . $id_pagina . $pagina;

    return $url . ".html";
}

function productos_categorias($id, $lan)
{
    global $link;
    global $nIdiomas;
    global $id_pagina_productos;

    $categoria = "";

    $sql = "SELECT parent FROM productos_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

//    $default = lang("productos_url");
    $default = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_productos));

    $prods_cat = obtener_url_friendly_public($lan, "productos_categoria", "url_friendly_" . $lan, $id, "");

    if ($fila['parent'] > 0) {
        $categoria = obtener_ruta("productos_categoria", $fila['parent'], $lan);
    }

    $url = $idioma_aux . $default . "/" . $categoria . $id . "/" . $prods_cat;

    return $url . "/";
}

function productos($id, $lan)
{
    global $link;
    global $nIdiomas;
    global $id_pagina_productos;

    /* obtengo el nombre el producto */
    $sql = "SELECT categoria FROM productos WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    $default = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_productos));

    $producto = obtener_url_friendly_public($lan, "productos", "url_friendly_" . $lan, $id, "");

    $categoria = obtener_ruta("productos_categoria", $fila['categoria'], $lan);

    $url = $idioma_aux . $default . "/" . $categoria . $id . "/" . $producto;

    return $url . ".html";
}

/* Noticias */

function noticias_categoria($id, $lan)
{
    global $nIdiomas;
    global $id_pagina_noticias;

    $categoria = url_amigable_limpiar(obtener_valor($lan, "noticias_categoria", "descripcion", $id));

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    $default = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_noticias));

    $url = $idioma_aux . $default . "/" . $id . "/" . $categoria;

    return $url . "/";
}

function noticias($id, $lan)
{
    global $link;
    global $nIdiomas;
    global $id_pagina_noticias;
    global $activar_categorias_noticias;

    $sql = "SELECT parent FROM noticias WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    $categoria = $default = "";
    if ($activar_categorias_noticias) {
        $default = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_noticias)) . "/";
        $categoria = obtener_url_friendly_public($lan, "noticias_categoria", "url_friendly_" . $lan, $fila['parent'], "") . "/"; //categoria de la noticias
    }
    $noticias = obtener_url_friendly_public($lan, "noticias", "url_friendly_" . $lan, $id, ""); //noticia

    $url = $idioma_aux . $default . $categoria . $id . "/" . $noticias;

    return $url . ".html";
}

/* Galeria */

function obtener_ruta_galeria($cat, $lan)
{
    global $link;

    $aux = "";

    $sql = "SELECT parent, id FROM galeria_categoria WHERE id=$cat";
    //echo $sql; 
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($fila['parent'] > 0) {
        $aux = obtener_ruta_galeria($fila['parent'], $lan);
    }

    $descripcion = obtener_url_friendly_public($lan, "galeria_categoria", "url_friendly_" . $lan, $fila['id'], "");

    return $aux . $descripcion . "/";
}

function galeria($id, $lan)
{
    global $link;
    global $nIdiomas;
    global $id_pagina_galeria_imagenes;

    $categoria = "";

    $sql = "SELECT parent FROM galeria_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    $galeria = obtener_url_friendly_public($lan, "galeria_categoria", "url_friendly_" . $lan, $id, "");

    if ($fila['parent'] > 0) {
        $categoria = obtener_ruta_galeria($fila['parent'], $lan);
    }

    $ruta_img = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_galeria_imagenes));

    $url = $categoria . $id . "/" . $galeria . "/";

    return $idioma_aux . $ruta_img . "/" . $url;
}

/* Videos */

function obtener_ruta_videos($cat, $lan)
{
    global $link;
    $aux = "";

    $sql = "SELECT parent, id FROM videos_categoria WHERE id=$cat";
    //echo $sql; 
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($fila['parent'] > 0) {
        $aux = obtener_ruta_videos($fila['parent'], $lan);
    }

    $descripcion = obtener_url_friendly_public($lan, "videos_categoria", "url_friendly_" . $lan, $fila['id'], "");

    return $aux . $descripcion . "/";
}

function videos_categoria($id, $lan)
{

    global $link;
    global $nIdiomas;
    global $id_pagina_videos;

    $categoria = "";

    $sql = "SELECT parent FROM videos_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    $ruta_videos = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_videos));

    $galeria = obtener_url_friendly_public($lan, "videos_categoria", "url_friendly_" . $lan, $id, "");

    if ($fila['parent'] > 0) {
        $categoria = obtener_ruta_videos($fila['parent'], $lan);
    }

    $url = $categoria . $id . "/" . $galeria . "/";

    return $idioma_aux . $ruta_videos . "/" . $url;
}

function videos($id, $lan)
{
    global $link;
    global $nIdiomas;
    global $id_pagina_videos;

    $sql = "SELECT categoria FROM videos WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($nIdiomas > 1) {
        $idioma_aux = $lan . "/";
    } else {
        $idioma_aux = "";
    }

    $video = obtener_url_friendly_public($lan, "videos", "url_friendly_" . $lan, $id, "");

    $categoria = obtener_ruta("videos_categoria", $fila['categoria'], $lan);

    $ruta_videos = url_amigable_limpiar(obtener_valor($lan, "paginas", "nombre_menu", $id_pagina_videos));

    $url = $idioma_aux . $ruta_videos . "/" . $categoria . $id . "/" . $video;

    return $url . ".html";
}

function genera_url_amigable($url_parametros, $lang, $default = "")
{

    global $urlSite;
    $url = "";

    //echo $url_parametros[1];
    switch ($url_parametros[1]) {
        case "paginas":
            $url = paginas($url_parametros[0], $lang);
            break;
        case "noticias":
            $url = noticias($url_parametros[0], $lang);
            break;
        case "noticias_categoria":
            $url = noticias_categoria($url_parametros[0], $lang);
            break;
        case "galeria_categoria":
            $url = galeria($url_parametros[0], $lang);
            break;
        case "videos":
            $url = videos($url_parametros[0], $lang);
            break;
        case "videos_categoria":
            $url = videos_categoria($url_parametros[0], $lang);
            break;
        case "productos_categoria":
            $url = productos_categorias($url_parametros[0], $lang);
            break;
        case "productos":
            $url = productos($url_parametros[0], $lang);
            break;
    }

    return $url;
}
