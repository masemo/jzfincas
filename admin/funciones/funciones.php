<?php

function get_data($query) {
    global $link;
    
    $result = mysqli_query($link, $query);

    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[$row["id"]] = $row;
    }
    
    return $data;
}

function get_last_order($table) {
    global $link;
    $query = "SELECT orden FROM $table ORDER BY orden DESC LIMIT 1";
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_assoc($result);
    return $row["orden"];
}

function update_order($orden, $id, $table) {
    global $link;
    $query = "UPDATE $table SET orden = $orden WHERE id = $id";
    if (mysqli_query($link, $query)) {
        return true;
    }
    return false;
}

function is_special_page($page_id, $seccion, $special_pages) {
    if ($seccion == "paginas") {
        foreach ($special_pages as $page) {
            if ($page["id"] == $page_id) {
                return $page;
            }
        }
    }
    return false;
}

function eliminar_img_multiples($idsec, $sec) {
    global $link;
    $query_img = "SELECT id FROM imagenes WHERE sec='$sec' AND idsec=" . $idsec;
//    echo $query_img;
    $result_img = mysqli_query($link, $query_img);
    $nItemImg = mysqli_num_rows($result_img);
    if ($nItemImg > 0) {
        while ($row_img = mysqli_fetch_array($result_img)) {
            elimina_item_by_id("imagenes", $row_img["id"]);
        }
    }
}

function elimina_item_by_id($tabla, $id) {
    global $link;
    $query = "DELETE FROM $tabla WHERE id = $id";
    mysqli_query($link, $query);
}

function instructions($instructions) {
    include("includes/instructions/template-header.php");
    foreach ($instructions as $instruction) {
        include("includes/instructions/$instruction.php");
    }
    include("includes/instructions/template-footer.php");
}

function toggle_element($cookie_name) {
    if (get_cookie_value($cookie_name) == "1") {
        echo "closed";
    }
}

function ckeditor_manual_url($lang) {
    return "https://translate.google.es/translate?sl=en&tl=$lang&js=y&prev=_t&hl=$lang&ie=UTF-8&u=http%3A%2F%2Fdocs.cksource.com%2FCKEditor_3.x%2FUsers_Guide&edit-text=";
}

function obtener_sig_or_ant($tabla, $id, $campo_orden, $sig_or_ant, $direccion_orden = "DESC", $condicion = "") {
    global $link;
    //siguiente ASC | anterior DESC

    $sql = "SELECT id FROM $tabla $condicion ORDER BY $campo_orden $direccion_orden";
//    echo $sql."<br/>";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $ordenados[] = $fila['id'];
    }

    //print_r($ordenados);
    $posicion = array_search($id, $ordenados);

    switch ($sig_or_ant) {
        default :
        case "SIG":
            $posicion++;
            break;
        case "ANT":
            $posicion--;
            break;
    }

    $total_items = count($ordenados) - 1;

    if ($posicion > $total_items) {
        //$posicion = 0; //primera pos
        $posicion = -1;
    } else {
        if ($posicion < 0) {

            //$posicion = $total_items; //ultima pos
            $posicion = -1;
        }
    }

    if ($posicion > -1) {
        if ($tabla == "paginas" && !check_id_pagina_activa($ordenados[$posicion])) {
            return obtener_sig_or_ant("paginas", $ordenados[$posicion], $campo_orden, $sig_or_ant, $direccion_orden, $condicion);
        } else {
            return $ordenados[$posicion];
        }
    } else {
        return -1;
    }
}

function check_id_pagina_activa($id) {
    global $link;
    $query = "SELECT estatica, sec_public FROM paginas WHERE id = $id";
//    echo $query."<br />";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $row = mysqli_fetch_assoc($result);
        if (check_modul_contractat($row['estatica'], $row['sec_public'])) {
            return true;
        }
    }
    return false;
}

function obtener_ultimo_video($categoria) {
    global $link;
    $sql = "SELECT video FROM videos WHERE categoria = $categoria ORDER BY fecha_public DESC LIMIT 1";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
    return $fila['video'];
}

function tiene_subcategorias_productos($categoria) {
    global $link;
    $sql = "SELECT id FROM productos_categoria WHERE parent=$categoria";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}

function tiene_productos($categoria) {
    global $link;
    $sql = "SELECT id FROM productos WHERE categoria = $categoria";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}

function tiene_videos($categoria) {
    global $link;
    $sql = "SELECT id FROM videos WHERE categoria = $categoria";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}

function tiene_imagenes($categoria) {
    global $link;
    $sql = "SELECT id FROM imagenes WHERE idsec = $categoria AND sec = 'galeria'";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}

function tiene_subcategorias_videos($categoria) {
    global $link;
    $sql = "SELECT id FROM videos_categoria WHERE parent=$categoria";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}

function tiene_subcategorias($categoria) {
    global $link;
    $sql = "SELECT id FROM galeria_categoria WHERE parent=$categoria";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}

function migas_imagenes($id) {
    global $link;
    global $lang_default;

    $descripcion = "";

    if ($id > 0) {
        $sql = "SELECT * FROM galeria_categoria WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $miga = migas_imagenes($fila['parent']);

        $descripcion = obtener_valor($lang_default, "galeria_categoria", "descripcion", $id);
    }

    if ($descripcion != "") {
        return ($miga . " <a href='index.php?sec=multimedia&id=$id'>" . $descripcion . "</a> / ");
    } else {
        return;
    }
}

function obtnener_imagen($id) {
    global $link;
    $sql = "SELECT fichero FROM imagenes WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
    return $fila['fichero'];
}

function obtnener_imagen_idioma($sec, $id, $idioma, $default_img = true) {
    global $link;
    global $lang_default;
    $sql = "SELECT * FROM imagenes WHERE sec='$sec' AND idsec=$id AND idioma = '$idioma' ORDER BY orden DESC LIMIT 1";
//    echo $sql."<br />";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_assoc($consulta);
    if (empty($fila) && $idioma != $lang_default && $default_img) {
        return obtnener_imagen_idioma($sec, $id, $lang_default);
    }
    return $fila;
}

function pintar_imagen($imagen, $relative = false) {
    global $uploadUrl;
    global $siteUrl;
    if ($relative) {
        return "/" . $uploadUrl . $imagen;
    } else {
        return $siteUrl . $uploadUrl . $imagen;
    }
}

function guardar_imagen($fichero, $id, $sec, $sub = "", $idioma = "") {
    global $link;
    $sql = "DELETE FROM imagenes WHERE idsec = $id AND sec='$sec' AND sub='$sub' AND idioma = '$idioma'";
    mysqli_query($link, $sql);

    $sql = "INSERT INTO imagenes (idsec,sec,sub,fichero,idioma) VALUES ($id,'$sec','$sub','$fichero','$idioma')";
    mysqli_query($link, $sql);
    $last_id = mysqli_insert_id($link);

    return $last_id;
}

function borrar_imagen($id, $sec, $sub = "", $idioma = "") {
    global $link;
    $sql = "DELETE FROM imagenes WHERE idsec = $id AND sec='$sec' ";
    if ($sub != "") {
        $sql .= "AND sub='$sub'";
    }
    if ($idioma != "") {
        $sql .= "AND idioma='$idioma'";
    }
//    echo $sql;
    mysqli_query($link, $sql);

    return;
}

function obtener_valor($idioma, $seccion, $campo, $id_seccion) {
    global $link;
    global $adminName;
    global $lang_default;
    $publicSite = true;
    if (isset($adminName)) {
        $publicSite = false;
    }
    $sql = "SELECT texto FROM idiomas WHERE idioma = '$idioma' AND seccion = '$seccion' AND campo = '$campo' AND id_seccion = $id_seccion LIMIT 1";
    //echo $sql."<br />";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $texto = $fila['texto'];
    if ($publicSite) {
        $texto = replace_links($texto, $idioma);
    }

    if ($texto == "") {
        if ($lang_default != $idioma && $publicSite) {
            $texto = obtener_valor($lang_default, $seccion, $campo, $id_seccion);
        }
    }

    return $texto;
}

function replace_links($texto, $lang)
{
    global $tag_fu;
    $tag_fu_arr = split("#", $tag_fu);
    $start = $tag_fu_arr[0];
    $end = $tag_fu_arr[1];

    if ($pos_start = strpos($texto, $start)) {
        $protocol_used = get_protocol_link($texto, $pos_start);
        $texto_mod = substr($texto, $pos_start + strlen($start));
        $pos_end = strpos($texto_mod, $end);
        $id_fu = substr($texto_mod, 0, $pos_end);
        $link = $protocol_used . $start . $id_fu . $end;

        if ($url = obtener_url_amigable_from_id($lang, $id_fu)) {
            $texto = str_replace($link, $url, $texto);
        } else {
            $texto = str_replace($link, "#", $texto);
        }

        if (strpos($texto, $start)) {
            $texto = replace_links($texto, $lang);
        }
    }
    return $texto;
}

function elimina_idiomas($id, $seccion, $seccion_public) {
    global $link;
    $sql = "DELETE FROM idiomas WHERE seccion='$seccion' AND seccion_public='$seccion_public' AND id_seccion = " . $id;
    //echo $sql;
    mysqli_query($link, $sql);
}

function inserta_url_friendly_completa($id, $sec, $campo, $tabla, $idioma, $default = "") {
    global $link;

    $url_amigable_completa[0] = $id;
    $url_amigable_completa[1] = $sec;

    $url = genera_url_amigable($url_amigable_completa, $idioma, $default);

    $sql = "UPDATE $tabla SET url_friendly_complet='$url' WHERE idioma = '$idioma' AND seccion = '$sec' AND campo = '$campo' AND id_seccion = $id";
    //echo "-->".$sql."<br/>";
    mysqli_query($link, $sql);

    return $url;
}

function generar_meta_title($meta, $ruta, $defecto) {
    if ($ruta != " | ") {
        $meta_aux = $meta . " | " . $ruta . $defecto;
    } else {
        $meta_aux = $meta . " | " . $defecto;
    }

    return $meta_aux;
}

function inserta_campo_idiomas($idioma, $seccion, $campo, $texto, $id_seccion, $noborrar = 0, $is_html = false) {
    global $link;

    $texto = prepare_to_db($texto, $is_html);

    $query = "INSERT INTO idiomas (idioma, seccion, id_seccion, campo, texto, noborrar)"
            . " VALUES ('$idioma', '$seccion', $id_seccion, '$campo', '$texto', $noborrar) "
            . " ON DUPLICATE KEY UPDATE "
            . " texto = '$texto', noborrar = $noborrar";

    //echo "-->".$sql."<br/>";
    mysqli_query($link, $query);
}

function borrar_campo_idioma($seccion, $id_seccion) {
    global $link;
    $sql = "DELETE FROM idiomas WHERE seccion = '$seccion' AND id_seccion = $id_seccion"; //borro la linea
    //echo "-->".$sql."<br/>";
    mysqli_query($link, $sql);
}

/* menu */

function selectar_menu($sec, $seccio, $default = "") {
    if ($sec == "") {
        $sec = $default;
    }
    if ($sec == $seccio) {
        return "selected";
    }
}

function menu_navegacion($id, $user) {
    global $link;
    $sql = "SELECT * FROM config_menu_admin WHERE "
            . "parent = $id AND activo = 1 "
            . "AND sec IN (SELECT config_secciones_admin.menu AS menu FROM admins_secciones, config_secciones_admin WHERE admins_secciones.admin = $user AND admins_secciones.seccion = config_secciones_admin.id AND config_secciones_admin.activo = 1 AND config_secciones_admin.contratado = 1 ) "
            . "ORDER BY orden ASC";
    //echo $sql;
    $consulta = mysqli_query($link, $sql);

    return $consulta;
}

function get_array_privileges($user) {
    global $link;
    $sql = "SELECT config_secciones_admin.menu AS menu FROM admins_secciones, config_secciones_admin WHERE admins_secciones.admin = $user AND admins_secciones.seccion = config_secciones_admin.id AND config_secciones_admin.activo = 1 AND config_secciones_admin.contratado = 1";
    //echo $sql;
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $secciones[] = $fila['menu'];
    }

    return $secciones;
}

function migas_videos($id, $sub, $enlace = true) {
    global $link;
    global $lang_default;

    $descripcion = "";

    if ($id > 0) {
        $sql = "SELECT * FROM videos_categoria WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $miga = migas_videos($fila['parent'], $enlace);

        $descripcion = obtener_valor($lang_default, "videos_categoria", "descripcion", $id);
    }

    if ($descripcion != "") {
        if ($enlace) {
            return ($miga . " <a href='index.php?sec=videos&sub=$sub&id=$id'>" . $descripcion . "</a> / ");
        } else {
            return ($miga . " " . $descripcion . " > ");
        }
    } else {
        return;
    }
}

function migas_de_pan_productos($id) { //usada en productos
    global $link;
    global $lang_default;

    if ($id > 0) {
        $sql = "SELECT * FROM productos_categoria WHERE id=$id";
        $consulta = mysqli_query($link, $sql);
        $fila = mysqli_fetch_array($consulta);
        $miga = migas_de_pan_productos($fila['parent']);
    }

    $descripcion = obtener_valor($lang_default, "productos_categoria", "descripcion", $id);

    if ($descripcion != "") {
        if (tiene_productos($id)) {
            return ($miga . " <a href='index.php?sec=productos&sub=listado-productos&id=$id'>" . $descripcion . "</a> / ");
        } else {
            return ($miga . " <a href='index.php?sec=productos&id=$id&parent=" . $fila['parent'] . "'>" . $descripcion . "</a> / ");
        }
    } else {
        return;
    }
}

function activar_seccion($admin, $seccion) {
    global $link;
    $sql = "SELECT id FROM admins_secciones WHERE admin = $admin AND seccion = " . $seccion;
    $consulta = mysqli_query($link, $sql);
    return mysqli_num_rows($consulta);
}

function tiene_paginas_subcats($categoria) {
    global $link;
    $sql = "SELECT id FROM paginas WHERE parent=$categoria";
    $consulta = mysqli_query($link, $sql);
    return mysqli_num_rows($consulta);
}
