<script type="text/javascript">
    var siteUrl = "<? echo $siteUrl; ?>";
    var siteName = "<? echo $siteName; ?>";
    var adminUrl = "<? echo $adminUrl; ?>";
    var adminThemeUrl = "<? echo $adminThemeUrl; ?>";
    var uploadUrl = "<? echo $siteUrl . $uploadUrl; ?>";
    var relativeUploadUrl = "<? echo $uploadUrl; ?>";
    var sec = "<? echo $sec; ?>";
    var sub = "<? echo $sub; ?>";
    var id = "<? if (isset($id)) {echo $id;};?>";
    var timThumbGalleryParams = "<? echo $timThumbGalleryParams; ?>";
    var timThumbBigGalleryParams = "<? echo $timThumbBigGalleryParams; ?>";
    var confirm_txt = "<? echo lang("confirm_txt"); ?>";
    var paginas_borrar_error = "<? echo lang("paginas_borrar_error"); ?>";
    var lang = "<? echo $lang_default; ?>";
    var idioma_defecto = "<? echo $lenguajes["defecto"]["idioma"]; ?>";
    var idiomas_activos = <? echo count($lenguajes["activos"]); ?>;
    var grid = <? echo $sortableGridSize; ?>;
    var paginas_ocultas_txt = "<? echo lang("paginas_ocultas_txt"); ?>";
    var toggle_nav_cookie = "<? echo $toggle_nav_cookie; ?>";
    var toggle_legend_cookie = "<? echo $toggle_legend_cookie; ?>";

    var btn_anyadir_nuevo = "<? echo lang("anyadir_nuevo"); ?>";
    var btn_anyadir = "<? echo lang("anyadir"); ?>";
    var btn_guardar = "<? echo lang("guardar"); ?>";
    var btn_guardar_volver = "<? echo lang("guardar_volver"); ?>";
    var btn_volver = "<? echo lang("volver"); ?>";
    var btn_guardar_volver_listado = "<? echo lang("guardar_volver_listado"); ?>";
    var btn_volver_listado = "<? echo lang("volver_listado"); ?>";
    var btn_preview = "<? echo lang("previsualizar"); ?>";
    var admin_sec = "<? echo $sec; ?>";
    var admin_sub = "<? echo $sub; ?>";

    var eliminar_subcategorias_txt = "<? echo lang("eliminar_subcategorias_txt"); ?>";
    var eliminar_cat_articulos_txt = "<? echo lang("eliminar_cat_articulos_txt"); ?>";

    var cancelar_txt = "<? echo lang("cancelar"); ?>";
</script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/jQuery/jQuery-1.11.0.min.js"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/chosen/<? echo minify("chosen.jquery", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/chosen/docsupport/<? echo minify("prism", "js", $development); ?>" charset="utf-8"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/<? echo minify("funciones", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/tablesorter/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>js/<? echo minify("main", "js", $development); ?>"></script>

<script type="text/javascript" src="<? echo $adminUrl; ?>includes/plugins/seo/<? echo minify("main", "js", $development); ?>"></script>

<script type="text/javascript" src="<? echo $adminUrl; ?>js/<? echo minify("paginas", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/fran6validate/<? echo minify("fran6validate", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/tinyValidate/<? echo minify("tinyValidate", "js", $development); ?>"></script>

<script type="text/javascript" src="<? echo $adminUrl; ?>lib/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/ckfinder/ckfinder.js"></script>

<script type="text/javascript" src="<? echo $adminUrl; ?>lib/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<? echo $adminUrl; ?>lib/jQuery/ui-1.11.4/jquery-ui.min.js"></script>