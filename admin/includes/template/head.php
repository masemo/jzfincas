<meta charset="iso-8859-1" />
<title>Admin | <? echo $adminName; ?></title>
<meta name="description" content="<? echo lang("description"); ?>" />
<meta name="keywords" content="<? echo lang("keywords"); ?>" />
<? if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    header('X-UA-Compatible: IE=edge,chrome=1');
} ?>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if IE]><script src="<? echo $adminUrl; ?>lib/html5-ie/trunk.js"></script><![endif]-->
<link rel="stylesheet" href="<? echo $adminUrl; ?>lib/fancybox/<? echo minify("jquery.fancybox", "css", $development) ?>" type="text/css" />
<link rel="stylesheet" href="<? echo $adminUrl; ?>lib/jQuery/ui-1.11.4/jquery-ui.structure.min.css" type="text/css" />
<link rel="stylesheet" href="<? echo $adminUrl; ?>lib/jQuery/ui-1.11.4/jquery-ui.theme.min.css" type="text/css" />
<link rel="stylesheet" href="<? echo $adminThemeUrl; ?>style/<? echo minify("main", "css", $development) ?>" type="text/css" />
<link rel="stylesheet" href="<? echo $adminUrl; ?>lib/chosen/<? echo minify("chosen", "css", $development) ?>">