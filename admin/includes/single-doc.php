<?
if (isset($fila)) {
    $doc = obtnener_imagen($fila['doc']);
}
?>

<input type="hidden" name="id_doc_old" id="id_doc_old" value="<? echo isset($id) ? $fila['doc'] : ''; ?>"/>
<input type="hidden" name="doc_old" id="doc_old" value="<? echo isset($id) ? $doc : ''; ?>"/>
<div class="single_image filebrowser" data-target="imagen">
    <label for="imagen"><? echo lang("documento"); ?></label>
    <figure>
        <img src="<? echo $adminThemeUrl; ?>images/no-file.png" class="imgprincipal no-image" title="<? echo lang("abrir_multimedia"); ?>"/>
        <? if (isset($fila) && $fila["doc"] != 0) { ?>
            <a href="<? echo $siteUrl; ?>upload/<? echo $doc; ?>" target="blank" class="docSelected"><img src="<? echo $adminThemeUrl; ?>images/file.png" class="imgprincipal"/></a>
            <button data-id="<? echo $id; ?>" data-sec="<? echo $sec; ?>" class="btn_borrar_doc" type="button" title="<? echo lang("eliminar"); ?>">X</button>
        <? } ?>
    </figure>
    <div class="fields">
        <p>
            <button type="button" id="fileBrowseImage"><? echo lang("abrir_multimedia"); ?></button>
            <input id="imagen" name="doc" type="text" value="" readonly="readonly" placeholder="<? echo lang("elegir_imagen"); ?>"/>
        </p>
    </div>
</div>