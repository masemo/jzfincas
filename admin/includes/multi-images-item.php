<?php
if (isset($_POST["img_data"])) { //si ve de ajax
    include("../system/vars.php"); //variables del sistema 
    include("../system/conexiondb.php"); //conexión a la base de datos
    include("../system/config.php"); //configuración del entorno
    include("../funciones/funciones.php"); //configuración del entorno
    include("../common/functions.php"); //funciones que se comparten con la parte publica
    include("../../functions/main.php"); //funciones que se comparten con la parte publica
    include("../system/preload.php"); //variables del sistema 
    $lang_file_url = "../";
    include("../funciones/lang.php"); //gestión de lenguajes
    $multi_images_item = $_POST["img_data"];
    $sub = $_POST["sub"];
    $lenguajes = carga_idioma();
    $titulo = lang("titulo");
    $pestanyeraCondensed = true;
} else {
    $titulo = lang("titulo");
}
?>
<? foreach ($multi_images_item as $img) { ?>
    <li class="image-item" data-id="<? echo $img['id']; ?>">
        <figure>
            <img src="<? echo $img["imagen"]; ?>"/> 
            <button type="button" class="btn_borrar_img" data-sec="multimedia" data-id="<? echo $img['id']; ?>">X</button>
        </figure>
        <div class="content">
            <div class="pestanyera_area" data-for="img_alt_form_<? echo $img['id']; ?>">
                <? include("pestanyera-lang.php"); ?>
            </div>
            <div class="form_area img_alt_form" id="img_alt_form_<? echo $img['id']; ?>">
                <input type="hidden" name="img_id" id="img_id" value="<? echo $img['id']; ?>" />
                <div class="lang_field_group">
                    <?
                    foreach ($lenguajes["activos"] as $lenguaje) {
                        $leng = $lenguaje["idioma"];
                        ?>
                        <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                            <p>
                                <label for="img_alt_<? echo $leng . "_" . $img['id']; ?>"><? echo $titulo; ?></label>
                                <input type="text" name="img_alt_<? echo $leng; ?>" id="img_alt_<? echo $leng . "_" . $img['id']; ?>" value="<?
                                if ($sub == "editar") {
                                    echo obtener_valor($leng, "imagen", "img_alt", $img['id']);
                                }
                                ?>" data-validate="required"/>
                            </p>
                        </fieldset>
                    <? } ?>
                    <div class="row">
                        <div class="col-40 noRp">
                            <button type="button" class="primary_btn save_form_area"><? echo lang("guardar"); ?></button>
                        </div>
                        <div class="col-60 noLp alert_group">
                            <p class="alert ok"><? echo lang("guardado"); ?></p>
                            <p class="alert catastrofe"><? echo lang("error_generico"); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
<? } ?>