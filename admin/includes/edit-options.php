<?
    if(!isset($guardar_txt)){
        $guardar_txt = lang("guardar");
    }
    if(!isset($guardar_volver_txt)){
        $guardar_volver_txt = lang("guardar_volver_listado");
    }
    if(!isset($volver_txt)){
        $volver_txt = lang("volver_listado");
    }
    if(!isset($previsualizar_txt)){
        $previsualizar_txt = lang("previsualizar");
    }
?>
<div class="edit_options">
    <button type="button" class="save" title="<? echo $guardar_txt; ?>">
        <span class="icon"></span>
        <span class="text" data-text="<? echo $guardar_txt; ?>"></span>
    </button>
    <button type="button" class="save_back" title="<? echo $guardar_volver_txt; ?>">
        <span class="icon"></span>
        <span class="text" data-text="<? echo $guardar_volver_txt; ?>"></span>
    </button>
    <button type="button" class="back" title="<? echo $volver_txt; ?>">
        <span class="icon"></span>
        <span class="text" data-text="<? echo $volver_txt; ?>"></span>
    </button>
    <? if (($sec == "paginas" && !$estatica  && !$homePage) || $sec == "noticias") { ?>
        <a class="button preview" href="" target="_blank" title="<? echo $previsualizar_txt; ?>">
            <span class="icon"></span>
            <span class="text" data-text="<? echo $previsualizar_txt; ?>"></span> (<span class="preview_lang"></span>)
        </a>
    <? } ?>
</div>