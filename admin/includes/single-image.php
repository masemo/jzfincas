<?
$id_imagen_old = $imagen_old = $sufix = $leng = $img_src = "";
$oculto = "hidden";
if (isset($lenguaje)) {
    $leng = $lenguaje["idioma"];
    $sufix = "_" . $leng;
}
if (isset($id) && isset($fila)) {
    if (isset($lenguaje)) {
        $imagen_data = obtnener_imagen_idioma($sec, $id, $leng, false);
        $imagen = $imagen_data["fichero"];
        $id_imagen_old = $imagen_data["id"];
        $imagen_old = $imagen;
    } else {
        $imagen = obtnener_imagen($fila['imagen']);
        $id_imagen_old = $fila['imagen'];
        $imagen_old = $imagen;
    }
    if ($imagen != "") {
        $thumbParams = "&amp;w=240&amp;h=240&amp;q=60&amp;zc=1";
        if ($sec == "banners") {
            $thumbParams = "&amp;w=240&amp;h=240&amp;q=60&amp;zc=2";
        }
        $img_src = $adminUrl . "lib/timThumb/timThumb.php?src=" . pintar_imagen($imagen, true) . $thumbParams;
    }
    if ($id_imagen_old != 0) {
        $oculto = "";
    }
} else {
    $id = 0;
}
?>
<input type="hidden" name="id_imagen_old<? echo $sufix; ?>" id="id_imagen_old<? echo $sufix; ?>" value="<? echo $id_imagen_old; ?>" />
<input type="hidden" name="imagen_old<? echo $sufix; ?>" id="imagen_old<? echo $sufix; ?>" value="<? echo $imagen_old; ?>" />
<div class="single_image filebrowser" data-target="imagen<? echo $sufix; ?>" data-lang="<? echo $leng; ?>">
    <label for="imagen"><? echo lang("imagen"); ?></label>
    <figure>
        <img src="<? echo $adminThemeUrl; ?>images/no-image.png" class="imgprincipal no-image" title="<? echo lang("abrir_galeria"); ?>"/>
        <div class="img_group <? echo $oculto; ?>">
            <img src="<? echo $img_src; ?>" class="imgprincipal imgSelected"/>
            <button data-id="<? echo $id; ?>" data-sec="<? echo $sec; ?>" class="btn_borrar" type="button" title="<? echo lang("eliminar"); ?>">X</button>
        </div>
    </figure>
    <div class="fields">
        <p>
            <button type="button" id="fileBrowseImage"><? echo lang("abrir_galeria"); ?></button>
            <input id="imagen<? echo $sufix; ?>" name="imagen<? echo $sufix; ?>" type="text" value="" readonly="readonly" placeholder="<? echo lang("elegir_imagen"); ?>" />
        </p>
    </div>
</div>