<?php

header('Content-Type: application/json; charset=utf-8');


include("../system/vars.php");
include("../system/conexiondb.php");
include("../system/config.php");
include("../funciones/url.php");
include("../funciones/funciones.php");
include("../common/functions.php");
include("../../functions/main.php");
$idiomas = carga_idioma();
$moduls_contractats = cargar_modulos_contratados();

function check_modul_contractat($estatica, $sec_public)
{
    if ((!$estatica) || ($estatica && modul_contractat($sec_public))) {
        return true;
    } else {
        return false;
    }
}

function seccion_online($id_page)
{
    global $link;
    $query = "SELECT activo, sec_public, estatica FROM paginas WHERE id = $id_page";
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_assoc($result);
    if ($row["activo"] == 1 && check_modul_contractat($row["estatica"], $row["sec_public"])) {
        return true;
    }
    return false;
}

function fetch_array($id, $lang, $sec, $campo, $sec_val = false, $id_val = false)
{
    if (!$id_val) {
        $id_val = $id;
    }
    if (!$sec_val) {
        $sec_val = $sec;
    }

    $aux = array();
    //$level = getLevel($id, $lang, $sec); print_level($level) .
    $aux["label"] = obtener_valor($lang, $sec_val, $campo, $id_val);
    $aux["url"] = encode_url_dinamic(obtener_url_id($lang, $sec, $id));
    return $aux;
}

function getLevel($data, $return = array(), $level = 0)
{
    foreach ($data as $key => $value) {
        if ($key == "categoria") {
            $level++;
        }
        if (isset($value["label"]) && isset($value["url"])) {
            $return[] = array("label" => print_level($level) . $value["label"], "url" => $value["url"]);
        } else {
            $return[] = array("label" => print_level($level) . $value["categoria"]["label"], "url" => $value["categoria"]["url"]);
            $return = getLevel($value["items"], $return, $level);
        }
    }
    return $return;
}

function obtener_alternate($lang, $id, $sec)
{
    global $idiomas;
    $princ = array();
    if (count($idiomas["activos"]) > 1) {
        foreach ($idiomas["activos"] as $idioma) {
            $leng = $idioma["idioma"];
            if ($lang != $leng) {
                $aux = array();
                $aux["lang"] = $leng;
                $aux["url"] = obtener_alternate_url($id, $leng, $sec);
                $princ[] = $aux;
            }
        }
    }
    return $princ;
}

function obtener_alternate_url($id, $leng, $sec)
{
    global $siteUrl;
    $fu = $siteUrl . $leng . "/";
    if ($sec != "" && $sec != "home") {
        $fu = obtener_url_amigable($leng, $sec, $id);
    }
    return $fu;
}

function print_level($level)
{
    $space = "";
    if ($level > 1) {
        for ($x = 1; $x <= $level; $x++) {
            $space .= "·";
        }
        return "$space ";
    } else {
        return "· ";
    }
}

/* --------------------------------------------------------------- CONTENIDO -- */

function crea_xml_contenido($lang)
{
    global $link;
    $sql = "SELECT id, estatica, estatica_sec, sec_public, external_link FROM paginas WHERE activo = 1 ORDER BY orden ASC";
    $consulta = mysqli_query($link, $sql);
    $totals = mysqli_num_rows($consulta);
    $princ = array();
    if ($totals > 0) {
        while ($row = mysqli_fetch_assoc($consulta)) {
            if (seccion_online($row['id']) && !$row['external_link']) {
                if ($row['estatica'] == 1) {
                    $cat = fetch_array(0, $lang, $row['estatica_sec'], 'nombre_menu', 'paginas', $row['id']);
                    switch ($row['estatica_sec']) {
                        case "noticias_categoria":
                            global $activar_categorias_noticias;
                            $items = crea_xml_noticias_categorias($lang, $activar_categorias_noticias);
                            break;
                        case "galeria_categoria":
                            $items = crea_xml_galeria($lang);
                            break;
                        case "productos_categoria":
                            $items = crea_xml_productos_categorias($lang);
                            break;
                        case "videos_categoria":
                            $items = crea_xml_videos($lang);
                            break;
                        default:
                            break;
                    }
                    if (isset($items)) {
                        $princ[] = array("categoria" => $cat, "items" => $items);
                    } else {
                        $princ[] = $cat;
                    }
                } else {
                    $data = fetch_array($row['id'], $lang, "paginas", 'nombre_menu', "paginas", $row['id']);
                    $princ[] = $data;
                }
            }
        }
    }
    return $princ;
}

/* ---------------------------------------------------------------- NOTICIAS -- */

function crea_xml_noticias_categorias($lang, $activar_categorias_noticias)
{
    global $link;
    global $id_pagina_noticias;
    $princ = array();
    if ($activar_categorias_noticias && seccion_online($id_pagina_noticias)) {
        $query = "SELECT id FROM noticias_categoria WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $cat = fetch_array($row['id'], $lang, "noticias_categoria", "descripcion");
                $princ[] = array("categoria" => $cat, "items" => crea_xml_noticias($lang, $row['id']));
            }
        } else {
            $princ[] = crea_xml_noticias($lang);
        }
    }
    return $princ;
}

function crea_xml_noticias($lang, $id = NULL)
{
    global $link;
    global $id_pagina_noticias;
    $princ = array();
    if (seccion_online($id_pagina_noticias)) {

        if (is_null($id)) {
            $query = "SELECT id FROM noticias WHERE activo = 1 ORDER BY fecha_public DESC";
        } else {
            $query = "SELECT id FROM noticias WHERE activo = 1 AND parent = $id ORDER BY fecha_public DESC";
        }

        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "noticias", "titular");
            }
        }
    }
    return $princ;
}

/* ------------------------------------------------------------- PRODUCTOS -- */

function crea_xml_productos_categorias($lang)
{
    global $link;
    global $id_pagina_productos;
    $princ = array();
    if (seccion_online($id_pagina_productos)) {
        $query = "SELECT id FROM productos_categoria WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $cat = fetch_array($row['id'], $lang, "productos_categoria", "descripcion");
                $princ[] = array("categoria" => $cat, "items" => crea_xml_productos($lang, $row['id']));
            }
        } else {
            $princ[] = crea_xml_productos($lang);
        }
    }
    return $princ;
}

function crea_xml_productos($lang, $id = NULL)
{
    global $link;
    global $id_pagina_productos;
    $princ = array();
    if (seccion_online($id_pagina_productos)) {
        if (is_null($id)) {
            $query = "SELECT id FROM productos WHERE activo = 1 ORDER BY orden ASC";
        } else {
            $query = "SELECT id FROM productos WHERE categoria = $id AND activo = 1 ORDER BY orden ASC";
        }
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "productos", "nombre");
            }
        }
    }
    return $princ;
}

/* --------------------------------------------------------------- GALERIA -- */

function crea_xml_galeria($lang)
{
    global $link;
    global $id_pagina_galeria_imagenes;
    $princ = array();
    if (seccion_online($id_pagina_galeria_imagenes)) {
        $query = "SELECT id FROM galeria_categoria WHERE activo = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 1) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "galeria_categoria", "descripcion");
            }
        }
    }
    return $princ;
}

/* --------------------------------------------------------------- GALERIA -- */

function crea_xml_videos($lang)
{
    global $link;
    global $id_pagina_videos;
    $princ = array();
    if (seccion_online($id_pagina_videos)) {
        $query = "SELECT id FROM videos_categoria WHERE activo = 1 ORDER BY id, orden ASC";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 1) {
            while ($row = mysqli_fetch_assoc($result)) {
                $princ[] = fetch_array($row['id'], $lang, "videos_categoria", "descripcion");
            }
        }
    }
    return $princ;
}

/* -------------------------------------------------------------------------- */
if(isset($_GET["lang"])){
    $lang_default = $_GET["lang"];
}else{
    die();
}


$data = array(
    array(
        'label' => 'Selecciona una página',
        'url' => ''
    )
);
$data = array_merge($data, crea_xml_contenido($lang_default));
$data = getLevel($data);
echo json_encode($data);