<?
if ($item_ant > -1 || $item_sig > -1) {
    ?>
    <div class="next_prev_nav row">
        <div class="col-50 prev">
            <? if ($item_ant > -1) { ?>
                <a href="<? echo $link_ant; ?>" title="<? echo $sec_title . " " . lang("anterior"); ?>" <? if ($img_ant != "") { ?>class="img"<? } ?>>
                    <span>&lt;</span>
                    <? if ($img_ant != "") { ?>
                        <img style="" src="lib/timThumb/timThumb.php?src=<? echo $img_ant; ?>&w=60&h=35&zc=1&q=60" id="img_ant" alt="::<? echo lang("anterior"); ?>:"/>
                    <? } else { ?>
                        <span class="text"><? echo $title_ant; ?></span>
                    <? } ?>
                </a> 
            <? } else { ?>
                &nbsp; 
            <? } ?>
        </div>
        <div class="col-50 next">
            <? if ($item_sig > -1) { ?>
                <a href="<? echo $link_sig; ?>" title="<? echo $sec_title . " " . lang("siguiente"); ?>" <? if ($img_sig != "") { ?>class="img"<? } ?>>
                    <? if ($img_sig != "") { ?>
                        <img style="" src="lib/timThumb/timThumb.php?src=<? echo $img_sig; ?>&w=60&h=35&zc=1&q=60" id="img_sig" alt="::<? echo lang("siguiente"); ?>::"/>
                    <? } else { ?>
                        <span class="text"><? echo $title_sig; ?></span>
                    <? } ?>
                    <span>&gt;</span>
                </a>
            <? } ?>
        </div>
    </div>
<? } ?>