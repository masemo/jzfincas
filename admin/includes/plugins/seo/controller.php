<?php

$length_title = 60;
$length_descripcion = 155;
$length_friendly_url = 70;

$obtener_metas_title = "";
$obtener_metas_description = "";
$obtener_friendly_url = "";
$fu_params = "";

$hide_friendly_url = false;
if (isset($estatica_sec) && $estatica_sec == "home") {
    $hide_friendly_url = true;
}
if (isset($id)) {
    $id_seo = $id;
    $sec_seo = $sec;
    if (isset($estatica) && $estatica == 1) {
        $id_seo = 0;
        $sec_seo = $estatica_sec;
    }
    $fu_params = obtener_friendly_url_params($lenguajes["defecto"]["idioma"], $sec_seo, $id_seo);
}
$plugin_idioma = lang_file($adminLang, "includes/plugins/seo/");

$pluginState = "";
if (isset($openPlugin)) {
    $pluginState = "opened";
}