/*------------------------------------------------------------ friendly url --*/
function clean_url(url) {
    var clean_url = url.toLowerCase();
    clean_url = clean_url.replace(/[@באהג]/g, 'a');
    clean_url = clean_url.replace(/[יטכך]/g, 'e');
    clean_url = clean_url.replace(/[םלןמ]/g, 'i');
    clean_url = clean_url.replace(/[ףעצפ]/g, 'o');
    clean_url = clean_url.replace(/[תש]/g, 'u');
    clean_url = clean_url.replace(/[ס]/g, 'n');
    clean_url = clean_url.replace(/[\.]/g, '-');
    clean_url = clean_url.replace(/[^a-z0-9\-_\/ ]/g, '');
    clean_url = clean_url.replace(/\s{2,}/g, ' ');
    clean_url = clean_url.replace(/\s/g, "-");
    clean_url = clean_url.replace(/-{2,}/g, "-");
    clean_url = clean_url.replace(/-$/g, "");
    return clean_url;
}

function current_lang(field) {
    var current_lang = field.closest(".lang_group").attr("data-lang");
    return current_lang;
}

function genera_friendly_url(field) {
    var language = current_lang(field);
    var seo_plugin_fu_input = $("#friendly_url_" + language);
    if (seo_plugin_fu_input.val() === "") {
        var file = clean_url(field.val());
        var parent = $("#parent").val();
        var params = params_seccion(sec, id);
        $.post("includes/plugins/seo/functions.php", {seo_action: "genera_friendly_url", sec: params.seccion, id: params.id_seccion, parent: parent, language: language, file: file}, function (data) {
            if (exists(".seo_admin")) {
                var target = $(".seo_admin #friendly_url_" + language);
                display_friendly_url(data["fu"], target, language);
                if (data["valid"] === 0) {
                    invalid_friendly_url(target);
                }
            }
        }, "json");
    }
}
function display_friendly_url(fu, target, language) {
    var validation_input = target.prev(".fu_validation");
    var validation_alert = target.closest(".friendly_url_content").find(".fu_invalid");
    target.closest(".friendly_url_content").removeClass("highlight");
    target.removeClass("required");
    validation_input.removeClass("required");
    validation_alert.hide();
    $(".seo_admin #friendly_url_" + language + "_preview .text").text(fu);
}
function invalid_friendly_url(target) {
    if (!target.hasClass("required")) {
        var validation_input = target.prev(".fu_validation");
        var validation_alert = target.closest(".friendly_url_content").find(".fu_invalid");
        target.addClass("required");
        validation_input.addClass("required");
        validation_alert.fadeIn(300);
        alert(alert_fu_txt);
        target.closest(".friendly_url_content").addClass("highlight");
    }
}

/*------------------------------------------------------------------- title --*/
function genera_meta_title(field) {
    var language = current_lang(field);
    var seo_plugin_title = $("#meta_title_" + language).val();
    var separador = " | ";
    if (seo_plugin_title === "") {
        var parent = $("#parent").val();
        $.post("includes/plugins/seo/functions.php", {seo_action: "obtener_parents", sec: sec, id: id, parent: parent, language: language, separador: separador}, function (data) {
            display_meta_title(language, field, data, separador);
        });
    }
}
function display_meta_title(language, field, parents, separador) {
    if (parents !== separador) {
        parents = separador + parents;
    }
    if (exists(".seo_admin")) {
        var title = field.val() + parents + siteName;
        $(".seo_admin #meta_title_" + language + "_preview .text").text(title);
    }
}

/*------------------------------------------------------------- description --*/
function genera_meta_description(value, field) {
    var language = current_lang(field);
    var seo_plugin_description = $("#meta_description_" + language).val();
    console.log(seo_plugin_description);
    if (seo_plugin_description === "") {
        var stripped_tags = value.replace(/(<([^>]+)>)/ig, "");
        stripped_tags = stripped_tags.replace(/\s+/g, " ");
        var short_text = stripped_tags.substr(0, 152) + "...";
        $(".seo_admin #meta_description_" + language + "_preview .text").text(short_text);
    }
}
function editor_seo_event(target, field) {
    target.on("focus", function () {
        var focus_value = this.document.getBody().getText();
        $(this).data("focus_value", focus_value);
    });
    target.on("blur", function () {
        var blur_value = this.document.getBody().getText();
        if (blur_value !== $(this).data("focus_value")) {
            genera_meta_description(blur_value, field);
        }
    });
}

$(document).ready(function () {
    $(".seo_friendly_url").on({
        focus: function () {
            var focus_value = $(this).val();
            $(this).data("focus_value", focus_value);
        },
        blur: function () {
            var blur_value = $(this).val();
            if (blur_value !== $(this).data("focus_value")) {
                genera_friendly_url($(this));
            }
        }
    });
    $("#parent").on("change", function () {
        $(".seo_friendly_url").each(function () {
            genera_friendly_url($(this));
        });
        $(".seo_title").each(function () {
            genera_meta_title($(this));
        });
    });
    $(".seo_title").on({
        focus: function () {
            var focus_value = $(this).val();
            $(this).data("focus_value", focus_value);
        },
        blur: function () {
            var blur_value = $(this).val();
            if (blur_value !== $(this).data("focus_value")) {
                genera_meta_title($(this));
            }
        }
    });
    $("input.seo_description").on({
        focus: function () {
            var focus_value = $(this).val();
            $(this).data("focus_value", focus_value);
        },
        blur: function () {
            var blur_value = $(this).val();
            if (blur_value !== $(this).data("focus_value")) {
                genera_meta_description($(this).val(), $(this));
            }
        }
    });
    if (exists(".metas_estatico")) {
        $(".metas_estatico input").addClass("required");
    }
});