<?php
if (isset($_POST["seo_action"])) { // si venim per ajax
    include_once("../../../system/vars.php"); //variables del sistema 
    include_once("../../../system/conexiondb.php"); //conexión a la base de datos
    include_once("../../../system/config.php"); //configuración del entorno
    include_once("../../../lib/funciones.php"); //variables del sistema 
    include_once("../../../funciones/funciones.php"); //variables del sistema 
}

function url_amigable_limpiar($string, $todo_minusculas = true, $separator = '-')
{
    if ($todo_minusculas) {
        $string = strtolower($string);
    }
    $accents = array(
        '&amp;' => 'and',   '@' => 'at',    '©' => 'c', '®' => 'r', 'À' => 'a',
        'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'Å' => 'a', 'Æ' => 'ae','Ç' => 'c',
        'È' => 'e', 'É' => 'e', 'Ë' => 'e', 'Ì' => 'i', 'Í' => 'i', 'Î' => 'i',
        'Ï' => 'i', 'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Õ' => 'o', 'Ö' => 'o',
        'Ø' => 'o', 'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'Ý' => 'y',
        'ß' => 'ss','à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', 'å' => 'a',
        'æ' => 'ae','ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
        'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ò' => 'o', 'ó' => 'o',
        'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u',
        'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'þ' => 'p', 'ÿ' => 'y', 'Ā' => 'a',
        'ā' => 'a', 'Ă' => 'a', 'ă' => 'a', 'Ą' => 'a', 'ą' => 'a', 'Ć' => 'c',
        'ć' => 'c', 'Ĉ' => 'c', 'ĉ' => 'c', 'Ċ' => 'c', 'ċ' => 'c', 'Č' => 'c',
        'č' => 'c', 'Ď' => 'd', 'ď' => 'd', 'Đ' => 'd', 'đ' => 'd', 'Ē' => 'e',
        'ē' => 'e', 'Ĕ' => 'e', 'ĕ' => 'e', 'Ė' => 'e', 'ė' => 'e', 'Ę' => 'e',
        'ę' => 'e', 'Ě' => 'e', 'ě' => 'e', 'Ĝ' => 'g', 'ĝ' => 'g', 'Ğ' => 'g',
        'ğ' => 'g', 'Ġ' => 'g', 'ġ' => 'g', 'Ģ' => 'g', 'ģ' => 'g', 'Ĥ' => 'h',
        'ĥ' => 'h', 'Ħ' => 'h', 'ħ' => 'h', 'Ĩ' => 'i', 'ĩ' => 'i', 'Ī' => 'i',
        'ī' => 'i', 'Ĭ' => 'i', 'ĭ' => 'i', 'Į' => 'i', 'į' => 'i', 'İ' => 'i',
        'ı' => 'i', 'Ĳ' => 'ij','ĳ' => 'ij','Ĵ' => 'j', 'ĵ' => 'j', 'Ķ' => 'k',
        'ķ' => 'k', 'ĸ' => 'k', 'Ĺ' => 'l', 'ĺ' => 'l', 'Ļ' => 'l', 'ļ' => 'l',
        'Ľ' => 'l', 'ľ' => 'l', 'Ŀ' => 'l', 'ŀ' => 'l', 'Ł' => 'l', 'ł' => 'l',
        'Ń' => 'n', 'ń' => 'n', 'Ņ' => 'n', 'ņ' => 'n', 'Ň' => 'n', 'ň' => 'n',
        'ŉ' => 'n', 'Ŋ' => 'n', 'ŋ' => 'n', 'Ō' => 'o', 'ō' => 'o', 'Ŏ' => 'o',
        'ŏ' => 'o', 'Ő' => 'o', 'ő' => 'o', 'Œ' => 'oe','œ' => 'oe','Ŕ' => 'r',
        'ŕ' => 'r', 'Ŗ' => 'r', 'ŗ' => 'r', 'Ř' => 'r', 'ř' => 'r', 'Ś' => 's',
        'ś' => 's', 'Ŝ' => 's', 'ŝ' => 's', 'Ş' => 's', 'ş' => 's', 'Š' => 's',
        'š' => 's', 'Ţ' => 't', 'ţ' => 't', 'Ť' => 't', 'ť' => 't', 'Ŧ' => 't',
        'ŧ' => 't', 'Ũ' => 'u', 'ũ' => 'u', 'Ū' => 'u', 'ū' => 'u', 'Ŭ' => 'u',
        'ŭ' => 'u', 'Ů' => 'u', 'ů' => 'u', 'Ű' => 'u', 'ű' => 'u', 'Ų' => 'u',
        'ų' => 'u', 'Ŵ' => 'w', 'ŵ' => 'w', 'Ŷ' => 'y', 'ŷ' => 'y', 'Ÿ' => 'y',
        'Ź' => 'z', 'ź' => 'z', 'Ż' => 'z', 'ż' => 'z', 'Ž' => 'z', 'ž' => 'z',
        'ſ' => 'z', 'Ə' => 'e', 'ƒ' => 'f', 'Ơ' => 'o', 'ơ' => 'o', 'Ư' => 'u',
        'ư' => 'u', 'Ǎ' => 'a', 'ǎ' => 'a', 'Ǐ' => 'i', 'ǐ' => 'i', 'Ǒ' => 'o',
        'ǒ' => 'o', 'Ǔ' => 'u', 'ǔ' => 'u', 'Ǖ' => 'u', 'ǖ' => 'u', 'Ǘ' => 'u',
        'ǘ' => 'u', 'Ǚ' => 'u', 'ǚ' => 'u', 'Ǜ' => 'u', 'ǜ' => 'u', 'Ǻ' => 'a',
        'ǻ' => 'a', 'Ǽ' => 'ae','ǽ' => 'ae','Ǿ' => 'o', 'ǿ' => 'o', 'ə' => 'e',
        'Ё' => 'jo','Є' => 'e', 'І' => 'i', 'Ї' => 'i', 'А' => 'a', 'Б' => 'b',
        'В' => 'v', 'Г' => 'g', 'Д' => 'd', 'Е' => 'e', 'Ж' => 'zh','З' => 'z',
        'И' => 'i', 'Й' => 'j', 'К' => 'k', 'Л' => 'l', 'М' => 'm', 'Н' => 'n',
        'О' => 'o', 'П' => 'p', 'Р' => 'r', 'С' => 's', 'Т' => 't', 'У' => 'u',
        'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c', 'Ч' => 'ch','Ш' => 'sh','Щ' => 'sch',
        'Ъ' => '-', 'Ы' => 'y', 'Ь' => '-', 'Э' => 'je','Ю' => 'ju','Я' => 'ja',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ж' => 'zh','з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l',
        'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
        'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
        'ш' => 'sh','щ' => 'sch','ъ' => '-','ы' => 'y', 'ь' => '-', 'э' => 'je',
        'ю' => 'ju','я' => 'ja','ё' => 'jo','є' => 'e', 'і' => 'i', 'ї' => 'i',
        'Ґ' => 'g', 'ґ' => 'g', 'א' => 'a', 'ב' => 'b', 'ג' => 'g', 'ד' => 'd',
        'ה' => 'h', 'ו' => 'v', 'ז' => 'z', 'ח' => 'h', 'ט' => 't', 'י' => 'i',
        'ך' => 'k', 'כ' => 'k', 'ל' => 'l', 'ם' => 'm', 'מ' => 'm', 'ן' => 'n',
        'נ' => 'n', 'ס' => 's', 'ע' => 'e', 'ף' => 'p', 'פ' => 'p', 'ץ' => 'C',
        'צ' => 'c', 'ק' => 'q', 'ר' => 'r', 'ש' => 'w', 'ת' => 't', '™' => 'tm',
    );
    $string = strtr($string, $accents);

    $string = preg_replace('/[@áàäâ]/', 'a', $string);
    $string = preg_replace('/[éèëê]/', 'e', $string);
    $string = preg_replace('/[íìïî]/', 'i', $string);
    $string = preg_replace('/[óòöô]/', 'o', $string);
    $string = preg_replace('/[úùüû]/', 'u', $string);
    $string = preg_replace('/[ñ]/', 'n', $string);
    $string = preg_replace('/[\.]/', '-', $string);
    $string = preg_replace('/[^a-z0-9\-_\/ ]/', '', $string);
    $string = preg_replace('/\s{2,}/', ' ', $string);
    $string = preg_replace('/\s/', "-", $string);
    $string = preg_replace('/-{2,}/', "-", $string);
    $string = preg_replace('/-$/', "", $string);
    return $string;
}

function eliminar_seo($seccion, $id_seccion)
{
    global $link;
    $query = "DELETE FROM metas WHERE seccion = '$seccion' AND id_seccion = $id_seccion AND noborrar = 0";
    if (mysqli_query($link, $query)) {
        $query = "DELETE FROM friendly_url WHERE seccion = '$seccion' AND id_seccion = $id_seccion AND noborrar = 0";
        if (mysqli_query($link, $query)) {
            return true;
        }
    }
    return false;
}

function obtener_metas($idioma, $seccion, $campo, $id_seccion, $valor_default = 'texto')
{
    global $link;
    global $adminName;
    global $lang_default;
    $publicSite = true;
    if (isset($adminName)) {
        $publicSite = false;
    }
    $query = "SELECT $valor_default FROM metas WHERE idioma = '$idioma' AND campo = '$campo' AND seccion = '$seccion' AND id_seccion = $id_seccion LIMIT 1";
    //echo $query . '<br/>';
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_array($result);
    $texto = $row[$valor_default];
    if ($texto == "") {
        if ($lang_default != $idioma && $publicSite) {
            $texto = obtener_metas($lang_default, $seccion, $campo, $id_seccion, $valor_default = 'texto');
        }
    }

    return ($texto);
}

function obtener_friendly_url($idioma, $seccion, $id_seccion)
{
    global $link;
    $query = "SELECT url FROM friendly_url WHERE idioma = '$idioma' AND seccion = '$seccion' AND id_seccion=$id_seccion";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $row = mysqli_fetch_array($result);
        return $row["url"];
    }
    return "";
}

function obtener_friendly_url_params($idioma, $seccion, $id_seccion)
{
    global $link;
    $query = "SELECT params FROM friendly_url WHERE idioma = '$idioma' AND seccion = '$seccion' AND id_seccion=$id_seccion";
//    echo $query."<br />";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $row = mysqli_fetch_array($result);
        return $row["params"];
    }
    return "";
}

function obtener_parents($parent, $lang, $tabla, $campo_nombre, $limpiar = false, $separador = "/")
{
    global $link;

    $aux = "";

    $sql = "SELECT parent FROM $tabla WHERE id = $parent";
//    echo $sql."<br/>";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($fila['parent'] > 0) {
        $aux = obtener_parents($fila['parent'], $lang, $tabla, $campo_nombre, $limpiar, $separador);
    }

    $valor = obtener_valor($lang, $tabla, $campo_nombre, $parent);
    if ($limpiar) {
        $valor = url_amigable_limpiar($valor);
    }

    return $valor . $separador . $aux;
}

function genera_friendly_url($parents, $file)
{
    return $parents . $file;
}

function validate_friendly_url($fu, $sec, $id)
{
    global $link;
    $fu_validation = array(
        "fu" => $fu,
        "valid" => 1
    );
    if ($fu != "") {
        $query = "SELECT * FROM friendly_url WHERE url = '$fu'";
        $result = mysqli_query($link, $query);
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                if ($row["seccion"] != $sec || $row["id_seccion"] != $id) {
                    $fu_validation["valid"] = 0;
                }
            }
        } else {
            $fu_validation["valid"] = 1;
        }
    }
    return json_encode($fu_validation);
}

function inserta_metas($idioma, $seccion, $campo, $campo_valor, $id_seccion, $noborrar = 0, $is_html = false)
{
    if ($campo_valor != "") {
        global $link;

        $campo_valor = prepare_to_db($campo_valor, $is_html);

        $query = "INSERT INTO metas (idioma,seccion,id_seccion,campo,texto,noborrar) VALUES ('$idioma','$seccion',$id_seccion,'$campo','$campo_valor',$noborrar)"
            . " ON DUPLICATE KEY UPDATE "
            . " texto = '$campo_valor', noborrar = $noborrar ";
//        echo $query . "<br/>";
        if (mysqli_query($link, $query)) {
            return true;
        }
    }
    return false;
}

function inserta_friendly_url($idioma, $seccion, $friendly_url, $id_seccion, $noborrar = 0, $params = "")
{
    if ($friendly_url != "") {
        global $link;
        $clean_url = url_amigable_limpiar($friendly_url);
        $query = "INSERT INTO friendly_url (idioma, seccion, id_seccion, url, params, noborrar) VALUES ('$idioma', '$seccion', $id_seccion, '$clean_url', '$params', $noborrar)"
            . " ON DUPLICATE KEY UPDATE "
            . " url = '$clean_url', params = '$params' ";
//    echo $query."<br />";
        if (mysqli_query($link, $query)) {
            return true;
        }
    }
    return false;
}

/* -------------------------------------------------------------- MANDANGA -- */

function gestion_metas($leng, $frm, $id, $parent, $seo_vars, $noborrar = 0)
{
    global $siteName;
    $seccion = $seo_vars["seccion"];
    $seccion_parents = $seo_vars["seccion_parents"];
    $fu_field = $seo_vars["fu_field"];
    $title_field = $seo_vars["title_field"];
    $description_field = $seo_vars["description_field"];
    $parent_title_field = $seo_vars["parent_title_field"];
    $estatica = $seo_vars["estatica"];
    $estatica_sec = $seo_vars["estatica_sec"];
    $id_seccion = $id;
    $parents = "";

    if ($estatica == 1) {
        $id_seccion = 0;
        $seccion = $estatica_sec; // páginas estáticas
    }
    /* -------------------------------------------------------- friendly url -- */
    if ($estatica_sec == "home") {
        $friendly_url = "";
    } else {
        if (isset($frm["seoPlugin"]) && $frm["friendly_url_" . $leng] != "") {
            $friendly_url = $frm["friendly_url_" . $leng];
        } else {
            $friendly_url = gestion_friendly_url_automaticas($leng, $seccion_parents, $parent, $frm[$fu_field . $leng], $parent_title_field);
        }
        if ($estatica == 1) {

        }
    }
    $params = $frm["fu_params_" . $leng];
    inserta_friendly_url($leng, $seccion, $friendly_url, $id_seccion, $noborrar, $params);

    /* --------------------------------------------------------------- title -- */
    if (isset($frm["seoPlugin"]) && $frm["meta_title_" . $leng] != "") {
        $title = $frm["meta_title_" . $leng];
    } else {
        if ($seccion_parents != "") {
            $parents = obtener_parents($parent, $leng, $seccion_parents, $parent_title_field, false, " | ");
        }
        $title = generar_meta_title($frm[$title_field . $leng], $parents, $siteName);
    }
    inserta_metas($leng, $seccion, "title", $title, $id_seccion, $noborrar);

    /* --------------------------------------------------------- description -- */
    if ($frm["meta_description_" . $leng] != "") {
        $description = $frm["meta_description_" . $leng];
    } else {
        $description = extrae_cadena($frm[$description_field . $leng], 155);
    }
    inserta_metas($leng, $seccion, "description", $description, $id_seccion, $noborrar);
}

function gestion_friendly_url_automaticas($idioma, $seccion_parents, $parent, $nombre, $parent_title_field)
{
    $parents = "";
    $file = url_amigable_limpiar($nombre);

    if ($parent > 0) {
        $parents = obtener_parents($parent, $idioma, $seccion_parents, $parent_title_field, true);
    }
    return genera_friendly_url($parents, $file);
}

/* ------------------------------------------------------------------- SEO -- */
if (isset($_POST["seo_action"])) { // si venim per ajax
    $action = $_POST["seo_action"];
    $sec = $_POST["sec"];
    $id = $_POST["id"];
    $lang = $_POST["language"];
    $parent = 0;
    $file = "";
    $fu = "";
    $separador = "/";
    switch ($sec) {
        case "paginas":
            $tabla_parents = "paginas";
            $parent_title_field = "nombre_menu";
            break;
        case "noticias":
        case "noticias_categoria":
            $tabla_parents = "noticias_categoria";
            $parent_title_field = "descripcion";
            break;
        case "productos":
        case "productos_categoria":
            $tabla_parents = "productos_categoria";
            $parent_title_field = "descripcion";
            break;
        case "galeria_categoria":
            $tabla_parents = "galeria_categoria";
            $parent_title_field = "descripcion";
            break;
        case "videos_categoria":
            $tabla_parents = "videos_categoria";
            $parent_title_field = "descripcion";
            break;
    }
    switch ($action) {
        case "genera_friendly_url":
            if (isset($_POST["file"])) {
                $file = $_POST["file"];
            }
            if (isset($_POST["parent"])) {
                $parent = $_POST["parent"];
            }
            $parents = "";
            if ($parent > 0) {
                $parents = obtener_parents($parent, $lang, $tabla_parents, $parent_title_field, true);
            }
            $fu = genera_friendly_url($parents, $file);
            echo validate_friendly_url($fu, $sec, $id);
            break;

        case "validate_friendly_url":
            if (isset($_POST["friendly_url"])) {
                $fu = $_POST["friendly_url"];
            }
            echo validate_friendly_url($fu, $sec, $id);
            break;
        case "obtener_parents":
            if (isset($_POST["separador"])) {
                $separador = $_POST["separador"];
            }
            if (isset($_POST["parent"])) {
                $parent = $_POST["parent"];
            }
            echo obtener_parents($parent, $lang, $tabla_parents, $parent_title_field, false, $separador);
            break;
    }
}