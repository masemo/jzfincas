var init = false;
function seoPlugin_init() { // data-id + "_init"
    if (!init) {
        init = true;
        find_fields("meta_title");
        find_fields("meta_description");
        find_fields("friendly_url");
        resize_fu_input();
    }
}
function find_fields(target) {
    var field = $(".seo_admin p:not(.char_counter) input[id^='" + target + "'], .seo_admin p:not(.char_counter) textarea[id^='" + target + "']");
    field.each(function () {
        var id = $(this).attr("id");
        var counter = $("#" + id + "_length");
        $(this).data("type", target);
        $(this).on("keyup blur", function () {
            char_count($(this), counter);
            if ($(this).val() === "") {
                default_seo_value($(this));
            }
        });
        char_count($(this), counter);
    });
}
function char_count(field, counter) {
    var valueLength = field.val().length;
    counter.val(valueLength);
    var max = parseInt(counter.attr("data-maxchar"));
    validate_length(counter, valueLength, max);
    if (valueLength > 0) {
        updatePreview(field, max);
    }
}
function validate_length(counter, valueLength, max) {
    var margin = 10;
    if (valueLength > (max + (margin / 2)) || valueLength < margin) {
        counter.removeClass().addClass("alert catastrofe");
    }
    if (valueLength > margin && valueLength < (max / 2)) {
        counter.removeClass().addClass("alert notok");
    }
    if (valueLength > (max / 2) && valueLength < max + (margin / 2)) {
        counter.removeClass().addClass("alert ok");
    }
}
function updatePreview(field, max) {
    var substring_margin = 5;
    var content = field.val();
    var id = field.attr("id");
    var target = $("#" + id + "_preview .text");
    if (field.data("type") === "friendly_url") {
        content = clean_url(content);
        valida_friendly_url(field, content);
        substring_margin = 25;
    }
    if (content.length > max) {
        content = content.substring(0, max - substring_margin);
        if (field.data("type") !== "friendly_url") {
            content = content.substr(0, Math.min(content.length, content.lastIndexOf(" "))); // per a no tallar paraules            
        }
        content += " ..."
    }
    target.text(content);
}

function default_seo_value(input) {
    var idioma = current_lang(input);
    var target = $(".lang_group[data-lang='"+idioma+"']");
    switch (input.data("type")) {
        case "meta_title":
            genera_meta_title($(".seo_title", target));
            break;
        case "meta_description":
            $(".seo_description", target).each(function () {
                var content;
                if($(this).hasClass("ckeditor")){
                    var id = $(this).attr("id");
                    var target = CKEDITOR.instances[id];
                    content = target.document.getBody().getText();
                }else{
                    content = $(this).val();
                }                
                genera_meta_description(content, $(this));
            });
            break;
        case "friendly_url":
            genera_friendly_url($(".seo_friendly_url", target));
            break;
    }
}
function params_seccion(seccion, id_seccion){
    var estatica = $("#estatica").val();
    var estatica_sec = $("#estatica_sec").val();
    var params = {
        seccion: seccion,
        id_seccion: id_seccion
    };
    if(estatica === "1"){
        params.seccion = estatica_sec;
        params.id_seccion = 0;
    }
    return params;
}
function valida_friendly_url(field, friendly_url) {
    var language = current_lang(field);
    var params = params_seccion(sec, id);
    $.post("includes/plugins/seo/functions.php", {seo_action: "validate_friendly_url", sec: params.seccion, id: params.id_seccion, language: language, friendly_url: friendly_url}, function (data) {
        var target = $(".seo_admin #friendly_url_" + language);
        if (data["valid"] === 0) {
            invalid_friendly_url(target);
        } else {
            display_friendly_url(friendly_url, field, language)
//            field.closest(".friendly_url_content").removeClass("highlight");
//            field.removeClass("required");
        }
    }, "json");
}
function resize_fu_input() {
    var prefix = $(".fu_prefix");
    var sufix = $(".fu_sufix");
    var prefix_w = prefix.width() + 1;
    sufix.css({
        width: "calc(100% - " + prefix_w + "px)"
    });
}