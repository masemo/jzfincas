<?php
include_once("controller.php");
?>
<link type="text/css" href="<? echo $adminUrl; ?>includes/plugins/seo/style.css" rel="stylesheet" />
<script type="text/javascript">var alert_fu_txt = "<? echo lang("url_existente", $plugin_idioma); ?>";</script>
<script type="text/javascript" src="<? echo $adminUrl; ?>includes/plugins/seo/script.js"></script>
<input type="hidden" name="seoPlugin" value="1" />
<section class="plugin seo_admin" data-id="seoPlugin">
    <header>
        <h3>
            <span class="icon"></span>
            SEO
        </h3>
        <span class="icon toggler"></span>
    </header>
    <div class="content <? echo $pluginState; ?>">
        <ul class="pestanyera">
            <?
            foreach ($lenguajes["activos"] as $lenguaje) {
                $leng = $lenguaje["idioma"];
                ?>
                <li data-lang="<? echo $leng; ?>">
                    <button type="button" class="lang_btn"><? echo $lenguaje["desc"]; ?> (<? echo $leng; ?>)</button>
                </li>
            <? } ?>
        </ul>
        <div class="lang_field_group">
            <? foreach ($lenguajes["activos"] as $lenguaje) {
                $friendly_url_prefix = $siteUrl;
                $leng = $lenguaje["idioma"];
                if (count($lenguajes["activos"]) > 1) {
                    $friendly_url_prefix = $friendly_url_prefix . $leng . "/";
                }
                if (isset($id)) {
                    $obtener_metas_title = obtener_metas($leng, $sec_seo, "title", $id_seo);
                    $obtener_metas_description = obtener_metas($leng, $sec_seo, "description", $id_seo);
                    $obtener_friendly_url = obtener_friendly_url($leng, $sec_seo, $id_seo);
                }
                ?>
                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                    <input type="hidden" name="fu_params_<? echo $leng; ?>" id="fu_params_<? echo $leng; ?>" value="<? echo $fu_params; ?>" />
                    <fieldset>
                        <p>
                            <label for="meta_title_<? echo $leng; ?>"><? echo lang("meta_title_txt", $plugin_idioma); ?></label>
                            <input type="text" id="meta_title_<? echo $leng; ?>" name="meta_title_<? echo $leng; ?>" value="<?php echo $obtener_metas_title; ?>" />
                        </p>
                        <p class="char_counter">
                            <input readonly="readonly" tabindex="-1" type="text" id="meta_title_<? echo $leng; ?>_length" name="meta_title_<? echo $leng; ?>_length" value="0"  data-maxChar="<?php echo $length_title ?>"/>
                            <? echo str_replace("[#]", $length_title, lang("caracteres_uso", $plugin_idioma)); ?>
                        </p>
                        <br />
                        <p>
                            <label for="meta_description_<? echo $leng; ?>"><? echo lang("meta_description_txt", $plugin_idioma); ?></label>
                            <textarea id="meta_description_<? echo $leng; ?>" name="meta_description_<? echo $leng; ?>"><?php echo $obtener_metas_description; ?></textarea>
                        </p>
                        <p class="char_counter">
                            <input readonly="readonly" tabindex="-1" type="text" id="meta_description_<? echo $leng; ?>_length" name="meta_description_<? echo $leng; ?>_length" value="0" data-maxChar="<?php echo $length_descripcion ?>" />
                            <? echo str_replace("[#]", $length_descripcion, lang("caracteres_uso", $plugin_idioma)); ?>
                        </p>
                        <br />
                        <div class="input_group friendly_url_content">
                            <label for="friendly_url_<? echo $leng; ?>_validation"><? echo lang("friendly_url", $plugin_idioma); ?> <span class="fu_invalid hide"><? echo lang("friendly_url_invalid", $plugin_idioma); ?></span></label>
                            <p class="fu_prefix">
                                <span class="text"><? echo $friendly_url_prefix; ?></span>
                            </p
                            ><p class="fu_sufix">
                                <input type="text" class="hidden fu_validation" name="friendly_url_<? echo $leng; ?>_validation" id="friendly_url_<? echo $leng; ?>_validation" value="" />
                                <input  <? echo hide_item($hide_friendly_url,true); ?> type="text" name="friendly_url_<? echo $leng; ?>" id="friendly_url_<? echo $leng; ?>" value="<? echo $obtener_friendly_url; ?>" />
                            </p>
                        </div>
                        <p class="char_counter <? echo hide_item($hide_friendly_url); ?>">
                            <input readonly="readonly" tabindex="-1" type="text" id="friendly_url_<? echo $leng; ?>_length" name="friendly_url_<? echo $leng; ?>_length" value="0" data-maxChar="<?php echo $length_friendly_url ?>" />
                            <? echo str_replace("[#]", $length_friendly_url, lang("caracteres_max", $plugin_idioma)); ?>
                        </p>
                    </fieldset>

                    <div class="preview">
                        <p class="subtle"><? echo lang("previsualizacion_seo", $plugin_idioma); ?>:</p>
                        <div class="content">
                            <div class="result">
                                <h4 class="meta_title_preview" id="meta_title_<? echo $leng; ?>_preview">
                                    <span class="text"></span>
                                </h4>
                                <cite id="friendly_url_<? echo $leng; ?>_preview">
                                    <?php echo $friendly_url_prefix; ?><span class="text"><?php echo $obtener_friendly_url; ?></span>&#9662;
                                </cite>
                                <p class="meta_description_preview" id="meta_description_<? echo $leng; ?>_preview">
                                    <span class="text"></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <?
                unset($lenguaje);
            }
            ?>
        </div>
    </div>
</section>
