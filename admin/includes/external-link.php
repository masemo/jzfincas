<div class="checkbox_group">
    <p class="checkbox">
        <label for="external_link_<? echo $lenguaje["idioma"]; ?>" class="external_link"><span class="icon"></span><? echo lang("paginas_convertir_external_link"); ?></label>
        <input class="checkbox" type="checkbox" name="external_link_<? echo $lenguaje["idioma"]; ?>" id="external_link_<? echo $lenguaje["idioma"]; ?>" <?
        if (isset($fila) && $fila['external_link'] > 0) {
            echo "checked='checked'";
        }
        ?>/>
    </p>
</div>
<p data-eLink="enable">
    <label for="external_link_url_<? echo $lenguaje["idioma"]; ?>"><? echo lang("paginas_external_link"); ?> (url)</label>
    <input type="text" name="external_link_url_<? echo $lenguaje["idioma"]; ?>" id="external_link_url_<? echo $lenguaje["idioma"]; ?>" value="<?
    if ($sub == "editar") {
        echo obtener_valor($lenguaje["idioma"], $sec, "external_link_url", $id);
    }
    ?>" <? if ($estatica) { ?>readonly="readonly"<? } ?> placeholder="http://www.example.com"/>
</p>