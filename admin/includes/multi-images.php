<?
if($sec == "multimedia"){
    $label_title =  lang("images");
}else{
    $label_title =  lang("imagenes");
}
?>
<label for="imagenes"><? echo $label_title; ?></label>
<div class="multi_images">
    <?
    if ($sub == "editar" || $sub == "editar-articulo") {
        $sql_imagenes = "SELECT * FROM imagenes WHERE idsec = $id AND sec = '$dataSec' ORDER BY orden ASC, ID ASC";
        $consulta_imagenes = mysqli_query($link, $sql_imagenes);

        $instructions = array("move-img", "img-alt");
        instructions($instructions);
        ?>
        <p class="button_group">
            <button type="button" id="multiFileBrowseImage" class="multiFileBrowse" data-target="imagen" data-sec="<? echo $dataSec; ?>" data-id="<? echo $id; ?>"><? echo lang("anyadir_imagenes"); ?></button>
            <button type="button" class="secondary_btn show_item" data-transition-target="#multi-images .content"><? echo lang("mostrar_opciones_titulo"); ?></button>
            <button type="button" class="secondary_btn hide_item hide" data-transition-target="#multi-images .content"><? echo lang("ocultar_opciones_titulo"); ?></button>
        </p>
        <ul id="multi-images">
            <?
            $multi_images_item = array();
            while ($fila_imagenes = mysqli_fetch_array($consulta_imagenes)) {
                $aux = array();
                $aux["id"] = $fila_imagenes['id'];
                $aux["imagen"] = $adminUrl."lib/timThumb/timThumb.php?src=".pintar_imagen($fila_imagenes['fichero'], true) . $timThumbBigGalleryParams;
                $aux["sub"] = $sub;
                $multi_images_item[] = $aux;
            }
            $pestanyeraCondensed = true;
            include("multi-images-item.php");
            ?>
        </ul>
    <? } else { ?>
        <p class="alert notok">
            <? echo lang("guardar_para_multi_imagen"); ?>
        </p>
    <? } ?>
</div>