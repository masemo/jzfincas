<ul class="pestanyera">
    <? foreach ($lenguajes["activos"] as $lenguaje) { 
        if(count($lenguajes["activos"] > 2) && isset($pestanyeraCondensed) && $pestanyeraCondensed == true){
            $lang_desc = $lenguaje["idioma"];
        }else{
            $lang_desc = $lenguaje["desc"]. " (" .$lenguaje["idioma"].")";
        }
        ?>
        <li data-lang="<? echo $lenguaje["idioma"]; ?>">
            <button type="button" class="lang_btn"><? echo $lang_desc; ?></button>
        </li>
    <? } ?>
</ul>