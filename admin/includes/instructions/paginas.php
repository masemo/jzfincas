<?
global $sub;
if (isset($sub) && ($sub == "anyadir" || $sub == "editar")) { ?>
    <li>
        <p class="activo">
            <span class="icon"></span>
            <span><? echo lang("activo_txt"); ?></span>
        </p>
    </li>
<? } ?>
<? if (empty($sub)) { ?>
    <li>
        <p class="move">
            <span class="icon"></span>
            <span><? echo lang("move_txt"); ?></span>
        </p>
    </li>
<? } ?>
<li>
    <p class="nodo">
        <span class="icon"></span>
        <span><? echo lang("sin_enlace_txt"); ?></span>
    </p>
</li>
<li>
    <p class="oculto">
        <span class="icon"></span>
        <span><? echo lang("oculto_txt"); ?></span>
    </p>
</li>
<? if (empty($sub)) { ?>
    <li>
        <p class="no_activo">
            <span class="icon"></span>
            <span><? echo lang("no_activo_txt"); ?></span>
        </p>
    </li>
    <li>
        <p class="external_link">
            <span class="icon"></span>
            <span><? echo lang("external_link_txt"); ?></span>
        </p>
    </li>
    <li>
        <p class="static_link">
            <span class="icon"></span>
            <span><? echo lang("static_link_txt"); ?></span>
        </p>
    </li>
<? } ?>