<h1><? echo $sec_title; ?></h1>
<?
if ($addLink != "") {
    if (!isset($addTxt)) {
        $addTxt = lang("anyadir_nuevo");
    }
    ?>
    <div class="add_group">
        <a href="<? echo $addLink; ?>" class="button anyadir" data-text="<? echo $addTxt; ?>"><? echo $addTxt; ?></a>
    </div>
<? } ?>
<? if (isset($search_term) && $search_term != "") { ?>
    <div class="subtle search_term_group">
        <p>
            <span class="icon"></span>
            <? echo lang("termino_busqueda"); ?> <span class="search_term"><? echo $search_term; ?></span>
            <a href="" class="x_delete" title="<? echo lang("eliminar_filtro"); ?>">X</a>
        </p>
    </div>
<? } ?>
<? 
if ( isset($nItems) && !isset($breadcrumbs_home) ) { ?>
    <p class="subtle">
        <? echo lang("total"); ?> (<? echo $nItems; ?>)
    </p>
<? } ?>
<? if (isset($sec) && $sec != "paginas" && (!isset($hideSearch) || $hideSearch == true)) { ?>    
    <div class="search_box">
        <form id="search_box_form" action="index.php?sec=<? echo $search_action; ?>" method="post">
            <p>
                <input type="text" name="search_term" value="" />
            </p>
        </form>
    </div>
<? } ?>