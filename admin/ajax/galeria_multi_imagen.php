<?php
include("../system/vars.php"); //variables del sistema 
include("../system/conexiondb.php"); //conexión a la base de datos
include("../system/config.php"); //configuración del entorno
include("../funciones/funciones.php"); //configuración del entorno

$action = $_POST["action"];

switch ($action) {
    case "save":
        $id = $_POST["id"];
        $lastOrden = $_POST["lastOrden"];
        $sec = $_POST["sec"];
        $allFiles = $_POST["allFiles"];


        $itemValues = "";
        foreach ($allFiles as $file) {
            $lastOrden++;
            $fileUrl = str_replace($siteUrl . $uploadUrl, "", $file["url"]);
            $itemValues .= "($id, '$fileUrl', 1, '$sec',$lastOrden),";
        }
        $values = trim($itemValues, ",");
        $sql = "INSERT INTO imagenes (idsec,fichero,activo,sec,orden) VALUES " . $values;

        if (mysqli_query($link, $sql)) {
            $insertedId = mysqli_insert_id($link);

            $imagesId = array();
            foreach ($allFiles as $file) {
                $relative_url = str_replace($uploadUrl, "", $file["url"]);
                $aux = array();
                $aux["id"] = $insertedId++;
                $aux["imagen"] = $adminUrl."lib/timThumb/timThumb.php?src=".$uploadUrl.$relative_url.$timThumbBigGalleryParams;
                $imagesId[] = $aux;
            }
            echo json_encode($imagesId);
        } else {
            echo "notok";
        }
        break;
    case "img_alt":
        $lenguajes = carga_idioma();
        $formData = $_POST["formData"];
        foreach ($lenguajes["activos"] as $lenguaje) {
            $leng = $lenguaje["idioma"];
            $alt_text = $formData["img_alt_" . $leng];
            $id = $formData["img_id"];
            inserta_campo_idiomas($leng, "imagen", "img_alt", $alt_text, $id);
        }
        break;
}
