<?php
/*-- carga idiomas --*/
function carga_idioma() {
    global $link;

    $array_lenguajes = array();
    $array_lenguajes["activos"] = array();

    $sql_config_idiomas = "SELECT * FROM config_idiomas WHERE activo = 1 ORDER BY orden ASC";
    $consulta_config_idiomas = mysqli_query($link, $sql_config_idiomas);

    $i = 0;
    while ($fila_config_idiomas = mysqli_fetch_array($consulta_config_idiomas)) {
        $idioma_activo = array();
        $idioma_activo["idioma"] = $fila_config_idiomas['idioma'];
        $idioma_activo["desc"] = $fila_config_idiomas['descripcion'];
        $array_lenguajes["activos"][] = $idioma_activo;

        if ($fila_config_idiomas["defecto"] > 0) {
            $array_lenguajes["defecto"] = $idioma_activo;
        }
        $i++;
    }

    return $array_lenguajes;
}

/*-- moduls contractats --*/
function cargar_modulos_contratados() {
    global $link;

    $modulos = array();
    $sql = "SELECT id, sec FROM config_secciones_public WHERE activo = 1";
    //echo $sql;
    $consulta = mysqli_query($link, $sql);
    while ($fila = mysqli_fetch_array($consulta)) {
        $modulos[$fila['id']] = true;
    }
    return $modulos;
}

function modul_contractat($modul) {
    global $moduls_contractats; //config_public.php
    //print_r($moduls_contractats);

    if (array_key_exists($modul, $moduls_contractats)) {
        if ($moduls_contractats[$modul]) {
            return true;
        }
    }
    return false;
}

function getHTML($path, $url, $relative = 1) {
    $handler = curl_init();
    curl_setopt($handler, CURLOPT_FORBID_REUSE, true); //"TRUE to force the connection to explicitly close when it has finished processing, and not be pooled for reuse."
    curl_setopt($handler, CURLOPT_NETRC, TRUE); //TRUE to scan the ~/.netrc file to find a username and password for the remote site that a connection is being established with.

    if ($relative == 1) {
        curl_setopt($handler, CURLOPT_URL, $path . $url);
    } else {
        curl_setopt($handler, CURLOPT_URL, $url);
    }

    $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
    session_write_close();
    curl_setopt($handler, CURLOPT_COOKIE, $strCookie);


    curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($handler);
    curl_close($handler);
    return $result;
}

function fecha_caducada($fecha, $hoy = "") {
    if($fecha != "0000-00-00") {
        if ($hoy == "") {
            $hoy = date("Y-m-d");
        }
        $hoy = strtotime($hoy);
        $fecha_caduca = strtotime($fecha);
        return $hoy >= $fecha_caduca;
    }
    return false;
}
function basic_query($field, $table, $id) {
    global $link;

    $query = "SELECT $field AS valor FROM $table WHERE id = $id";
    $consulta = mysqli_query($link, $query);
    $total = mysqli_num_rows($consulta);
    if ($total) {
        $row = mysqli_fetch_array($consulta);
        return $row["valor"];
    }
    return false;
}

function row_query($field, $table, $clause) {
    global $link;
    $query = "SELECT $field FROM $table WHERE $clause";
//    echo $query;
    $result = mysqli_query($link, $query);
    if ($result) {
        $nItems = mysqli_num_rows($result);
        if ($nItems > 0) {
            $row = mysqli_fetch_assoc($result);
            return $row;
        }
    }
    return false;
}
