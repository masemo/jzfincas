<?php
function sendmailAUTH($to,$toname,$sender,$sendername,$subject,$body,$smtpHost,$smtpUser,$smtpPass){

	$mail = new PHPMailer();

	$mail->IsSMTP();

	$mail->Host = $smtpHost;
	$mail->Port = 25;

	$mail->From = $sender;
	$mail->FromName = $sendername;

	$mail->Subject = $subject;

	//$mail->AltBody = "prueba";

	$mail->IsHTML(true);
	$mail->MsgHTML($body);

	$mail->AddAddress($to,$toname);
	//$mail->AddAddress("jmarza@sollutia.com","Jose"); //pruebas

	$mail->SMTPAuth = true;

	$mail->Username = $smtpUser;
	$mail->Password = $smtpPass;

	if(!$mail->Send()) {
		return $mail->ErrorInfo;
	} else {
		return"ok";
	}

}
?>
