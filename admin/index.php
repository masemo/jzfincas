<?
session_start();
include("system/vars.php"); //variables del sistema 
include("system/conexiondb.php"); //conexión a la base de datos
include("system/config.php"); //configuración del entorno
include("lib/funciones.php"); //funciones de libreria
include("funciones/funciones.php");
include("funciones/url.php"); //generador de url amigables
include("common/functions.php"); //funciones que se comparten con la parte publica
include("../functions/main.php");
include("system/preload.php"); //pre-carga de variables
include("funciones/lang.php"); //gestión de lenguajes
include("funciones/navigation.php"); //generador del menú
include("funciones/logged.php"); //test logeado
include("funciones/secciones.php");
include_once("includes/plugins/seo/functions.php");
include(seccion_activa($sec, "funciones"));
include(seccion_activa($sec, "controller"));
?>
<!DOCTYPE html>
<html lang="<? echo $lang_default; ?>">
    <head>
        <? include($adminThemeIncludeUrl . "template/head.php"); ?>
        <? include("includes/template/head.php"); ?>
    </head>
    <body>
        <div id="wrapper" class="<? toggle_element($toggle_nav_cookie); ?>">
            <? include($adminThemeIncludeUrl . "template/header.php"); ?>
            <? include($adminThemeIncludeUrl . "template/nav.php"); ?>
            <main id="main_section">
                <? // include(seccion_activa($sec)); ?>          
                <? include($include); ?>          
            </main>
            <? include("includes/template/popup.php"); ?>
            <? include($adminThemeIncludeUrl . "template/footer.php"); ?>
        </div>
        <? include($adminThemeIncludeUrl . "template/javascript.php"); ?>
        <? include("includes/template/javascript.php"); ?>
    </body>
</html>