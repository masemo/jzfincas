<header id="main_header">
    <div class="wrap">
        <div class="logo">
            <a href="<? echo $adminUrl; ?>index.php?sec=defecto" rel="home">
                <img src="<? echo $adminThemeUrl; ?>images/logo.png" alt="<? echo $adminName; ?>" title="<? echo lang("inicio", $lang_default); ?>" />
            </a>
        </div>
        <div class="sitename">
            <p><? echo $adminName; ?></p>
        </div>
        <? if ($logged_in) { ?>
            <div class="user_options">
                <span class="icon"></span>
                <div class="options">
                    <a href="<? echo $adminUrl; ?>index.php?sec=admins&sub=editar&id=<? echo $_SESSION["sessionId"]; ?>">
                        <? echo $_SESSION["sessionNombre"]; ?>
                    </a> | 
                    <a href="index.php?sec=logout">
                        <? echo lang("cerrar_sesion"); ?>
                    </a>
                </div>
            </div>
        <? } ?>
    </div>
</header>