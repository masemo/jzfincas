<?
$privileges = get_array_privileges($_SESSION["sessionId"]);
?>
<nav id="main_nav">
    <div class="wrap">
        <div class="fixable">
            <p class="launcher">
                <? echo lang("menu"); ?>
                <span class="icon"></span>
            </p>
            <? echo genera_menu(0, $sec, $privileges, $_SESSION["sessionId"]); ?>
            <div class="nav_options">
                <div title="<? echo lang("ocultar_menu"); ?>" class="ocultar_menu">
                    <span class="icon ocultar"></span>
                    <? echo lang("ocultar_menu"); ?>
                </div>
                <div title="<? echo lang("mostrar_menu"); ?>" class="mostrar_menu">
                    <span class="icon mostrar"></span>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="nav_bg"></div>
