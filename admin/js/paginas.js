/*------------------------------------------------------------- páginas --*/
function calcula_level(level_now) {
    var level;
    level = level_now / currentGrid;
    return level;
}

function buscar_parent(item, level) {
    var parent_data = [];
    if (parseInt(level) > 0) {
        var parent = item.prev();
        if (parent.length > 0) {
            var parent_level = parseInt(parent.attr("data-level"));
            if (level <= parent_level) {
                return buscar_parent(parent, level);
            } else {
                parent_data["level"] = parent_level;
                parent_data["level-old"] = parseInt(parent.attr("data-level-old"));
                parent_data["id"] = parseInt(parent.attr("data-id"));
                return parent_data;
            }
        }
    }
    parent_data["level"] = 0;
    parent_data["level-old"] = 0;
    parent_data["id"] = 0;
    return parent_data;
}
function update_item_position(item, level, parent_data) {
    var parent_level = parseInt(parent_data["level"]);
    var parent_id = parent_data["id"];
    move_to_level(item, level, parent_level);
    update_parent(item, parent_id);
}
function move_to_level(item, level, level_parent) {
    if (level < 0) { //evita que se salga por delante
        level = 0;
    }
    if (level > (level_parent + 1)) { //i que s'ascaparre per raere
        level = level_parent + 1;
    }
    update_level(item, level);
    move_to_level_position(item, level);
}
function move_to_level_position(item, level) {
    var distance = currentGrid * level;
    item.css({
        left: distance
    });
}
function update_level(item, level) {
    var level_old = item.attr("data-level");
    item.attr({
        "data-level": level,
        "data-level-old": level_old
    });
}
function update_parent(target, parent) {
    var parent_old = target.attr("data-parent");
    target.attr({
        "data-parent": parent,
        "data-parent-old": parent_old
    });
}
function arrastra_subemu(item, huerfano) {
    var dataLevel = "data-level";
    if (huerfano) {
        dataLevel = "data-level-old";
    }
    var selected = $(" ~ li:not(.placeholder)", item);
    var parentLevel = parseInt(item.attr(dataLevel));
    if (selected.length > 0) {
        item.append("<ul></ul>");
        var targetUl = $("ul", item);
        selected.each(function () {
            var level = parseInt($(this).attr(dataLevel));
            var distance = ((currentGrid * level)) - ((parentLevel * currentGrid) + currentGrid);
            if (level > parentLevel) {
                $(this).appendTo(targetUl).css({
                    left: distance
                });
            } else {
                return false;
            }
        });
    }
}
function solta_subemu(item) {
    var subemu_items = $("li", item);
    if (subemu_items.length > 0) {
        var inserted = item;
        subemu_items.each(function () {
            $(this).insertAfter(inserted);
            var level = parseInt($(this).attr("data-level"));
            var parent_id = $(this).attr("data-parent");
            var parent_level_old = parseInt($("li[data-id='" + parent_id + "']").attr("data-level-old"));
            var parent_level = parseInt($("li[data-id='" + parent_id + "']").attr("data-level"));
            var diferencia = parseInt(level - parent_level_old);
            var parent_data_aux = [];
            parent_data_aux["level"] = parent_level;
            parent_data_aux["id"] = parent_id;
            update_item_position($(this), parseInt(parent_level + diferencia), parent_data_aux);
            inserted = $(this);
        });
    }
    $("ul", item).remove();
}
function find_next_valid_item(item, level) {
    var next_item = item.next(":not(.placeholder)");
    if (next_item.length > 0) {
        if (parseInt(next_item.attr("data-level")) > parseInt(level)) {
            return find_next_valid_item(next_item, level);
        } else {
            return next_item;
        }
    } else {
        return false;
    }
}

function canvi_de_pare(huerfano, item_data) {
    $.post("secciones/paginas/ajax/functions.php", {action: "guardar_parent", id: item_data.attr("data-id"), parent: item_data.attr("data-parent")}, function (data) {
        if (data === "notok") {
            alert("error [guardar_parent]");
        }
    });
    update_parent(huerfano, item_data.attr("data-parent"));
}
function moveTo_next_valid_item(item, target) {
    if (!target) {
        target = $("#paginas_contenido li").last();
        insert_moveTo(item, target, "after");
    } else {
        insert_moveTo(item, target, "before");
    }
    return false;
}
function start_sortable() {
    var parentUl;
    var sortableUl = $("#paginas_contenido");
    sortableUl.height(sortableUl.height());
    sortableUl.sortable({
        placeholder: "placeholder",
        grid: [currentGrid, 1],
        start: function (event, ui) {
            ui.item.startPos = ui.item.index();
            arrastra_subemu(ui.item, false);
            totalItemsSubemu = $("li", ui.item).length;
        },
        sort: function (event, ui) {
            ui.placeholder.css({
                left: ui.position.left,
                height: (totalItemsSubemu * 45) + 33
            });
            parentUl = ui.placeholder.closest("ul");
            if (!parentUl.is("#paginas_contenido")) {
                ui.placeholder.appendTo(sortableUl);
            }
        },
        beforeStop: function (event, ui) {
            //
        },
        stop: function (event, ui) {
            var level = calcula_level(ui.position.left); //obtengo la cantidad de movimientos izq, der
            var parent_data = buscar_parent(ui.item, level);
            update_item_position(ui.item, level, parent_data);

            var next_valid_item = find_next_valid_item(ui.item, level);
            if (next_valid_item) {

                var next_item_id = ui.item.next(":not(.placeholder)").attr("data-id");
                var next_valid_item_id = next_valid_item.attr("data-id");

                if (next_item_id !== next_valid_item_id) {
                    moveTo_next_valid_item(ui.item, next_valid_item);
                }
            }
            solta_subemu(ui.item);

            var sql = "";
            $("#paginas_contenido li").each(function (index) {
                var item_data = $(this);
                var orden = index + 1;

                //canvi de pare
                if (item_data.attr("data-parent") !== item_data.attr("data-parent-old")) {
                    canvi_de_pare($(this), item_data);
                }

                sql += "(" + item_data.attr("data-id") + "," + orden + "),";
            });
            //orden
            sql = sql.substring(0, sql.length - 1);
            $.post("secciones/paginas/ajax/functions.php", {action: "guardar_orden", query: sql}, function (data) {
                if (data === "notok") {
                    alert("error [guardar_orden]");
                }
            });
            // esto no deberia ser asin...
            // Y no lo és! Zasca! Soy el homer ma-lo!
//            load_paginas();
        }
    }).disableSelection();
}
function insert_moveTo(item, target, where) {
    var top = item.position().top;
    var left = item.position().left;
    var width = item.width();
    if (where === "before") {
        item.insertBefore(target);
    }
    if (where === "after") {
        item.insertAfter(target);
    }
    var top_target = item.position().top;
    var left_target = item.position().left;
    var speed = 500;
    if (top === top_target) {
        speed = 0;
    }
    var clone = item.clone();
    item.css({
        opacity: 0
    });
    if ($("li", clone).length === 0) {
        $("ul", clone).remove();
        $("ul", item).remove();
    }
    clone.css({
        position: "absolute",
        top: top,
        left: left,
        width: width,
        opacity: .5
    }).insertAfter(item).animate({
        top: top_target,
        left: left_target
    }, speed, function () {
        $(this).fadeOut(500, function () {
            $(this).remove();
        });
        item.animate({
            opacity: 1
        }, speed);
        solta_subemu(item);
    });

}
function find_lower_level(from, level) {
    if (level > 0) {
        var target = $(" ~ li[data-level='" + level + "']", from).first();
        if (target.length === 0) {
            //console.log("no hay: " + level);
            find_lower_level(from, (level - 1));
        } else {
            //console.log("SÍ hay: " + level);
            return level;
        }
    }
    return 0;
}
function load_paginas() {
    $("#generador_paginas").load("secciones/paginas/ajax/genera_paginas.php", function () {
        start_sortable();
        paginas_level_adjust();
        delete_item();
        url_link();
        $(this).css({background: "none"});
    });
}
function delete_item() {
    $(".delete_confirm").off().on("click", function () {
        $(this).off();
        var id = parseInt($(this).attr("data-id"));
        var target = $(this).closest("li");
        if (window.confirm(confirm_txt) === false) {
            delete_item();
            return false;
        } else {
            $.post("secciones/paginas/ajax/functions.php", {action: "eliminar_paginas", id: id},
                function (data) {
                    if (data === "notok") {
                        alert(paginas_borrar_error);
                        delete_item();
                        return false;
                    } else {
                        $("a", target).off();
                        var item_height = target.height();
                        target.css({
                            overflow: "hidden",
                            minHeight: 0,
                            height: item_height,
                            transition: "none"
                        })
                            .animate({
                                opacity: 0,
                                height: 0
                            }, 1000, function () {
                                $(this).remove();
                            });
                    }
                });
        }
    });
}
function url_link() {
    $("#paginas_contenido li .oculto").on("click", function () {
        var id = $(this).parents("li").attr("data-id");
        $.post("secciones/paginas/ajax/functions.php", {action: "get_url", id: id},
            function (data) {
                if (data === "notok") {
                    alert("follón...");
                    return false;
                } else {
                    pop_it_up(data, paginas_ocultas_txt);
                }
            });
    });
}
function paginas_level_adjust() {
    if (is_mobile()) {
        currentGrid = parseInt(grid) / 3;
    } else {
        currentGrid = grid;
    }
    start_sortable();
    $("#paginas_contenido li").each(function () {
        var level = $(this).attr("data-level");
        move_to_level_position($(this), level);
    });
}
var totalItemsSubemu = 0;
var resizeTimer;
var currentGrid = grid;
$(document).ready(function () {
    if (exists("#generador_paginas")) {
        load_paginas();
        $(window).on("resize", function () {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function () {
                paginas_level_adjust();
            }, 250);
        });
    }
});