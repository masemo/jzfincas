/*------------------------------------------------------------------ popper --*/
function pop_it_up(content, title) {
    var active_popup = display_popup(false, false);
    $(".content", active_popup).html(content);
    $("h4", active_popup).html(title).show();
}
function display_popup(loadFile, jsonParams) {
//    console.log(jsonParams);
    var last_popup = $(".popup").last();
    var clone = $(".popup.dolly").clone().removeClass("dolly");
//    $(".popup").removeClass("active");
    clone.insertAfter(last_popup).addClass("active").fadeIn(200);
    hide_popup(clone);
    if (loadFile !== "") {
        $(".content", clone).load(loadFile, {json: jsonParams});
    }
    return clone;
}
function hide_popup(active_popup) {
    active_popup.on('click', function (e) {
        if (e.target === this) {
            pop_it_down(active_popup);
            return;
        }
    });
    $(".close", active_popup).on("click", function (event) {
        event.preventDefault();
        pop_it_down(active_popup);
    });
}
function pop_it_down(active_popup) {
    active_popup.fadeOut(200, function () {
        $(this).remove();
    });
}
/*------------------------------------------------------------------ legend --*/
function toggle_legend(target) {
    var opened = $("ul", target).css("display");
    if (opened === "none") {
        $("ul", target).slideDown(200);
        $(".info", target).fadeOut(200, function () {
            target.removeClass("closed");
        });
        createCookie(toggle_legend_cookie, 0, 120);
    } else {
        close_legend(target);
    }
}
function close_legend(legend) {
    createCookie(toggle_legend_cookie, 1, 120);
    $("ul", legend).slideUp(200);
    $(".info", legend).fadeIn(200, function () {
        legend.addClass("closed");
    });
}
/*---------------------------------------------------------------- main nav --*/
function toggle_menu() {
    var closed = $("#wrapper").hasClass("closed");
    if (!closed) {
        collapse_menu();
    } else {
        open_menu();
    }
}
function open_menu() {
    $("#wrapper").removeClass("closed");
    createCookie(toggle_nav_cookie, 0, 120);
}
function collapse_menu() {
    $("#wrapper").addClass("closed");
    createCookie(toggle_nav_cookie, 1, 120);
}
/*----------------------------------------------------------------- cookies --*/
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

/*------------------------------------------------------------- tablesorter --*/
function tablesorter_header_setup() {
    $(".tablesorter thead tr th:not(:last-child)")
        .append("<img src='" + adminThemeUrl + "images/icons/asc.gif' class='sorticons down' alt='' />")
        .append("<img src='" + adminThemeUrl + "images/icons/desc.gif' class='sorticons up' alt='' />")
        .append("<img src='" + adminThemeUrl + "images/icons/bg.gif' class='sorticons sort' alt='' />");
}


/*---------------------------------------------------------- delete confirm --*/
function delete_confirm(delete_btn) {
    var row = delete_btn.parent().parent();
    var name = $("td[data-field='title']", row).text();
    var text = delete_btn.prop("title") + ":\n'" + name.trim() + "'\n\n" + confirm_txt;
    var href = delete_btn.prop("href");
    if (window.confirm(text)) {
        window.location = href;
    }
}


/*------------------------------------------------------------ edit options --*/
function check_window_scroll() {
    var target = $("#wrapper");
    var scrollLimit = 90;
    if (is_mobile()) {
        scrollLimit = 110;
    }
    if ($(this).scrollTop() > scrollLimit) {
        target.addClass("fixed");
    } else {
        target.removeClass("fixed");
    }
}

/*----------------------------------------------------------------- preview --*/
function previsualizar(lang) {
    var formData = $("#edit_form").serializeArray();
    var url = $(".button.preview").attr("href");
    submit_post_via_hidden_form(url, formData, lang);
}
function submit_post_via_hidden_form(url, params, lang) {
    var f = $("<form target='_blank' method='POST' style='display:none;'></form>").attr({
        action: url
    }).appendTo(document.body);

    for (var i in params) {
        if (params.hasOwnProperty(i)) {
            var inputName = params[i]["name"];
            var inputValue = params[i]["value"]
            if ($("*[name='" + inputName + "']").hasClass("ckeditor")) {
                inputValue = CKEDITOR.instances[inputName].getData();
            }
            $('<input type="hidden" />').attr({
                name: inputName,
                value: inputValue
            }).appendTo(f);
        }
    }
    $('<input type="hidden" />').attr({
        name: "previewLang",
        value: lang
    }).appendTo(f);

    f.submit();

    f.remove();
}

/*-------------------------------------------------------- relojito portada --*/
function liveclock() {
    var daysweek = new Array("domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado");
    var months = new Array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

    if (!document.layers && !document.all && !document.getElementById)
        return;

    var Digital = new Date();
    var hours = Digital.getHours();
    var minutes = Digital.getMinutes();
    var seconds = Digital.getSeconds();

    var dn = "PM";
    if (hours < 12)
        dn = "AM";
    if (hours > 12)
        hours = hours - 12;
    if (hours == 0)
        hours = 12;

    if (minutes <= 9)
        minutes = "0" + minutes;
    if (seconds <= 9)
        seconds = "0" + seconds;

    myclock = "<font size='5' face='Arial'><strong>" + hours + ":" + minutes + ":" + seconds + " " + dn + "</strong></font><br/>" + daysweek[Digital.getDay()] + ", " + Digital.getDate() + " de " + months[Digital.getMonth()] + " " + Digital.getFullYear();

    if (document.layers) {
        document.layers.liveclock.document.write(myclock);
        document.layers.liveclock.document.close();
    }
    else if (document.all)
        liveclock.innerHTML = myclock;
    else if (document.getElementById)
        document.getElementById("liveclock").innerHTML = myclock;

    setTimeout("liveclock()", 1000);
}

/*------------------------------------------------------------------- forms --*/
function activate_form_submit() {
    $("form.fran6validate").on("submit", function (event) {
        event.preventDefault();
        $().fran6validate($(this), "submit");
    });
}
function fortaleza_pass(password) {

    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{6,}).*", "g");

    if (password !== "") {
        if (false === enoughRegex.test(password)) {
            fortaleza = 'débil';
        } else if (strongRegex.test(password)) {
            fortaleza = 'fuerte';
        } else if (mediumRegex.test(password)) {
            fortaleza = 'media';
        } else {
            fortaleza = 'débil';
        }
        $(".passstrength").fadeIn(200);
    } else {
        fortaleza = 'vacio';
    }

    return fortaleza;
}
function disable_area(target) {
    var disabler = $("<span class='disabler'></span>");
    disabler.appendTo(target);
}
function enable_area(target) {
    $(".disabler", target).remove();
}
function external_link_setup(link) {
    if (link.prop("checked")) {
        $("input[type='checkbox'][id^='external_link']").prop("checked", true);
        $("#external_link").val("1");
        disable_area($("*[data-eLink='disable']"));
        enable_area($("*[data-eLink='enable']"));
        $("input[id^='nombre']").removeClass("required");
    } else {
        $("input[type='checkbox'][id^='external_link']").prop("checked", false);
        $("#external_link").val("0");
        disable_area($("*[data-eLink='enable']"));
        enable_area($("*[data-eLink='disable']"));
        $(".lang_field_group fieldset:first input[id^='nombre']").addClass("required");
    }
}
/*------------------------------------------------------ status label alert --*/
function set_status_label(target) {
    var label = '<div class="status_label">&nbsp;</div>';
    $(label)
        .insertAfter(target)
        .addClass("loading")
        .css({
            top: target.position().top,
            left: target.position().left
        })
        .animate({
            opacity: 1
        }, 100);
}
function update_status_label(status, target) {
    var status_txt = "?";
    switch (status) {
        case "ok":
            status_txt = "&#10004;";
            break;
        case "notok":
            status_txt = "X";
            break;
    }
    var label = target.next(".status_label");
    $(label)
        .removeClass("loading")
        .addClass(status)
        .html(status_txt)
        .animate({
            marginTop: -20,
            opacity: 0
        }, 1000, function () {
            $(this).remove();
        });
}
/*------------------------------------------------------------------ activo --*/
function activo_toggle_item(checkbox, activo, tabla, id) {
    set_status_label(checkbox);
    $.post("ajax/activo_toggle.php", {tabla: tabla, activo: activo, id: id}, function (data) {
        setTimeout(function () {
            update_status_label(data, checkbox);
        }, 500);
        if (data === "notok") { // resetejem el checkbox per si peta la consulta
            if (activo === "true") {
                checkbox.prop("checked", false);
            } else {
                checkbox.prop("checked", true);
            }
        }
    });
}
/*------------------------------------------------------------------- orden --*/
function guardar_orden(tabla) {
    $("input[name^='orden-']").each(function () {
        if (!/^([0-9])*$/.test($(this).val())) {
            alert('Valores no validos en el orden.')
            return;
        } else {
            orden = $(this).val();
            id = $(this).attr("name").split('-');
            id = id[1];
            $.post("lib/guardar_orden/guardar_orden.php", {tabla: tabla, orden: orden, id: id}, function (data) {
                //alert(data);
            });
        }
    }).promise().done(function () {
        alert("Orden guardado");
    });
}

function ordenar_multi_images() {
    var sql = "";
    $("#multi-images li.image-item").each(function (index) {
        var id = $(this).attr("data-id");
        var orden = index + 1;
        sql += "(" + id + "," + orden + "),";
    });
    //orden
    sql = sql.substring(0, sql.length - 1);
    $.post("secciones/multimedia/ajax/functions.php", {action: "guardar_orden", query: sql}, function (data) {
        if (data === "notok") {
            alert("error [guardar_orden]");
        }
    });
}

function fixWidthHelper(e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
}
function updateTrPos(tbody) {
    $("tr", tbody).each(function (index) {
        $("td:first", this).text((index + 1) + ".");
    });
}
function ordenar_filas(tbody) {
    var sql = "";
    var tableDB = tbody.attr("data-tableDB");
    $("tr", tbody).each(function (index) {
        var id = $(this).attr("data-id");
        var orden = index + 1;
        sql += "(" + id + "," + orden + "),";
    });
    sql = sql.substring(0, sql.length - 1);
    $.post("ajax/guardar_orden.php", {tableDB: tableDB, query: sql}, function (data) {
        if (data === "notok") {
            alert("error [guardar_orden]");
        } else {
            updateTrPos(tbody);
        }
    });
}

/*------------------------------------------------------------------ lang --*/
function activar_pestanyera() {
    $(".pestanyera li:not(.active) button").off().on("click", function () {
        var idioma = $(this).parent().attr("data-lang");
        toggle_lang(idioma);
    });
}
function toggle_lang(lang) {
//    console.log(lang);
    $(".pestanyera li").removeClass("active");
    $(".pestanyera li[data-lang='" + lang + "']").addClass("active");
    $(".lang_group").hide();
    $(".lang_group[data-lang='" + lang + "']").fadeIn(150);

    var preview_url = $("#preview_url_" + lang).val();

    $(".edit_options .preview .preview_lang").text(lang);
    $(".edit_options .preview").attr("href", preview_url);
}

/*------------------------------------------------------------------ images --*/
function activar_borrar_imagen() {
    $(".btn_borrar_img").off().on("click", function () {
        var item = $(this).closest(".image-item");
        var id = item.attr('data-id');
        $.post("secciones/multimedia/ajax/functions.php", {action: "borrar_imagen", id: id});
        item.hide(500, function () {
            $(this).remove();
        });
    });
}
function activar_multi_imagen_opciones() {
    $("#multi-images figure").off().on("click", function () {
        $(this).next(".content").slideToggle(200);
    });
    $(".img_alt_form button.save_form_area").off().on("click", function (event) {
        event.preventDefault();
        var form = $(this).closest(".form_area");
        img_alt_text($(this), form);
    });
}

function display_single_thumbnail(cleanUrl, filebrowser) {
    var thumbParams = "&w=240&h=240&q=60&zc=1";
    if (sec === "banners") {
        thumbParams = "&w=240&h=240&q=60&zc=2";
    }
    $(".img_group.thumb", target).remove();
    var target = $("figure", filebrowser);
    var img_group = $(".img_group", filebrowser).clone().addClass("thumb").removeClass("hidden");
    var src = siteUrl + "admin/lib/timThumb/timThumb.php?src=/upload/" + cleanUrl + thumbParams;
    var thumb = $(".imgprincipal", img_group).attr("class", "singleThumbPreview");
    var deleteThumb = $(".btn_borrar", img_group);
    deleteThumb.attr("class", "delete_btn").text(cancelar_txt).attr("title", cancelar_txt)
        .on("click", function (event) {
            event.preventDefault();
            delete_single_thumbnail(img_group);
        });
    thumb.attr("src", src);
    img_group.appendTo(target).fadeIn(500);
}
function delete_single_thumbnail(img_group) {
    var filebrowser = img_group.closest(".filebrowser");
    var target = filebrowser.attr('data-target');
    img_group.fadeOut(500, function () {
        $(this).remove();
    });
    $("#" + target).val("");
}

function delete_main_image(button) {
    var sec = button.attr("data-sec");
    var id = button.attr("data-id");
    var lang = button.closest(".single_image").attr("data-lang");
    $.post(adminUrl + "ajax/borrar_main_imagen.php", {sec: sec, id: id, lang: lang}, function (data) {
        if (data === "ok") {
            $(".imgSelected").fadeOut(300);
            $(button).fadeOut(300);
            $("#id_imagen_old").val("0");
            $("#imagen_old").val("");
        }
    });
}
function filebrowser_single_image(button) {
    var finder = new CKFinder();
    var filebrowser = button.closest(".filebrowser");
    var target = filebrowser.attr('data-target');
    finder.selectActionFunction = function (fileUrl, data) {
        var cleanUrl = fileUrl.replace(uploadUrl, "");
        document.getElementById(target).value = cleanUrl;
        display_single_thumbnail(cleanUrl, filebrowser);
    };
    finder.popup();
}
function filebrowser_multi_image(button) {
    var finder = new CKFinder();
    var sec = button.attr('data-sec');
    var id = button.attr('data-id');
    var lastOrden = $("#multi-images li").length;
    finder.selectActionFunction = function (fileUrl, data, allFiles) {
        /*for (var file in allFiles) {
         console.log(allFiles[file]);
         }*/
        $.post(adminUrl + "ajax/galeria_multi_imagen.php", {action: "save", sec: sec, id: id, lastOrden: lastOrden, allFiles: allFiles}, function (data) {
            if (data !== "notok") {
                var jsonData = $.parseJSON(data);
                var i = 0;
                var all_items = [];
                console.log(jsonData);
                /*while (jsonData[i]) {
                 var id = jsonData[i]["id"];
                 var fileUrl = jsonData[i]["fileUrl"].replace(uploadUrl, "");
                 var img_data = {
                 "id": id,
                 "imagen": adminUrl + "lib/timThumb/timThumb.php?src=" + relativeUploadUrl + fileUrl + timThumbBigGalleryParams,
                 "sub": admin_sub
                 };
                 $.post(adminUrl + "includes/multi-images-item.php", {img_data: jsonData, sub:admin_sub, uploadUrl:uploadUrl}, function (data) {
                 var item
                 item = data.replace(/(\r\n|\n|\r)/gm, " ");
                 all_items.push(item);
                 });
                 i++;
                 }*/
                $.post(adminUrl + "includes/multi-images-item.php", {img_data: jsonData, sub: admin_sub, uploadUrl: uploadUrl}, function (data) {
                    var item = data.replace(/(\r\n|\n|\r)/gm, " ");
                    $("#multi-images").append(item);
                    activar_borrar_imagen();
                    activar_pestanyera();
                    toggle_lang(idioma_defecto);
                    activar_multi_imagen_opciones();
                    if ($("#multi-images li:first .content").css("display") === "block") {
                        $(".content").show();
//                            console.log($(".content", item));
                    }
                });
            } else {
                alert("Error galeria_guardar_multi_imagen.");
            }
        });
    };
    finder.popup();
}
function img_alt_text(button, form) {
    button.prop("disabled", true).addClass("loading disabled");
    var validForm = validate_target_inputs(form);
    if (validForm) {
        var json_formData = serialize_target_inputs(form);
        if (!isEmpty(json_formData)) {
            $.post(adminUrl + "ajax/galeria_multi_imagen.php", {action: "img_alt", formData: json_formData}, function (data) {
                setTimeout(function () {
                    if (data !== "notok") {
                        $(".alert.ok", form).fadeIn(200).delay(2000).fadeOut(400);
                    } else {
                        $(".alert.catastrofe", form).fadeIn(200).delay(2000).fadeOut(400);
                        alert("Error [galeria_guardar_multi_imagen]");
                    }
                    button.prop("disabled", false).removeClass("loading disabled");
                }, 1000);
            });
        } else {
            alert("Error [empty_data]");
        }
    } else {
        button.prop("disabled", false).removeClass("loading disabled");
    }
}
/*-------------------------------------------------------------- responsive --*/
function is_mobile() {
    if ($("#main_nav .launcher").css("display") === "block") {
        return true;
    } else {
        return false;
    }
}
function check_responsive() {
    if (is_mobile()) {
        mobile_display();
    } else {
        desktop_display();
    }
}
function mobile_display() {
    $("#wrapper").addClass("mobile");
    $("#main_nav .launcher").off().on("click", function () {
        $("#main_nav ul").slideToggle(300);
    });
    $(".edit_options .save_back .text").text(btn_guardar_volver);
    $(".edit_options .back .text, .edit_options .preview .text, .edit_options .save .text").hide();
    $(".add_group .anyadir").text(btn_anyadir);
}
function desktop_display() {
    $("#wrapper").removeClass("mobile");

    $(".edit_options button, .edit_options a").each(function () {
        var btn_txt = $(".text", this).attr("data-text");
        $(".text", this).text(btn_txt).show();
    });

    $(".add_group .anyadir").text($(this).attr("data-text"));
}

$(document).ready(function () {
    /*------------------------------------------------------- hide main nav --*/
    $(".nav_options div").on("click", function () {
        toggle_menu();
    });

    $("#main_nav li:has(.selected)").addClass("selected");

    /*--------------------------------------------------------- tablesorter --*/
    if (exists(".tablesorter")) {
        $(".tablesorter").tablesorter({
            sortReset: true
        });
        tablesorter_header_setup();
    }

    /*------------------------------------------------------ delete confirm --*/
    $(".delete_confirm").on("click", function (event) {
        event.preventDefault();
        delete_confirm($(this));
    });

    /*------------------------------------------------------------ ckeditor --*/
    $('.ckeditor').each(function () {
        var name = $(this).attr("name");
        if ($(this).hasClass("basic_toolbar")) {
            var ckeditor = CKEDITOR.replace(name, {
                toolbar: 'Basic', //Basic
                height: '180',
                extraAllowedContent: 'div(*)',
                templates_replaceContent: false
            });
        } else {
            var ckeditor = CKEDITOR.replace(name, {
                toolbar: 'Contenido', //Basic
                height: '430',
                extraAllowedContent: 'div(*)',
                templates_replaceContent: false
            });
        }
        if ($(this).hasClass("seo_description")) {
            editor_seo_event(ckeditor, $(this)); //includes/plugins/seo/functions.php
        }
        CKFinder.setupCKEditor(ckeditor, 'lib/ckfinder/');
    });

    /*-------------------------------------------------------- options menu --*/
    $(window).scroll(function () {
        check_window_scroll();
    });
    check_window_scroll();
    /*if (exists(".options_header")) {
     $(".options_header").css("min-height",$(".options_header").height());
     }*/
    if (exists(".add_btn")) {
        $(".add_group").empty();
        $(".add_btn").appendTo(".add_group");
        $(".add_btn, .add_btn a").css({
            display: "inline-block"
        });
    }

    $(".edit_options .preview").on("click", function (event) {
        event.preventDefault();
        var lang = $(".preview_lang", this).text();
        previsualizar(lang);
    });

    /*------------------------------------------------------- fran6validate --*/
    $(".required").prev("label").append("*");
    $(".save, .save_back, .enviar").click(function (event) {
        event.preventDefault();

        var button = $(this);

        if (button.hasClass("save")) {
            $("#go_back").val("0");
        }
        if (button.hasClass("save_back")) {
            $("#go_back").val("1");
        }
        if (button.hasClass("enviar")) {
            $("#guardar_enviar").val("1");
            $("#go_back").val("1");
        }

        $("form.fran6validate").off();

        $().fran6validate("form.fran6validate", "submit");
        activate_form_submit();
    });
    activate_form_submit();

    $(".back").on("click", function () {
        var back_url = $("#back_url").val();
        if ($(sec).val() === "multimedia") {
            if ($("#multi-images li").length === 0) {
                var id = $("#id").val();
                back_url = back_url + "&id=" + id;
            } else {
                var parent = $("#categoria").val();
                back_url = back_url + "&id=" + parent;
            }
        }
        window.location = back_url;
    });
    /*------------------------------------------------------------ fancybox --*/
    if (exists(".dark.popper")) {
        $(".dark.popper").fancybox({
            nextEffect: "fade",
            prevEffect: "fade",
            beforeClose: function () {
                location.reload();
            }
        });
    }
    /*---------------------------------------------------------- datepicker --*/
    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy'
    });

    /*---------------------------------------------------- relojito portada --*/
    if (exists("#liveclock")) {
        window.onload = liveclock;
    }


    /*-------------------------------------------------------------- admins --*/
    if (exists(".form_admins")) {
        $(".secciones_admins").change(function () {
            sec = $(this).attr("value");
            idcontrol = $(this).attr("id");
            activado = $("#" + idcontrol + ":checked").length;
            adm = $(this).attr("rel") //falta

            $.post("secciones/admins/ajax/add_seccion.php", {admin: adm, seccion: sec, activo: activado}, function (data) {
                //alert(data);
            });

        });
    }

    /*--------------------------------------------------------------- forms --*/
    if (exists(".repeat_pass")) {
        $("#password").val("");
        $("#password, #confirm_password").change(function () {
            if (($("#password").val() != "" && $("#confirm_password").val() != "") || ($("#password").val() != "" && $("#confirm_password").val() == "") || ($("#password").val() == "" && $("#confirm_password").val() != "")) {
                //console.log('required_pass');
                $("#password").addClass("required");
                $("#confirm_password").addClass("required");
            } else {
                //console.log('norequired_pass');
                $("#password").removeClass("required");
                $("#confirm_password").removeClass("required");
            }
        });
    }
    if (exists(".stength_pass")) {
        $('#password').keyup(function (e) {
            $('#passstrength').html(fortaleza_pass($(this).val()));
            return true;
        });
    }

    /*------------------------------------------------------------- ordenar --*/
    $("#orden").on("click", function () {
        var table = $(this).attr("data-table");
        guardar_orden(table);
    });
    if (exists("#multi-images")) {
        $("#multi-images").sortable({
            start: function (event, ui) {
                var target = ui.item;
                target.addClass("drag");
            },
            stop: function (event, ui) {
                var target = ui.item;
                target.removeClass("drag");
                ordenar_multi_images();
            }
        });
    }
    if (exists(".drag_sortable")) {
        $(".drag_sortable tbody").sortable({
            helper: fixWidthHelper,
            start: function (event, ui) {
                $(".list_msk").addClass("dragging");
                var target = ui.item;
                target.addClass("drag");
            },
            stop: function (event, ui) {
                var target = ui.item;
                var tbody = target.parent();
                $(".list_msk").removeClass("dragging");
                target.removeClass("drag");
                ordenar_filas(tbody);
            }
        });
    }

    /*------------------------------------------------------------- activar --*/
    $(".activo_toggler").on("click", function () {
        var checkbox = $(this);
        var activo = checkbox.prop("checked");
        var id = checkbox.closest("tr").attr("data-id");
        var tabla = checkbox.closest("tbody").attr("data-tableDB");
        activo_toggle_item(checkbox, activo, tabla, id);
    });

    /*--------------------------------------------------------- filebrowser --*/
    $(".filebrowser img, .filebrowser .fields button").on("click", function () {
        filebrowser_single_image($(this));
    });

    $(".multiFileBrowse").on("click", function () {
        filebrowser_multi_image($(this));
    });


    /*----------------------------------------------------------------- img --*/
    if (exists(".btn_borrar_img")) {
        activar_borrar_imagen();
        activar_multi_imagen_opciones();
    }

    $(".single_image .btn_borrar").on("click", function (event) {
        event.preventDefault();
        delete_main_image($(this));
    });
    $(".lang_field_group").css({
        backgroundImage: "none"
    });
    $(".toggle_item").on("click", function (event) {
        event.preventDefault();
        var target = $(this).attr("data-transition-target");
        $(target).slideToggle(200);
    });
    $(".show_item").on("click", function (event) {
        event.preventDefault();
        $(this).hide();
        $(this).siblings(".hide_item").show();
        var target = $(this).attr("data-transition-target");
        $(target).slideToggle(200);
    });
    $(".hide_item").on("click", function (event) {
        event.preventDefault();
        $(this).hide();
        $(this).siblings(".show_item").show();
        var target = $(this).attr("data-transition-target");
        $(target).slideUp(200);
    });

    /*-------------------------------------------------------------- delete --*/
    $(".delete_confirm_galeria, .delete_confirm_cats_prods").on("click", function (event) {
        event.preventDefault();
        var href = $(this).attr("href");
        var subcategorias = $(this).attr("data-subcategorias");
        if (subcategorias !== "0") {
            if (window.confirm(eliminar_subcategorias_txt)) {
                window.location = href;
            }
        } else {
            var control = $(this);
            delete_confirm(control);
        }
    });
    $(".delete_confirm_cats_articulos").on("click", function (event) {
        event.preventDefault();
        var href = $(this).attr("href");
        var subcategorias = $(this).attr("data-items");
        if (subcategorias !== "0") {
            if (window.confirm(eliminar_cat_articulos_txt)) {
                window.location = href;
            }
        } else {
            var control = $(this);
            delete_confirm(control);
        }
    });

    /*---------------------------------------------------------------- lang --*/
    activar_pestanyera();
    toggle_lang(idioma_defecto);

    /*-------------------------------------------------------------- legend --*/
    $(".legend").on("click", function (event) {
        event.preventDefault();
        toggle_legend($(this));
    });

    /*------------------------------------------------------------- paginas --*/
    $("input[type='checkbox'][id^='external_link']").on("change", function () {
        external_link_setup($(this));
    });
    if (exists("input[type='checkbox'][id^='external_link']")) {
        external_link_setup($("input[type='checkbox'][id^='external_link']"));
    }

    /*------------------------------------------------------------- plugins --*/
    $(".plugin > header").on("click", function (event) {
        event.preventDefault();
        $(this).next(".content").slideToggle(400);
    });
    if (exists(".plugin")) {
        $(".plugin").each(function () {
            var pluginId = $(this).attr("data-id");
            var pluginInit = new Function(pluginId + "_init()");
            pluginInit();
        });
    }

    /*---------------------------------------------------------- responsive --*/
    check_responsive();
    $(window).resize(function () {
        check_responsive();
    });
});