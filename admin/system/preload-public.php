<?php

$moduls_contractats = cargar_modulos_contratados();
//print_r($moduls_contractats);

$request_uri = $_SERVER['REQUEST_URI'];
$params_url = explode("?", $request_uri);
$url = $params_url[0];
$parametros_url = $parametros_fu = array();
/* -------------------------------------------------------------------- LANG -- */
if (!empty($development_lang)) {
    $idiomas = $development_lang;
    $lang_default = "es";
} else {
    $idiomas = carga_idioma();
    $lang_default = $idiomas["defecto"]["idioma"];
}
$lang = define_lang($url, $arrail, $idiomas);

if (!isset($lang_file_url)) {
    $lang_file_url = "";
}
$idiomaXml = lang_file($lang, $lang_file_url);

/* --------------------------------------------------------------------- SEC -- */
$friendly_url = format_url_fu($url, $lang, $idiomas["activos"]);
$query = "SELECT * FROM friendly_url WHERE url = '" . $friendly_url . "' AND idioma = '$lang' LIMIT 1";
$result = mysqli_query($link, $query);
$nItems = mysqli_num_rows($result);
$sec = $seccion = $id = $id_seccion = $id_pagina = "";
if ($nItems > 0) {
    $row = mysqli_fetch_assoc($result);
    $sec = $seccion = $row["seccion"];
    $id = $id_seccion = $id_pagina = $row["id_seccion"];
    if ($id == 0) {
        $id_pagina = get_id_pagina_estatica($sec);
    }
    if ($row["params"] != "") {
        $parametros_fu = obtener_parametros_url($row["params"]);
    }
} else {
    $sec = "error";
    $id = 0;
    $campo = "error";
    header('HTTP/1.1 404 Not Found');
}
if (count($params_url) > 1) {
    $parametros_url = obtener_parametros_url($params_url[1]);
}
$parametros = array_merge($parametros_fu, $parametros_url);

$mama_fes_debug = array(
    "url" => $url,
    "lang" => $lang,
    "fu" => $friendly_url,
    "sec" => $sec,
    "id" => $id,
    "id_pagina" => $id_pagina,
    "params_url" => $parametros_url,
    "params_fu" => $parametros_fu,
    "params" => $parametros
);
/*debug_var($mama_fes_debug);*/