<?
$siteName = "JZ fincas";
$siteUrl = "http://jzfincas/";
$siteCopyright = "2016";

$siteTheme = "jzfincas";
$siteVersion = "1.0";

$development = true;
$development_lang = array();

/* ------------------------------------------------------------- productos -- */
$product_cats_on_nav = false; // Categories de productes en el #main_nav (com a submenú)

$product_folder_style = false; // Maquetació tipo carpetes. If FALSE => maquetació amb menú (lateral) sempre visible.
$first_cat_by_default = true; // If (!$product_folder_style) al accedir a la página productes mostrem la primera categoria de productes activa amb productes

/* ---------------------------------------------------------------- social -- */
$facebook = "https://www.facebook.com/jzfincas/";
$twitter = "";
$googleplus = "";
$youtube = "";
$instagram = "";
$linkedIn = "";
$sollutia = "http://www.sollutia.com";

/* --------------------------------------------------------------- contact -- */
$contact_to = "maurosempere@gmail.com";
$toname = "Web JZ fincas";
$sendername = "Contacto Web JZ fincas";
$subject = "Contacto desde ".$siteName;
$logo = "images/logo.png";


/* ---------------------------------------------- no toques, pa que tocas! -- */
/* -------------------------------------------------------------------------- */
$themeUrl = $siteUrl . "themes/" . $siteTheme . "/";
$themeIncludeUrl = "themes/" . $siteTheme . "/";
$sender = $smtpUser;