<?
/* ------------------------------------------------------------------ SITE -- */
$siteUrl = "http://jzfincas/";
$siteName = "JZ fincas";

$adminName = "JZ fincas";
$adminUrl = $siteUrl."admin/";
$adminLang = "es";

/* -------------------------------------------------------------- NOTICIAS -- */
$activar_categorias_noticias = true;

/* ------------------------------------------------------------ DESARROLLO -- */
$development = true;

/*-------------------------------------------------------------- MAIL ADMIN --*/
$mailAdmin = "info@jzfincas.com";

/*------------------------------------------------------------ LANG OPTIONS --*/
$require_all_langs = true;

/*--------------------------------------------------------------- SECCIONES --*/
$seccion_por_defecto = "paginas";
$sortableGridSize = 40;
$toggle_nav_cookie = "toggle_nav";
$toggle_legend_cookie = "toggle_legend";

/* timThumb */
$timThumbGalleryParams = "&w=120&h=80&zc=2&q=60";
$timThumbBigGalleryParams = "&w=640&h=480&zc=1&q=60";

/*------------------------------------------------------------------- THEME --*/
$adminTheme = "sollutia";
$adminThemeUrl = $adminUrl."themes/".$adminTheme."/";
$adminThemeIncludeUrl = "themes/".$adminTheme."/";

/*----------------------------------------------------------------- sec sub --*/
$sec = $sub = "";
if (isset($_GET['sec'])) { $sec = $_GET['sec']; } else if (isset($_POST['sec'])) { $sec = $_POST['sec'];}
if (isset($_GET['sub'])) { $sub = $_GET['sub']; } else if (isset($_POST['sub'])) { $sub = $_POST['sub'];}