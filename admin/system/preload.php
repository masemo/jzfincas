<?php
$moduls_contractats = cargar_modulos_contratados();
$arraySeccions["default_section"] = $seccion_por_defecto;

foreach($moduls_contractats as $modul => $contractat){
    if($contractat){
        $arraySeccions[$modul] = $contractat;
    }
}
/*-- idiomas públicos --*/
$lenguajes = carga_idioma();
$nIdiomas = count($lenguajes["activos"]);

$logged_in = false;

if (isset($_GET['err'])) {
	$error = $_GET['err'];
} else {
	$error = "";
}
?>
