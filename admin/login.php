<?
session_start();
include("system/vars.php");
include("system/conexiondb.php");
include("system/config.php");

include("lib/funciones.php");
include("funciones/funciones.php");

include("common/functions.php"); //funciones que se comparten con la parte publica
include("../functions/main.php");

include("system/preload.php");

include("funciones/lang.php");
?>
<!DOCTYPE html>
<html lang="<? echo $lang_default; ?>">
    <head>
        <? include($adminThemeIncludeUrl."template/head.php"); ?>
        <? include("includes/template/head.php"); ?>
        <title><? echo $siteName; ?></title>
        <meta name="description" content="<? echo lang("description"); ?>" />
        <meta name="keywords" content="<? echo lang("keywords"); ?>" />
    </head>
    <body>
        <div id="wrapper">
            <? include($adminThemeIncludeUrl . "template/header.php"); ?>
            <main id="main_section">
                <section class="login">
                    <div class="wrap">
                        <header>
                            <h1><? echo lang("panel_administracion"); ?></h1>
                        </header>
                        <form id="login_form" action="funciones/login.php" method="post" class="fran6validate">
                            <fieldset>
                                <p>
                                    <label for="admin_login_usuario"><? echo lang("usuario"); ?></label>
                                    <input type="text" name="admin_login_usuario" id="admin_login_usuario" value="" class="required" />
                                </p>
                                <p>
                                    <label for="admin_login_password"><? echo lang("password"); ?></label>
                                    <input type="password" name="admin_login_password" id="admin_login_password" value="" class="required" />
                                </p>
                                <? if ($error != "") { ?>
                                    <p class="alert catastrofe">
                                        <? echo lang("usuario_password_incorrecto"); ?>
                                    </p>
                                <? } ?>
                                <p>
                                    <button type="submit"><? echo lang("acceder"); ?></button>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                </section>
            </main>
        </div>
        <script type="text/javascript" src="<? echo $adminUrl; ?>lib/jQuery/jQuery-1.11.0.min.js"></script>
        <script type="text/javascript" src="<? echo $adminUrl; ?>lib/fran6validate/<? echo minify("fran6validate", "js", $development); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#admin_login_usuario").focus();
                $(".login form button[type='submit']").on("click", function(event) {
                    event.preventDefault();
                    $().fran6validate("form.fran6validate", "submit");
                });
            });
        </script>
    </body>
</html>