<?php
function debug_var($var)
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

function http_to_url($url)
{
    if ($url != "") {
        if (preg_match("#https?://#", $url) === 0) {
            $url = 'http://' . $url;
        }
    }
    return $url;
}

function hide_item($hide, $attrClass = false)
{
    if ($hide) {
        if ($attrClass) {
            echo 'class="hide"';
        } else {
            echo 'hide';
        }
    }
}

function get_cookie_value($cookie_name)
{
    if (isset($_COOKIE[$cookie_name])) {
        return $_COOKIE[$cookie_name];
    }
    return false;
}

function elimina_acentos($cadena)
{
    $tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
    $replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";

    $cadena = strtr($cadena, $tofind, $replac);

    $tofind = "&";
    $replac = "and";

    $cadena = str_replace($tofind, $replac, $cadena);

    $tofind = "/";
    $replac = "-";

    $cadena = str_replace($tofind, $replac, $cadena);

    $tofind = "?";
    $replac = "";

    $cadena = str_replace($tofind, $replac, $cadena);

    return ($cadena);
}

function total_query($table)
{
    global $link;
    $query = "SELECT id FROM $table";
    $result = mysqli_query($link, $query);
    $total = mysqli_num_rows($result);
    return $total;
}

//-- MOBILE DETECT
function mobile_detect()
{
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        return true;
    } else {
        return false;
    }
}

//-- SANEAR STRING
function sanear_string($string, $respetar_mayusculas = true)
{
    /**
     * Reemplaza todos los acentos por sus equivalentes sin ellos
     *
     * @param $string
     *  string la cadena a sanear
     *
     * @respetar_mayusculas
     *  si false, return minï¿½suculas
     *
     * @return $string
     *  string saneada
     */
    $string = trim($string);
    if ($respetar_mayusculas) {
        $sanear_a = array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A', 'A');
        $sanear_e = array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E');
        $sanear_i = array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I');
        $sanear_o = array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O');
        $sanear_u = array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U');
    } else {
        $sanear_a = "a";
        $sanear_e = "e";
        $sanear_i = "i";
        $sanear_o = "o";
        $sanear_u = "u";
    }
    $string = str_replace(
        array('Ã¡', 'Ã ', 'Ã¤', 'Ã¢', 'Âª', 'Ã', 'Ã€', 'Ã‚', 'Ã„', 'Ãƒ'), $sanear_a, $string
    );
    $string = str_replace(
        array('Ã©', 'Ã¨', 'Ã«', 'Ãª', 'Ã‰', 'Ãˆ', 'ÃŠ', 'Ã‹'), $sanear_e, $string
    );
    $string = str_replace(
        array('Ã­', 'Ã¬', 'Ã¯', 'Ã®', 'Ã', 'ÃŒ', 'Ã', 'ÃŽ'), $sanear_i, $string
    );
    $string = str_replace(
        array('Ã³', 'Ã²', 'Ã¶', 'Ã´', 'Ã“', 'Ã’', 'Ã–', 'Ã”'), $sanear_o, $string
    );
    $string = str_replace(
        array('Ãº', 'Ã¹', 'Ã¼', 'Ã»', 'Ãš', 'Ã™', 'Ã›', 'Ãœ'), $sanear_u, $string
    );
    $string = str_replace(
        array('Ã±', 'Ã‘', 'Ã§', 'Ã‡'), array('n', 'N', 'c', 'C',), $string
    );
//Esta parte se encarga de eliminar cualquier caracter extraï¿½o
    $string = str_replace(
        array("\\", "Â¨", "Âº", "~",
            "#", "@", "|", "!", "\"",
            "Â·", "$", "%", "&", "/",
            "(", ")", "?", "'", "Â¡",
            "Â¿", "[", "^", "`", "]",
            "+", "}", "{", "Â¨", "Â´",
            ">", "<", ";", ",", ":",
            ".", " ", "?", "Â±", "?", "!",
            "^", "="), '_', $string);

    if (!$respetar_mayusculas) {
        strtolower($string);
    }
    return $string;
}

function extrae_cadena($text, $max_length, $quierotags = false)
{
    $tags = array();
    $result = "";

    $is_open = false;
    $grab_open = false;
    $is_close = false;
    $tag = "";

    $i = 0;
    $stripped = 0;
    $stripped_text = html2txt($text, false);

    if (!$quierotags) {
        if (strlen($stripped_text) < $max_length) {
            return entity_encode($stripped_text);
        } else {
            return entity_encode(mb_substr($stripped_text, 0, $max_length - 1, "UTF-8")) . '&hellip;';
        }
    }

    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length) {
        $symbol = $text{$i};
        $result .= $symbol;

        switch ($symbol) {
            case '<':
                $is_open = true;
                $grab_open = true;
                break;

            case '/':
                if ($is_open) {
                    $is_close = true;
                    $is_open = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open) {
                    $is_open = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                } else if ($is_close) {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }
        $i++;
    }

    while ($tags)
        $result .= "</" . array_pop($tags) . ">";

    if ((strlen($text)) > $max_length) {
        $result .= entity_decode("&hellip;");
    }

    return $result;
}

function prepare_to_db($data, $is_html = false)
{
    global $link;

    if ($is_html) {
        $data = mysqli_real_escape_string($link, $data);
    } else {
        $data = html2txt($data);
        $data = mysqli_real_escape_string($link, $data);
    }

    return $data;
}

function html2txt($data, $entities = true)
{
    //$data = trim(preg_replace("[\n|\r|\n\r]", " ", $data));
    $data = preg_replace("/\s\s+/", "", $data);
    $data = strip_tags($data);
    $data = entity_decode($data);
    $data = trim($data);
    $data = strip_tags($data);
    $data = preg_replace('/\t+/', ' ', $data);
    $data = trim($data);

    if ($entities) {
        $data = entity_encode($data);
    }

    return $data;
}

function entity_decode($data)
{
    return html_entity_decode($data, ENT_QUOTES, 'UTF-8');
}

function entity_encode($data)
{
    return htmlentities($data, ENT_QUOTES, 'UTF-8');
}

function fecha_formato($fecha, $format)
{
    $dia = date("d", strtotime($fecha));
    $mes = date("m", strtotime($fecha));
    $anyo = date("Y", strtotime($fecha));

    switch ($format) {
        case 'dd/mm/aaaa':
            $fecha = $dia . '/' . $mes . '/' . $anyo;
            break;
        case 'aaaa/mm/dd':
            $fecha = $anyo . '/' . $mes . '/' . $dia;
            break;
        default:
            break;
    }

    return $fecha;
}

function fecha_to_mysql($fecha)
{
    $trozos = explode("/", $fecha);
    $nova_fecha = $trozos[2] . "-" . $trozos[1] . "-" . $trozos[0];
    return $nova_fecha;
}

function fecha_to_view($fecha)
{
    $trozos = explode("-", $fecha);
    $nova_fecha = $trozos[2] . "/" . $trozos[1] . "/" . $trozos[0];
    return $nova_fecha;
}

//-------------  2008-12-12 12:06:30  PASSAR A  12:06 -------------------------------##
function formato_hora($fecha)
{
    $hora = substr($fecha, 11, 5);

    return $hora;
}

function obtener_hora($fecha)
{
    $hora = substr($fecha, 11, 2);

    return $hora;
}

function obtener_minuto($fecha)
{
    $minuto = substr($fecha, 14, 2);

    return $minuto;
}

//-------------  2008-12-12 12:06:30  PASSAR A  12/12/2008 12:06 -------------------------------##
function fecha_larga_view($fecha)
{
    $trozos = explode(" ", $fecha);
    $dia = $trozos[0];
    $hora = $trozos[1];

    $trozos_dia = explode("-", $dia);
    $nueva_fecha_dia = $trozos_dia[2] . "/" . $trozos_dia[1] . "/" . $trozos_dia[0];

    $nueva_hora = substr($hora, 0, 5);

    $nueva_fecha_foro = $nueva_fecha_dia . " " . $nueva_hora;
    return $nueva_fecha_foro;
}

/* ---------- obtiene el mï¿½s en texto -------------------------------------------------------- */

function fecha_mes_texto($num, $abbr = 1)
{
    $devuelve = "error";
    if ($num > 0) {
        if ($abbr == 1) {
            $meses = array("", "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
        } else {
            $meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        }

        $devuelve = $meses[$num];
    }
    return $devuelve;
}

//-------------------reescalar------------------------------------------------------------------------------------------------------##
function reescalar_img($nom_imatge, $ample_ideal, $alt_ideal, $ruta_imatge, $qualitat)
{
    $datosimg = getimagesize($ruta_imatge . $nom_imatge);
    $ample = $datosimg[0];
    $alt = $datosimg[1];
    if ($ample > $ample_ideal) {//si ï¿½s + ampla ke alta ajustem a AMPLE IDEAL
        if ($ample > $alt) {
            $nova_altura = ($alt * $ample_ideal) / $ample;
            $thumb = imagecreatetruecolor($ample_ideal, $nova_altura);
            $source = imagecreatefromjpeg($ruta_imatge . $nom_imatge);
            imagecopyresampled($thumb, $source, 0, 0, 0, 0, $ample_ideal, $nova_altura, $datosimg[0], $datosimg[1]);
        } else {//si ï¿½s + alta ke ampla ajustem a ALT IDEAL
            $nueva_anchura = ($alt_ideal * $ample) / $alt;
            $thumb = imagecreatetruecolor($nueva_anchura, $alt_ideal);
            $source = imagecreatefromjpeg($ruta_imatge . $nom_imatge);
            imagecopyresampled($thumb, $source, 0, 0, 0, 0, $nueva_anchura, $alt_ideal, $datosimg[0], $datosimg[1]);
        }
        imagejpeg($thumb, $ruta_imatge . $nom_imatge, $qualitat);
    }
}

/* -------------------------------------------------- marcar dates caducades -- */

function dataPublicada($fecha2, $public)
{

    $fecha1 = strtotime(date("Y-m-d"));
    $fecha2 = strtotime($fecha2);

    if ($public == "caduca") {
        if ($fecha1 >= $fecha2) {
            echo("style='color:red;'");
        }
    } else {
        if ($fecha1 < $fecha2) {
            echo("style='color:orange;'");
        }
    }
}

/* --------------------------------------------------------------------- log -- */

function insert_log($accion, $tabla, $sqlexec)
{
    global $link;

    $admin = $_SESSION["sessionNombre"];
    $sqlexec = str_replace("'", "\'", $sqlexec);
    $sql = "INSERT INTO log (fechahora,accion,tabla,sql_exec,admin) VALUES (NOW(),'$accion','$tabla','$sqlexec','$admin')";
    mysqli_query($link, $sql);
}

/* ----------------------------------------------------------- mostrar alert -- */

function alert($text, $info = "")
{
    if ($info != "") {
        $class = "info";
    }
    $alert = "<div id='alert' class='$class'><a class='close' rel='#alert' >X</a>";
    $alert .= "<table><tr><td valign='center'>$text</td></tr></table>";
    $alert .= "</div>";
    echo $alert;
}

function get_url_contents($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, count($numero));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $elements); //SETEAMOS LOS VALORES A ENVIAR
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $salida = curl_exec($ch);

    if ($salida === false) {
//echo 'Curl error: ' . curl_error($ch);
        echo "This service is not online now.";
    }

    curl_close($ch);
    return $salida;
}

//-- extensió .min si ja està en producció
function minify($arxiu, $extensio, $development)
{
    if (!$development) {
        return $arxiu . ".min." . $extensio;
    } else {
        return $arxiu . "." . $extensio;
    }
}

function zerofill($num, $zerofill = 6)
{
    return str_pad($num, $zerofill, '0', STR_PAD_LEFT);
}

function incident_code($num)
{
    return "JZ" . zerofill($num, 6);
}