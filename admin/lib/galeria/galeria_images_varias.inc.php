<?
include("../../system/vars.php");
include("../../system/conexiondb.inc.php");
include("../../system/config.inc.php");
include("../funciones.php");
$id = $_POST['id'];

$sql_cats = "SELECT * FROM imagenes_categoria ORDER BY descripcion ASC";
$consulta_cats = mysql_query($sql_cats);
$fila_cats = mysql_fetch_array($consulta_cats);
$id_cat = $fila_cats['id'];
?>
<script type="text/javascript">
    var carpeta_loaded = <? echo $id; ?>;
    function popup_window(destino){
        var popper = "includes/popper.php";
        var popup = window.open(popper,"ventana1","width=1020,height=700,scrollbars=yes, resizable=no");
        popup.onbeforeunload = function(){
            load_carpeta(carpeta_loaded);
        }
    }
    function load_carpeta(id_carpeta){
        $(".loading_page_galeria").fadeTo(300, 1);
        $('#images_varias').animate({opacity: 0}, 250, function() {
            $("#images_varias").load("lib/galeria/galeria_images_varias_view.inc.php", {id:id_carpeta,idprod:<? echo $id; ?>}, function(){
                $('#images_varias').animate({opacity: 1}, 250, function() {
                    carpeta_loaded = id_carpeta;
                });
            });
        });
    }
    $(document).ready(function() {

        $("#images_varias").load("lib/galeria/galeria_images_varias_view.inc.php",{id:<? echo $id_cat; ?>,idprod:<? echo $id; ?>});
        $("#images_incluidas").load("lib/galeria/galeria_images_varias_added.inc.php",{idprod:<? echo $id; ?>});

        $("#cerrar, #tapadora, .cerrar").click(function() {
            $("#images_list_transparent, #popup, #tapadora").fadeOut('slow', function(){
                $("#selector_imagenes").load("lib/galeria/imagenes.inc.php",{id:<? echo $id; ?>});
            });
        });

        $("#cats").change(function() {
            var id_carpeta = $(this).val();
            load_carpeta(id_carpeta);
        });
        $("#cats").change(function() {

        });
    });

    function paginacion(p_actual,p_inicio,cat,prod){
        $('#images_varias').animate({opacity: 0}, 250, function() {
            $("#images_varias").load("lib/galeria/galeria_images_varias_view.inc.php", {idprod:prod,id:cat,pagina_actual:p_actual,pagina_inicio:p_inicio}, function(){
                $('#images_varias').animate({opacity: 1}, 250, function() { });
            });
        });
    }
</script>
<div id="images_incluidas" style="margin-left: 10px;"></div>
<h2>Imágenes en la galería</h2>
<fieldset>
    <legend>
        <div style="margin: 0 10px;">
            <p class="icon_folders">&nbsp;</p>
            <select id="cats" name="cats">
                <?
                mysql_data_seek($consulta_cats, 0);
                while ($fila_cats = mysql_fetch_array($consulta_cats)) {
                    ?>
                    <option value="<? echo $fila_cats['id']; ?>"><? echo $fila_cats['descripcion']; ?></option>
                    <?
                }
                ?>
            </select>
            <!-- <input type="button" name="mostrar" id="mostrar" value="buscar"/> -->
        </div>
    </legend>
    <div id="images_varias"></div>

    <a href="index.php?sec=galeria&popup=1" class="popper">
        añadir nuevas imágenes/carpetas al gestor
    </a>
</fieldset>
<br/>
<input type="button" value="hecho" class="cerrar"/>