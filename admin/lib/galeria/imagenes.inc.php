<?
include("../../system/vars.php");
include("../../system/conexiondb.inc.php");
include("../../system/config.inc.php");
include("../funciones.php");
include("../../funciones/funciones.php");

$id = $_POST['id'];
?>
<script type="text/javascript">
    saveAnterior = "";

    function mostraSave(id) {
        if (saveAnterior == "") {
            document.getElementById("save_" + id).style.visibility = "visible";
        } else {
            document.getElementById(saveAnterior).style.visibility = "hidden";
            document.getElementById("save_" + id).style.visibility = "visible";
        }
        saveAnterior = "save_" + id;
    }
    function getURLVar(urlStr, urlVarName) {
        var urlHalves = urlStr.split('?');
        var urlVarValue = '';
        if (urlHalves[1]) {
            var urlVars = urlHalves[1].split('&');
            for (i = 0; i <= (urlVars.length); i++) {
                if (urlVars[i]) {
                    var urlVarPair = urlVars[i].split('=');
                    if (urlVarPair[0] && urlVarPair[0] == urlVarName) {

                        urlVarValue = urlVarPair[1];
                        break;
                    }
                }
            }
        }
        return urlVarValue
    }

    $(document).ready(function () {
        $("#anyadir_imagenes").click(function (event) {
            event.preventDefault();
            start_loading();
            $("#popup div").load("lib/galeria/galeria_images_varias.inc.php", {id:<? echo $id; ?>}, function () {
                mostra_popup();
            });
        });
    });
</script>

<!-- fancybox -->
<? include("../../includes/lightbox.inc.php"); ?>

<div id="images">
    <fieldset><legend>Otras imágenes incluidas.</legend>
        <ul class="fotos">
            <?
            $sql = "SELECT * FROM productos_imagen WHERE  producto = $id ORDER BY id ASC";
            //echo $sql;

            $consulta_imagenes = mysql_query($sql);
            $num_imagenes = mysql_num_rows($consulta_imagenes);
            if ($num_imagenes != 0) {
                while ($fila_imagenes = mysql_fetch_array($consulta_imagenes)) {
                    $imagen = obtnener_imagen($fila_imagenes['imagen']);
                    ?>
                    <li>
                        <a href="<? echo pintar_imagen($imagen); ?>" class="fancybox" title="imagen de producto"><img src="lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen, true); ?>&w=80&h=65&zc=1&q=60" alt=""/></a>
                    </li>
                    <?
                }//while
            } else {
                echo "<span><em>No hay otras imágenes incluidas.</em></span>";
            }//if hi ha algo
            ?>
        </ul>
        <br/>
        <input type="button" id="anyadir_imagenes" value="Añadir imágenes" />
    </fieldset>
</div>