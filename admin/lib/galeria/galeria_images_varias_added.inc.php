<?
include("../../system/vars.php");
include("../../system/conexiondb.inc.php");
include("../../system/config.inc.php");
include("../funciones.php");
include("../../funciones/funciones.php");

$id = $_POST['idprod'];
?>
<script type="text/javascript">
    function borrar(prod, img) {
        var answer = confirm('Seguro que desea eliminar esta imagen del producto?');
        if (!answer) {
            return false;
        }
        $.post("lib/galeria/galeria_images_varias_delete.php", {idprod: prod, imagen: img}, function (data) {
            $("#images_incluidas").load("lib/galeria/galeria_images_varias_added.inc.php", {idprod:<? echo $id; ?>});
        });
    }
    $(document).ready(function () {
        $(".fotos_added li").on("click", function () {
            var id_prod = $(" input[name='id_prod']", this).val();
            var img = $(" input[name='imagen']", this).val();
            borrar(id_prod, img);
        });
    })
</script>
<h2>Imágenes actualmente incluidas</h2>
<ul class="fotos_added">
    <?
    $sql = "SELECT * FROM productos_imagen WHERE producto = $id ORDER BY id ASC";
    //echo $sql;

    $consulta_imagenes = mysql_query($sql);
    $num_imagenes = mysql_num_rows($consulta_imagenes);
    if ($num_imagenes != 0) {
        while ($fila_imagenes = mysql_fetch_array($consulta_imagenes)) {
            $imagen = obtnener_imagen($fila_imagenes['imagen']);
            ?>
            <li>
                <input type="hidden" name="id_prod" value="<? echo $id; ?>" />
                <input type="hidden" name="imagen" value="<? echo $fila_imagenes['imagen']; ?>" />
                <div class="boton_borrar">
                    <img src="images/icones/borrar.png" title="::borrar::" alt="::borrar::" />
                </div>
                <img src="lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen, true); ?>&w=100&h=100&zc=2&q=80" alt="" identificador="<? echo $fila_imagenes['imagen']; ?>" />
            </li>
            <?
        }
    } else {
        echo "<span><em>No hay otras imágenes incluidas.</em></span>";
    }
    ?>
</ul>