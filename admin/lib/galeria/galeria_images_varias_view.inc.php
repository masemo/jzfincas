<?
include("../../system/vars.php");
include("../../system/conexiondb.inc.php");
include("../../system/config.inc.php");
include("../funciones.php");

$id = $_POST['id'];
//echo "id:".$id;
$idprod = $_POST['idprod'];
//echo "idrprd:".$idprod;
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fotos_selector li img").click(function() {
            imagenadd = $(this).attr("fich");
            var target_img = $("#images_incluidas .fotos_added img[identificador='"+imagenadd+"']");
            if(target_img.length > 0){ // si ja existeix la imatge no la tornem a clavar, indiquem amb un efecte que ja està
                $(target_img).css("opacity", "0").animate({
                    opacity : 1
                },300);
            }else{
                $.post("lib/galeria/galeria_images_varias_guardar.php", { idprod:<? echo $idprod; ?>, imagen:imagenadd },
                    function(data) {
                        //alert(data);
                        $("#images_incluidas").load("lib/galeria/galeria_images_varias_added.inc.php",{idprod:<? echo $idprod; ?>});
                    }
                ); 
            }
        });
    });
</script>
<ul class="fotos_selector">
    <?
    $pagina_inicio = $_POST['pagina_inicio'];
    $pagina_actual = $_POST['pagina_actual'];

    if ($pagina_inicio < 1)
        $pagina_inicio = 1;
    if ($pagina_actual < 1)
        $pagina_actual = 1;

    $sql = "SELECT * FROM imagenes WHERE categoria = $id ORDER BY id ASC";

    $consulta = mysql_query($sql);
    $numero_filas = mysql_num_rows($consulta);

    $registros_por_pagina = 35;
    $paginas_vistas = 10;
    $num_paginas = ceil($numero_filas / $registros_por_pagina);

    if (!isset($pagina_actual))
        $pagina_actual = 1;
    if (!isset($pagina_inicio))
        $pagina_inicio = 1;


    $pos_inicial = ( $pagina_actual * $registros_por_pagina ) - $registros_por_pagina;

    $sql2 = $sql . " LIMIT $pos_inicial,$registros_por_pagina";
//echo $sql2;
    $consulta_imagenes = mysql_query($sql2);
    $num_imagenes = mysql_num_rows($consulta_imagenes);



    if ($num_imagenes != 0) {
        while ($fila_imagenes = mysql_fetch_array($consulta_imagenes)) {
            ?>
            <li>
                <img id="<? echo $fila_imagenes['id']; ?>" fich="<? echo $fila_imagenes['id']; ?>" src="lib/timThumb/timThumb.php?src=<? echo $fila_imagenes['fichero']; ?>&w=160&h=160&zc=2&q=80" alt="<? echo $fila_imagenes['fichero']; ?>"/>
            </li>
            <?
        }
    } else {
        echo "<span><em>No hay imágenes en la categoría.</em></span>";
    }
    ?>
</ul>
<br />

<div align='right' class='paginacion'>p&aacute;gina&nbsp;
    <?php
    //PAGINACIÓN
    $a = $pagina_inicio;
    if ($pagina_inicio > 1) {
        ?>
        <a href='#' onclick="paginacion(<? echo ($pagina_inicio - $paginas_vistas); ?>,<? echo ($pagina_inicio - $paginas_vistas); ?>,<? echo $id; ?>,<? echo $idprod; ?>);return false;">...</a>&nbsp;
        <?
    }
    while ($a <= $pagina_inicio + $paginas_vistas - 1 && $num_paginas >= $a) {
        if ($pagina_actual == $a) {
            ?>
            <strong><? echo $a; ?></strong>&nbsp;
        <? } else { ?>
            <a href='#' onclick="paginacion(<? echo ($a); ?>,<? echo ($pagina_inicio); ?>,<? echo $id; ?>,<? echo $idprod; ?>);return false;"><? echo $a; ?></a>&nbsp;
            <?
        }
        $a++;
    }
    if ($pagina_inicio + $paginas_vistas - 1 < $num_paginas && $num_paginas > $pagina_inicio + $paginas_vistas - 1) {
        ?>
        <a href='#' onclick="paginacion(<? echo ($a); ?>,<? echo ($a); ?>,<? echo $id; ?>,<? echo $idprod; ?>);return false;">...</a>&nbsp;
    <? } ?>
</div>
