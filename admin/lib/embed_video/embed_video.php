<?php

/* ------------------------------------------------------------------ VIMEO -- */

function get_vimeo_id($url) {
    if (preg_match('#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $m)) {
        return $m[1];
    }
    return false;
}

function embed_vimeo($url, $width, $height) {
    return '<iframe src="http://player.vimeo.com/video/' . get_vimeo_id($url) . '" width="' . $width . '" height="' . $height . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
}

function url_miniatura_vimeo($video, $tamany = "large") {
    $video_id = get_vimeo_id($video);
    $video_info = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_id.php"));
    switch ($tamany) {
        case "small": //120x90
            $video_thumb = $video_info[0]['thumbnail_small'];
            $resized_thumb = str_replace("_100x75", "_120x90", $video_thumb);
            break;
        case "large": //640x480
            $video_thumb = $video_info[0]['thumbnail_large'];
            $resized_thumb = str_replace("_640", "_640x480", $video_thumb);
            break;
        default:  //480x360
            $video_thumb = $video_info[0]['thumbnail_medium'];
            $resized_thumb = str_replace("_200x150", "_480x360", $video_thumb);
            break;
    }
    return $resized_thumb;
}

/* ---------------------------------------------------------------- YOUTUBE -- */

function get_youtube_id($url) {
    $parts = parse_url($url);
    if (isset($parts['host'])) {
        $host = $parts['host'];
        if (false === strpos($host, 'youtube') && false === strpos($host, 'youtu.be')) {
            return false;
        }
    }
    if (isset($parts['query'])) {
        parse_str($parts['query'], $qs);
        if (isset($qs['v'])) {
            return $qs['v'];
        } else if (isset($qs['vi'])) {
            return $qs['vi'];
        }
    }
    if (isset($parts['path'])) {
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path) - 1];
    }
    return false;
}

function embed_youtube($ytURL, $width, $height) {
    $autoplay = 1;
    return '<iframe type="text/html" width="' . $width . '" height="' . $height . '" src="http://www.youtube.com/embed/' . get_youtube_id($ytURL) . '?showinfo=0&autohide=1&color=white" frameborder="0"></iframe>';
}

function url_miniatura_youtube($video, $tamany = "large") {
    $video_id = get_youtube_id($video);
    switch ($tamany) {
        case "small": //120x90
            $video_thumb = "http://img.youtube.com/vi/" . $video_id . "/default.jpg";
            break;
        case "large": //640x480
            $video_thumb = "http://img.youtube.com/vi/" . $video_id . "/sddefault.jpg";
            break;
        default: //480x360
            $video_thumb = "http://img.youtube.com/vi/" . $video_id . "/hqdefault.jpg";
            break;
    }
    return $video_thumb;
}

/* -------------------------------------------------- EMBED LO QUE SIGA, XÉ -- */

function check_domain($url) {
    $vimeo = strpos($url, "vimeo");
    if ($vimeo === false) {
        $youtube = strpos($url, "youtu");
        if ($youtube === false) {
            return false;
        } else {
            return "youtube";
        }
    } else {
        return "vimeo";
    }
}

function embed_video($url, $width, $height) {
    $domini = check_domain($url);
    if (!$domini) {
        $embed = "VIDEO ERROR: Only YOUTUBE and VIMEO URL's accepted.";
    } else {
        if ($domini == "youtube") { // és youtube
            $embed = embed_youtube($url, $width, $height);
        } else { // és vimeo
            $embed = embed_vimeo($url, $width, $height);
        }
    }
    echo $embed;
}

/* ---------------------------------------- GENERA LA MENIATURA QUE SIGA, XÉ -- */

function url_miniatura($url, $tamany = "large") {
    $domini = check_domain($url);
    if (!$domini) {
        echo "THUMBNAIL ERROR: Only YOUTUBE and VIMEO URL's accepted.";
    } else {
        if ($domini == "youtube") { // És youtube
            return url_miniatura_youtube($url, $tamany);
        } else { // És vimeo
            return url_miniatura_vimeo($url, $tamany);
        }
    }
}

?>
