/*-------------------------------------------------------------- validation --*/
function serialize_target_inputs(target) {
    var json_formData = {};
    $(":input", target).each(function () {
        var name = $(this).attr("name");
        if (name !== undefined) {
            var valor = $(this).val();
            json_formData[name] = valor;
        }
    });
    return json_formData;
}
function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return true && JSON.stringify(obj) === JSON.stringify({});
}
function validate_target_inputs(target) {
    var valid = true;
    $(".highlight.tinyValidate").removeClass("highlight tinyValidate");
    $(":input", target).each(function () {
        var input = $(this);
        var name = input.attr("name");
        if (name !== undefined) {
            var attr = input.attr("data-validate");
            if (attr !== undefined && attr !== "") {
                var validate = attr.replace(/ /g, '');
                var validate_requirements = validate.split(",");
                for (var i = 0; i < validate_requirements.length; i++) {
                    if (validate_input(validate_requirements[i], input, target) === false) {
                        valid = false;
                    }
                }
            }
        }
    });
    if (!valid) {
        check_invalid_lang_tab(target);
        return false;
    } else {
        return true;
    }
}
function validate_input(type, input, target) {
    var input_val = input.val();
    switch (type) {
        case "required":
            if (input_val === "") {
                tag_invalid(input, target);
                return false;
            }
            break;
        case "number":
            if (isNaN(input_val)) {
                tag_invalid(input, target);
                return false;
            }
            break;
    }
    return true;
}
function tag_invalid(input) {
    input.closest("p").addClass("highlight tinyValidate");
}
function check_invalid_lang_tab(target) {
    var id = target.attr("id");
    $(".lang_group", target).each(function () {
        var lang_group_with_error = $(".highlight.tinyValidate:first", this).parents(".lang_group");
        var lang_group_lang = lang_group_with_error.attr("data-lang");
        $("*[data-for='" + id + "'] .pestanyera li[data-lang='" + lang_group_lang + "'] button").addClass("highlight tinyValidate");
    });
}
/*------------------------------------------------------------- /validation --*/