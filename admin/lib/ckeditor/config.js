/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.extraPlugins = 'templates,magicline,oembed,dialog,fakeobjects,iframe,justify';
    config.magicline_everywhere = true;
    config.allowedContent = true;
    config.extraAllowedContent = 'span(*)';

    config.internalPages = $.getJSON('/admin/includes/pagesJSON.php', {lang: this.name.split("_").pop()});

    config.toolbar_Contenido = [
        {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        {name: 'document', items: ['Source', '-', 'Templates', 'Maximize']},
        '/',
        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
        {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        {name: 'links', items: ['Link', 'Unlink']},
        '/',
        {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar', '-', 'Iframe', 'oembed']},
        {name: 'styles', items: ['Format']}
    ];
    config.toolbar_Basic = [
        {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        {name: 'document', items: ['Source']},
        '/',
        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
        {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
        {name: 'links', items: ['Link', 'Unlink']}
    ];

    //config.removeButtons = 'SpellChecker,Print,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Font,BGColor,About,Styles';
};