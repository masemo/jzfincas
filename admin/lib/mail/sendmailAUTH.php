<?php 
function sendmailAUTH($to,$toname,$sender,$sendername,$subject,$body,$hostSmtp,$userSmtp,$passSmtp){

	$mail = new PHPMailer();

	$mail->IsSMTP(); 
	
	$mail->Host = $hostSmtp;

	$mail->Port = 25;

	$mail->From = $sender;

	$mail->FromName = $sendername;

	$mail->Subject = $subject;

	//$mail->AltBody = "prueba"; 
	
	$mail->IsHTML(true);
	$mail->MsgHTML($body);

	$mail->AddAddress($to,$toname);


	$mail->SMTPAuth = true;

	$mail->Username = $userSmtp;
	$mail->Password = $passSmtp;
	
	if(!$mail->Send()) {
		return $mail->ErrorInfo;
	} else {
		return true;
	}

}
?>