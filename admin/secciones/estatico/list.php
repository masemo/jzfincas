<?
$sql = "SELECT * FROM friendly_url WHERE noborrar = 1 ";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND texto LIKE '%" . $search_term . "%')";
}
$sql .= " AND seccion NOT IN (SELECT estatica_sec FROM paginas WHERE estatica = 1) ";
$sql .= " GROUP BY seccion ORDER BY seccion ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);

$sec_title = lang("gestion_estatico");
$nItems = mysqli_num_rows($consulta);
$addLink = "";
$search_action = "";
?>
<section class="estatico">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <div class="list_msk">
                    <table class="list tablesorter">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("seccion"); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $seccion_nombre = $fila["seccion"];
                                if($fila["seccion"] == "paginas"){
                                    $seccion_nombre = "Preview";
                                }
                                ?>
                                <tr>
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title"><a href="index.php?sec=estatico&sub=editar&seccion=<? echo $fila['seccion']; ?>&amp;id_seccion=<? echo $fila['id_seccion']; ?>">
                                        <? echo $seccion_nombre; ?>
                                        </a>
                                    </td>
                                    <td></td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>