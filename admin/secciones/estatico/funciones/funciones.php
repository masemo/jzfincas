<?php
function editar_guardar($frm, $lenguajes) {
    $parent = 0;
    $noborrar = 1;
    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => $frm["seccion"],
            "seccion_parents" => "",
            "fu_field" => "",
            "title_field" => "",
            "description_field" => "",
            "parent_title_field" => ""
        );
        gestion_metas($leng, $frm, $frm["id"], $parent, $variables_gestion_metas, $noborrar);
    }
}
