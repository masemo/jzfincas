<?
$accion_sub = "editar-guardar";
$sec_title = "Contenido estático: ".strtoupper($seccion);
$sec_action = lang("editar");

$back_url = "index.php?sec=estatico";

$id = $id_seccion;
?>
<section class="estatico">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="estatico" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="seccion" id="seccion" value="<? echo $seccion; ?>" />
                <input type="hidden" name="id" id="id" value="<? echo $id; ?>" />
                
                <fieldset class="metas_estatico">
                    <?
                    $sec = $seccion;
                    $openPlugin = true;
                    $seoPlugin = "includes/plugins/seo/index.php";
                    if (is_file($seoPlugin)) {
                        include($seoPlugin);
                    }
                    ?>
                </fieldset>
            </form>
            <footer>

            </footer>
        </section>
    </div>
</section>