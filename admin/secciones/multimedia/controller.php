<?php

$subDefault = "secciones/multimedia/list.php";

switch ($sub) {
    /* ----------------------------------------------------------------- accions -- */
    case "anyadir-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        $id = anyadir_guardar($frm, $parent, $imagen, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        } else {
            $back_url .= "&id=$parent";
        }
        header("location:$back_url");
        break;
    case "editar-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        editar_guardar($frm, $id, $parent, $imagen, $imagen_old, $id_imagen_old, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        } else {
            $sql_imagenes = "SELECT id FROM imagenes WHERE idsec = $id AND sec = 'galeria'";
            $consulta_imagenes = mysqli_query($link, $sql_imagenes);
            $nItems = mysqli_num_rows($consulta_imagenes);
            if ($nItems > 0) {
                $back_url .= "&id=$parent";
            } else {
                $back_url .= "&id=$id&parent=$parent";
            }
        }
        header("location:$back_url");
        break;
    case "eliminar":
        eliminar($id);
        header("location:index.php?sec=multimedia&id=$cat&parent=$parent");
        break;
//------------------subseccions------------------------------------    
    case "editar":
    case "anyadir":
        $include = "secciones/multimedia/edit.php";
        break;
    case "listado-multimedia":
        $include = "secciones/multimedia/list-multimedia.php";
        break;
    default:
        $include = $subDefault;
        break;
}
