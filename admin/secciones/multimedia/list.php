<?
if (!isset($id) || $id == "") {
    $id = 0;
}
if (!isset($parent) || $parent == "") {
    $parent_query = row_query("parent", "galeria_categoria", "id = $id");
    if(!empty($parent_query)){
        $parent = $parent_query["parent"];
    }else{
        $parent = 0;
    }
}
$sql = "SELECT * FROM galeria_categoria WHERE galeria_categoria.parent = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND id IN (SELECT id_seccion FROM idiomas WHERE texto LIKE '%" . $search_term . "%' AND seccion='galeria_categoria')";
}
$sql .= " ORDER BY galeria_categoria.orden ASC, galeria_categoria.id ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);
$tiene_imagenes = $imagenes = tiene_imagenes($id);
$tiene_subcats = tiene_subcategorias($id);

$sec_title = "Multimedia";
$nItems = mysqli_num_rows($consulta);

$addLink = "";

$search_action = "multimedia";

$breadcrumbs = migas_imagenes($id, "listado");
$breadcrumbs_home = "multimedia";

$tableDB = "galeria_categoria";
?>
<section class="banners">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>              
            <? include("includes/breadcrumbs.php"); ?>
        </header>
        <div class="filter_group">
            <div class="filter button_group">
                <a href="index.php?sec=multimedia&sub=anyadir&id=<? echo $id; ?>" class="button anyadir"><? echo lang("anyadir_carpeta"); ?></a>
            </div>
            <? if (!$tiene_subcats && ($nItems > 0 || ($id != 0))) { ?>
                <div class="filter button_group">
                    <a href="index.php?sec=multimedia&sub=editar&id=<? echo $id; ?>" class="button anyadir"><? echo lang("anyadir_imagenes"); ?></a>
                </div>
            <? } ?>
        </div>
        <section>
            <? if ($nItems > 0) { ?>
                <?
                $instructions = array("move");
                instructions($instructions);
                ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("carpeta"); ?></th>
                                <th style="width:70px;"><? echo lang("activa"); ?></th>
                                <th style="width:50px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $subcategorias = tiene_subcategorias($fila['id']);
                                $imagenes = tiene_imagenes($fila['id']);
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title">
                                        <? if ($imagenes > 0) { ?>
                                            <a href="index.php?sec=multimedia&sub=editar&id=<? echo $fila['id']; ?>" class="img_carpeta_galeria">
                                                <span class="icon"></span>
                                                <? echo obtener_valor($lang_default, "galeria_categoria", "descripcion", $fila['id']); ?> <span class="subtle">(<? echo $imagenes." ".lang("images"); ?>)</span>
                                            </a>
                                        <? } else { 
                                            if($subcategorias > 0){
                                                $nCats_txt = $subcategorias." ".lang("subcategorias");
                                            }else{
                                                $nCats_txt = lang("vacio");
                                            }
                                            ?>
                                            <a href="index.php?sec=multimedia&id=<? echo $fila['id']; ?>" class="img_carpeta">
                                                <span class="icon"></span>
                                                <? echo obtener_valor($lang_default, "galeria_categoria", "descripcion", $fila['id']); ?> <span class="subtle">(<? echo $nCats_txt; ?>)</span>
                                            </a>
                                        <? } ?>    
                                    </td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <? if ($imagenes == 0) { ?>
                                            <a href="index.php?sec=multimedia&sub=editar&id=<? echo $fila['id']; ?>" class="edit">
                                                <span class="icon">::<? echo lang("editar"); ?>::</span>
                                            </a>
                                        <? } ?>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=multimedia&sub=eliminar&id=<? echo $fila['id']; ?>&cat=<? echo $fila['parent']; ?>" data-subcategorias="<? echo $subcategorias; ?>" class="delete_confirm_galeria delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <?
                if ($id == 0) {
                    $instructions = array("minimo_una_carpeta");
                    instructions($instructions);
                }
                ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>