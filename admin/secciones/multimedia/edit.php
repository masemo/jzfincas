<?
if ($sub == "editar") {
    $accion_sub = "editar-guardar";
    $sec_title = "imágenes";
    $sec_action = lang("editar_carpeta") . " " . $sec_title;
    $id_cat = $id;
    $sql = "SELECT * FROM galeria_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
    $parent = $fila["parent"];

    $tiene_imagenes = tiene_imagenes($id);
    $tiene_subcats = tiene_subcategorias($id);
    if ($tiene_imagenes) {
        $only_back_id = $parent;
    } else {
        $only_back_id = $id;
    }
} else {
    $accion_sub = "anyadir-guardar";
    $sec_title = "imágenes";
    $sec_action = lang("anyadir_nueva_carpeta") . " " . $sec_title . " " . lang("en") . ":";
    $id_cat = "";
    $parent = $id;
    $only_back_id = $parent;
}

$sql_cats = "SELECT * FROM galeria_categoria WHERE parent = 0 ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $sql_cats);


$back_url = "index.php?sec=multimedia";

$breadcrumbs = migas_imagenes($id, "listado");
$breadcrumbs_home = "multimedia";
?>
<section class="multimedia">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action; ?></h1>
            <? include("includes/breadcrumbs.php"); ?>
            <?
            if ($sub == "anyadir") {
                $guardar_txt = lang("guardar") . " &amp; " . lang("anyadir_imagenes");
            }
            include("includes/edit-options.php");
            ?>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="multimedia" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <?
                if ($sub == "editar") {
                    if (($tiene_imagenes || !$tiene_subcats)) {
                        $dataSec = "galeria";
                        include("includes/multi-images.php");
                    }
                }
                ?>
                <div class="row">
                    <div class="col-60 noLp">
                        <? include("includes/pestanyera-lang.php"); ?>
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="descripcion_<? echo $leng; ?>"><? echo lang("carpeta"); ?></label>
                                        <input type="text" class="seo_title seo_description seo_friendly_url <? require_lang_field($leng); ?>" name="descripcion_<? echo $leng; ?>" id="descripcion_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar") {
                                            echo obtener_valor($leng, "galeria_categoria", "descripcion", $id);
                                        }
                                        ?>" />
                                    </p>
                                </fieldset>
                                <?
                                unset($lenguaje);
                            }
                            ?>
                        </div>
                        <br />
                        <p>
                            <label for="parent"><? echo lang("carpeta_superior"); ?></label>
                            <select id="parent" name="parent">
                                <option value="0" <?
                                if ((0) == $parent) {
                                    echo "SELECTED";
                                }
                                ?>><? echo lang("inicio"); ?></option>
                                <?
                                while ($fila_cats = mysqli_fetch_array($consulta_cats)) {
                                    if ($sub == "anyadir") {
                                        $id_cat = $parent;
                                    }
                                    echo genera_pagina_item($fila_cats, $id_cat);
                                    echo crea_item_menu($fila_cats['id'], $id_cat, 0);
                                }
                                ?>
                            </select>
                        </p>
                        <br />
                        <? include("includes/single-image.php"); ?>
                    </div>
                    <div class="col-40">
                        <?
                        $openPlugin = true;
                        $seoPlugin = "includes/plugins/seo/index.php";
                        if (is_file($seoPlugin)) {
                            $sec = "galeria_categoria";
                            include($seoPlugin);
                        }
                        ?>
                    </div>
                </div>
                <fieldset>

                    <div class="checkbox_group">
                        <?
                        $instructions = array("activo");
                        instructions($instructions);
                        ?>
                        <p class="checkbox">
                            <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                            if (isset($fila) && $fila['activo'] > 0) {
                                echo "checked='checked'";
                            }
                            ?>/>
                        </p>
                    </div>
                </fieldset>
            </form>
        </section>
    </div>
</section>