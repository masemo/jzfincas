<?php
/* -- select categoria -- */

function print_level($level) {
    $space = "";
    if ($level > 0) {
        for ($x = 0; $x <= $level; $x++) {
            $space .= "&nbsp;&nbsp;&nbsp;";
        }
        return $space;
    } else {
        return "- ";
    }
}

function genera_pagina_item($row_categorias, $id_actual = "", $level = 0) {
    global $lang_default;
    global $parent;
    $id = $row_categorias['id'];
    if ($id_actual == "" || $id != $id_actual) {
        ?>
        <option value="<? echo $id; ?>" <?
        if ($parent == $id) {
            echo "selected='selected'";
        }
        ?>>
                    <? echo print_level($level) . obtener_valor($lang_default, "galeria_categoria", "descripcion", $id); ?>
        </option>
        <?
    }
}

function genera_pagina_item_optgroup($id, $level = 0) {
    global $lang_default;
    ?>
    <optgroup label="<? echo print_level($level) . obtener_valor($lang_default, "galeria_categoria", "descripcion", $id); ?>"></optgroup>
    <?
}

function crea_item_menu($id_cat, $id_actual, $level) {
    global $link;
    global $sub;
    if ($id_actual != "" && $id_cat == $id_actual) {
        if($sub == "anyadir"){
            echo genera_pagina_item($id_cat, $id_actual, $level);            
        }else{
            echo genera_pagina_item_optgroup($id_actual, $level);
        }
    } else {
        $level++;
        $sql = "SELECT * FROM galeria_categoria WHERE parent = $id_cat ORDER BY orden ASC";
        $result = mysqli_query($link, $sql);
        $totals = mysqli_num_rows($result);
        if ($totals > 0) {
            while ($row = mysqli_fetch_array($result)) {
                echo genera_pagina_item($row, $id_actual, $level);
                $itemMenu = crea_item_menu($row['id'], $id_actual, $level);
            }
        }
    }
    return;
}

function obtener_ruta_metas($id, $lan) {
    global $link;

    $aux = "";

    $sql = "SELECT parent FROM galeria_categoria WHERE id=$id";
//    echo $sql."<br/>";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    if ($fila['parent'] > 0) {
        $aux = obtener_ruta_metas($fila['parent'], $lan);
    }

    $valor = obtener_valor($lan, "galeria_categoria", "descripcion", $id);

    return $aux . $valor . " | ";
}

function gestion_metas_old($leng, $frm, $url_friendly, $parent, $id) {
    if (isset($frm["seoPlugin"]) && $frm["meta_title_" . $leng] != "" && $frm["meta_description_" . $leng] != "") {
        include_once("includes/plugins/seo/functions.php");
        gestion_metas_plugin($frm["meta_title_" . $leng], $frm["meta_description_" . $leng], "galeria_categoria", $id, $leng, $url_friendly);
    } else {
        gestion_metas_automaticas($frm["descripcion_" . $leng], $leng, $id, $parent, $url_friendly);
    }
}

function gestion_metas_automaticas($descripcion, $leng, $id, $parent, $url_friendly) {
    global $id_pagina_galeria_imagenes;
    global $siteName;
    $categoria_title = obtener_valor($leng, "paginas", "nombre_menu", $id_pagina_galeria_imagenes);
    $title = generar_meta_title($descripcion, obtener_ruta_metas($parent, $leng), $categoria_title) . " | " . $siteName;
    inserta_metas($leng, "galeria_categoria", "title", $title, $id, $url_friendly);
    inserta_metas($leng, "galeria_categoria", "description", extrae_cadena($descripcion, 155), $id, $url_friendly);
}

function editar_guardar($frm, $id, $parent, $imagen, $imagen_old, $id_imagen_old, $lenguajes) {
    global $link;

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "galeria_categoria");
    } else {
        $imagen = $id_imagen_old;
    }

    $sql = "UPDATE galeria_categoria SET parent=$parent, imagen=$imagen WHERE id = $id";
    //echo $sql;
    mysqli_query($link, $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "galeria_categoria", "descripcion", $frm["descripcion_" . $leng], $id);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "galeria_categoria",
            "seccion_parents" => "galeria_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    insert_log("UPDATE", "galeria_categoria", $sql);
}

function anyadir_guardar($frm, $parent, $imagen, $lenguajes) {
    global $link;

    $sql = "INSERT INTO galeria_categoria (activo,parent) VALUES (1,$parent)";
    //echo $sql;
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "galeria_categoria", "descripcion", $frm["descripcion_" . $leng], $id);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "galeria_categoria",
            "seccion_parents" => "galeria_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    insert_log("INSERT", "galeria_categoria", $sql);

    if ($imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "galeria_categoria");
    } else {
        $imagen = 0;
    }
    $orden = get_last_order("galeria_categoria");
    $sql = "UPDATE galeria_categoria SET imagen = $imagen, orden = $orden WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "imagenes", $sql);

    return $id;
}

function eliminar_categoria($id_categoria) {
    eliminar_img_multiples($id_categoria, "galeria");
    elimina_idiomas($id_categoria, "galeria_categoria", "galeria_categoria");
    eliminar_seo("galeria_categoria", $id_categoria);
    elimina_item_by_id("galeria_categoria", $id_categoria);
}

function eliminar($id) {
    global $link;
    $query = "SELECT id FROM galeria_categoria WHERE parent = $id";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        while ($row = mysqli_fetch_array($result)) {
            eliminar($row["id"]);
            eliminar_categoria($row["id"]);
        }
    }
    eliminar_img_multiples($id, "galeria");
    eliminar_categoria($id);

    insert_log("DELETE", "galeria_categoria", $query);
}
