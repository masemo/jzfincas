<?
if ($sub == "editar") {

    $accion_sub = "editar-guardar";
    $sec_title = "banners";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM banners WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    /* -- anterior siguiente -- */
    $item_ant = obtener_sig_or_ant("banners", $id, "orden", "ANT", "ASC");
    $item_sig = obtener_sig_or_ant("banners", $id, "orden", "SIG", "ASC");

    if ($item_ant > -1) {
        $link_ant = "index.php?sec=banners&sub=editar&id=" . $item_ant;
        $title_ant = "";
        $img_ant = pintar_imagen(obtnener_imagen(basic_query("imagen", "banners", $item_ant)), true);
    }

    if ($item_sig > -1) {
        $link_sig = "index.php?sec=banners&sub=editar&id=" . $item_sig;
        $title_sig = "";
        $img_sig = pintar_imagen(obtnener_imagen(basic_query("imagen", "banners", $item_sig)), true);
    }
} else {

    $accion_sub = "anyadir-guardar";
    $sec_title = "banners";
    $sec_action = lang("anyadir_nuevos");
}

$sql = "SELECT * FROM banners_posicion ORDER BY descripcion ASC";
$consulta_cats = mysqli_query($link, $sql);

$back_url = "index.php?sec=banners";
?>
<section class="admins">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <?
            include("includes/edit-options.php");
            ?>
        </header>
        <section>
            <form id = "edit_form" action = "index.php" method = "post" class = "fran6validate form_admins repeat_pass stength_pass">
                <input type = "hidden" name = "back_url" id = "back_url" value = "<? echo $back_url; ?>" />
                <input type = "hidden" name = "go_back" id = "go_back" value = "0" />
                <input type = "hidden" name = "sec" id = "sec" value = "banners" />
                <input type = "hidden" name = "sub" id = "sub" value = "<? echo $accion_sub; ?>" />
                <input type = "hidden" name = "id" id = "id" value = "<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <input type = "hidden" name = "id_imagen_old" id = "id_imagen_old" value = "<?
                if (isset($id)) {
                    echo $fila['imagen'];
                }
                ?>" />
                <input type = "hidden" name = "imagen_old" id = "imagen_old" value = "<?
                if (isset($id)) {
                    echo obtnener_imagen($fila['imagen']);
                }
                ?>" />
                <fieldset>
                    <p>
                        <label for = "nombre"><? echo lang("ubicacion_en_web");?></label>
                        <select id="posicion" name="posicion">
                            <? while ($fila_posiciones = mysqli_fetch_array($consulta_cats)) { ?>
                                <option value="<? echo $fila_posiciones['id']; ?>" <?
                                if (isset($fila) && $fila_posiciones['id'] == $fila['posicion']) {
                                    echo "SELECTED";
                                }
                                ?>><? echo $fila_posiciones['descripcion']; ?></option>
                                    <? } ?>
                        </select>
                    </p>
                    <? include("includes/single-image.php"); ?>
                    <p>
                        <label for="enlace"><? echo lang("enlace"); ?></label>
                        <input type="text" name="enlace" id="enlace" value="<?
                        if (isset($fila['url'])) {
                            echo $fila['url'];
                        }
                        ?>" />
                    </p>
                    <p>
                        <label for="alt"><? echo lang("etiqueta_alt"); ?></label>
                        <input type="text" name="alt" id="alt" value="<?
                        if (isset($fila['alt'])) {
                            echo $fila['alt'];
                        }
                        ?>" class="required"/>
                    </p>
                    <p>
                        <label for="title"><? echo lang("etiqueta_title"); ?></label>
                        <input type="text" name="title" id="title" value="<?
                        if (isset($fila['title'])) {
                            echo $fila['title'];
                        }
                        ?>" />
                    </p>

                    <div class="input_group">
                        <p>
                            <label for="fecha_public"><? echo lang("publicar"); ?></label>
                            <input type="text" name="fecha_public" id="fecha_public" placeholder="00/00/0000" class="date datepicker" value="<?
                            if (isset($fila['fecha_public'])) {
                                echo fecha_to_view($fila['fecha_public']);
                            } else {
                                echo date("d/m/Y");
                            }
                            ?>" class="required"/>
                        </p>
                        <p>
                            <label for="fecha_caduca"><? echo lang("caduca"); ?></label>
                            <input type="text" name="fecha_caduca" id="fecha_caduca" placeholder="00/00/0000" class="date datepicker" value="<?
                            if (isset($id) > 0) {
                                if (isset($fila['fecha_caduca']) && $fila['fecha_caduca'] != "0000-00-00") {
                                    echo fecha_to_view($fila['fecha_caduca']);
                                }
                            }
                            ?>" class="required"/>
                        </p>
                    </div>

                    <div class="checkbox_group">
                        <?
                        $instructions = array("activo");
                        instructions($instructions);
                        ?>
                        <p class="checkbox">
                            <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                                   if (isset($fila) && $fila['activo'] > 0) {
                                       echo "checked='checked'";
                                   }
                                   ?>/>
                        </p>
                    </div>
                </fieldset>
            </form>
            <footer>
                <?
                if ($sub == "editar") {
                    include("includes/item-anterior-siguiente.php");
                }
                ?>
            </footer>
        </section>
    </div>
</section>