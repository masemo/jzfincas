<?
$sql = "SELECT banners.*, banners_posicion.descripcion AS posicion FROM banners, banners_posicion";
if (isset($search_term) && $search_term != "") {
    $sql .= " WHERE (banners.url LIKE '%" . $search_term . "%' OR banners.title LIKE '%" . $search_term . "%' OR banners.alt LIKE '%" . $search_term . "%')";
}
$sql .= " ORDER BY banners.orden ASC, banners.id ASC";
//echo $sql;

$sec_title = "Banners";
$consulta = mysqli_query($link, $sql);
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=banners&sub=anyadir";
$search_action = "banners";
$tableDB = "banners";
?>
<section class="banners">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <?
                $instructions = array("move");
                instructions($instructions);
                ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                            <tr>
                                <th></th>
                                <th style="width: 150px;"><? echo lang("banner"); ?></th>
                                <th><? echo lang("etiqueta_title"); ?></th>
                                <th><? echo lang("ubicacion"); ?></th>
                                <th><? echo lang("publicar"); ?></th>
                                <th><? echo lang("caduca"); ?></th>
                                <th style="width:70px;"><? echo lang("activo"); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $imagen = obtnener_imagen($fila['imagen']);
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td>
                                        <a href="index.php?sec=banners&sub=editar&id=<? echo $fila['id']; ?>">
                                            <img style="margin: 5px 0 5px 0;" src="<? echo $adminUrl; ?>lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen, true) . $timThumbGalleryParams; ?>" id="imgprincipal" alt="::editar::"/>
                                        </a>
                                    </td>
                                    <td data-field="title"><a href="index.php?sec=banners&sub=editar&id=<? echo $fila['id']; ?>"><? echo $fila['title']; ?></a></td>
                                    <td><? echo $fila["posicion"]; ?></td>
                                    <td <? dataPublicada($fila['fecha_public'], "public"); ?> ><? echo fecha_to_view($fila['fecha_public']); ?></td>
                                    <td <?
                                    if ($fila['fecha_caduca'] != "0000-00-00") {
                                        dataPublicada($fila['fecha_caduca'], "caduca");
                                    }
                                    ?> >
                                            <?
                                            if ($fila['fecha_caduca'] != "0000-00-00") {
                                                echo fecha_to_view($fila['fecha_caduca']);
                                            } else {
                                                echo "-";
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=banners&sub=eliminar&id=<? echo $fila['id']; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>