<?php
function anyadir_guardar($posicion,$imagen,$enlace,$title,$alt,$fecha_public,$fecha_caduca,$activo){
    global $link;
    
    $sql="INSERT INTO banners (url,title,alt,posicion,fecha_public,fecha_caduca,activo) VALUES ('$enlace','$title','$alt',$posicion,'$fecha_public','$fecha_caduca',$activo)";
    mysqli_query($link,$sql);
    $id = mysqli_insert_id($link);
    
    insert_log("INSERT","banners",$sql);
    
    if ($imagen != "") {
        $imagen = guardar_imagen($imagen,$id,"banners");
    } else {
        $imagen = 0;
    }
    $orden = get_last_order("banners");
    $sql="UPDATE banners SET imagen = $imagen, orden = $orden WHERE id = $id";
    mysqli_query($link,$sql);
	
    insert_log("INSERT","imagenes",$sql);
    return $id;
}
function editar_guardar($posicion,$imagen,$imagen_old,$id_imagen_old,$enlace,$title,$alt,$fecha_public,$fecha_caduca,$activo,$id){
    global $link;
    
    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen,$id,"banners");
    } else {
        $imagen = $id_imagen_old;
    }
    
    $sql="UPDATE banners "
            . "SET url='$enlace', imagen=$imagen, title='$title', alt='$alt', posicion=$posicion, fecha_public='$fecha_public', fecha_caduca='$fecha_caduca', activo=$activo "
            . "WHERE id = $id";
    //echo $sql;
    mysqli_query($link,$sql);
	
    insert_log("UPDATE","banners",$sql);

}
function eliminar($subDefault,$id){
    global $link;
    
    borrar_imagen($id,"banners");
     
    $sql="DELETE FROM banners WHERE id = $id";
    mysqli_query($link,$sql);
	
    insert_log("DELETE","banners",$sql);
}
