<?php

$subDefault = "secciones/banners/list.php";
switch ($sub) {
    /* ----------------------------------------------------------------- accions -- */
    case "anyadir-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $fecha_public = fecha_to_mysql($fecha_public);
        if ($fecha_caduca != "") {
            $fecha_caduca = fecha_to_mysql($fecha_caduca);
        }

        $id = anyadir_guardar($posicion, $imagen, $enlace, $title, $alt, $fecha_public, $fecha_caduca, $activo);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }
        header("location:$back_url");
        break;
    case "editar-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $fecha_public = fecha_to_mysql($fecha_public);
        if ($fecha_caduca != "") {
            $fecha_caduca = fecha_to_mysql($fecha_caduca);
        }

        editar_guardar($posicion, $imagen, $imagen_old, $id_imagen_old, $enlace, $title, $alt, $fecha_public, $fecha_caduca, $activo, $id);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }

        header("location:$back_url");
        break;
    case "eliminar":
        eliminar($subDefault, $id);
        $include = $subDefault;
        break;
//------------------subseccions------------------------------------    
    case "editar":
    case "anyadir":
        $include = "secciones/banners/edit.php";
        break;
    default:
        $include = $subDefault;
        break;
}
