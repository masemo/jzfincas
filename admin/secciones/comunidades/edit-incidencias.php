<?
if ($sub == "editar-incidencias") {
    $accion_sub = "editar-guardar-incidencias";
    $sec_title = "incidencia";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM comunidades_incidencias WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_assoc($consulta);
    $parent = $fila["parent"];
} else {
    $accion_sub = "anyadir-guardar-incidencias";
    $sec_title = "incidencia";
    $sec_action = lang("anyadir");
}

$sql = "SELECT * FROM comunidades_estados";
$estados = mysqli_query($link, $sql);

$sql = "SELECT * FROM comunidades_incidencias_sector";
$sectors = mysqli_query($link, $sql);

$back_url = "index.php?sec=comunidades&sub=listar-incidencias&id=$parent";
?>
<section class="comunidades">
    <fieldset class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>"/>
                <input type="hidden" name="go_back" id="go_back" value="0"/>
                <input type="hidden" name="sec" id="sec" value="comunidades"/>
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>"/>
                <input type="hidden" name="parent" id="parent" value="<? echo (isset($parent)) ? $parent : ''; ?>"/>
                <input type="hidden" name="id" id="id" value="<? echo (isset($id)) ? $id : ''; ?>"/>
                <div class="row">
                    <div class="col-70">
                        <fieldset>
                            <div class="row">
                                <div class="col-50">
                                    <p>
                                        <label for="f_apertura"><? echo lang("comunidades_incidencias_f_apertura"); ?></label>
                                        <input type="date" name="f_apertura" id="f_apertura" value="<? echo ($sub == "editar-incidencias") ? $fila['f_apertura'] : ''; ?>" class="required" required/>
                                    </p>
                                </div>
                                <div class="col-50">
                                    <p>
                                        <label for="f_finalizacion"><? echo lang("comunidades_incidencias_f_finalizacion"); ?></label>
                                        <input type="date" name="f_finalizacion" id="f_finalizacion" value="<? echo ($sub == "editar-incidencias") ? $fila['f_finalizacion'] : ''; ?>"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-100">
                                    <p>
                                        <label for="siniestro"><? echo lang("comunidades_incidencias_siniestro"); ?></label>
                                        <textarea name="siniestro" id="siniestro" class="required" required><? echo ($sub == "editar-incidencias") ? $fila['siniestro'] : ''; ?></textarea>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-50">
                                    <p>
                                        <label for="sector"><? echo lang("comunidades_incidencias_sector"); ?></label>
                                        <select type="text" name="sector" id="sector">
                                            <?php while ($sector = mysqli_fetch_array($sectors)) { ?>
                                                <option value="<?php echo $sector["id"] ?>" <?php echo($sub == "editar-incidencias" && $fila['sector'] == $sector["id"] ? 'selected' : '') ?>><?php echo $sector["nombre"]; ?></option>
                                            <?php } ?>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-50">
                                    <p>
                                        <label for="estado"><? echo lang("comunidades_incidencias_estado"); ?></label>
                                        <select type="text" name="estado" id="estado">
                                            <?php while ($estado = mysqli_fetch_array($estados)) { ?>
                                                <option value="<?php echo $estado["id"] ?>" <?php echo($sub == "editar-incidencias" && $fila['estado'] == $estado["id"] ? 'selected' : '') ?>><?php echo $estado["nombre"]; ?></option>
                                            <?php } ?>
                                        </select>
                                    </p>
                                </div>
                            </div>
                            <?php if (!is_null($fila['email'])) { ?>
                                <div class="row">
                                    <div class="col-100 peticion-usuario">
                                        <p><i><?php echo lang("comunidades_incidencias_usuario_label"); ?></i></p>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><?php echo lang("comunidades_incidencias_nombre_label"); ?></td>
                                                <td><p><strong><?php echo $fila['nombre'] ?></strong></p></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo lang("comunidades_incidencias_telefono_label"); ?></td>
                                                <td><p><strong><?php echo $fila['telefono'] ?></strong></p></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo lang("comunidades_incidencias_piso_label"); ?></td>
                                                <td><p><strong><?php echo $fila['piso'] ?></strong></p></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo lang("comunidades_incidencias_email_label"); ?></td>
                                                <td><p><strong><?php echo $fila['email'] ?></strong></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-100">
                                    <div class="checkbox_group">
                                        <p class="checkbox">
                                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                                            if (isset($fila) && $fila['activo'] > 0) {
                                                echo "checked='checked'";
                                            }
                                            ?>/>
                                            <label for="activo"><? echo lang("activo"); ?></label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </form>
            <footer></footer>
        </section>
    </fieldset>
</section>