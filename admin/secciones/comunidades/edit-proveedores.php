<?
if ($sub == "editar-proveedores") {
    $accion_sub = "editar-guardar-proveedores";
    $sec_title = "proveedores";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM comunidades WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_assoc($consulta);
}
$back_url = "index.php?sec=comunidades&sub=editar&id=$id";
?>
<section class="comunidades">
    <fieldset class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>"/>
                <input type="hidden" name="go_back" id="go_back" value="0"/>
                <input type="hidden" name="sec" id="sec" value="comunidades"/>
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>"/>
                <input type="hidden" name="id" id="id" value="<? echo (isset($id)) ? $id : ''; ?>"/>
                <fieldset>
                    <p class="<? hide_item($estatica); ?>" data-eLink="disable">
                        <label for="proveedores">
                            <? echo lang("contenido"); ?> <? if (isset($estatica) && $estatica > 0) { ?>(<? echo lang("desactivado"); ?>)<? } ?>
                            <a href="<? echo ckeditor_manual_url($lang_default); ?>" class="manual" target="blank">::<? echo lang("manual_editor"); ?>::</a>
                        </label>
                        <textarea  cols="" rows="" class="ckeditor newsletter <? if ($sub != "editar") { ?>vacio<? } ?>" id="proveedores" name="proveedores" cols="0" rows="0"><? echo isset($fila["proveedores"]) ? $fila["proveedores"] : ''; ?></textarea>
                    </p>
                </fieldset>
            </form>
            <footer></footer>
        </section>
    </fieldset>
</section>