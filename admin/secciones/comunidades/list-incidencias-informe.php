<?
$estados = get_data("SELECT * FROM comunidades_estados");
$sectores = get_data("SELECT * FROM comunidades_incidencias_sector");
$comunidades = get_data("SELECT * FROM comunidades");
$sec_title = "Informe de incidencias";
$addLink = "";
$editComunidad = "index.php?sec=comunidades";
$search_action = "comunidades_incidencias";
$tableDB = "comunidades_incidencias";


?>
<section class="comunidades">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
        </header>
        <section>
            <?php foreach ($estados as $estado) {

                $sql = "SELECT * FROM comunidades_incidencias WHERE estado = " . $estado["id"];
                if (isset($search_term) && $search_term != "") {
                    $sql .= " AND idpublic LIKE '%" . $search_term . "%'";
                }
                $sql .= " ORDER BY id DESC";
//echo $sql;
                $consulta = mysqli_query($link, $sql);
                $nItems = mysqli_num_rows($consulta); ?>
                <div class="list_msk">
                    <table class="list">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="width: 100px;"><? echo lang("comunidades_id_incidencia"); ?></th>
                            <th style="width: 20%;"><? echo lang("comunidades_nombre"); ?></th>
                            <th style="width: 20%;"><? echo lang("comunidades_siniestro"); ?></th>
                            <th style="width: 110px;"><? echo lang("comunidades_sector"); ?></th>
                            <th style="width: 110px;"><? echo lang("comunidades_f_apertura"); ?></th>
                            <th style="width: 100px;"><? echo lang("comunidades_estado"); ?></th>
                            <td style="width: 50px;"><? echo lang("comunidades_activo"); ?></td>
                            <th></th>
                        </tr>
                        </thead>
                        <?php if ($nItems > 0) { ?>
                            <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $imagen = obtnener_imagen_idioma($sec, $fila["id"], $lang_default); ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="id-incidencia"><a href="index.php?sec=comunidades&sub=editar-incidencias&id=<? echo $fila['id']; ?>"><?php echo $fila['idpublic'] ?></a></td>
                                    <td data-field="comunidad"><? echo $comunidades[$fila["parent"]]["nombre"] ?></td>
                                    <td data-field="siniestro"><? echo $fila["siniestro"] ?></td>
                                    <td data-field="sector"><? echo $sectores[$fila["sector"]]["nombre"] ?></td>
                                    <td data-field="f_apertura"><? echo $fila["f_apertura"] ?></td>
                                     <td data-field="estado"><span class="estado <? echo $estados[$fila["estado"]]["color"] ?>"><? echo $estados[$fila["estado"]]["nombre"] ?></span></td>
                                    <td data-field="activo"><input class="checkbox activo_toggler" type="checkbox" <? echo ($fila['activo'] > 0) ? "checked='checked'" : ''; ?>/></td>
                                    <td>
                                        <a href="index.php?sec=comunidades&sub=eliminar-incidencias&id=<? echo $fila['id']; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <? $i++;
                            } ?>
                            </tbody>
                        <? } else { ?>
                            <tbody>
                            <tr>
                                <td colspan="9"><? echo lang("sin_resultados"); ?> :(</td>
                            </tr>
                            </tbody>
                        <? } ?>
                        <tfoot>
                        <tr>
                            <td colspan="9">&nbsp;</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            <?php } ?>
        </section>
    </div>
</section>