<?

$sql = "SELECT * FROM comunidades WHERE id=$id";
$comunidad_query = mysqli_query($link, $sql);
$comunidad = mysqli_fetch_array($comunidad_query);

$sql = "SELECT * FROM comunidades_tablon WHERE parent = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND nombre LIKE '%" . $search_term . "%'";
}
$sql .= " ORDER BY f_reunion DESC";
//echo $sql;
$consulta = mysqli_query($link, $sql);

$sec_title = "Tablón de anuncios de la comunidad: " . $comunidad["nombre"];
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=comunidades&sub=anyadir-tablon&parent=$id";
$editComunidad = "index.php?sec=comunidades&sub=editar&id=$id";
$search_action = "comunidades_tablon";
$tableDB = "comunidades_tablon";
?>
<section class="comunidades">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <div class="edit_options">
                <a class="back" href="<?php echo $editComunidad ?>">< <? echo lang("volver") . " " . $comunidad["nombre"]; ?></a>
            </div>
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <div class="list_msk">
                    <table class="list">
                        <thead>
                        <tr>
                            <th></th>
                            <th><? echo lang("comunidades_documento"); ?></th>
                            <th><? echo lang("comunidades_fecha_subida"); ?></th>
                            <th><? echo lang("comunidades_fecha_reunion"); ?></th>
                            <th><? echo lang("comunidades_doc"); ?></th>
                            <th style="width:70px;"><? echo lang("activa"); ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                        <?
                        $i = 1;
                        while ($fila = mysqli_fetch_array($consulta)) {
                            $imagen = obtnener_imagen_idioma($sec, $fila["id"], $lang_default); ?>
                            <tr data-id="<? echo $fila['id']; ?>">
                                <td><? echo $i; ?>.</td>
                                <td data-field="nombre"><a href="index.php?sec=comunidades&sub=editar-tablon&id=<? echo $fila['id']; ?>"><? echo $fila["nombre"] ?></a></td>
                                <td data-field="f_subida"><? echo $fila["f_subida"] ?></td>
                                <td data-field="f_reunion"><? echo $fila["f_reunion"] ?></td>
                                <td data-field="doc"><? echo ($fila["doc"] == 0) ? "No" : "Sí"; ?></td>
                                <td><input class="checkbox activo_toggler" type="checkbox" <? echo ($fila['activo'] > 0) ? "checked='checked'" : ''; ?>/></td>
                                <td>
                                    <a href="index.php?sec=comunidades&sub=eliminar-tablon&id=<? echo $fila['id']; ?>&parent=<?php echo $id; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                        <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                    </a>
                                </td>
                            </tr>
                            <?
                            $i++;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br/>
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>