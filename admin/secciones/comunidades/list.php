<?
$sql = "SELECT * FROM comunidades";
if (isset($search_term) && $search_term != "") {
    $sql .= " WHERE codigo = $search_term";
}
$sql .= " ORDER BY codigo ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);

$sec_title = "Gestión de comunidades";
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=comunidades&sub=anyadir";
$search_action = "comunidades";
$tableDB = "comunidades";
?>
<section class="comunidades">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
        </header>
        <section>
            <div class="">
                <a href="index.php?sec=comunidades&amp;sub=listar-incidencias-informe" class="button informe" data-text="Añadir nuevo">Informe de incidencias</a>
                <br>
                <br>
            </div>
            <? if ($nItems > 0) { ?>
                <? $instructions = array("move");
                instructions($instructions); ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="width: 80px;"><? echo lang("comunidades_codigo"); ?></th>
                            <th><? echo lang("comunidades_nombre"); ?></th>
                            <th><? echo lang("comunidades_direccion"); ?></th>
                            <th><? echo lang("comunidades_presidente"); ?></th>
                            <th style="width:70px;"><? echo lang("activa"); ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                        <?
                        $i = 1;
                        while ($fila = mysqli_fetch_array($consulta)) {
                            $imagen = obtnener_imagen_idioma($sec, $fila["id"], $lang_default); ?>
                            <tr data-id="<? echo $fila['id']; ?>">
                                <td><? echo $i; ?>.</td>
                                <td><? echo $fila['codigo']; ?></td>
                                <td data-field="nombre"><a href="index.php?sec=comunidades&sub=editar&id=<? echo $fila['id']; ?>"><? echo $fila["nombre"] ?></a></td>
                                <td data-field="direccion"><? echo $fila["direccion"] ?></td>
                                <td data-field="presidente"><? echo $fila["presidente"] ?></td>
                                <td><input class="checkbox activo_toggler" type="checkbox" <? echo ($fila['activo'] > 0) ? "checked='checked'" : ''; ?>/></td>
                                <td>
                                    <a href="index.php?sec=comunidades&sub=eliminar&id=<? echo $fila['id']; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                        <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                    </a>
                                </td>
                            </tr>
                            <?
                            $i++;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br/>
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>