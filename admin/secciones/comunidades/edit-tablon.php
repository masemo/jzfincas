<?
if ($sub == "editar-tablon") {
    $accion_sub = "editar-guardar-tablon";
    $sec_title = "tablón";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM comunidades_tablon WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_assoc($consulta);
    $parent = $fila["parent"];
} else {
    $accion_sub = "anyadir-guardar-tablon";
    $sec_title = "tablón";
    $sec_action = lang("anyadir");
}
$back_url = "index.php?sec=comunidades&sub=listar-tablon&id=$parent";
?>
<section class="comunidades">
    <fieldset class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>"/>
                <input type="hidden" name="go_back" id="go_back" value="0"/>
                <input type="hidden" name="sec" id="sec" value="comunidades"/>
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>"/>
                <input type="hidden" name="parent" id="parent" value="<? echo (isset($parent)) ? $parent : ''; ?>"/>
                <input type="hidden" name="id" id="id" value="<? echo (isset($id)) ? $id : ''; ?>"/>
                <div class="row">
                    <div class="col-70">
                        <fieldset>
                            <div class="row">
                                <div class="col-33">
                                    <p>
                                        <label for="nombre"><? echo lang("comunidades_nombre_tablon"); ?></label>
                                        <input type="text" name="nombre" id="nombre" value="<? echo ($sub == "editar-tablon") ? $fila['nombre'] : ''; ?>" class="required"/>
                                    </p>
                                </div>
                                <div class="col-33">
                                    <p>
                                        <label for="f_subida"><? echo lang("comunidades_fecha_subida"); ?></label>
                                        <input type="date" name="f_subida" id="f_subida" value="<? echo ($sub == "editar-tablon") ? $fila['f_subida'] : ''; ?>"/>
                                    </p>
                                </div>
                                <div class="col-33">
                                    <p>
                                        <label for="f_reunion"><? echo lang("comunidades_fecha_reunion"); ?></label>
                                        <input type="date" name="f_reunion" id="f_reunion" value="<? echo ($sub == "editar-tablon") ? $fila['f_reunion'] : ''; ?>"/>
                                    </p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div>
                    <fieldset>
                        <div class="row">
                            <div class="col-50"><?
                                $sec  = "comunidades_tablon";
                                include("includes/single-doc.php"); ?></div>
                        </div>
                    </fieldset>
                </div>
                <fieldset>
                    <div class="checkbox_group">
                        <? $instructions = array("activo");
                        instructions($instructions); ?>
                        <p class="checkbox">
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <? echo (isset($fila) && $fila['activo'] > 0) ? "checked='checked'" : ""; ?>/>
                            <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                        </p>
                    </div>
                </fieldset>
            </form>
            <footer></footer>
        </section>
    </fieldset>
</section>