<?php

function anyadir_guardar($frm, $nombre, $cif, $n_cuenta, $n_propietarios, $direccion, $ano_construcion, $presidente, $vicepresidente, $imagen, $usuario, $codigo)
{
    global $link;

    $nombre = prepare_to_db($nombre);
    $cif = prepare_to_db($cif);
    $n_cuenta = prepare_to_db($n_cuenta);
    $n_propietarios = prepare_to_db($n_propietarios);
    $direccion = prepare_to_db($direccion);
    $ano_construcion = prepare_to_db($ano_construcion);
    $presidente = prepare_to_db($presidente);
    $vicepresidente = prepare_to_db($vicepresidente);
    $usuario = prepare_to_db($usuario);

    $sql = "INSERT INTO comunidades (codigo, nombre, cif, n_cuenta, n_propietarios, direccion, ano_construcion, presidente, vicepresidente, usuario, activo) VALUES (\"$codigo\", \"$nombre\", \"$cif\", \"$n_cuenta\", \"$n_propietarios\", \"$direccion\", \"$ano_construcion\", \"$presidente\", \"$vicepresidente\", \"$usuario\", 1);";

    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);
    insert_log("INSERT", "comunidades", $sql);
    if (isset($frm["password"]) && !empty($frm["password"])) {
        $password = $frm["password"];

        $sql = "UPDATE comunidades SET password = md5('$password') WHERE id=$id";
        mysqli_query($link, $sql);
    }


    $imagen = guardar_imagen($imagen, $id, "comunidades");
    $sql = "UPDATE comunidades SET imagen=$imagen WHERE id = $id";
    mysqli_query($link, $sql);

    $orden = get_last_order("comunidades");
    update_order($orden, $id, "comunidades");
    insert_log("INSERT", "comunidades", $sql);

    return $id;
}

function anyadir_guardar_tablon($frm, $nombre, $f_subida, $f_reunion, $doc, $parent, $activo)
{
    global $link;

    $nombre = prepare_to_db($nombre);

    $sql = "INSERT INTO comunidades_tablon (nombre, f_subida, f_reunion, parent, activo) VALUES ('$nombre',  '$f_subida',  '$f_reunion',  $parent, $activo);";

    mysqli_query($link, $sql);

    $id = mysqli_insert_id($link);
    insert_log("INSERT", "comunidades", $sql);

    //doc
    if ($doc != "") {
        $doc = guardar_imagen($doc, $id, "comunidades", "tablon");
    } else {
        $doc = 0;
    }

    $sql = "UPDATE comunidades_tablon SET doc = $doc WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "doc", $sql);
    return $id;
}

function anyadir_guardar_incidencia($frm, $f_apertura, $f_finalizacion, $sector, $estado, $siniestro, $parent)
{
    global $link;

    $sector = prepare_to_db($sector);
    $siniestro = prepare_to_db($siniestro);

    $sql = "INSERT INTO comunidades_incidencias (f_apertura, f_finalizacion, sector, estado, siniestro, parent, activo) VALUES ('$f_apertura',  '$f_finalizacion',  '$sector',  $estado, '$siniestro', $parent, 1);";

    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    $sql = "UPDATE comunidades_incidencias SET idpublic = '" . incident_code($id) . "' WHERE id=$id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "incidencias", $sql);

    return $id;
}

function editar_guardar($frm, $nombre, $cif, $n_cuenta, $n_propietarios, $direccion, $ano_construcion, $presidente, $vicepresidente, $usuario, $activo, $codigo, $id)
{
    global $link;
    $imagen = $frm["imagen"];
    $imagen_old = $frm["imagen_old"];

    $codigo = prepare_to_db($codigo);
    $nombre = prepare_to_db($nombre);
    $cif = prepare_to_db($cif);
    $n_cuenta = prepare_to_db($n_cuenta);
    $n_propietarios = prepare_to_db($n_propietarios);
    $direccion = prepare_to_db($direccion);
    $ano_construcion = prepare_to_db($ano_construcion);
    $presidente = prepare_to_db($presidente);
    $vicepresidente = prepare_to_db($vicepresidente);
    $usuario = prepare_to_db($usuario);

    $sql = "UPDATE comunidades SET codigo = '$codigo', nombre = '$nombre', cif =  '$cif', n_cuenta =  '$n_cuenta', n_propietarios =  '$n_propietarios', direccion =  '$direccion', ano_construcion =  '$ano_construcion', presidente =  '$presidente', vicepresidente =  '$vicepresidente', usuario =  '$usuario', activo =  '$activo' WHERE id = $id;";
    mysqli_query($link, $sql);

    if (isset($frm["password"]) && !empty($frm["password"])) {
        $password = $frm["password"];
        $sql = "UPDATE comunidades SET password = md5('$password') WHERE id=$id";
        mysqli_query($link, $sql);
    }

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "comunidades");
        $sql = "UPDATE comunidades SET imagen=$imagen WHERE id = $id";
        mysqli_query($link, $sql);
    }

    insert_log("UPDATE", "comunidades", $sql);
}

function editar_guardar_tablon($frm, $nombre, $f_subida, $f_reunion, $parent, $activo, $id)
{
    global $link;
    $doc = $frm["doc"];
    $doc_old = $frm["doc_old"];

    $nombre = prepare_to_db($nombre);

    $sql = "UPDATE  comunidades_tablon SET nombre = '$nombre', f_subida = '$f_subida', f_reunion = '$f_reunion', doc =  '$doc', activo =  $activo WHERE id = $id";

    mysqli_query($link, $sql);

    if ($doc != $doc_old && $doc != "") {
        $doc = guardar_imagen($doc, $id, "comunidades", "tablon");
        $sql = "UPDATE comunidades_tablon SET doc=$doc WHERE id = $id";
        mysqli_query($link, $sql);
    }

    insert_log("UPDATE", "comunidades_tablon", $sql);
}

function editar_guardar_incidencia($frm, $f_apertura, $f_finalizacion, $sector, $estado, $siniestro, $activo, $id)
{
    global $link;

    $sector = prepare_to_db($sector);
    $siniestro = prepare_to_db($siniestro);

    $sql = "UPDATE comunidades_incidencias SET f_apertura = '$f_apertura', f_finalizacion = '$f_finalizacion', sector = '$sector', estado =  $estado, siniestro =  '$siniestro', activo = $activo WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("UPDATE", "comunidades_tablon", $sql);

    return $id;
}

function editar_guardar_proveedores($frm, $id)
{
    global $link;

    $contenido = prepare_to_db($frm["proveedores"], true);

    $sql = "UPDATE comunidades SET proveedores = '$contenido' WHERE id = $id ";
    mysqli_query($link, $sql);

    insert_log("UPDATE", "comunidades", $sql);
}

function eliminar($subDefault, $id)
{
    global $link;

    elimina_idiomas($id, "comunidades", "comunidades");
    //elimina_metas($id, "comunidades", "comunidades");

    borrar_imagen($id, "comunidades");

    $sql = "SELECT id FROM comunidades_incidencias WHERE parent = $id";
    foreach (get_data($sql) as $item) {
        eliminar_incidencia($item["id"]);
    }

    $sql = "SELECT id FROM comunidades_tablon WHERE parent = $id";
    foreach (get_data($sql) as $item) {
        eliminar_tablon($item["id"]);
    }

    $sql = "DELETE FROM comunidades WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "comunidades", $sql);
}

function eliminar_incidencia($id)
{
    global $link;

    $sql = "DELETE FROM comunidades_incidencias WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "comunidades_incidencias", $sql);
}

function eliminar_tablon($id)
{
    global $link;

    borrar_imagen($id, "comunidades", "tablon");

    $sql = "DELETE FROM comunidades_tablon WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "comunidades_tablon", $sql);
}