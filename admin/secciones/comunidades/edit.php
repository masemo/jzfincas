<?
if ($sub == "editar") {
    $accion_sub = "editar-guardar";
    $sec_title = "comunidades";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM comunidades WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
} else {

    $accion_sub = "anyadir-guardar";
    $sec_title = "comunidades";
    $sec_action = lang("anyadir");
}
$back_url = "index.php?sec=comunidades";
?>
<section class="comunidades">
    <fieldset class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>"/>
                <input type="hidden" name="go_back" id="go_back" value="0"/>
                <input type="hidden" name="sec" id="sec" value="comunidades"/>
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>"/>
                <input type="hidden" name="id" id="id" value="<? echo (isset($id)) ? $id : ''; ?>"/>
                <div class="row">
                    <div class="col-70">
                        <fieldset>
                            <div class="row">
                                <div class="col-100">
                                    <p>
                                        <label for="nombre"><? echo lang("comunidades_nombre"); ?></label>
                                        <input type="text" name="nombre" id="nombre" value="<? echo ($sub == "editar") ? $fila['nombre'] : ''; ?>" class="required"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-33">
                                    <p>
                                        <label for="codigo"><? echo lang("comunidades_codigo"); ?></label>
                                        <input type="text" name="codigo" id="codigo" value="<? echo ($sub == "editar") ? $fila['codigo'] : ''; ?>" class="required"/>
                                    </p>
                                </div>
                                <div class="col-33">
                                    <p>
                                        <label for="n_propietarios"><? echo lang("comunidades_n_propietarios"); ?></label>
                                        <input type="number" name="n_propietarios" id="n_propietarios" value="<? echo ($sub == "editar") ? $fila['n_propietarios'] : ''; ?>"/>
                                    </p>
                                </div>
                                <div class="col-33">
                                    <p>
                                        <label for="cif"><? echo lang("comunidades_cif"); ?></label>
                                        <input type="text" name="cif" id="cif" value="<? echo ($sub == "editar") ? $fila['cif'] : ''; ?>"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-100">
                                    <p>
                                        <label for="n_cuenta"><? echo lang("comunidades_n_cuenta"); ?></label>
                                        <input type="text" name="n_cuenta" id="n_cuenta" value="<? echo ($sub == "editar") ? $fila['n_cuenta'] : ''; ?>"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-50">
                                    <p>
                                        <label for="direccion"><? echo lang("comunidades_direccion"); ?></label>
                                        <input type="text" name="direccion" id="direccion" value="<? echo ($sub == "editar") ? $fila['direccion'] : ''; ?>"/>
                                    </p>
                                </div>
                                <div class="col-50">
                                    <p>
                                        <label for="ano_construcion"><? echo lang("comunidades_ano_construcion"); ?></label>
                                        <input type="number" name="ano_construcion" id="ano_construcion" value="<? echo ($sub == "editar") ? $fila['ano_construcion'] : ''; ?>"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-50">
                                    <p>
                                        <label for="presidente"><? echo lang("comunidades_presidente"); ?></label>
                                        <input type="text" name="presidente" id="presidente" value="<? echo ($sub == "editar") ? $fila['presidente'] : ''; ?>"/>
                                    </p>
                                </div>
                                <div class="col-50">
                                    <p>
                                        <label for="vicepresidente"><? echo lang("comunidades_vicepresidente"); ?></label>
                                        <input type="text" name="vicepresidente" id="vicepresidente" value="<? echo ($sub == "editar") ? $fila['vicepresidente'] : ''; ?>"/>
                                    </p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-30">
                        <fieldset>
                            <p>
                                <label for="usuario"><? echo lang("comunidades_usuario"); ?></label>
                                <input type="text" name="usuario" id="usuario" value="<? echo ($sub == "editar") ? $fila['usuario'] : ''; ?>"/>
                            </p>
                            <div class="password">
                                <div class="inputs">
                                    <p>
                                        <label for="password"><? echo lang("comunidades_password"); ?></label>
                                        <input type="password" name="password" id="password" value="" class="confirmar" placeholder="<? echo ($sub == "editar") ? '&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;' : ''; ?>" autocomplete="off">
                                    </p>
                                    <p>
                                        <label for="confirm_password"><? echo lang("comunidades_confirm_password"); ?></label>
                                        <input type="password" name="confirm_password" id="confirm_password" value="" placeholder="<? echo ($sub == "editar") ? '&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;' : ''; ?>" autocomplete="off">
                                    </p>
                                </div>
                                <div class="passstrength">
                                    <p>
                                        <em><? echo lang("fortaleza_pass"); ?></em>
                                        <strong id="passstrength"><? echo lang("vacio"); ?></strong>
                                    </p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div>
                    <fieldset>
                        <div class="row">
                            <div class="col-50"><? include("includes/single-image.php"); ?></div>
                            <div class="col-50">
                                <?php if ($sub == "editar") { ?>
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="box tile">
                                                <div class="t-icon right"><span class="bg-danger"></span><i class="ti-shopping-cart-full"></i></div>
                                                <div class="t-content">
                                                    <h1><?php echo lang("comunidades_tablon_anuncios") ?></h1>
                                                    <a href="index.php?sec=comunidades&sub=listar-tablon&id=<?php echo $id ?>" class="cover"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="box tile">
                                                <div class="t-icon right"><span class="bg-green"></span><i class="ti-shopping-cart-full"></i></div>
                                                <div class="t-content">
                                                    <h1><?php echo lang("comunidades_proveedores") ?></h1>
                                                    <a href="index.php?sec=comunidades&sub=editar-proveedores&id=<?php echo $id ?>" class="cover"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="box tile">
                                                <div class="t-icon right"><span class="bg-orange"></span><i class="ti-shopping-cart-full"></i></div>
                                                <div class="t-content">
                                                    <h1><?php echo lang("comunidades_incidencias") ?></h1>
                                                    <a href="index.php?sec=comunidades&sub=listar-incidencias&id=<?php echo $id ?>" class="cover"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <?php if ($sub == "editar") { ?>
                    <div>
                        <fieldset>
                            <div class="checkbox_group">
                                <? $instructions = array("activo");
                                instructions($instructions); ?>
                                <p class="checkbox">
                                    <input class="checkbox" type="checkbox" name="activo" id="activo" <? echo (isset($fila) && $fila['activo'] > 0) ? "checked='checked'" : ""; ?>/>
                                    <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                                </p>
                            </div>
                        </fieldset>
                    </div>
                <?php } ?>
            </form>
            <footer></footer>
        </section>
    </fieldset>
</section>