<?

$sql = "SELECT * FROM comunidades WHERE id=$id";
$comunidad_query = mysqli_query($link, $sql);
$comunidad = mysqli_fetch_array($comunidad_query);

$sql = "SELECT * FROM comunidades_incidencias WHERE parent = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND siniestro LIKE '%" . $search_term . "%'";
}
$sql .= " ORDER BY id DESC";
//echo $sql;
$consulta = mysqli_query($link, $sql);

$estados = get_data("SELECT * FROM comunidades_estados");
$sectores = get_data("SELECT * FROM comunidades_incidencias_sector");
$sec_title = "Tablón de incidencias: " . $comunidad["nombre"];
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=comunidades&sub=anyadir-incidencias&parent=$id";
$editComunidad = "index.php?sec=comunidades&sub=editar&id=$id";
$search_action = "comunidades_incidencias";
$tableDB = "comunidades_incidencias";


?>
<section class="comunidades">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <div class="edit_options">
                <a class="back" href="<?php echo $editComunidad ?>">< <? echo lang("volver") . " " . $comunidad["nombre"]; ?></a>
            </div>
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <div class="list_msk">
                    <table class="list">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="width: 100px;"><? echo lang("comunidades_id_incidencia"); ?></th>
                            <th style="width: 35%;"><? echo lang("comunidades_siniestro"); ?></th>
                            <th><? echo lang("comunidades_sector"); ?></th>
                            <th><? echo lang("comunidades_f_apertura"); ?></th>
                            <th><? echo lang("comunidades_f_finalizacion"); ?></th>
                            <th style="width: 100px;"><? echo lang("comunidades_estado"); ?></th>
                            <td><? echo lang("comunidades_activo"); ?></td>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                        <?
                        $i = 1;


                        while ($fila = mysqli_fetch_array($consulta)) {
                            $imagen = obtnener_imagen_idioma($sec, $fila["id"], $lang_default); ?>
                            <tr data-id="<? echo $fila['id']; ?>">
                                <td><? echo $i; ?>.</td>
                                <td data-field="id-incidencia"><a href="index.php?sec=comunidades&sub=editar-incidencias&id=<? echo $fila['id']; ?>"><?php echo $fila['idpublic'] ?></a></td>
                                <td data-field="siniestro"><? echo $fila["siniestro"] ?></td>
                                <td data-field="sector"><? echo $sectores[$fila["sector"]]["nombre"] ?></td>
                                <td data-field="f_apertura"><? echo $fila["f_apertura"] ?></td>
                                <td data-field="f_finalizacion"><? echo $fila["f_finalizacion"] ?></td>
                                <td data-field="estado"><span class="estado <? echo $estados[$fila["estado"]]["color"] ?>"><? echo $estados[$fila["estado"]]["nombre"] ?></span></td>
                                <td data-field="activo"><input class="checkbox activo_toggler" type="checkbox" <? echo ($fila['activo'] > 0) ? "checked='checked'":''; ?>/></td>
                                <td>
                                    <a href="index.php?sec=comunidades&sub=eliminar-incidencias&id=<? echo $fila['id']; ?>&parent=<?php echo $id; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                        <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                    </a>
                                </td>
                            </tr>
                            <? $i++;
                        } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="8">&nbsp;</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br/>
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>