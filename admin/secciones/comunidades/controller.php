<?php
$subDefault = "secciones/comunidades/list.php";

switch ($sub) {
    /*----------------------------------------------------------------- accions --*/
    case "anyadir-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $frm = $_POST;

        $id = anyadir_guardar($frm, $nombre, $cif, $n_cuenta, $n_propietarios, $direccion, $ano_construcion, $presidente, $vicepresidente, $imagen, $usuario, $codigo);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }

        header("location:$back_url");
        break;
    case "anyadir-guardar-tablon":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $frm = $_POST;

        $id = anyadir_guardar_tablon($frm, $nombre, $f_subida, $f_reunion, $doc, $parent, $activo);

        if ($go_back == "0") {
            $back_url = "index.php?sec=comunidades&sub=editar-tablon&id=$id";
        }

        header("location:$back_url");
        break;
    case "anyadir-guardar-incidencias":
        $frm = $_POST;

        $id = anyadir_guardar_incidencia($frm, $f_apertura, $f_finalizacion, $sector, $estado, $siniestro, $parent);

        if ($go_back == "0") {
            $back_url = "index.php?sec=comunidades&sub=editar-incidencias&id=$id";
        }

        header("location:$back_url");
        break;
    case "editar-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $frm = $_POST;

        editar_guardar($frm, $nombre, $cif, $n_cuenta, $n_propietarios, $direccion, $ano_construcion, $presidente, $vicepresidente, $usuario, $activo, $codigo, $id);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }

        header("location:$back_url");
        break;
    case "editar-guardar-tablon":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $frm = $_POST;

        editar_guardar_tablon($frm, $nombre, $f_subida, $f_reunion, $parent, $activo, $id);

        if ($go_back == "0") {
            $back_url = "index.php?sec=comunidades&sub=editar-tablon&id=$id";
        }

        header("location:$back_url");
        break;
    case "editar-guardar-proveedores":
        $frm = $_POST;

        editar_guardar_proveedores($frm, $id);

        if ($go_back == "0") {
            $back_url = "index.php?sec=comunidades&sub=editar-proveedores&id=$id";
        }

        header("location:$back_url");
        break;
    case "editar-guardar-incidencias":
        $frm = $_POST;

        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        editar_guardar_incidencia($frm, $f_apertura, $f_finalizacion, $sector, $estado, $siniestro, $activo, $id);

        if ($go_back == "0") {
            $back_url = "index.php?sec=comunidades&sub=editar-incidencias&id=$id";
        }

        header("location:$back_url");
        break;
    case "eliminar-incidencias":

        eliminar_incidencia($id);

        if (isset($parent)) {
            $back_url = "index.php?sec=comunidades&sub=listar-incidencias&id=$parent";
        } else {
            $back_url = "index.php?sec=comunidades&sub=listar-incidencias-informe";
        }

        header("location:$back_url");
        break;
    case "eliminar-tablon":

        eliminar_tablon($id);

        $back_url = "index.php?sec=comunidades&sub=listar-tablon&id=$parent";

        header("location:$back_url");
        break;
    case "eliminar":
        eliminar($subDefault, $id);
        $include = $subDefault;
        break;
//------------------subseccions------------------------------------
    case "editar":
    case "anyadir":
        $include = "secciones/comunidades/edit.php";
        break;
    case "editar-tablon":
    case "anyadir-tablon":
        $include = "secciones/comunidades/edit-tablon.php";
        break;
    case "editar-incidencias":
    case "anyadir-incidencias":
        $include = "secciones/comunidades/edit-incidencias.php";
        break;
    case "editar-proveedores":
        $include = "secciones/comunidades/edit-proveedores.php";
        break;
    case "listar-incidencias":
        $include = "secciones/comunidades/list-incidencias.php";
        break;
    case "listar-incidencias-informe":
        $include = "secciones/comunidades/list-incidencias-informe.php";
        break;
    case "listar-tablon":
        $include = "secciones/comunidades/list-tablon.php";
        break;
    default:
        $include = $subDefault;
        break;
}
