<?php
/* -- select categoria -- */

function print_level($level) {
    $space = "";
    if ($level > 0) {
        for ($x = 0; $x <= $level; $x++) {
            $space .= "&nbsp;&nbsp;&nbsp;";
        }
        return $space;
    } else {
        return "- ";
    }
}

function genera_pagina_item($row_categorias, $id_actual = "", $level = 0) {
    global $lang_default;
    global $parent;
    $id = $row_categorias['id'];
    if ($id_actual == "" || $id != $id_actual) {
        ?>
        <option value="<? echo $id; ?>" <?
        if ($parent == $id) {
            echo "selected='selected'";
        }
        ?>>
                    <? echo print_level($level) . obtener_valor($lang_default, "videos_categoria", "descripcion", $id); ?>
        </option>
        <?
    }
}

function genera_pagina_item_optgroup($id, $level = 0) {
    global $lang_default;
    ?>
    <optgroup label="<? echo print_level($level) . obtener_valor($lang_default, "videos_categoria", "descripcion", $id); ?>"></optgroup>
    <?
}

function crea_item_menu($id_cat, $id_actual, $level) {
    global $link;
    if ($id_actual != "" && $id_cat == $id_actual) {
        echo genera_pagina_item_optgroup($id_actual, $level);
    } else {
        $level++;
        $sql = "SELECT * FROM videos_categoria WHERE parent = $id_cat ORDER BY orden ASC";
        $result = mysqli_query($link, $sql);
        $totals = mysqli_num_rows($result);
        if ($totals > 0) {
            while ($row = mysqli_fetch_array($result)) {
                echo genera_pagina_item($row, $id_actual, $level);
                $itemMenu = crea_item_menu($row['id'], $id_actual, $level);
            }
        }
    }
    return;
}

function anyadir_guardar($frm, $video, $categoria, $fecha_public, $fecha_caduca, $activo, $cat, $parent, $lenguajes) {
    global $link;
    $url = "";

    $sql = "INSERT INTO videos (activo,video,fecha_public,fecha_caduca,categoria) VALUES ($activo,'$video','$fecha_public', '$fecha_caduca', $categoria)";
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "videos", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "videos", "titulo", $frm["titulo_" . $leng], $id);
    }
    $orden = get_last_order("videos");
    update_order($orden, $id, "videos");
    return $id;
}

function editar_guardar($frm, $video, $parent, $fecha_public, $fecha_caduca, $activo, $lenguajes, $id) {
    global $link;

    $sql = "UPDATE videos SET activo = $activo, video = '$video', fecha_public = '$fecha_public', fecha_caduca = '$fecha_caduca', categoria = $parent WHERE id = $id";
    echo $sql;
    mysqli_query($link, $sql);

    insert_log("UPDATE", "videos", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "videos", "titulo", $frm["titulo_" . $leng], $id);
    }
}

function eliminar($id) {
    global $link;

    elimina_idiomas($id, "videos", "videos");
    elimina_metas($id, "videos", "videos");

    $sql = "DELETE FROM videos WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "videos", $sql);
}

function anyadir_guardar_cat($frm, $parent, $lenguajes, $activo) {
    global $link;

    $sql = "INSERT INTO videos_categoria (activo,parent) VALUES ($activo,$parent)";
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "videos_categoria", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "videos_categoria", "descripcion", $frm["descripcion_" . $leng], $id);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "videos_categoria",
            "seccion_parents" => "videos_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }
    $orden = get_last_order("videos_categoria");
    update_order($orden, $id, "videos_categoria");
    return $id;
}

function editar_guardar_cat($frm, $parent, $lenguajes, $activo, $id) {
    global $link;

    $sql = "UPDATE videos_categoria SET parent=$parent, activo=$activo WHERE id = $id";
//    echo $sql;
    mysqli_query($link, $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "videos_categoria", "descripcion", $frm["descripcion_" . $leng], $id);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "videos_categoria",
            "seccion_parents" => "videos_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }
    insert_log("UPDATE", "videos_categoria", $sql);
}

function eliminar_cat($id) {
    global $link;

    $sql = "SELECT id FROM videos WHERE categoria = $id";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        eliminar($fila['id']);
    }

    elimina_idiomas($id, "videos_categoria", "videos_categoria");
    eliminar_seo("videos_categoria", $id);

    $sql = "DELETE FROM videos_categoria WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "videos_categoria", $sql);
}
