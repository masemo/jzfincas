<?
if ($sub == "editar-video") {

    $accion_sub = "editar-guardar-video";
    $sec_title = "vídeo";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM videos WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $parent = $fila["categoria"];
    /* -- anterior siguiente -- */
    $item_ant = obtener_sig_or_ant("videos", $id, "orden", "ANT", "ASC", " WHERE categoria = $parent ");
    $item_sig = obtener_sig_or_ant("videos", $id, "orden", "SIG", "ASC", " WHERE categoria = $parent ");

    $id_cat = $id;

    if ($item_ant > -1) {
        $link_ant = "index.php?sec=videos&sub=editar-video&parent=$parent&id=" . $item_ant;
        $title_ant = obtener_valor($lang_default, $sec, "titulo", basic_query("id", $sec, $item_ant));
        $img_ant = "";
    }
    if ($item_sig > -1) {
        $link_sig = "index.php?sec=videos&sub=editar-video&parent=$parent&id=" . $item_sig;
        $title_sig = obtener_valor($lang_default, $sec, "titulo", basic_query("id", $sec, $item_sig));
        $img_sig = "";
    }
} else {
    $id_cat = "";
    $accion_sub = "anyadir-guardar-video";
    $sec_title = "vídeo";
    $sec_action = lang("anyadir_nuevo");
}

$back_url = "index.php?sec=videos&sub=listado-videos&id=$parent";
$sql_categorias_videos = "SELECT * FROM videos_categoria WHERE parent = 0 ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $sql_categorias_videos);

$breadcrumbs = migas_videos($parent, "listado-videos");
$breadcrumbs_home = "videos";
?>
<section class="videos">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/breadcrumbs.php"); ?>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="videos" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="cat" id="cat" value="<?
                if (isset($cat)) {
                    echo $cat;
                } else {
                    echo 0;
                }
                ?>" />
                <input type="hidden" name="parent" id="parent" value="<?
                if (isset($cat)) {
                    echo $parent;
                } else {
                    echo 0;
                }
                ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />

                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="titulo_<? echo $leng; ?>"><? echo lang("titulo"); ?></label>
                                        <input type="text" name="titulo_<? echo $leng; ?>" id="titulo_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar-video") {
                                            echo obtener_valor($leng, $sec, "titulo", $id);
                                        }
                                        ?>" <? require_lang_field($leng, true); ?> />
                                    </p>
                                </fieldset>
                            <? } ?>
                        </div>
                        <br />
                        <p>
                            <label for="video"><? echo lang("url"); ?></label>
                            <input type="text" name="video" id="video" value="<?
                            if ($sub == "editar-video") {
                                echo $fila['video'];
                            }
                            ?>" class="required"/>
                        </p>
                        <?
                        $instructions = array("video");
                        instructions($instructions);
                        ?>
                        <br />
                        <?
                        if (isset($fila) && $fila['video'] != "") {
                            embed_video($fila['video'], 450, 340);
                        }
                        ?>
                    </div>
                    <div class="col-40">
                        <p>
                            <label for="parent"><? echo lang("categoria"); ?></label>
                            <select id="parent" name="parent">
                                <?
                                while ($fila_cats = mysqli_fetch_array($consulta_cats)) {
                                    if ($sub == "anyadir") {
                                        $parent = $id;
                                    }
                                    echo genera_pagina_item($fila_cats, $id_cat);
                                    echo crea_item_menu($fila_cats['id'], $id_cat, 0);
                                }
                                ?>
                            </select>
                        </p>

                        <div class="input_group">
                            <p>
                                <label for="fecha_public"><? echo lang("publicar"); ?></label>
                                <input type="text" name="fecha_public" id="fecha_public" placeholder="00/00/0000" class="date datepicker required" value="<?
                                if ($sub == "editar-video") {
                                    echo fecha_to_view($fila['fecha_public']);
                                } else {
                                    echo date("d/m/Y");
                                }
                                ?>"/>
                            </p>
                            <p>
                                <label for="fecha_caduca"><? echo lang("caduca"); ?></label>
                                <input type="text" name="fecha_caduca" id="fecha_caduca" placeholder="00/00/0000" class="date datepicker" value="<?
                                if ($sub == "editar-video") {
                                    if ($fila['fecha_caduca'] != "0000-00-00") {
                                        echo fecha_to_view($fila['fecha_caduca']);
                                    }
                                }
                                ?>"/>
                            </p>
                        </div>
                    </div>
                </div>
                <fieldset>
                    <div class="checkbox_group">
                        <?
                        $instructions = array("activo");
                        instructions($instructions);
                        ?>
                        <p class="checkbox">
                            <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                            if ($fila['activo'] > 0) {
                                echo "checked='checked'";
                            }
                            ?>/>
                        </p>
                    </div>
                </fieldset>
            </form>
            <footer>
                <?
                if ($sub == "editar-video") {
                    include("includes/item-anterior-siguiente.php");
                }
                ?>
            </footer>
        </section>
    </div>
</section>