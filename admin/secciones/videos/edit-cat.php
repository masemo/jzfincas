<?
if ($sub == "editar-cat") {

    $accion_sub = "editar-guardar-cat";
    $sec_title = "vídeos";
    $sec_action = lang("editar_carpeta");
    $id_cat = $id;
    $sql = "SELECT * FROM videos_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
} else {
    $accion_sub = "anyadir-guardar-cat";
    $sec_title = "vídeos";
    $sec_action = lang("anyadir_nueva_carpeta");
    $id_cat = "";
    $id = $parent;
}

$sql = "SELECT * FROM videos_categoria WHERE parent = 0 ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $sql);

$back_url = "index.php?sec=videos&id=$parent";

$breadcrumbs = migas_videos($parent, "listado-videos");
$breadcrumbs_home = "videos";
?>
<section class="videos">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/breadcrumbs.php"); ?>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="videos" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />

                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="descripcion_<? echo $leng; ?>"><? echo lang("carpeta"); ?></label>
                                        <input type="text" class="seo_title seo_friendly_url seo_description <? require_lang_field($leng); ?>" name="descripcion_<? echo $leng; ?>" id="descripcion_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar-cat") {
                                            echo obtener_valor($leng, "videos_categoria", "descripcion", $id);
                                        }
                                        ?>" />
                                    </p>
                                </fieldset>
                            <? } ?>
                        </div>
                        <br />
                        <p>
                            <label for="parent"><? echo lang("carpeta_superior"); ?></label>
                            <select id="parent" name="parent">
                                <option value="0" <?
                                if ((0) == $parent) {
                                    echo "SELECTED";
                                }
                                ?>><? echo lang("inicio"); ?></option>
                                <?
                                while ($fila_cats = mysqli_fetch_array($consulta_cats)) {
                                    if (isset($fila)) {
                                        $parent = $fila["parent"];
                                    }
                                    echo genera_pagina_item($fila_cats, $id_cat);
                                    echo crea_item_menu($fila_cats['id'], $id_cat, 0);
                                }
                                ?>
                            </select>
                        </p>
                        <div class="checkbox_group">
                            <?
                            $instructions = array("activo");
                            instructions($instructions);
                            ?>
                            <p class="checkbox">
                                <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                                <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                                if (isset($fila) && $fila['activo'] > 0) {
                                    echo "checked='checked'";
                                }
                                ?>/>
                            </p>
                        </div>
                    </div>
                    <div class="col-40">
                        <?
                        $openPlugin = true;
                        $seoPlugin = "includes/plugins/seo/index.php";
                        if (is_file($seoPlugin)) {
                            $sec = "videos_categoria";
                            include($seoPlugin);
                        }
                        ?>
                    </div>
                </div>
            </form>
        </section>
    </div>
</section>