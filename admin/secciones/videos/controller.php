<?php

require "lib/embed_video/embed_video.php";

$subDefault = "secciones/videos/list.php";

switch ($sub) {
    /* ----------------------------------------------------------------- accions -- */
    case "anyadir-guardar-video":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $fecha_public = fecha_to_mysql($fecha_public);
        if ($fecha_caduca != "") {
            $fecha_caduca = fecha_to_mysql($fecha_caduca);
        }

        $frm = $_POST;

        $id = anyadir_guardar($frm, $video, $parent, $fecha_public, $fecha_caduca, $activo, $cat, $parent, $lenguajes);

        if ($go_back == "0") {
            $back_url = "&sub=editar-video&id=$id&parent=$parent";
        }
        header("location:$back_url");
        break;
    case "editar-guardar-video":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $fecha_public = fecha_to_mysql($fecha_public);
        if ($fecha_caduca != "") {
            $fecha_caduca = fecha_to_mysql($fecha_caduca);
        }

        $frm = $_POST;

        editar_guardar($frm, $video, $parent, $fecha_public, $fecha_caduca, $activo, $lenguajes, $id);
        if ($go_back == "0") {
            $back_url = "&sub=editar-video&id=$id&parent=$parent";
        }
        header("location:$back_url");
        break;
    case "eliminar-video":
        $subDefault = "&sub=listado-videos&id=$cat&parent=0";
        eliminar($id);
        header("location:$subDefault");
        break;
    case "anyadir-guardar-cat":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $frm = $_POST;

        $id = anyadir_guardar_cat($frm, $parent, $lenguajes, $activo);
        if ($go_back == "0") {
            $back_url .= "&sub=editar-cat&id=$id&parent=$parent";
        }

        header("location:$back_url");
        break;
    case "editar-guardar-cat":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $frm = $_POST;

        editar_guardar_cat($frm, $parent, $lenguajes, $activo, $id);
        if ($go_back == "0") {
            $back_url .= "&sub=editar-cat&id=$id&parent=$parent";
        }

        header("location:$back_url");
        break;
    case "eliminar-cat":
        $subDefault = "index.php?sec=videos&id=$cat&parent=0";
        eliminar_cat($id);
        header("location:$subDefault");
        break;
//------------------subseccions------------------------------------    
    case "editar-video":
    case "anyadir-video":
        $include = "secciones/videos/edit.php";
        break;
    case "editar-cat":
    case "anyadir-cat":
        $include = "secciones/videos/edit-cat.php";
        break;
    case "listado-videos":
        $include = "secciones/videos/list-videos.php";
        break;
    default:
        $include = $subDefault;
        break;
}
?>
