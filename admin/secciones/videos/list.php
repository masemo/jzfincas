<?
if (!isset($id) || $id == "") {
    $id = 0;
}
if (!isset($parent) || $parent == "") {
    $parent = 0;
}
$sql = "SELECT * FROM videos_categoria WHERE videos_categoria.parent = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND id IN (SELECT id_seccion FROM idiomas WHERE texto LIKE '%" . $search_term . "%' AND seccion='videos_categoria')";
}
$sql .= " ORDER BY videos_categoria.orden ASC, videos_categoria.id ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);
$tiene_videos = $videos = tiene_videos($id);
$tiene_subcats = tiene_subcategorias_videos($id);

$sec_title = "Videos";
$nItems = mysqli_num_rows($consulta);

/*
if (($tiene_subcats > 0 || $tiene_videos == 0)) {
    $addLink = "index.php?sec=videos&sub=anyadir-cat&parent=$id";
}
if (($tiene_videos >= 0 && $tiene_subcats == 0)) {
    $addLink = "index.php?sec=videos&sub=anyadir-video&parent=$id";
}
 */

$addLink = "";

$search_action = "videos";

$breadcrumbs = migas_videos($id, "listado");
$breadcrumbs_home = "videos";
$tableDB = "videos_categoria";
?>
<section class="videos">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <? include("includes/breadcrumbs.php"); ?>  
        </header>
         <div class="filter_group">
            <div class="filter button_group">
                <a href="index.php?sec=videos&sub=anyadir-cat&parent=<? echo $id; ?>" class="button anyadir"><? echo lang("anyadir_carpeta"); ?></a>
            </div>
            <? if (!$tiene_subcats) { ?>
            <div class="filter button_group">
                <a href="index.php?sec=videos&sub=anyadir-video&parent=<? echo $id; ?>" class="button anyadir"><? echo lang("anyadir_video"); ?></a>
            </div>
            <? } ?>
        </div>
        <section>
            <? if ($nItems > 0) { ?>
                <? 
                    $instructions = array("move");
                    instructions($instructions); 
                ?>
                <div class="list_msk">
                    <table class="list drag_sortable ">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("carpeta"); ?></th>
                                <th style="width:70px;"><? echo lang("activa"); ?></th>
                                <th style="width:50px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $subcategorias = tiene_subcategorias_videos($fila['id']);
                                $videos = tiene_videos($fila['id']);
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title">
                                        <? if ($videos > 0) { ?>
                                            <a href="index.php?sec=videos&sub=listado-videos&id=<? echo $fila['id']; ?>&parent=<? echo $fila['parent']; ?>" class="img_carpeta_galeria">
                                                <span class="icon"></span>
                                                <? echo obtener_valor($lang_default, "videos_categoria", "descripcion", $fila['id']); ?> 
                                                <span class="subtle">(<? echo $videos." ".lang("videos"); ?> )</span>
                                            </a>
                                        <? } else { 
                                            if($subcategorias > 0){
                                                $nCats_txt = $subcategorias." ".lang("subcategorias");
                                            }else{
                                                $nCats_txt = lang("vacio");
                                            }
                                            ?>
                                            <a href="index.php?sec=videos&id=<? echo $fila['id']; ?>&parent=<? echo $fila['parent']; ?>" class="img_carpeta">
                                                <span class="icon"></span>
                                                <? echo obtener_valor($lang_default, "videos_categoria", "descripcion", $fila['id']); ?> 
                                                <span class="subtle">(<? echo $nCats_txt; ?>)</span>
                                            </a>
                                        <? } ?>    
                                    </td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>                                    
                                        <a href="index.php?sec=videos&sub=editar-cat&id=<? echo $fila['id']; ?>&parent=<? echo $fila['parent']; ?>" class="edit">
                                            <span class="icon">::<? echo lang("editar"); ?>::</span>
                                        </a>
                                    </td>
                                    <td>
                                        <? if ($subcategorias < 1) { ?>
                                            <a href="index.php?sec=videos&sub=eliminar-cat&id=<? echo $fila['id']; ?>&cat=<? echo $id; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                                <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                            </a>
                                        <? } ?>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>