<?
if (!isset($id) || $id == "") {
    $id = 0;
}
if (!isset($parent) || $parent == "") {
    $parent = 0;
}
$sql = "SELECT * FROM videos WHERE categoria = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND id IN (SELECT id_seccion FROM idiomas WHERE texto LIKE '%" . $search_term . "%' AND seccion='videos')";
}
$sql .= " ORDER BY orden ASC, id ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);

$tiene_videos = $videos = tiene_videos($id);
$tiene_subcats = tiene_subcategorias_videos($id);

$sec_title = "Videos";
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=videos&sub=anyadir-video&parent=$id";
$search_action = "videos&sub=listado-videos&id=$id&parent=$parent";

$breadcrumbs = migas_videos($id, "listado-videos");
$breadcrumbs_home = "videos";
$tableDB = "videos";
?>
<section class="banners">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <? include("includes/breadcrumbs.php"); ?>  
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <?
                $instructions = array("move");
                instructions($instructions);
                ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("video"); ?></th>
                                <th><? echo lang("titulo"); ?></th>
                                <th><? echo lang("publicar"); ?></th>
                                <th><? echo lang("caduca"); ?></th>
                                <th style="width: 50px;" class="sorter-false"><? echo lang("activo"); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $subcategorias = tiene_subcategorias_videos($fila['id']);
                                $videos = tiene_videos($fila['id']);
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td>
                                        <a href="index.php?sec=videos&sub=editar-video&id=<? echo $fila['id']; ?>&cat=<? echo $id; ?>&parent=<? echo $parent; ?>">
                                            <img src="<? echo url_miniatura($fila['video'], "small"); ?>" alt="video"/>
                                        </a>
                                    </td>
                                    <td data-field="title"><a href="index.php?sec=videos&sub=editar-video&id=<? echo $fila['id']; ?>&cat=<? echo $id; ?>&parent=<? echo $parent; ?>"><? echo obtener_valor($lang_default, "videos", "titulo", $fila['id']); ?></td>
                                    <td <? dataPublicada($fila['fecha_public'], "public"); ?> ><? echo fecha_to_view($fila['fecha_public']); ?></td>
                                    <td <?
                                    if ($fila['fecha_caduca'] != "0000-00-00") {
                                        dataPublicada($fila['fecha_caduca'], "caduca");
                                    }
                                    ?> >
                                            <?
                                            if ($fila['fecha_caduca'] != "0000-00-00") {
                                                echo fecha_to_view($fila['fecha_caduca']);
                                            } else {
                                                echo "-";
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=videos&sub=eliminar-video&id=<? echo $fila['id']; ?>&cat=<? echo $id; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>