<?php
/* -- categoria productos -- */

function print_level($level) {
    $space = "";
    if ($level > 0) {
        for ($x = 0; $x <= $level; $x++) {
            $space .= "&nbsp;&nbsp;&nbsp;";
        }
        return $space;
    } else {
        return "- ";
    }
}

function genera_pagina_item($row_categorias, $id_actual = "", $level = 0) {
    global $lang_default;
    global $parent;
    $id = $row_categorias['id'];
    if ($id_actual == "" || $id != $id_actual) {
        ?>
        <option value="<? echo $id; ?>" <?
        if ($parent == $id) {
            echo "selected='selected'";
        }
        ?>>
                    <? echo print_level($level) . obtener_valor($lang_default, "productos_categoria", "descripcion", $id); ?>
        </option>
        <?
    }
}

function genera_pagina_item_optgroup($id, $level = 0) {
    global $lang_default;
    ?>
    <optgroup label="<? echo print_level($level) . obtener_valor($lang_default, "productos_categoria", "descripcion", $id); ?>"></optgroup>
    <?
}

function crea_item_menu($id_cat, $id_actual, $level) {
    global $link;
    if ($id_actual != "" && $id_cat == $id_actual) {
        echo genera_pagina_item_optgroup($id_actual, $level);
    } else {
        $level++;
        $sql = "SELECT * FROM productos_categoria WHERE parent = $id_cat ORDER BY orden ASC";
        $result = mysqli_query($link, $sql);
        $totals = mysqli_num_rows($result);
        if ($totals > 0) {
            while ($row = mysqli_fetch_array($result)) {
                echo genera_pagina_item($row, $id_actual, $level);
                $itemMenu = crea_item_menu($row['id'], $id_actual, $level);
            }
        }
    }
    return;
}

function editar_guardar_cat($frm, $id, $parent, $imagen, $imagen_old, $id_imagen_old, $lenguajes) {
    global $link;

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "productos_categoria");
    } else {
        if ($id_imagen_old != "") {
            $imagen = $id_imagen_old;
        } else {
            $imagen = 0;
        }
    }

    $sql = "UPDATE productos_categoria SET parent=$parent, imagen=$imagen WHERE id = $id";
//echo $sql;
    mysqli_query($link, $sql);

    insert_log("UPDATE", "productos_categoria", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        //nombre
        inserta_campo_idiomas($leng, "productos_categoria", "descripcion", $frm["descripcion_" . $leng], $id);
        //descripcion
        inserta_campo_idiomas($leng, "productos_categoria", "contenido", $frm["contenido_" . $leng], $id, 0, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "productos_categoria",
            "seccion_parents" => "productos_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "contenido_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }
}

function anyadir_guardar_cat($frm, $parent, $imagen, $lenguajes) {
    global $link;

    $sql = "INSERT INTO productos_categoria (activo,parent) VALUES (1,$parent)";
//echo $sql;
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "productos_categoria", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        //nombre
        inserta_campo_idiomas($leng, "productos_categoria", "descripcion", $frm["descripcion_" . $leng], $id);
        //descripcion
        inserta_campo_idiomas($leng, "productos_categoria", "contenido", $frm["contenido_" . $leng], $id, 0, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "productos_categoria",
            "seccion_parents" => "productos_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "contenido_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    if ($imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "productos_categoria");
    } else {
        $imagen = 0;
    }
    $orden = get_last_order("productos_categoria");
    $sql = "UPDATE productos_categoria SET imagen = $imagen, orden = $orden WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "imagenes", $sql);
    return $id;
}

function anyadir_guardar($frm, $parent, $imagen, $precio, $referencia, $activo, $destacado, $lenguajes) {
    global $link;

    $sql = "INSERT INTO productos (destacado,precio,referencia,activo,categoria) VALUES ($destacado,'$precio','$referencia',$activo,$parent)";
//    echo $sql;
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "productos", "nombre", $frm["nombre_" . $leng], $id);

        inserta_campo_idiomas($leng, "productos", "descripcion", $frm["descripcion_" . $leng], $id, 0, true);
        inserta_campo_idiomas($leng, "productos", "caracteristicas", $frm["caracteristicas_" . $leng], $id, 0, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "productos",
            "seccion_parents" => "productos_categoria",
            "fu_field" => "nombre_",
            "title_field" => "nombre_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    insert_log("INSERT", "productos", $sql);

    if ($imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "productos");
    } else {
        $imagen = 0;
    }

    $orden = get_last_order("productos");
    $sql = "UPDATE productos SET imagen = $imagen, orden = $orden WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "imagenes", $sql);

    return $id;
}

function editar_guardar($frm, $parent, $imagen, $imagen_old, $id_imagen_old, $precio, $referencia, $activo, $destacado, $id, $lenguajes) {
    global $link;

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "productos");
    } else {
        if ($id_imagen_old != "") {
            $imagen = $id_imagen_old;
        } else {
            $imagen = 0;
        }
    }

    $sql = "UPDATE productos SET categoria=$parent, imagen=$imagen, precio='$precio', referencia='$referencia', activo=$activo, destacado=$destacado WHERE id = $id";
//echo $sql;
    mysqli_query($link, $sql);

    insert_log("UPDATE", "productos", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "productos", "nombre", $frm["nombre_" . $leng], $id);
        inserta_campo_idiomas($leng, "productos", "descripcion", $frm["descripcion_" . $leng], $id, 0, true);
        inserta_campo_idiomas($leng, "productos", "caracteristicas", $frm["caracteristicas_" . $leng], $id, 0, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "productos",
            "seccion_parents" => "productos_categoria",
            "fu_field" => "nombre_",
            "title_field" => "nombre_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }
}

function eliminar_producto($id) {
    global $link;

    $sql = "DELETE FROM productos WHERE id = " . $id;
    mysqli_query($link, $sql);

    eliminar_img_multiples($id, "productos");
    eliminar_img_multiples($id, "productos-multi");
    elimina_idiomas($id, "productos", "productos");
    eliminar_seo("productos", $id);

    insert_log("DELETE", "productos", $sql);
}

function eliminar_categoria($id_categoria) {
    eliminar_img_multiples($id_categoria, "productos_categoria");
    elimina_idiomas($id_categoria, "productos_categoria", "productos_categoria");
    eliminar_seo("productos_categoria", $id_categoria);
    elimina_item_by_id("productos_categoria", $id_categoria);
}

function eliminar_cat($id) {
    global $link;
    $sql_cat = "SELECT * FROM productos_categoria WHERE parent = $id";
    $consulta_cat = mysqli_query($link, $sql_cat);
    $nCats = mysqli_num_rows($consulta_cat);
    if ($nCats > 0) {
        while ($fila_cats = mysqli_fetch_array($consulta_cat)) {
            eliminar_cat($fila_cats['id']);
            eliminar_categoria($fila_cats['id']);
        }
    }
    $sql = "SELECT * FROM productos WHERE categoria = $id";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        eliminar_producto($fila['id']);
    }
    eliminar_categoria($id);

    insert_log("DELETE", "productos_categoria", $sql);
}
