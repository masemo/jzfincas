<?
if ($sub == "editar-cat") {
    $accion_sub = "editar-guardar-cat";
    $sec_action = lang("editar_categoria");
    $id_cat = $id;
    $sql = "SELECT * FROM productos_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
} else {
    $accion_sub = "anyadir-guardar-cat";
    $sec_action = lang("anyadir_nueva_categoria");
    $id_cat = "";
    $id = $parent;
}
$sec_title = "productos";

$query_cats = "SELECT * FROM productos_categoria WHERE parent = 0 ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $query_cats);

$back_url = "index.php?sec=productos&id=" . $parent;
$breadcrumbs = migas_de_pan_productos($id);
$breadcrumbs_home = "productos";
?>
<section class="productos">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/breadcrumbs.php"); ?>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="productos" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <input type="hidden" name="imagen_old" id="imagen_old" value="<?
                if (isset($fila)) {
                    echo $fila["imagen"];
                }
                ?>" />
                <input type="hidden" name="idprincipal" id="idprincipal" value="<?
                if (isset($fila)) {
                    echo $fila["imagen"];
                }
                ?>" />
                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="descripcion_<? echo $leng; ?>"><? echo lang("nombre_minus"); ?></label>
                                        <input type="text" class="seo_title seo_friendly_url <? require_lang_field($leng); ?> " name="descripcion_<? echo $leng; ?>" id="descripcion_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar-cat") {
                                            echo obtener_valor($leng, "productos_categoria", "descripcion", $id);
                                        }
                                        ?>" />
                                    </p>
                                    <p>
                                        <label for="contenido_<? echo $leng; ?>">
                                            <? echo lang("descripcion"); ?>
                                            <a href="<? echo ckeditor_manual_url($lang_default); ?>" class="manual" target="blank">::<? echo lang("manual_editor"); ?>::</a>
                                        </label>
                                        <textarea  <? if ($estatica) { ?>readonly="readonly"<? } ?> cols="" rows="" class="ckeditor newsletter seo_description <? if ($sub != "editar-cat") { ?>vacio<? } ?>" id="contenido_<? echo $leng; ?>" name="contenido_<? echo $leng; ?>" cols="0" rows="0"><?
                                            if ($sub == "editar-cat") {
                                                echo obtener_valor($leng, "productos_categoria", "contenido", $id);
                                            }
                                            ?></textarea>
                                    </p>
                                </fieldset>
                                <?
                                unset($lenguaje);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-40">
                        <p>
                            <label for="parent"><? echo lang("categoria_padre"); ?></label>
                            <select id="parent" name="parent">
                                <option value="0" <?
                                if ((0) == $parent) {
                                    echo "selected='selected'";
                                }
                                ?>><? echo lang("inicio"); ?></option>
                                        <?
                                        while ($fila_cats = mysqli_fetch_array($consulta_cats)) {
                                            if (isset($fila)) {
                                                $parent = $fila["parent"];
                                            }
                                            echo genera_pagina_item($fila_cats, $id_cat);
                                            echo crea_item_menu($fila_cats['id'], $id_cat, 0);
                                        }
                                        ?>
                            </select>
                        </p>
                        <?
                        $openPlugin = true;
                        $seoPlugin = "includes/plugins/seo/index.php";
                        if (is_file($seoPlugin)) {
                            $sec = "productos_categoria";
                            include($seoPlugin);
                        }
                        ?>
                    </div>
                </div>
                <fieldset>
                    <? include("includes/single-image.php"); ?>
                    <div class="checkbox_group">
                        <p class="checkbox">
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                                   if (!isset($fila) || isset($fila) && $fila['activo'] > 0) {
                                       echo "checked='checked'";
                                   }
                                   ?>/>
                            <label for="activo"><? echo lang("activo"); ?></label>
                        </p>
                    </div>
                </fieldset>
            </form>
        </section>
    </div>
</section>