<?php

$subDefault = "secciones/productos/list.php";

switch ($sub) {
    /* ----------------------------------------------------------------- accions -- */
    case "anyadir-guardar-cat":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        $id = anyadir_guardar_cat($frm, $parent, $imagen, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar-cat&id=$id&parent=$parent";
        } else {
            $back_url .= "&id=$parent";
        }

        header("location:$back_url");
        break;
    case "editar-guardar-cat":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        editar_guardar_cat($frm, $id, $parent, $imagen, $imagen_old, $id_imagen_old, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar-cat&id=$id&parent=$parent";
        }
        header("location:$back_url");
        break;

    case "eliminar-cat":
        eliminar_cat($id);
        header("location:?sec=productos&sub=listado&id=$parent");
        break;

    case "eliminar":
        eliminar_producto($id);
        if($parent == 0){
            header("location:?sec=productos");
        }else{
            header("location:?sec=productos&id=$parent");
        }
        break;

    case "anyadir-guardar":
        if (isset($destacado) && $destacado != "") {
            $destacado = 1;
        } else {
            $destacado = 0;
        }
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        $id = anyadir_guardar($frm, $parent, $imagen, $precio, $referencia, $activo, $destacado, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id&parent=$parent";
        }

        header("location:$back_url");
        break;

    case "editar-guardar":
        if (isset($destacado) && $destacado != "") {
            $destacado = 1;
        } else {
            $destacado = 0;
        }
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        editar_guardar($frm, $parent, $imagen, $imagen_old, $id_imagen_old, $precio, $referencia, $activo, $destacado, $id, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id&parent=$parent";
        }

        header("location:$back_url");
        break;
//------------------subseccions------------------------------------    
    case "editar":
    case "anyadir":
        $include = "secciones/productos/edit-prods.php";
        break;
    case "listado-productos":
        $include = "secciones/productos/list-prods.php";
        break;
    case "editar-cat":
    case "anyadir-cat":
        $include = "secciones/productos/edit.php";
        break;
    default:
        $include = $subDefault;
        break;
}
