<?
if ($sub == "editar") {
    $accion_sub = "editar-guardar";
    $sec_title = "producto";
    $sec_action = lang("editar");
    $id_cat = $id;
    $sql = "SELECT * FROM productos WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
} else {
    $accion_sub = "anyadir-guardar";
    $sec_title = "producto";
    $sec_action = lang("anyadir");
    $id_cat = "";
//    $id = $parent;
}

$query_cats = "SELECT * FROM productos_categoria WHERE parent = 0 ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $query_cats);

$back_url = "?sec=productos&id=" . $parent;
$breadcrumbs = migas_de_pan_productos($parent);
$breadcrumbs_home = "productos";
?>
<section class="productos">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/breadcrumbs.php"); ?>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="productos" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />

                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="nombre_<? echo $leng; ?>"><? echo lang("nombre_minus"); ?></label>
                                        <input type="text" class="seo_title seo_friendly_url <? require_lang_field($leng); ?>" name="nombre_<? echo $leng; ?>" id="nombre_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar") {
                                            echo obtener_valor($leng, "productos", "nombre", $id);
                                        }
                                        ?>" />
                                    </p>
                                    <p>
                                        <label for="descripcion_<? echo $leng; ?>">
                                            <? echo lang("descripcion"); ?>
                                            <a href="<? echo ckeditor_manual_url($lang_default); ?>" class="manual" target="blank">::<? echo lang("manual_editor"); ?>::</a>
                                        </label>
                                        <textarea cols="" rows="" class="ckeditor newsletter seo_description <? if ($sub != "editar") { ?>vacio<? } ?>" id="descripcion_<? echo $leng; ?>" name="descripcion_<? echo $leng; ?>" cols="0" rows="0"><?
                                            if ($sub == "editar") {
                                                echo obtener_valor($leng, $sec, "descripcion", $id);
                                            }
                                            ?></textarea>
                                    </p>
                                    <p>
                                        <label for="caracteristicas_<? echo $leng; ?>">
                                            <? echo lang("caracteristicas"); ?>
                                            <a href="<? echo ckeditor_manual_url($lang_default); ?>" class="manual" target="blank">::<? echo lang("manual_editor"); ?>::</a>
                                        </label>
                                        <textarea cols="" rows="" class="ckeditor newsletter <? if ($sub != "editar") { ?>vacio<? } ?>" id="caracteristicas_<? echo $leng; ?>" name="caracteristicas_<? echo $leng; ?>" cols="0" rows="0"><?
                                            if ($sub == "editar") {
                                                echo obtener_valor($leng, $sec, "caracteristicas", $id);
                                            }
                                            ?></textarea>
                                    </p>
                                </fieldset>
                                <?
                                unset($lenguaje);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-40">
                        <p>
                            <label for="parent"><? echo lang("categoria_padre"); ?></label>
                            <select id="parent" name="parent">
                                <option value="0" <?
                                if ((0) == $parent) {
                                    echo "SELECTED";
                                }
                                ?>><? echo lang("inicio"); ?></option>
                                        <?
                                        while ($fila_cats = mysqli_fetch_array($consulta_cats)) {
                                            if (isset($fila)) {
                                                $parent = $fila["categoria"];
                                            }
                                            echo genera_pagina_item($fila_cats, $id_cat);
                                            echo crea_item_menu($fila_cats['id'], $id_cat, 0);
                                        }
                                        ?>
                            </select>
                        </p>
                        <?
                        $openPlugin = true;
                        $seoPlugin = "includes/plugins/seo/index.php";
                        if (is_file($seoPlugin)) {
                            include($seoPlugin);
                        }
                        ?>
                        <p>
                            <label for="precio"><? echo lang("precio"); ?></label>
                            <input type="text" name="precio" id="precio" value="<?
                            if (isset($fila)) {
                                echo $fila['precio'];
                            }
                            ?>" />
                        </p>
                        <p>
                            <label for="referencia"><? echo lang("referencia"); ?></label>
                            <input type="text" name="referencia" id="referencia" value="<?
                            if (isset($fila)) {
                                echo $fila['referencia'];
                            }
                            ?>" />
                        </p>
                    </div>
                </div>
                <fieldset>
                    <?
                    include("includes/single-image.php");
                    $dataSec = "productos-multi";
                    include("includes/multi-images.php");
                    ?>
                    <div class="checkbox_group">    
                        <p class="checkbox">
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                            if (!isset($fila) || isset($fila) && $fila['activo'] > 0) {
                                echo "checked='checked'";
                            }
                            ?>/>
                            <label for="activo"><? echo lang("activo"); ?></label>
                        </p>
                        <p class="checkbox">
                            <input class="checkbox" type="checkbox" name="destacado" id="destacado" <?
                                   if (isset($fila) && $fila['destacado'] > 0) {
                                       echo "checked='checked'";
                                   }
                                   ?>/>
                            <label for="destacado"><? echo lang("destacado"); ?></label>
                        </p>
                    </div>
                </fieldset>
            </form>
        </section>
    </div>
</section>