<?
if (!isset($id) || $id == "") {
    $id = 0;
}
if (!isset($parent) || $parent == "") {
    $parent = 0;
}
$sql = "SELECT * FROM productos_categoria WHERE productos_categoria.parent = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND id IN (SELECT id_seccion FROM idiomas WHERE texto LIKE '%" . $search_term . "%' AND seccion='productos_categoria')";
}
$sql .= " ORDER BY productos_categoria.orden ASC, productos_categoria.id ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);
$nItems = mysqli_num_rows($consulta);

$tiene_productos = tiene_productos($id);
$tiene_subcats = tiene_subcategorias_productos($id);
if ($nItems == 0 && $tiene_productos) {
    echo "<script>window.location='index.php?sec=productos&sub=listado-productos&id=$id'</script>";
}

$sec_title = "Productos";
//echo $tiene_subcats."|".$tiene_productos;
$addLink = "";
if (($tiene_subcats > 0) || ($tiene_subcats == 0 && $tiene_productos == 0)) {
    $addLink1 = "index.php?sec=productos&sub=anyadir-cat&parent=$id";
}
if (($tiene_productos > 0) || ($tiene_subcats == 0 && $tiene_productos == 0)) {
    $addLink2 = "index.php?sec=productos&sub=anyadir&parent=$id";
}
$search_action = "productos&id=$id&parent=$parent";

$breadcrumbs = migas_de_pan_productos($id);
$breadcrumbs_home = "productos";
$tableDB = "productos_categoria";
?>
<section>
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <? include("includes/breadcrumbs.php"); ?>  
        </header>
        <div class="filter_group">
            <? if (isset($addLink1)) { ?><a href="<? echo $addLink1; ?>" class="button"><? echo lang("anyadir_categoria"); ?></a><? } ?>
            <? if (isset($addLink2)) { ?><a href="<? echo $addLink2; ?>" class="button"><? echo lang("anyadir_producto"); ?></a><? } ?>
        </div>
        <section>
            <? if ($nItems > 0) { ?>
                <?
                $instructions = array("move");
                instructions($instructions);
                ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("carpeta"); ?></th>
                                <th style="width:70px;"><? echo lang("activa"); ?></th>
                                <th style="width:20px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $cats = tiene_subcategorias_productos($fila['id']);
                                $prods = tiene_productos($fila['id']);
                                $title = obtener_valor($lang_default, "productos_categoria", "descripcion", $fila['id']);
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title">
                                        <? if ($prods > 0) { ?>
                                            <a href="index.php?sec=productos&sub=listado-productos&id=<? echo $fila['id']; ?>&parent=<? echo $fila['parent']; ?>" class="img_carpeta_galeria">
                                                <span class="icon"></span>
                                                <? echo $title; ?> <span class="subtle">(<? echo $prods . " " . lang("productos"); ?>)</span>
                                            </a>
                                            <?
                                        } else {
                                            if ($cats > 0) {
                                                $nCats_txt = $cats . " " . lang("subcategorias");
                                            } else {
                                                $nCats_txt = lang("vacio");
                                            }
                                            ?>
                                            <a href="index.php?sec=productos&id=<? echo $fila['id']; ?>&parent=<? echo $fila['parent']; ?>" class="img_carpeta">
                                                <span class="icon"></span>
                                                <? echo $title; ?> <span class="subtle">(<? echo $nCats_txt; ?>)</span>
                                            </a>
                                        <? } ?>    
                                    </td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=productos&sub=editar-cat&id=<? echo $fila['id']; ?>&parent=<? echo $fila['parent']; ?>" class="edit"><span class="icon">::<? echo lang("editar"); ?>::</span></a>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=productos&sub=eliminar-cat&id=<? echo $fila['id']; ?>&parent=<? echo $id; ?>" data-subcategorias="<? echo $cats; ?>" class="delete_confirm_cats_prods delete" title="<? echo lang("eliminar") . " " . $title; ?>" data-sec="productos">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>