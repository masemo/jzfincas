<?
if (!isset($id) || $id == "") {
    $id = 0;
}
if (!isset($parent) || $parent == "") {
    $parent = 0;
}
$sql = "SELECT * FROM productos WHERE productos.categoria = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND productos.id IN (SELECT id_seccion FROM idiomas WHERE idiomas.texto LIKE '%" . $search_term . "%' AND idiomas.seccion='productos')";
}
$sql .= " ORDER BY productos.orden ASC, productos.id ASC";
$consulta = mysqli_query($link, $sql);

$sec_title = "Productos";
$nItems = mysqli_num_rows($consulta);
$addLink = "";
$addLinkFilter = "index.php?sec=productos&sub=anyadir&parent=$id";
$search_action = "productos&id=$id&parent=$parent";
$tiene_productos = tiene_productos($id);

$breadcrumbs = migas_de_pan_productos($id);
$breadcrumbs_home = "productos";
$tableDB = "productos";
?>
<section class="productos-list">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <? include("includes/breadcrumbs.php"); ?>
        </header>
        <div class="filter_group">
            <a href="<? echo $addLinkFilter; ?>" class="button"><? echo lang("anyadir_producto"); ?></a>
        </div>
        <section>
            <? if ($nItems > 0) { ?>
                <?
                $instructions = array("move");
                instructions($instructions);
                ?>
                <table class="list drag_sortable ">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="width:140px;"></th>
                            <th><? echo lang("producto"); ?></th>
                            <th style="width:70px;"><? echo lang("activo"); ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody data-tableDB="<? echo $tableDB; ?>">
                        <?
                        $i = 1;
                        while ($fila = mysqli_fetch_array($consulta)) {
                            $prods = tiene_productos($fila['id']);
                            ?>
                            <tr data-id="<? echo $fila['id']; ?>">
                                <td><? echo $i; ?>.</td>
                                <td>
                                    <a href="index.php?sec=productos&sub=editar&id=<? echo $fila['id']; ?>&parent=<? echo $id; ?>">
                                        <?
                                        if ($fila['imagen'] > 0) {
                                            $imagen = obtnener_imagen($fila['imagen']);
                                            ?>
                                            <img src="lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen, true); ?>&w=120&q=60" id="imgprincipal" alt="Image"/>
                                            <?
                                        } else {
                                            echo lang("sin_imagen");
                                        }
                                        ?>
                                    </a>
                                </td>
                                <td data-field = "title">
                                    <a href = "index.php?sec=productos&sub=editar&id=<? echo $fila['id']; ?>&parent=<? echo $id; ?>"><? echo obtener_valor($lang_default, "productos", "nombre", $fila['id']);
                                        ?></a>
                                </td>
                                <td>
                                    <input class="checkbox activo_toggler" type="checkbox" <?
                                    if ($fila['activo'] > 0) {
                                        echo "checked='checked'";
                                    }
                                    ?>/>
                                </td>
                                <td>
                                    <a href="index.php?sec=productos&sub=eliminar&id=<? echo $fila['id']; ?>&parent=<? echo $id; ?>" data-parent="<? echo $id; ?>" title="<? echo lang("eliminar") . " " . $sec_title; ?>" class="delete_confirm delete">
                                        <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                    </a>
                                </td>
                            </tr>
                            <?
                            $i++;
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                    </tfoot>
                </table>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>