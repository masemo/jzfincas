<?php

$subDefault = "secciones/noticias/list-categorias.php";

switch ($sub) {
    /* ----------------------------------------------------------------- accions -- */
    case "anyadir-guardar-articulo":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $fecha_public = fecha_to_mysql($fecha_public);
        if ($fecha_caduca != "") {
            $fecha_caduca = fecha_to_mysql($fecha_caduca);
        }

        $frm = $_POST;

        $id = anyadir_guardar_articulo($frm, $imagen, $parent, $fecha_public, $fecha_caduca, $activo, $lenguajes, $activo);
        if ($go_back == "0") {
            $back_url = "index.php?sec=noticias&sub=editar-articulo&id=$id";
        }

        header("location:$back_url");
        break;
    case "editar-guardar-articulo":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }

        $fecha_public = fecha_to_mysql($fecha_public);
        if ($fecha_caduca != "") {
            $fecha_caduca = fecha_to_mysql($fecha_caduca);
        }

        $frm = $_POST;

        editar_guardar_articulo($frm, $imagen, $imagen_old, $id_imagen_old, $parent, $fecha_public, $fecha_caduca, $activo, $lenguajes, $id);
        if ($go_back == "0") {
            $back_url = "index.php?sec=noticias&sub=editar-articulo&id=$id";
        }

        header("location:$back_url");
        break;
    case "eliminar-articulo":
        eliminar_articulo($id);
        header("location:?sec=noticias&sub=list-articulos&id=$parent");
        break;

    case "editar-guardar-categoria":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        editar_guardar_categoria($frm, $id, $activo, $imagen, $imagen_old, $id_imagen_old, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar-categoria&id=$id";
        }
        header("location:$back_url");
        break;
    case "anyadir-guardar-categoria":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        $frm = $_POST;
        $id = anyadir_guardar_categoria($frm, $activo, $imagen, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar-categoria&id=$id";
        } else {
            $back_url .= "&id=$parent";
        }

        header("location:$back_url");
        break;
    case "eliminar-categoria":
        eliminar_categoria($id);
        header("location:?sec=noticias");
        break;
//------------------subseccions------------------------------------    
    case "editar-articulo":
    case "anyadir-articulo":
        $include = "secciones/noticias/edit-articulo.php";
        break;
    case "list-articulos":
        $include = "secciones/noticias/list-articulos.php";
        break;
    case "editar-categoria":
    case "anyadir-categoria":
        $include = "secciones/noticias/edit-categoria.php";
        break;
    default:
        $include = $subDefault;
        break;
}
