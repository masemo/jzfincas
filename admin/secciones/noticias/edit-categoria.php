<?
if ($sub == "editar-categoria") {
    $accion_sub = "editar-guardar-categoria";
    $sec_title = "noticias";
    $sec_action = lang("editar_categoria");

    $sql = "SELECT * FROM noticias_categoria WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
} else {
    $accion_sub = "anyadir-guardar-categoria";
    $sec_title = "noticias";
    $sec_action = lang("anyadir_nueva_categoria");
}

$query_cats = "SELECT * FROM noticias_categoria WHERE parent = 0 ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $query_cats);

$back_url = "index.php?sec=noticias";
?>
<section class="noticias">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <? include("includes/edit-options.php"); ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="noticias" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <input type="hidden" name="imagen_old" id="imagen_old" value="<?
                if (isset($fila)) {
                    echo $fila["imagen"];
                }
                ?>" />
                <input type="hidden" name="idprincipal" id="idprincipal" value="<?
                if (isset($fila)) {
                    echo $fila["imagen"];
                }
                ?>" />
                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="descripcion_<? echo $leng; ?>"><? echo lang("nombre_minus"); ?></label>
                                        <input type="text" class="seo_title seo_friendly_url seo_description <? require_lang_field($leng); ?>" name="descripcion_<? echo $leng; ?>" id="descripcion_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar-categoria") {
                                            echo obtener_valor($leng, "noticias_categoria", "descripcion", $id);
                                        }
                                        ?>" />
                                    </p>
                                </fieldset>
                                <?
                                unset($lenguaje);
                            }
                            ?>
                        </div>
                        <fieldset>
                            <? include("includes/single-image.php"); ?>
                            <div class="checkbox_group">
                                <p class="checkbox">
                                    <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                                    if (isset($fila) && $fila['activo'] > 0) {
                                        echo "checked='checked'";
                                    }
                                    ?>/>
                                    <label for="activo"><? echo lang("activo"); ?></label>
                                </p>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-40">
                        <?
                        $openPlugin = true;
                        $seoPlugin = "includes/plugins/seo/index.php";
                        if (is_file($seoPlugin)) {
                            $sec = "noticias_categoria";
                            include($seoPlugin);
                        }
                        ?>
                    </div>
                </div>
            </form>
        </section>
    </div>
</section>