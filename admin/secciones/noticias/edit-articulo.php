<?
if ($sub == "editar-articulo") {

    $accion_sub = "editar-guardar-articulo";
    $sec_title = "noticias";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM noticias WHERE id=$id";
//    echo $sql;
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $parent = $fila["parent"];

    /* -- anterior siguiente -- */
    $item_ant = obtener_sig_or_ant("noticias", $id, "fecha_public", "ANT", "ASC");
    $item_sig = obtener_sig_or_ant("noticias", $id, "fecha_public", "SIG", "ASC");

    if ($item_ant > -1) {
        $link_ant = "index.php?sec=noticias&sub=editar&id=" . $item_ant;
        $title_ant = obtener_valor($lang_default, "noticias", "titular", basic_query("id", "noticias", $item_ant));
        $img_ant = "";
    }

    if ($item_sig > -1) {
        $link_sig = "index.php?sec=noticias&sub=editar&id=" . $item_sig;
        $title_sig = obtener_valor($lang_default, "noticias", "titular", basic_query("id", "noticias", $item_sig));
        $img_sig = "";
    }
} else {

    $accion_sub = "anyadir-guardar-articulo";
    $sec_title = "Noticias";
    $sec_action = lang("anyadir_nuevas");
}

$sql_cats = "SELECT * FROM noticias_categoria ORDER BY orden ASC";
$consulta_cats = mysqli_query($link, $sql_cats);
$hideCats = false;
$nCats = mysqli_num_rows($consulta_cats);
if ($nCats <= 1) {
    $hideCats = true;
}

$sql_galeria_imagenes = "SELECT * FROM galeria_categoria ORDER BY id ASC";
$consulta_galeria_imagenes = mysqli_query($link, $sql_galeria_imagenes);
$public_url = "";
$back_url = "index.php?sec=noticias&sub=list-articulos&id=" . $parent;

$breadcrumbs = "<a href='index.php?sec=noticias&id=$parent'>" . obtener_valor($lang_default, "noticias_categoria", "descripcion", $parent) . "</a> /";
$breadcrumbs_home = "noticias";
?>
<section class="noticias">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <?
            if ($activar_categorias_noticias || $nCats > 0) {
                include("includes/breadcrumbs.php");
            }
            ?>
            <?
            include("includes/edit-options.php");
            ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="noticias" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="preview_url" id="preview_url" value="<? echo $siteUrl; ?>preview-news.html" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />

                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <p>
                                        <label for="titular_<? echo $leng; ?>"><? echo lang("titular"); ?></label>
                                        <input type="text" class="seo_title seo_friendly_url <? require_lang_field($leng); ?>" name="titular_<? echo $leng; ?>" id="titular_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar-articulo") {
                                            echo obtener_valor($leng, $sec, "titular", $id);
                                        }
                                        ?>" />
                                    </p>
                                    <p>
                                        <label for="contenido_<? echo $leng; ?>">
                                            <? echo lang("contenido"); ?>
                                            <a href="<? echo ckeditor_manual_url($lang_default); ?>" class="manual" target="blank">::<? echo lang("manual_editor"); ?>::</a>
                                        </label>
                                        <textarea cols="" rows="" class="ckeditor newsletter seo_description <? if ($sub != "editar") { ?>vacio<? } ?>" id="contenido_<? echo $leng; ?>" name="contenido_<? echo $leng; ?>" cols="0" rows="0"><?
                                            if ($sub == "editar-articulo") {
                                                echo obtener_valor($leng, $sec, "contenido", $id);
                                            }
                                            ?></textarea>
                                    </p>
                                    <?
                                    if (isset($id)) {
                                        $public_url = obtener_url_amigable($leng, "noticias", $id);
                                        ?>
                                        <div>
                                            <label for="enlace_publico_<? echo $leng; ?>"><? echo lang("paginas_enlace_publico"); ?>:</label>
                                            <a id="enlace_publico_<? echo $leng; ?>" href="<? echo $public_url; ?>" target="blank" class="public_link">
                                                <span class="icon"></span>
                                                <? echo $public_url; ?>
                                            </a>
                                        </div>
                                    <? } ?>


                                </fieldset>
                                <?
                                unset($lenguaje);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-40">
                        <p <? hide_item($hideCats, true); ?>>
                            <label for="parent"><? echo lang("categoria"); ?></label>
                            <select id="parent" name="parent">
                                <? if (!$activar_categorias_noticias) { ?>
                                    <option value="0"></option>
                                <? } ?>
                                <? while ($fila_cats = mysqli_fetch_array($consulta_cats)) { ?>
                                    <option value="<? echo $fila_cats['id']; ?>" <?
                                    if ($fila_cats['id'] == $parent) {
                                        echo "SELECTED";
                                    }
                                    ?>><? echo obtener_valor($lang_default, "noticias_categoria", "descripcion", $fila_cats['id']); ?></option>
                                        <? } ?>
                            </select>
                        </p>
                        <div class="input_group">
                            <p>
                                <label for="fecha_public"><? echo lang("publicar"); ?></label>
                                <input type="text" name="fecha_public" id="fecha_public" placeholder="00/00/0000" class="date datepicker required" value="<?
                                if ($sub == "editar-articulo") {
                                    echo fecha_to_view($fila['fecha_public']);
                                } else {
                                    echo date("d/m/Y");
                                }
                                ?>"/>
                            </p>
                            <p>
                                <label for="fecha_caduca"><? echo lang("caduca"); ?></label>
                                <input type="text" name="fecha_caduca" id="fecha_caduca" placeholder="00/00/0000" class="date datepicker" value="<?
                                if ($sub == "editar-articulo") {
                                    if ($fila['fecha_caduca'] != "0000-00-00") {
                                        echo fecha_to_view($fila['fecha_caduca']);
                                    }
                                }
                                ?>"/>
                            </p>
                        </div>
                        <div class="<? hide_item($estatica); ?> <? hide_item($homePage); ?>" data-eLink="disable" >
                            <?
                            $openPlugin = true;
                            $seoPlugin = "includes/plugins/seo/index.php";
                            if (is_file($seoPlugin)) {
                                include($seoPlugin);
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <fieldset>
                    <?
                    include("includes/single-image.php");

                    $dataSec = "noticias_galeria";
                    include("includes/multi-images.php");
                    ?>
                    <div class="checkbox_group">
                        <?
                        $instructions = array("activo");
                        instructions($instructions);
                        ?>
                        <p class="checkbox">
                            <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                                   if (isset($fila) && $fila['activo'] > 0) {
                                       echo "checked='checked'";
                                   }
                                   ?>/>
                        </p>
                    </div>
                </fieldset>
            </form>
            <footer>
                <?
                if ($sub == "editar-articulo") {
                    include("includes/item-anterior-siguiente.php");
                }
                ?>
            </footer>
        </section>
    </div>
</section>