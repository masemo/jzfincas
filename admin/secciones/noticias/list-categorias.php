<?
if (!isset($id) || $id == "") {
    $id = 0;
}
$sql = "SELECT * FROM noticias_categoria ORDER BY orden ASC, id ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);
$nItems = mysqli_num_rows($consulta);

$tiene_noticias = tiene_noticias($id);
if ($nItems == 0 && !$activar_categorias_noticias) {
    echo "<script>window.location='index.php?sec=noticias&sub=list-articulos&id=$id'</script>";
}

$addLink = "index.php?sec=noticias&sub=anyadir-categoria";
$addTxt = lang("anyadir_categoria");

$sec_title = lang("noticias")." ".lang("categorias");

$breadcrumbs_home = "noticias";
$breadcrumbs = "";
$tableDB = "noticias_categoria";
?>
<section>
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <? include("includes/breadcrumbs.php"); ?>  
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("categoria"); ?></th>
                                <th style="width:70px;"><? echo lang("activa"); ?></th>
                                <th style="width:20px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $prods = tiene_noticias($fila['id']);
                                $title = obtener_valor($lang_default, "noticias_categoria", "descripcion", $fila['id']);
                                $icon = "img_carpeta_galeria";
                                if($prods == 0){
                                    $icon = "img_carpeta";
                                }
                                    
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title">
                                        <a href="index.php?sec=noticias&sub=list-articulos&id=<? echo $fila['id']; ?>" class="<? echo $icon; ?>">
                                            <span class="icon"></span>
                                            <? echo $title; ?> <span class="subtle">(<? echo $prods . " " . lang("articulos"); ?>)</span>
                                        </a>
                                    </td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=noticias&sub=editar-categoria&id=<? echo $fila['id']; ?>" class="edit"><span class="icon">::<? echo lang("editar"); ?>::</span></a>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=noticias&sub=eliminar-categoria&id=<? echo $fila['id']; ?>" data-items="<? echo $prods; ?>" class="delete_confirm_cats_articulos delete" title="<? echo lang("eliminar") . " " . $title; ?>" data-sec="noticias">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>