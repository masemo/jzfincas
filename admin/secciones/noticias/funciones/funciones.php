<?php
function anyadir_guardar_articulo($frm, $imagen, $parent, $fecha_public, $fecha_caduca, $activo, $lenguajes, $activo) {
    global $link;

    $sql = "INSERT INTO noticias (parent,fecha_public,fecha_caduca,activo) VALUES ($parent,'$fecha_public','$fecha_caduca',$activo)";
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "noticias", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "noticias", "titular", $frm["titular_" . $leng], $id, true);
        inserta_campo_idiomas($leng, "noticias", "contenido", $frm["contenido_" . $leng], $id, 0, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "noticias",
            "seccion_parents" => "noticias_categoria",
            "fu_field" => "titular_",
            "title_field" => "titular_",
            "description_field" => "contenido_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }


    if ($imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "noticias");
    } else {
        $imagen = 0;
    }

    $sql = "UPDATE noticias SET imagen = $imagen WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "imagenes", $sql);
    return $id;
}

function editar_guardar_articulo($frm, $imagen, $imagen_old, $id_imagen_old, $parent, $fecha_public, $fecha_caduca, $activo, $lenguajes, $id) {
    global $link;

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "noticias");
    } else {
        if ($id_imagen_old != "") {
            $imagen = $id_imagen_old;
        } else {
            $imagen = 0;
        }
    }

    $sql = "UPDATE noticias SET parent=$parent, imagen=$imagen, fecha_public='$fecha_public', fecha_caduca='$fecha_caduca', activo=$activo WHERE id = $id";
    //echo $sql;
    mysqli_query($link, $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "noticias", "titular", $frm["titular_" . $leng], $id);
        inserta_campo_idiomas($leng, "noticias", "contenido", $frm["contenido_" . $leng], $id, 0, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "noticias",
            "seccion_parents" => "noticias_categoria",
            "fu_field" => "titular_",
            "title_field" => "titular_",
            "description_field" => "contenido_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    insert_log("UPDATE", "noticias", $sql);
}

function eliminar_articulo($id) {
    global $link;

    eliminar_img_multiples($id, "noticias");
    elimina_idiomas($id, "noticias", "noticias");
    eliminar_seo("noticias", $id);

    $sql = "DELETE FROM noticias WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "noticias", $sql);
}

function editar_guardar_categoria($frm, $id, $activo, $imagen, $imagen_old, $id_imagen_old, $lenguajes) {
    global $link;
    $parent = 0;

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "noticias_categoria");
    } else {
        if ($id_imagen_old != "") {
            $imagen = $id_imagen_old;
        } else {
            $imagen = 0;
        }
    }

    $sql = "UPDATE noticias_categoria SET imagen=$imagen, activo = $activo WHERE id = $id";
//echo $sql;
    mysqli_query($link, $sql);

    insert_log("UPDATE", "noticias_categoria", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "noticias_categoria", "descripcion", $frm["descripcion_" . $leng], $id);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "noticias_categoria",
            "seccion_parents" => "noticias_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }
}

function anyadir_guardar_categoria($frm, $activo, $imagen, $lenguajes) {
    global $link;
    $parent = 0;

    $sql = "INSERT INTO noticias_categoria (activo) VALUES ($activo)";
//echo $sql;
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "noticias_categoria", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        inserta_campo_idiomas($leng, "noticias_categoria", "descripcion", $frm["descripcion_" . $leng], $id);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "noticias_categoria",
            "seccion_parents" => "noticias_categoria",
            "fu_field" => "descripcion_",
            "title_field" => "descripcion_",
            "description_field" => "descripcion_",
            "parent_title_field" => "descripcion"
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    if ($imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "noticias_categoria");
    } else {
        $imagen = 0;
    }
    $orden = get_last_order("noticias_categoria");
    $sql = "UPDATE noticias_categoria SET imagen = $imagen, orden = $orden WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "imagenes", $sql);
    return $id;
}

function eliminar_categoria_item($id_categoria) {
    eliminar_img_multiples($id_categoria, "noticias_categoria");
    elimina_idiomas($id_categoria, "noticias_categoria", "noticias_categoria");
    eliminar_seo("noticias_categoria", $id_categoria);
    elimina_item_by_id("noticias_categoria", $id_categoria);
}

function eliminar_categoria($id) {
    global $link;
    $sql = "SELECT * FROM noticias WHERE parent = $id";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        eliminar_articulo($fila['id']);
    }
    eliminar_categoria_item($id);
    insert_log("DELETE", "productos_categoria", $sql);
}

function tiene_noticias($parent) {
    global $link;
    $sql = "SELECT id FROM noticias WHERE parent = $parent";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);
    return $numero_filas;
}
