<?
$sql = "SELECT * FROM noticias WHERE parent = $id";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND id IN (SELECT id_seccion FROM idiomas WHERE texto LIKE '%" . $search_term . "%' AND seccion='noticias')";
}
$sql .= " ORDER BY fecha_public DESC";
//echo $sql;
$consulta = mysqli_query($link, $sql);


$sec_title = "Noticias";
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=noticias&sub=anyadir-articulo&parent=".$id;
$addTxt = lang("anyadir_articulo");
$search_action = "noticias";
$tableDB = "noticias";

if ($nItems == 0 && $id == 0 && $activar_categorias_noticias) {
    echo "<script>window.location='index.php?sec=noticias'</script>";
}

$breadcrumbs_home = "noticias";
if($id > 0){
    $categoria_nombre = obtener_valor($lang_default, "noticias_categoria", "descripcion", $id);
    $breadcrumbs = "<a href='index.php?sec=noticias&id=$id'>" . $categoria_nombre . "</a> /";
}else{
    $breadcrumbs = "";
}
?>
<section class="noticias">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
            <? include("includes/breadcrumbs.php"); ?>  
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <div class="list_msk">
                    <table class="list tablesorter ">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("noticia"); ?></th>
                                <th><? echo lang("publicar"); ?></th>
                                <th><? echo lang("caduca"); ?></th>
                                <th style="width: 70px;"><? echo lang("activa"); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title"><a href="index.php?sec=noticias&sub=editar-articulo&id=<? echo $fila['id']; ?>&parent=<? echo $id;?>"><? echo obtener_valor($lang_default, "noticias", "titular", $fila['id']); ?> </a></td>
                                    <td <? dataPublicada($fila['fecha_public'], "public"); ?> ><? echo fecha_to_view($fila['fecha_public']); ?></td>
                                    <td <?
                                    if ($fila['fecha_caduca'] != "0000-00-00") {
                                        dataPublicada($fila['fecha_caduca'], "caduca");
                                    }
                                    ?> >
                                            <?
                                            if ($fila['fecha_caduca'] != "0000-00-00") {
                                                echo fecha_to_view($fila['fecha_caduca']);
                                            } else {
                                                echo "-";
                                            }
                                            ?>
                                    </td>                                    
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=noticias&sub=eliminar-articulo&id=<? echo $fila['id']; ?>&parent=<? echo $id; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>