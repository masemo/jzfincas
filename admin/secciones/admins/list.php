<?
$sql = "SELECT * FROM admins WHERE id <> 0";
if (isset($search_term) && $search_term != "") {
    $sql .= " AND (nombre LIKE '%" . $search_term . "%' OR mail LIKE '%" . $search_term . "%' OR usuario LIKE '%" . $search_term . "%')";
}
$sql .= " ORDER BY nombre ASC";
$consulta = mysqli_query($link, $sql);

$sec_title = "Administradores";
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=admins&sub=anyadir";
$search_action = "admins";
?>
<section class="contactos">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <div class="list_msk">
                    <table class="list tablesorter ">
                        <thead>
                            <tr>
                                <th></th>
                                <th><? echo lang("user_admins"); ?></th>
                                <th><? echo lang("tipo_admins"); ?></th>
                                <th><? echo lang("nombre_admins"); ?></th>
                                <th><? echo lang("email"); ?></th>
                                <th><? echo lang("ultimo_acceso"); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                ?>
                                <tr>
                                    <td><? echo $i; ?>.</td>
                                    <td data-field="title"><a href="index.php?sec=admins&sub=editar&id=<? echo $fila['id']; ?>"><? echo $fila['usuario']; ?></a></td>
                                    <td><? if ($fila['root']) { ?>super-administrador<? } else { ?>administrador<? } ?></td>
                                    <td><? echo $fila['usuario']; ?></td>
                                    <td><? echo $fila['mail']; ?></td>
                                    <td><?
                                        if ($fila['fecha_acceso'] != "0000-00-00") {
                                            echo fecha_to_view($fila['fecha_acceso']);
                                        } else {
                                            echo "-----------";
                                        }
                                        ?></td>
                                    <td>
                                        <a href="index.php?sec=admins&sub=eliminar&id=<? echo $fila['id']; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>