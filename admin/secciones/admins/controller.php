<?php

$subDefault = "secciones/admins/list.php";
switch ($sub) {
    /* ----------------------------------------------------------------- accions -- */
    case "anyadir-guardar":
        $id = anyadir_guardar($nombre, $mail, $usuario, $password);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }
        header("location:$back_url");
        break;
    case "editar-guardar":
        editar_guardar($nombre,$mail,$usuario,$password,$id);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }
        header("location:$back_url");
        break;
    case "eliminar":
        eliminar($subDefault, $id);
        $include = $subDefault;
        break;
//------------------subseccions------------------------------------    
    case "editar":
    case "anyadir":
        $include = "secciones/admins/edit.php";
        break;
    default:
        $include = $subDefault;
        break;
}
