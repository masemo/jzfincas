<?
if ($sub == "editar") {

    $accion_sub = "editar-guardar";
    $sec_title = "administradores";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM admins WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    $sql_secciones_admin = "SELECT * FROM config_secciones_admin WHERE activo = 1 AND contratado = 1 AND id <> 13";
    if ($fila['root'] == 1) {
        $sql_secciones_admin .= " AND menu <> 'admins'";
    }
    $consulta_secciones_admin = mysqli_query($link, $sql_secciones_admin);
    
    /* -- anterior siguiente -- */
    $item_ant = obtener_sig_or_ant("admins", $id, "nombre", "ANT", "ASC");
    $item_sig = obtener_sig_or_ant("admins", $id, "nombre", "SIG", "ASC");

    if ($item_ant > -1) {
        $link_ant = "index.php?sec=admins&sub=editar&id=" . $item_ant;
        $title_ant = basic_query("nombre", "admins", $item_ant);
        $img_ant = "";
    }

    if ($item_sig > -1) {
        $link_sig = "index.php?sec=admins&sub=editar&id=" . $item_sig;
        $title_sig = basic_query("nombre", "admins", $item_sig);
        $img_sig = "";
    }
} else {

    $accion_sub = "anyadir-guardar";
    $sec_title = "administradores";
    $sec_action = lang("anyadir_nuevos");
}
$back_url = "index.php?sec=admins";
?>
<section class="admins">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <!--<button class="button add"><? echo lang("anyadir_nuevo"); ?></button>-->
            <div class="edit_options">
                <button type="button" class="save">
                    <span class="icon"></span>
                    <? echo lang("guardar"); ?>
                </button>
                <button type="button" class="save_back">
                    <span class="icon"></span>
                    <? echo lang("guardar_volver"); ?>
                </button>
                <button type="button" class="back">
                    <span class="icon"></span>
                    <? echo lang("volver"); ?>
                </button>
            </div>
        </header>
        <section>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="admins" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <fieldset>
                    <p>
                        <label for="nombre"><? echo lang("nombre_admins"); ?></label>
                        <input type="text" name="nombre" id="nombre" value="<?
                        if (isset($fila['nombre'])) {
                            echo $fila['nombre'];
                        }
                        ?>" class="required"/>
                    </p>
                    <p>
                        <label for="mail"><? echo lang("email"); ?></label>
                        <input type="text" name="mail" id="mail" value="<?
                        if (isset($fila['mail'])) {
                            echo $fila['mail'];
                        }
                        ?>" class="required mail"/>
                    </p>
                    <p>
                        <label for="usuario"><? echo lang("user_admins"); ?></label>
                        <input type="text" name="usuario" id="usuario" value="<?
                        if (isset($fila['usuario'])) {
                            echo $fila['usuario'];
                        }
                        ?>" class="required"/>
                    </p>
                    <div class="password">
                        <div class="inputs">
                            <p>
                                <label for="password"><? echo lang("pass_admins"); ?></label>
                                <input type="password" name="password" id="password" value="" class="confirmar" autocomplete="off"/>
                            </p>
                            <p>
                                <label for="confirm_password"><? echo lang("pass_confirm_admins"); ?></label>
                                <input type="password" name="confirm_password" id="confirm_password" value="" autocomplete="off"/>
                            </p>
                        </div>
                        <div class="passstrength">
                            <p>
                                <em><? echo lang("fortaleza_pass"); ?></em>
                                <strong id="passstrength"><? echo lang("vacio"); ?></strong>                            
                            </p>
                        </div>
                    </div>
                    <? if ($sub == "editar") { ?>
                        <div class="checkbox_group">
                            <?
                            $instructions = array("admin-permisos");
                            instructions($instructions);
                            ?>
                            <p>
                                <label for="permisos"><? echo lang("permisos"); ?></label>
                            </p>
                            <? while ($fila_secciones_admin = mysqli_fetch_array($consulta_secciones_admin)) { ?>
                                <p class="checkbox">
                                    <input class="secciones_admins" type="checkbox" id="seccion_<? echo $fila_secciones_admin['id']; ?>" name="seccion_<? echo $fila_secciones_admin['id']; ?>" value="<? echo $fila_secciones_admin['id']; ?>" rel="<? echo $id; ?>" <?
                                           if (activar_seccion($id, $fila_secciones_admin['id'])) {
                                               echo "checked";
                                           }
                                           ?> />
                                    <label for="seccion_<? echo $fila_secciones_admin['id']; ?>"><? echo $fila_secciones_admin['descripcion']; ?></label>
                                </p>
                            <? } ?>
                        </div>
                    <? } ?>
                </fieldset>
            </form>
            <footer>
                <?
                if ($sub == "editar") {
                    include("includes/item-anterior-siguiente.php");
                }
                ?>
            </footer>
        </section>
    </div>
</section>