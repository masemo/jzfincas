<?     
$menu_principal = menu_navegacion(0,$_SESSION["sessionId"]);
?>
<section class="defecto">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo lang('panel_administracion'); ?></h1>
            <p class="subtle"><? echo lang('bienvenida'); ?> <? echo $adminName; ?>!</p>

            <div class="search_box">
                <p id="liveclock"></p>
            </div>
        </header>
        <section>
           <ul class="col-20" id="botonera_principal">
                <?
                while ($fila_navegacion = mysqli_fetch_array($menu_principal)) {
                    if ($fila_navegacion['sec'] != "defecto") {
                        ?><li>
                            <a href="index.php?sec=<? echo $fila_navegacion['sec']; ?>"><span class="icon <? echo $fila_navegacion['sec']; ?>"></span><span class="title"><? echo $fila_navegacion['descripcion']; ?></span></a>
                        </li><? }
                } ?>
            </ul>
        </section>
    </div>
</section>