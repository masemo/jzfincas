<?
$external_link_hide = false;
$estatica = 0;
$homePage = false;
if ($sub == "editar") {
    $accion_sub = "editar-guardar";
    $sec_title = "páginas de contenido";
    $sec_action = lang("editar");

    $id_cat = $id;
    $sql = "SELECT * FROM paginas WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
    $parent = $fila["parent"];
//    if ($id != $id_pagina_productos) {
    $estatica = $fila["estatica"];
    $estatica_sec = $fila["estatica_sec"];
//    }
    if ($id == 1) {
        $homePage = true;
    }

    /* -- anterior siguiente -- */
    $item_ant = obtener_sig_or_ant("paginas", $id, "orden", "ANT", "ASC", "WHERE visible_menu = 1");
    $item_sig = obtener_sig_or_ant("paginas", $id, "orden", "SIG", "ASC", "WHERE visible_menu = 1");

    if ($item_ant > -1) {
        $link_ant = "index.php?sec=paginas&sub=editar&id=" . $item_ant;
        $title_ant = obtener_valor($lang_default, "paginas", "nombre_menu", basic_query("id", "paginas", $item_ant));
        $img_ant = "";
    }

    if ($item_sig > -1) {
        $link_sig = "index.php?sec=paginas&sub=editar&id=" . $item_sig;
        $title_sig = obtener_valor($lang_default, "paginas", "nombre_menu", basic_query("id", "paginas", $item_sig));
        $img_sig = "";
    }
} else {
    $accion_sub = "anyadir-guardar";
    $sec_title = "Páginas de contenido";
    $sec_action = lang("anyadir_nuevas");
    $id_cat = "";
}

/* -- paginas -- */
$query_paginas = "SELECT paginas.* FROM config_secciones_public INNER JOIN paginas ON config_secciones_public.id = paginas.sec_public WHERE paginas.parent = 0 AND paginas.visible_menu = 1 AND config_secciones_public.activo = 1 ORDER BY paginas.orden ASC";
$result_paginas = mysqli_query($link, $query_paginas);
$nPaginas = mysqli_num_rows($result_paginas);

$sql_galeria_imagenes = "SELECT * FROM galeria_categoria ORDER BY id ASC";
$consulta_galeria_imagenes = mysqli_query($link, $sql_galeria_imagenes);

$back_url = "index.php?sec=paginas";
?>
<section class="paginas">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?> <? if (isset($estatica) && $estatica > 0) { ?>(<? echo lang("pagina_estatica"); ?>)<? } ?></h1>
            <?
            include("includes/edit-options.php");
            ?>
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="paginas" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="estatica" id="estatica" value="<? echo $fila["estatica"]; ?>" />
                <input type="hidden" name="estatica_sec" id="estatica_sec" value="<? echo $fila["estatica_sec"]; ?>" />
                <input type="hidden" name="external_link" id="external_link" value="<? echo $fila["external_link"]; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <div class="row">
                    <div class="col-60 noLp">
                        <div class="lang_field_group">
                            <?
                            foreach ($lenguajes["activos"] as $lenguaje) {
                                $leng = $lenguaje["idioma"];
                                if (isset($id)) {
                                    $public_url = obtener_url_amigable($leng, "paginas", $id);
                                }
                                ?>
                                <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                                    <input type="hidden" name="preview_url_<? echo $leng; ?>" id="preview_url_<? echo $leng; ?>" value="<? echo genera_page_preview($lenguajes["activos"], $leng); ?>" />
                                    <?
                                    if (isset($fila) && $fila["external_link"] > 0) {
                                        include("includes/external-link.php");
                                    }
                                    ?>
                                    <p>
                                        <label for="nombre_menu_<? echo $leng; ?>"><? echo lang("paginas_nombre_menu"); ?></label>
                                        <input type="text" class="seo_title <? require_lang_field($leng); ?>" name="nombre_menu_<? echo $leng; ?>" id="nombre_menu_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar") {
                                            echo obtener_valor($leng, $sec, "nombre_menu", $id);
                                        }
                                        ?>" />
                                    </p>
                                    <p data-eLink="disable">
                                        <label for="nombre_<? echo $leng; ?>"><? echo lang("paginas_nombre"); ?></label>
                                        <input type="text" class="seo_friendly_url <? require_lang_field($leng); ?>" name="nombre_<? echo $leng; ?>" id="nombre_<? echo $leng; ?>" value="<?
                                        if ($sub == "editar") {
                                            echo obtener_valor($leng, $sec, "nombre", $id);
                                        }
                                        ?>" />
                                    </p>
                                    <p data-eLink="disable" >
                                        <label for="descripcion_<? echo $leng; ?>">
                                            <? echo lang("contenido"); ?>
                                            <a href="<? echo ckeditor_manual_url($lang_default); ?>" class="manual" target="blank">::<? echo lang("manual_editor"); ?>::</a>
                                        </label>
                                        <textarea  cols="" rows="" class="ckeditor newsletter seo_description <? if ($sub != "editar") { ?>vacio<? } ?>" id="descripcion_<? echo $leng; ?>" name="descripcion_<? echo $leng; ?>" cols="0" rows="0"><?
                                            if ($sub == "editar") {
                                                echo obtener_valor($leng, $sec, "descripcion", $id);
                                            }
                                            ?></textarea>
                                    </p>
                                    <?
                                    if (!$homePage && !$estatica) {
                                        echo "<hr />";
                                        $instructions = array("paginas-enlaces");
                                        instructions($instructions);
                                        if (isset($id)) {
                                            ?>
                                            <div class="<? hide_item($estatica); ?> <? hide_item($homePage); ?>" data-eLink="disable" >
                                                <label for="enlace_publico_<? echo $leng; ?>"><? echo lang("paginas_enlace_publico"); ?>:</label>
                                                <a id="enlace_publico_<? echo $leng; ?>" href="<? echo $public_url; ?>" target="blank" class="public_link">
                                                    <span class="icon"></span>
                                                    <? echo $public_url; ?>
                                                </a>
                                            </div>
                                            <?
                                        }
                                    }
                                    ?>

                                    <?
                                    if ((!isset($fila)) || (isset($fila) && $fila["external_link"] == 0) && (!$estatica) && (!$homePage)) {
                                        include("includes/external-link.php");
                                    }
                                    ?>

                                </fieldset>
                                <?
                                unset($lenguaje);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-40 noLp">
                        <p class="<? hide_item($homePage); ?>">
                            <label for="parent"><? echo lang("paginas_pagina_superior"); ?></label>
                            <select id="parent" name="parent">
                                <option value="0" <?
                                if (isset($fila) && $fila['parent'] == 0) {
                                    echo "selected='selected'";
                                }
                                ?>><? echo lang("inicio"); ?>
                                </option>
                                <?
                                $parent = "";
                                while ($row_paginas = mysqli_fetch_array($result_paginas)) {
                                    if (isset($fila)) {
                                        $parent = $fila["parent"];
                                    }
                                    echo genera_pagina_item($row_paginas, $id_cat);
                                    echo crea_item_menu($row_paginas['id'], $id_cat, 0);
                                }
                                ?>
                            </select>
                        </p>
                        <div data-eLink="disable" >
                            <?
                            $openPlugin = true;
                            $seoPlugin = "includes/plugins/seo/index.php";
                            if (is_file($seoPlugin)) {
                                include($seoPlugin);
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="<? //hide_item($estatica); ?>"  data-eLink="disable">
                    <fieldset>
                        <? include("includes/single-image.php"); ?>
                    </fieldset>
                </div>
                <fieldset>
                    <div class="<? hide_item($estatica); ?>"  data-eLink="disable">
                        <?
                        $pestanyeraCondensed = true;
                        $dataSec = "paginas_galeria";
                        include("includes/multi-images.php");
                        ?>
                    </div>
                </fieldset>

                <div class="checkbox_group">
                    <p class="checkbox">
                        <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                        <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                        if ((!isset($fila)) || (isset($fila) && $fila['activo'] > 0)) {
                            echo "checked='checked'";
                        }
                        ?>/>
                    </p>
                    <p class="checkbox <? hide_item($estatica); ?> <? hide_item($homePage); ?>"  data-eLink="disable">
                        <label for="nodo" class="nodo"><span class="icon"></span><? echo lang("paginas_sin_enlace"); ?></label>
                        <input class="checkbox" type="checkbox" name="nodo" id="nodo" <?
                        if (isset($fila) && $fila['nodo'] > 0) {
                            echo "checked='checked'";
                        }
                        ?>/>
                    </p>
                    <p class="checkbox <? hide_item($homePage); ?>">
                        <label for="oculto" class="oculto"><span class="icon"></span><? echo lang("paginas_oculto"); ?></label>
                        <input class="checkbox" type="checkbox" name="oculto" id="oculto" <?
                        if (isset($fila) && $fila['oculto'] > 0) {
                            echo "checked='checked'";
                        }
                        ?>/>
                    </p>

                    <?
                    if ($homePage) {
                        $instructions = array("activo");
                    } else {
                        $instructions = array("paginas");
                    }
                    instructions($instructions);
                    ?>
                </div>
            </form>
            <footer>
                <?
                if ($sub == "editar") {
                    include("includes/item-anterior-siguiente.php");
                }
                ?>
            </footer>
        </section>
    </div>
</section>