<?
$sec_title = "Contenido";
$addLink = "index.php?sec=paginas&sub=anyadir";
?>
<section class="banners">
    <div class="wrap">
        <header class="options_header">
            <?
            include("includes/header.php");

            $seoPlugin = "includes/plugins/seo/index.php";
            if (is_file($seoPlugin) && $development == true) {
                ?>
                <a href="<? echo $adminUrl; ?>index.php?sec=estatico" class="secondary_btn"><? echo lang("gestion_estatico"); ?></a>
            <? } ?>
        </header>
        <section>
            <?
            $instructions = array("paginas");
            instructions($instructions);
            ?>
            <ul class="paginas_sortable home">
                <li>
                    <a href="index.php?sec=paginas&sub=editar&id=1">
                        <? echo lang("home"); ?>
                    </a>
                    <div class="option_group">
                        <div class="option estatica_0" title="<? echo lang("home"); ?>">
                            <span class="icon"></span>
                        </div>
                    </div>
                </li>
            </ul>
            <div id="generador_paginas"></div>
        </section>
    </div>
</section>