<?php
$subDefault = "secciones/paginas/list.php";

switch ($sub) {
    /*----------------------------------------------------------------- accions --*/
    case "anyadir-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        if (isset($nodo) && $nodo != "") {
            $nodo = 1;
        } else {
            $nodo = 0;
        }
        if (isset($oculto) && $oculto != "") {
            $oculto = 1;
        } else {
            $oculto = 0;
        }

        $frm = $_POST;

        $id = anyadir_guardar($frm, $imagen, $activo, $parent, $nodo, $external_link, $oculto, $lenguajes);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }
        header("location:$back_url");
        break;
    case "editar-guardar":
        if (isset($activo) && $activo != "") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        if (isset($nodo) && $nodo != "") {
            $nodo = 1;
        } else {
            $nodo = 0;
        }
        if (isset($oculto) && $oculto != "") {
            $oculto = 1;
        } else {
            $oculto = 0;
        }

        $frm = $_POST;

        editar_guardar($frm, $imagen, $imagen_old, $id_imagen_old, $activo, $parent, $nodo, $external_link, $oculto, $lenguajes, $id);
        if ($go_back == "0") {
            $back_url .= "&sub=editar&id=$id";
        }
        header("location:$back_url");
        break;
//------------------subseccions------------------------------------    
    case "editar":
    case "anyadir":
        $include = "secciones/paginas/edit.php";
        break;
    default:
        $include = $subDefault;
        break;
}
