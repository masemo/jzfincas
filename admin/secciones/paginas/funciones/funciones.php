<?php

function genera_page_preview($lenguages_activos, $idioma)
{
    global $siteUrl;
    $preview_url = "page-preview";
    if (count($lenguages_activos) > 1) {
        $preview_url = $idioma . "/" . $preview_url;
    }
    return $siteUrl . $preview_url;
}

/* -- select categoria -- */

function print_level($level)
{
    $space = "";
    if ($level > 0) {
        for ($x = 0; $x <= $level; $x++) {
            $space .= "&nbsp;&nbsp;&nbsp;";
        }
        return $space;
    } else {
        return "- ";
    }
}

function genera_pagina_item($row_categorias, $id_actual = "", $level = 0)
{
    global $lang_default;
    global $parent;
    $id = $row_categorias['id'];
    if ($id_actual == "" || $id != $id_actual) {
        ?>
        <option value="<? echo $id; ?>" <?
        if ($parent == $id) {
            echo "selected='selected'";
        }
        ?>>
            <? echo print_level($level) . obtener_valor($lang_default, "paginas", "nombre_menu", $id); ?>
        </option>
        <?
    }
}

function genera_pagina_item_optgroup($id, $level = 0)
{
    global $lang_default;
    ?>
    <optgroup label="<? echo print_level($level) . obtener_valor($lang_default, "paginas", "nombre_menu", $id); ?>"></optgroup>
    <?
}

function crea_item_menu($id_cat, $id_actual, $level)
{
    global $link;
    if ($id_actual != "" && $id_cat == $id_actual) {
        echo genera_pagina_item_optgroup($id_actual, $level);
    } else {
        $level++;
        $sql = "SELECT * FROM paginas WHERE parent = $id_cat AND visible_menu = 1 ORDER BY orden ASC";
        $result = mysqli_query($link, $sql);
        $totals = mysqli_num_rows($result);
        if ($totals > 0) {
            while ($row = mysqli_fetch_array($result)) {
                if (check_modul_contractat($row['estatica'], $row['sec_public'])) {
                    echo genera_pagina_item($row, $id_actual, $level);
                    $itemMenu = crea_item_menu($row['id'], $id_actual, $level);
                }
            }
        }
    }
    return;
}

function check_modul_contractat($estatica, $sec_public)
{
    if ((!$estatica) || ($estatica && modul_contractat($sec_public))) {
        return true;
    } else {
        return false;
    }
}

function obtener_seccion_public_pagina_estatica($id)
{
    $sec_public_id = basic_query("sec_public", "paginas", $id);
    return $seccion_public = basic_query("sec_admin", "config_secciones_public", $sec_public_id);
}

function anyadir_guardar($frm, $imagen, $activo, $parent, $nodo, $external_link, $oculto, $lenguajes)
{
    global $link;
    $noborrar = 0;

    $sql_orden = "SELECT MAX(orden) + 1 AS siguiente FROM paginas WHERE parent = $parent";
    $consulta_orden = mysqli_query($link, $sql_orden);
    $fila_orden = mysqli_fetch_array($consulta_orden);

    $ordenado = $fila_orden['siguiente'];

    if ($ordenado == "") {
        $ordenado = 1;
    }

    $sql = "INSERT INTO paginas (activo, parent, nodo, external_link, oculto, orden, sec_public) VALUES ($activo, $parent, $nodo, $external_link, $oculto, $ordenado, 4)";
//    echo $sql;
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "paginas", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        $external_link_url = http_to_url($frm["external_link_url_" . $leng]);

        inserta_campo_idiomas($leng, "paginas", "nombre_menu", $frm["nombre_menu_" . $leng], $id, $noborrar);
        inserta_campo_idiomas($leng, "paginas", "nombre", $frm["nombre_" . $leng], $id, $noborrar);
        inserta_campo_idiomas($leng, "paginas", "descripcion", $frm["descripcion_" . $leng], $id, $noborrar, true);
        inserta_campo_idiomas($leng, "paginas", "external_link_url", $external_link_url, $id, $noborrar, true);

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "paginas",
            "seccion_parents" => "paginas",
            "fu_field" => "nombre_",
            "title_field" => "nombre_menu_",
            "description_field" => "descripcion_",
            "parent_title_field" => "nombre_menu",
            "estatica" => $frm["estatica"],
            "estatica_sec" => $frm["estatica_sec"],
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas, $noborrar);
    }

    if ($imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "paginas");
    } else {
        $imagen = 0;
    }

    $sql = "UPDATE paginas SET imagen = $imagen WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("INSERT", "imagenes", $sql);
    return $id;
}

function editar_guardar($frm, $imagen, $imagen_old, $id_imagen_old, $activo, $parent, $nodo, $external_link, $oculto, $lenguajes, $id)
{
    global $link;

    if ($imagen != $imagen_old && $imagen != "") {
        $imagen = guardar_imagen($imagen, $id, "paginas");
    } else {
        if ($id_imagen_old != "") {
            $imagen = $id_imagen_old;
        } else {
            $imagen = 0;
        }
    }

    $sql = "UPDATE paginas SET activo=$activo, parent=$parent, imagen=$imagen, nodo=$nodo, external_link = $external_link, oculto=$oculto  WHERE id = $id";
//echo $sql;
    mysqli_query($link, $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        $noborrar = 0;
        $seccion_public = "paginas";
        if ($frm["estatica"] == 1) {
            $noborrar = 1;
            $seccion_public = obtener_seccion_public_pagina_estatica($id);
        }
        inserta_campo_idiomas($leng, "paginas", "nombre_menu", $frm["nombre_menu_" . $leng], $id, $noborrar);
        inserta_campo_idiomas($leng, "paginas", "nombre", $frm["nombre_" . $leng], $id, $noborrar);
        inserta_campo_idiomas($leng, "paginas", "descripcion", $frm["descripcion_" . $leng], $id, $noborrar, true);

        if ($external_link > 0) {
            $external_link_url = http_to_url($frm["external_link_url_" . $leng]);
            inserta_campo_idiomas($leng, "paginas", "external_link_url", $external_link_url, $id, $noborrar, true);
        }

        /* -- METAS -- */
        $variables_gestion_metas = array(
            "seccion" => "paginas",
            "seccion_parents" => "paginas",
            "fu_field" => "nombre_",
            "title_field" => "nombre_menu_",
            "description_field" => "descripcion_",
            "parent_title_field" => "nombre_menu",
            "estatica" => $frm["estatica"],
            "estatica_sec" => $frm["estatica_sec"],
        );
        gestion_metas($leng, $frm, $id, $parent, $variables_gestion_metas);
    }

    insert_log("UPDATE", "paginas", $sql);
}
