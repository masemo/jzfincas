<?
include("../../../system/vars.php"); //variables del sistema
include("../../../system/conexiondb.php"); //conexión a la base de datos
include("../../../system/config.php"); //configuración del entorno

include("../../../lib/funciones.php"); //funciones de libreria
include("../../../funciones/funciones.php");

include("../../../funciones/url.php"); //generador de url amigables

include("../../../../functions/main.php"); 
include("../../../common/functions.php"); //funciones que se comparten con la parte publica
include("../../../system/config-public.php"); //configuración del entorno

include("../../../system/preload.php"); //pre-carga de variables

$lang_file_url = "../../../";
include("../../../funciones/lang.php"); //gestión de lenguajes

if (isset($_POST["gridSize"])) {
    $gridSize = $_POST["gridSize"];
}
function link_to_edit_static($id_static){
    global $admin_static_sections;
    global $adminUrl;
    if(isset($admin_static_sections[$id_static])){
        $url = $adminUrl."index.php?sec=".$admin_static_sections[$id_static];
        return "<a href='$url'><span class='icon'></span></a>";
    }else{
     return "<span class='icon'></span>";
    }
}
function genera_pagina_item($row, $level = 0) {
    global $lang_default;
    global $sortableGridSize;
    global $special_pages;

    $is_special_page = is_special_page($row["id"], "paginas", $special_pages);
    
    $titulo = obtener_valor($lang_default, "paginas", "nombre_menu", $row['id']);
    ?>
    <li data-id="<? echo $row['id']; ?>" data-parent="<? echo $row['parent']; ?>" data-parent-old="<? echo $row['parent']; ?>" data-level="<? echo $level; ?>" data-level-old="<? echo $level; ?>" style="left: <? echo ($sortableGridSize * $level); ?>px" <? if (!$row['activo']) { ?>class="no_activo"<? } ?>>
        <span class="icon move"></span>
        <a href="index.php?sec=paginas&sub=editar&id=<? echo $row['id']; ?>">
            <? echo $titulo; ?>
        </a>
        <div class="option_group">
            <? if ($row['oculto'] > 0) { ?>
                <div class="option oculto" title="<? echo lang("oculto"); ?>">
                    <span class="icon"></span>
                </div>
            <? } ?>
            <? if ($row['nodo'] > 0) { ?>
                <div class="option nodo" title="<? echo lang("sin_enlace"); ?>">
                    <span class="icon"></span>
                </div>
            <? } ?>
            <? if ($row['external_link'] > 0) { ?>
                <div class="option external_link" title="<? echo lang("paginas_external_link"); ?>">
                    <span class="icon"></span>
                </div>
            <? } ?>
            <? if ($row['estatica'] == 1) { ?>
                <div class="option estatica_<? echo $row['id']; ?>" title="<? echo lang("editar"); ?> <? echo $titulo; ?>">
                    <? echo link_to_edit_static($row['id']); ?>
                </div>
            <? } ?>
            <? if (!empty($is_special_page)) { ?>
                <div class="option special_page" title="<? echo lang("special_page"); ?>">
                    <span class="icon"></span>
                </div>
            <? } ?>
            <? if ($row['estatica'] < 1 && empty($is_special_page)) { ?>
                <div class="option">
                    <button type="button" class="delete_confirm delete" data-id="<? echo $row['id']; ?>" title="<? echo lang("eliminar") . " " . obtener_valor($lang_default, "paginas", "nombre_menu", $row['id']); ?>">
                        <span class="icon">::<? echo obtener_valor($lang_default, "paginas", "nombre_menu", $row['id']); ?>::</span>
                    </button>
                </div>
            <? } ?>
        </div>
    </li>
    <?
}

function check_modul_contractat($estatica, $sec_public) {
    if ((!$estatica) || ($estatica && modul_contractat($sec_public))) {
        return true;
    } else {
        return false;
    }
}

function crea_item_menu($id, $level) {
    global $link;

    $level++;

    $sql = "SELECT * FROM paginas WHERE parent = $id AND visible_menu = 1 AND id > 1 ORDER BY orden ASC";
    $result = mysqli_query($link, $sql);
    $totals = mysqli_num_rows($result);
    if ($totals > 0) {
        while ($row = mysqli_fetch_array($result)) {
            if (check_modul_contractat($row['estatica'], $row['sec_public'])) {
                echo genera_pagina_item($row, $level);
                $itemMenu = crea_item_menu($row['id'], $level);
            }
        }
    }
    return;
}

$sql = "SELECT * FROM paginas WHERE parent = 0 AND visible_menu = 1 AND id > 1 ORDER BY orden ASC";
$consulta = mysqli_query($link, $sql);
$nPaginas = mysqli_num_rows($consulta);
?>
<ul class="paginas_sortable" id="paginas_contenido">
    <?
    while ($fila = mysqli_fetch_array($consulta)) {
        if (check_modul_contractat($fila['estatica'], $fila['sec_public'])) {
            echo genera_pagina_item($fila);
            echo crea_item_menu($fila['id'], 0);
        }
    }
    ?>
</ul>