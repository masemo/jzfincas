<?php
session_start();
include("../../../system/vars.php"); //variables del sistema
include("../../../system/config.php"); //variables del sistema
include("../../../system/conexiondb.php"); //conexión a la base de datos
include("../../../lib/funciones.php");
include("../../../funciones/funciones.php");
include("../../../includes/plugins/seo/functions.php");
include("../../../../functions/main.php");
include("../../../common/functions.php");
$lenguajes = carga_idioma();
$lang_file_url = "../../../";
include("../../../funciones/lang.php"); //gestión de lenguajes
include("../../../funciones/url.php"); //generador de url amigables

$action = $_POST['action'];
switch ($action) {
    case "guardar_parent":
        $parent = $_POST['parent'];
        $id = $_POST['id'];

        $sql = "UPDATE paginas SET parent = " . $parent . " WHERE id = " . $id;
        //echo $sql;
        if (!mysqli_query($link, $sql)) {
            echo "notok";
        }
        break;

    case "eliminar_paginas":
        $id_pagina = $_POST['id'];

        $sql = "SELECT id FROM paginas WHERE parent = $id_pagina";
        $consulta = mysqli_query($link, $sql);
        $hijos = mysqli_num_rows($consulta);

        if ($hijos > 0) {
            echo "notok";
        } else {
            eliminar_img_multiples($id_pagina,"paginas");
            eliminar_img_multiples($id_pagina,"paginas_galeria");
            elimina_idiomas($id_pagina, "paginas", "paginas");
            eliminar_seo("paginas", $id_pagina);

            $sql = "DELETE FROM paginas WHERE id = $id_pagina";
            mysqli_query($link, $sql);

            insert_log("DELETE", "paginas", $sql);
        }
        break;
        
    case "guardar_orden":
        $orden_query = $_POST['query'];

        $sql = "INSERT INTO paginas (id, orden) VALUES ";
        $sql .= $orden_query;
        $sql .= " ON DUPLICATE KEY UPDATE orden = VALUES(orden)";
        if (!mysqli_query($link, $sql)) {
            echo "notok";
        }
        break;
        
    case "get_url":
        $id = $_POST['id'];
        $url = "";
        foreach ($lenguajes["activos"] as $lenguaje) {
            $url_amigable = obtener_url_amigable($lenguaje["idioma"], "paginas", $id);
            $url .= "<p>";
            $url .= "<strong>".$lenguaje["desc"].":</strong><br />";
            $url .= "<a href='".$url_amigable."' target='_blank'>".$url_amigable."</a>\n\n";
            $url .= "</p>";
        }
        echo $url;
        break;
}
