<?php

function anyadir_guardar($frm, $activo, $lenguajes) {
    global $link;

    $sql = "INSERT INTO showroom (activo) VALUES ($activo)";
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);

    insert_log("INSERT", "showroom", $sql);

    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        $auxLeng = "_".$leng;
        $imagen = $frm["imagen".$auxLeng];
        if($imagen != ""){
            guardar_imagen($imagen, $id, "showroom","",$leng);
        }
        inserta_campo_idiomas($leng, "showroom", "alt", $frm["alt_" . $leng], $id);
        inserta_campo_idiomas($leng, "showroom", "enlace", $frm["enlace_" . $leng], $id);
    }
    $orden = get_last_order("showroom");
    update_order($orden, $id, "showroom");
    insert_log("INSERT", "showroom", $sql);

    return $id;
}

function editar_guardar($frm, $activo, $lenguajes, $id) {
    global $link;
    foreach ($lenguajes["activos"] as $lenguaje) {
        $leng = $lenguaje["idioma"];
        $auxLeng = "_".$leng;
        $imagen = $frm["imagen".$auxLeng];
        $imagen_old = $frm["imagen_old".$auxLeng];
        
        inserta_campo_idiomas($leng, "showroom", "alt", $frm["alt_" . $leng], $id);
        inserta_campo_idiomas($leng, "showroom", "enlace", $frm["enlace_" . $leng], $id);

        if ($imagen != $imagen_old && $imagen != "") {
            guardar_imagen($imagen, $id, "showroom","",$leng);
        }

        $sql = "UPDATE showroom SET activo=$activo WHERE id = $id";
        //echo $sql;
        mysqli_query($link, $sql);
    }

    insert_log("UPDATE", "showroom", $sql);
}

function eliminar($subDefault, $id) {
    global $link;

    elimina_idiomas($id, "showroom", "showroom");

    borrar_imagen($id, "showroom");

    $sql = "DELETE FROM showroom WHERE id = $id";
    mysqli_query($link, $sql);

    insert_log("DELETE", "showroom", $sql);
}
