<?
if ($sub == "editar") {
    $accion_sub = "editar-guardar";
    $sec_title = "showroom";
    $sec_action = lang("editar");

    $sql = "SELECT * FROM showroom WHERE id=$id";
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);

    /* -- anterior siguiente -- */
    $item_ant = obtener_sig_or_ant("showroom", $id, "orden", "ANT", "ASC");
    $item_sig = obtener_sig_or_ant("showroom", $id, "orden", "SIG", "ASC");

    if ($item_ant > -1) {

        $link_ant = "index.php?sec=showroom&sub=editar&id=" . $item_ant;
        $title_ant = "";
//        $img_ant = pintar_imagen(obtnener_imagen(basic_query("imagen", "showroom", $item_ant)), true);
        $img_ant = "";
    }

    if ($item_sig > -1) {
        $link_sig = "index.php?sec=showroom&sub=editar&id=" . $item_sig;
        $title_sig = "";
//        $img_sig = pintar_imagen(obtnener_imagen(basic_query("imagen", "showroom", $item_sig)), true);
        $img_sig = "";
    }
} else {

    $accion_sub = "anyadir-guardar";
    $sec_title = "Showroom";
    $sec_action = lang("anyadir_imagen");
}
$back_url = "index.php?sec=showroom";
?>
<section class="showroom">
    <div class="wrap">
        <header class="options_header">
            <h1><? echo $sec_action . " " . $sec_title; ?></h1>
            <?
            include("includes/edit-options.php");
            ?>            
        </header>
        <section>
            <? include("includes/pestanyera-lang.php"); ?>
            <form id="edit_form" action="index.php" method="post" class="fran6validate form_admins repeat_pass stength_pass">
                <input type="hidden" name="back_url" id="back_url" value="<? echo $back_url; ?>" />
                <input type="hidden" name="go_back" id="go_back" value="0" />
                <input type="hidden" name="sec" id="sec" value="showroom" />
                <input type="hidden" name="sub" id="sub" value="<? echo $accion_sub; ?>" />
                <input type="hidden" name="id" id="id" value="<?
                if (isset($id)) {
                    echo $id;
                }
                ?>" />
                <div class="lang_field_group">
                    <?
                    foreach ($lenguajes["activos"] as $lenguaje) {
                        $leng = $lenguaje["idioma"];
                        ?>
                        <fieldset class="lang_group" data-lang="<? echo $leng; ?>">
                            <p>
                                <label for="alt_<? echo $leng; ?>"><? echo lang("texto_imagen"); ?></label>
                                <input type="text" name="alt_<? echo $leng; ?>" id="alt_<? echo $leng; ?>" value="<?
                                if ($sub == "editar") {
                                    echo obtener_valor($leng, $sec, "alt", $id);
                                }
                                ?>" <? require_lang_field($leng, true); ?>/>
                            </p>
                            <p>
                                <label for="enlace_<? echo $leng; ?>"><? echo lang("enlace"); ?></label>
                                <input type="text" name="enlace_<? echo $leng; ?>" id="enlace_<? echo $leng; ?>" value="<?
                                if ($sub == "editar") {
                                    echo obtener_valor($leng, $sec, "enlace", $id);
                                }
                                ?>" />
                            </p>
                        <? include("includes/single-image.php"); ?>
                        </fieldset>
                        <?
                        unset($lenguaje);
                    }
                    ?>
                </div>
                <fieldset>
                    <div class="checkbox_group">
                        <?
                        $instructions = array("activo");
                        instructions($instructions);
                        ?>
                        <p class="checkbox">
                            <input class="checkbox" type="checkbox" name="activo" id="activo" <?
                            if (isset($fila) && $fila['activo'] > 0) {
                                echo "checked='checked'";
                            }
                            ?>/>
                            <label for="activo" class="activo"><span class="icon"></span><? echo lang("activo"); ?></label>
                        </p>
                    </div>
                </fieldset>
            </form>
            <footer>
                <?
                if ($sub == "editar") {
                    include("includes/item-anterior-siguiente.php");
                }
                ?>
            </footer>
        </section>
    </div>
</section>