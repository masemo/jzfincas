<?
$sql = "SELECT * FROM showroom";
if (isset($search_term) && $search_term != "") {
    $sql .= " WHERE id IN (SELECT id_seccion FROM idiomas WHERE texto LIKE '%" . $search_term . "%' AND seccion='showroom')";
}
$sql .= " ORDER BY orden ASC, id ASC";
//echo $sql;
$consulta = mysqli_query($link, $sql);

$sec_title = "Showroom portada";
$nItems = mysqli_num_rows($consulta);
$addLink = "index.php?sec=showroom&sub=anyadir";
$search_action = "showroom";
$tableDB = "showroom";
?>
<section class="showroom">
    <div class="wrap">
        <header class="options_header">
            <? include("includes/header.php"); ?>
        </header>
        <section>
            <? if ($nItems > 0) { ?>
                <? 
                    $instructions = array("move");
                    instructions($instructions); 
                ?>
                <div class="list_msk">
                    <table class="list drag_sortable">
                        <thead>
                            <tr>
                                <th></th>
                                <th style="width:150px;"><? echo lang("imagen"); ?></th>
                                <th><? echo lang("texto_imagen"); ?></th>
                                <th><? echo lang("enlace"); ?></th>
                                <th style="width:70px;"><? echo lang("activa"); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-tableDB="<? echo $tableDB; ?>">
                            <?
                            $i = 1;
                            while ($fila = mysqli_fetch_array($consulta)) {
                                $imagen = obtnener_imagen_idioma($sec,$fila["id"],$lang_default);
                                ?>
                                <tr data-id="<? echo $fila['id']; ?>">
                                    <td><? echo $i; ?>.</td>
                                    <td>
                                        <a href="index.php?sec=showroom&sub=editar&id=<? echo $fila['id']; ?>">
                                            <img style="margin: 5px 0 5px 0;" src="<? echo $adminUrl; ?>lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen["fichero"], true) . $timThumbGalleryParams; ?>" id="imgprincipal" alt="::editar::"/>
                                        </a>
                                    </td>
                                    <td data-field="title"><? echo obtener_valor($lang_default, "showroom", "alt", $fila['id']); ?></td>
                                    <td><? echo obtener_valor($lang_default, "showroom", "enlace", $fila['id']); ?></td>
                                    <td>
                                        <input class="checkbox activo_toggler" type="checkbox" <?
                                        if ($fila['activo'] > 0) {
                                            echo "checked='checked'";
                                        }
                                        ?>/>
                                    </td>
                                    <td>
                                        <a href="index.php?sec=showroom&sub=eliminar&id=<? echo $fila['id']; ?>" class="delete_confirm delete" title="<? echo lang("eliminar") . " " . $sec_title; ?>">
                                            <span class="icon">::<? echo lang("eliminar") . " " . $sec_title; ?>::</span>
                                        </a>
                                    </td>
                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <? } else { ?>
                <br />
                <p><? echo lang("sin_resultados"); ?> :(</p>
            <? } ?>
        </section>
    </div>
</section>