<?php

/* -------------------------------------------------------- config-public.php -- 
  $product_cats_on_nav; // Categories de productes en el #main_nav (com a submenú)
  $product_folder_style; // Maquetació tipo carpetes. If FALSE => maquetació amb menú (lateral) sempre visible.
  $first_cat_by_default; // If (!$product_folder_style) al accedir a la página productes mostrem la primera categoria de productes activa amb productes. A '/includes/template/head.php' es genera un canonical
  ------------------------------------------------------------------------------ */

function categorias_de_productos($id_categoria, $product_folder_style) {
    global $link;
    global $lang;
    $query = "SELECT id, imagen FROM productos_categoria WHERE parent = " . $id_categoria . " AND activo = 1 ORDER BY orden ASC";
//    echo $query;
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $princ = array();
        while ($row = mysqli_fetch_array($result)) {
            $tiene_subcategorias = row_query("id", "productos_categoria", "parent = " . $row["id"]);
            $aux = array();
            $aux["imagen"] = pintar_imagen(obtnener_imagen($row['imagen']), true);
            $aux["id"] = $row["id"];
            $aux["desc"] = obtener_valor($lang, "productos_categoria", "descripcion", $row['id']);
            $aux["parent"] = $id_categoria;
            $aux["url"] = obtener_url_amigable($lang, "productos_categoria", $row['id']);
            if (!empty($tiene_subcategorias) && !$product_folder_style) {
                $aux["sub"] = categorias_de_productos($row["id"], $product_folder_style);
            }
            $princ[] = $aux;
        }
        return $princ;
    } else {
        return false;
    }
}

function productos_de_categoria($id_categoria) {
    global $link;
    global $lang;
    global $siteUrl;
    $query = "SELECT id, imagen, referencia, activo FROM productos WHERE productos.categoria = $id_categoria AND productos.activo = 1 ORDER BY productos.orden  ASC";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $princ = array();
        while ($row = mysqli_fetch_array($result)) {
            $aux = array();
            $aux["imagen"] = $siteUrl . "lib/timThumb/timThumb.php?src=" . pintar_imagen(obtnener_imagen($row["imagen"]), true) . "&amp;w=640&amp;h=360&amp;a=t";
            $aux["id"] = $row["id"];
            $aux["ref"] = $row["referencia"];
            $aux["nombre"] = obtener_valor($lang, "productos", "nombre", $row['id']);
            $aux["desc"] = obtener_valor($lang, "productos", "descripcion", $row['id']);
            $aux["url"] = obtener_url_amigable($lang, "productos", $row['id']);
            $princ[] = $aux;
        }
        return $princ;
    } else {
        return false;
    }
}

$productos = $categorias = $descripcion = "";
$id_categoria = $id; // sols pel concepte de categoria.id
$id_categoria_listado = 0;

$title_seccion = obtener_valor($lang, "paginas", "nombre", $id_pagina_productos);
$descripcion = obtener_valor($lang, "paginas", "descripcion", $id_pagina_productos);

if ($product_folder_style) {
    $id_categoria_listado = $id_categoria;
}

$categorias = categorias_de_productos($id_categoria_listado, $product_folder_style);
$migas = migas_de_pan_productos_public($id_categoria_listado, $lang);


if ($id_categoria == 0 && !$first_cat_by_default) { // si estic en la landing
    //    include("controllers/paginas.php");
    $productos_title = $title_seccion;
} else {
    if (!empty($categorias)) {
        if ($id_categoria == 0 && !$product_folder_style && $first_cat_by_default) {
            $id_categoria = primera_categoria_productos_activa();
        }
        $descripcion = obtener_valor($lang, $seccion, "contenido", $id_categoria);
    }
    $productos = productos_de_categoria($id_categoria);
    $productos_title = obtener_valor($lang, "productos_categoria", "descripcion", $id_categoria);
}

if (!empty($migas)) {
    $miga_inicio = array(
        "url" => obtener_url_amigable($lang, "productos_categoria", 0),
        "title" => $title_seccion
    );
    array_unshift($migas, $miga_inicio);
    array_pop($migas);
}    