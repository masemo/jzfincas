<?php

function noticias_categorias() {
    global $link;
    global $lang;
    $query = "SELECT * FROM noticias_categoria WHERE activo = 1 ORDER BY orden ASC";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $princ = array();
        while ($row = mysqli_fetch_array($result)) {
            $aux = array();
            if ($row["imagen"] > 0) {
                $aux["imagen"] = obtnener_imagen($row["imagen"]);
            }
            $aux["id"] = $row["id"];
            $aux["title"] = obtener_valor($lang, "noticias_categoria", "descripcion", $row["id"]);
            $aux["link"] = obtener_url_amigable($lang, "noticias_categoria", $row["id"]);

            $princ[] = $aux;
        }
        return $princ;
    } else {
        return false;
    }
}

function noticias_detalle($lang, $id) {
    global $link;
    $princ = array();

    $sql = "SELECT * FROM noticias  "
            . "WHERE activo = 1 AND id = $id "
            . " AND (DATEDIFF(fecha_public, current_date) <= 0) LIMIT 1";
    //echo $sql;
    $consulta = mysqli_query($link, $sql);
    $fila = mysqli_fetch_array($consulta);
    if (!fecha_caducada($fila["fecha_caduca"])) {
        if ($fila['imagen'] > 0) {
            $imagen = pintar_imagen(obtnener_imagen($fila['imagen']));
            $thumbnail = pintar_imagen(obtnener_imagen($fila['imagen']), true);
        } else {
            $imagen = $thumbnail = "";
        }
        $princ['id'] = $id;
        $princ['parent'] = $fila['parent'];
        $princ['link'] = obtener_url_amigable($lang, "noticias", $id);
        $princ['linkCategoria'] = obtener_url_amigable($lang, "noticias_categoria", $fila['parent']);
        $princ['imagen'] = $imagen;
        $princ['thumbnail'] = $thumbnail;
        $month = lang("mes_" . date("m", strtotime($fila['fecha_public'])));
        $year = date("Y", strtotime($fila['fecha_public']));
        $princ['date'] = $month . " " . $year;
        $princ['datetime'] = $fila['fecha_public'];
        $princ['categoria'] = obtener_valor($lang, "noticias_categoria", "descripcion", $fila['parent']);
        $princ['title'] = obtener_valor($lang, "noticias", "titular", $id);
        $princ['contenido'] = obtener_valor($lang, "noticias", "contenido", $id);
        $princ["galeria"] = obtener_galeria_multi_imagen($id, "noticias_galeria");
    }
    //print_r($princ);

    return $princ;
}

$noticias_categorias = noticias_categorias();

if (!isset($parametros["preview"])) {
    $noticia = noticias_detalle($lang, $id);
    $migas = array(
        array(
            "url" => $noticias_fu,
            "title" => obtener_valor($lang, "paginas", "nombre_menu", $id_pagina_noticias)
        )
    );
    if (!empty($noticias_categorias)) {
        $current_cat = array(
            "url" => obtener_url_amigable($lang, "noticias_categoria", $noticia["parent"]),
            "title" => obtener_valor($lang, "noticias_categoria", "descripcion", $noticia["parent"])
        );
        array_push($migas, $current_cat);
    }
} else {
    /* -- preview -- */
    $id_imagen = $imagen = $thumbnail = "";
    if (isset($_POST["previewLang"])) {
        $pl = $_POST["previewLang"];
        $date = strtotime(fecha_to_mysql($_POST["fecha_public"]));

        if ($_POST['imagen'] != "") {
            $id_imagen = $_POST['imagen'];
        } else {
            if ($_POST['id_imagen_old'] != "") {
                $id_imagen = obtnener_imagen($_POST['id_imagen_old']);
            }
        }
        $imagen = pintar_imagen($id_imagen);
        $thumbnail = pintar_imagen($id_imagen, true);
        $title = $_POST["titular_" . $pl];
        $datetime = $_POST["fecha_public"];
        $contenido = $_POST["contenido_" . $pl];
    } else {
        $date = strtotime(date("Y-m-d"));
        $title = lang("contenido_no_disponible");
        $contenido = "<p class='alert notok'>" . lang("contenido_no_disponible") . "</p>";
    }
    $datetime = date("Y-m-d");
    $month = lang("mes_" . date("m", $date));
    $year = date("Y", $date);
    $date = $month . " " . $year;
    $noticia = array(
        "imagen" => $imagen,
        "thumbnail" => $thumbnail,
        "title" => $title,
        "datetime" => $datetime,
        "date" => $date,
        "contenido" => $contenido
    );
    $id = 0;
    $migas = array(
        array(
            "url" => "",
            "title" => lang("preview")
        )
    );
}
