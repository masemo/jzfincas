<?php

function galeria_imagenes($id, $sec) {
    global $link;
    global $lang;
    $princ = array();

    $sql = "SELECT * FROM imagenes WHERE idsec = $id AND sec = '$sec' ORDER BY orden ASC, ID ASC";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $aux = array();

        $aux['image'] = obtnener_imagen($fila['id']);
        $aux['desc'] = obtener_valor($lang, "imagen", "img_alt", $fila['id']);

        $princ[] = $aux;
    }

    return $princ;
}

if (!isset($parametros["preview"])) {
    $sql_paginas = "SELECT * FROM paginas WHERE id = $id AND activo = 1 LIMIT 1";
    //echo $sql_paginas;
    $consulta_paginas = mysqli_query($link, $sql_paginas);
    
    $title = $desc = $imagen_principal = "";
    
    if ($consulta_paginas) {
        $nItems = mysqli_num_rows($consulta_paginas);
        if ($nItems > 0) {
            $filas_paginas = mysqli_fetch_array($consulta_paginas);

            $title = obtener_valor($lang, "paginas", "nombre", $id);
            $desc = obtener_valor($lang, "paginas", "descripcion", $id);
            $imagen_principal = pintar_imagen(obtnener_imagen($filas_paginas["imagen"]));

            $migas = migas_de_pan($id, $lang);
            array_pop($migas); // per a que no isca la mateixa pagina en la que estic

            $galeria = galeria_imagenes($id, "paginas_galeria");
        }
    }
} else {
    /* -- preview -- */
    $pl = $_POST["previewLang"];
    if ($_POST['id_imagen_old'] != "") {
        $imagen = obtnener_imagen($_POST['id_imagen_old']);
    } else {
        $imagen = $_POST['imagen'];
    }
    $imagen_principal = pintar_imagen($imagen);
    $title = $_POST["nombre_" . $pl];
    $desc = $_POST["descripcion_" . $pl];
    $migas = array(
        array(
            "url" => "",
            "title" => lang("preview"),
        )
    );
    if ($_POST['id'] != "") {
        $galeria = galeria_imagenes($_POST['id'], "paginas_galeria");
    }
}
