<?php
function obtener_galeria($parent, $lang) {
    global $link;
    $princ = array();

    $sql = "SELECT * FROM galeria_categoria WHERE parent = $parent AND activo = 1 ORDER BY orden ASC";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $aux = array();
        $imagen = obtnener_imagen($fila['imagen']);

        $aux['id'] = $fila['id'];
        $aux['title'] = obtener_valor($lang, "galeria_categoria", "descripcion", $fila['id']);
        $aux['image'] = pintar_imagen($imagen);
        $aux['imageRelative'] = pintar_imagen($imagen, true);
        $aux['link'] = obtener_url_amigable($lang, "galeria_categoria", $fila['id']);

        $princ[] = $aux;
    }

    return $princ;
}

function galeria_imagenes($id, $lang) {
    global $link;
    $princ = array();

    $sql = "SELECT imagenes.* FROM imagenes, galeria_categoria WHERE galeria_categoria.id = imagenes.idsec AND sec = 'galeria' AND galeria_categoria.id = $id ORDER BY orden ASC"; //primera pasada
//    echo $sql;
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $aux = array();
        $imagen = obtnener_imagen($fila['id']);
        $aux['title'] = obtener_valor($lang, "imagen", "img_alt", $fila['id']);
        $aux['link'] = $aux['image'] = pintar_imagen($imagen);
        $aux['imageRelative'] = pintar_imagen($imagen, true);

        $princ[] = $aux;
    }

    return $princ;
}

$tipo = "categorias";

$title_pagina = obtener_valor($lang, "paginas", "nombre_menu", $id_pagina_galeria_imagenes);
if (tiene_imagenes($id)) {
    $galeria = galeria_imagenes($id, $lang);
    $nGalerias = count($galeria);
    $tipo = "imagenes";
    $title = obtener_valor($lang, "galeria_categoria", "descripcion", $id);
} else {
    $title = $title_pagina;
    $galeria = obtener_galeria($id, $lang);
    $totalGalerias = row_query("COUNT(id) as total", "galeria_categoria", "activo = 1");
    $nGalerias = $totalGalerias["total"];
    if ($nGalerias == 1) { // si sols hi ha una categoria, bossem directament les imatges
        $id = $galeria[0]["id"];
        $galeria = galeria_imagenes($id, $lang);
        $tipo = "imagenes";
    }
}

$migas = migas_de_pan_galeria($id, $lang, "");
$miga_inicio = array(
    "url" => $imagenes_fu,
    "title" => $title_pagina
);
array_unshift($migas, $miga_inicio);
array_pop($migas);

