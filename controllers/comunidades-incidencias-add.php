<?php


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include("admin/common/mail/class.phpmailer.php");
    include("admin/common/mail/class.smtp.php");
    include("admin/common/mail/mail.php");

    $incidencia_add_siniestro = mysqli_real_escape_string($link, $_POST['incidencia_add_siniestro']);
    $incidencia_add_sector = mysqli_real_escape_string($link, $_POST['incidencia_add_sector']);
    $incidencia_add_nombre = mysqli_real_escape_string($link, $_POST['incidencia_add_nombre']);
    $incidencia_add_telefono = mysqli_real_escape_string($link, $_POST['incidencia_add_telefono']);
    $incidencia_add_email = mysqli_real_escape_string($link, $_POST['incidencia_add_email']);
    $incidencia_add_piso = mysqli_real_escape_string($link, $_POST['incidencia_add_piso']);
    $incidencias_sector = reset(get_data("SELECT * FROM comunidades_incidencias_sector WHERE id = $incidencia_add_sector"));
    $comunidad = decryptIt($_SESSION["login_usuario"]);
    $comunidad = reset(get_data("SELECT * FROM comunidades WHERE id = $comunidad"));
    $comunidad_id = $comunidad["id"];

    if ($incidencia_add_sector != "" && $incidencia_add_sector != "" && $incidencia_add_nombre != "" && $incidencia_add_telefono != ""&& $incidencia_add_email != ""&& $incidencia_add_piso != "") {
        $sql = "INSERT INTO comunidades_incidencias (f_apertura, sector, siniestro, estado, f_finalizacion, parent, nombre, telefono, piso, email, activo) VALUES (NOW(), $incidencia_add_sector, '$incidencia_add_siniestro', 1, NULL, $comunidad_id, '$incidencia_add_nombre', $incidencia_add_telefono, '$incidencia_add_piso', '$incidencia_add_email', 0);";

        mysqli_query($link, $sql);
        $id = mysqli_insert_id($link);

        $idpublic = incident_code($id);

        $sql = "UPDATE comunidades_incidencias SET idpublic = '$idpublic' WHERE id=$id";
        mysqli_query($link, $sql);

        $emailCreator = getHTML($siteUrl . "themes/", $siteTheme . "/template/mails/mail-nueva-incidencia.php");
        $emailCreator = str_replace("#logo", $themeUrl . $logo, $emailCreator);
        $emailCreator = str_replace("#siteUrl", $siteUrl, $emailCreator);
        $emailCreator = str_replace("#siteName", $siteName, $emailCreator);

        $data = array(
            "idpublic" => $idpublic,
            "finca" => $comunidad["nombre"],
            "incidencia" => $incidencia_add_siniestro,
            "sector" => htmlentities($incidencias_sector["nombre"]),
            "nombre" => $incidencia_add_nombre,
            "telefono" => $incidencia_add_telefono,
            "email" => $incidencia_add_email,
            "piso" => $incidencia_add_piso,
            "link" => $siteUrl . "admin/index.php?sec=comunidades&sub=editar-incidencias&id=$id",
        );

        foreach ($data as $key => $field) {
            $emailCreator = str_replace("#" . $key, $field, $emailCreator);
        }

        $envio_mail = sendmailAUTH($contact_to, $toname, $sender, $sendername, "Nueva incidencia " . $idpublic, $emailCreator, $smtpHost, $smtpUser, $smtpPass);

        if ($envio_mail == "ok") {
            $error_mail = false;
        } else {
            $error_mail = true;
        }
    }else{
        $error = lang("comunidades_incidencias_error_empty");
    }

}
$data_sectors = get_data("SELECT * FROM comunidades_incidencias_sector");

foreach ($data_sectors as $data_sector) {
    $data_sectors_options[] = array("id" => $data_sector["id"], "value" => $data_sector["nombre"]);
}

$fields = array(
    "incidencia_add_nombre" => array(
        "type" => "text",
        "class" => "required"
    ),
    "incidencia_add_telefono" => array(
        "type" => "number",
        "class" => "esnumero required"
    ),
    "incidencia_add_email" => array(
        "type" => "text",
        "class" => "email required"
    ),
    "incidencia_add_piso" => array(
        "type" => "text",
        "class" => "required"
    ),
    "incidencia_add_siniestro" => array(
        "type" => "textarea",
        "class" => "required"
    ),
    "incidencia_add_sector" => array(
        "type" => "select",
        "class" => "required",
        "options" => $data_sectors_options,

    ),
    "incidencia_add_enviar" => array(
        "type" => "submit",
        "class" => "",
        "attr" => array()
    )
);
?>