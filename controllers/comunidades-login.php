<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $usuario = mysqli_real_escape_string($link, $_POST['login_usuario']);
    $password = mysqli_real_escape_string($link, $_POST['login_password']);

    $sql = "SELECT id, activo FROM comunidades WHERE usuario = '$usuario' AND password = md5('$password') AND activo = 1";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $count = mysqli_num_rows($result);

    if ($count == 1 && $row["activo"] == 1) {
        $_SESSION['login_usuario'] = encryptIt($row['id']);

        header("location: " . obtener_url_amigable($lang, "comunidades", 1));
    } else {
        if ($row["activo"] == 0) {
            $error = lang("comunidades_usuario_desactivado");
        } else {
            $error = lang("comunidades_usuario_incorrecto");
        }
    }
} else if (isset($_SESSION['login_usuario'])) {
    $id = decryptIt($_SESSION['login_usuario']);
    
    $sql = "SELECT id, activo FROM comunidades WHERE id = '$id' AND activo = 1";

    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $count = mysqli_num_rows($result);

    if ($count == 1 && $row["activo"] == 1) {
        header("location: " . obtener_url_amigable($lang, "comunidades", 1));
    }
}

$fields = array(
    "login_usuario" => array(
        "type" => "text",
        "class" => "required"
    ),
    "login_password" => array(
        "type" => "password",
        "class" => "required"
    ),
    "login_enviar" => array(
        "type" => "submit",
        "class" => "",
        "attr" => array()
    )
); ?>