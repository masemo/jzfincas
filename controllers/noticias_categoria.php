<?php

function obtener_pagina_actual($param) {
    $param_array = explode("=", $param);
    return $param_array[1];
}

function preload_noticias($pagina_actual, $parent) {
    global $link;

    $registros_por_pagina = 8;
    $paginas_vistas = 8;

    $princ = array();

    $sql = "SELECT * FROM noticias WHERE activo = 1 ";
    if ($parent != "ultimas-noticias") {
        $sql .= " AND parent = $parent ";
    }
    $sql .= "AND (DATEDIFF(fecha_public, current_date) <= 0) ORDER BY fecha_public DESC";


    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);

    $num_paginas = ceil($numero_filas / $registros_por_pagina);
    $pos_inicial = ($pagina_actual * $registros_por_pagina) - $registros_por_pagina;

    $sql = $sql . " LIMIT $pos_inicial,$registros_por_pagina";
    $consulta = mysqli_query($link, $sql);

    $princ['consulta'] = $consulta;
    $princ['num_paginas'] = $num_paginas;
    $princ['pos_inicial'] = $pos_inicial;
    $princ['registros_por_pagina'] = $registros_por_pagina;
    $princ['paginas_vistas'] = $paginas_vistas;

    return $princ;
}

function noticias_listado_paginacion($consulta, $parent, $lang) {
    global $siteUrl;
    $princ = array();

    while ($fila = mysqli_fetch_array($consulta)) {
        if (!fecha_caducada($fila["fecha_caduca"])) {
            $aux = array();
            $month = lang("mes_" . date("m", strtotime($fila['fecha_public'])));
            $year = date("Y", strtotime($fila['fecha_public']));
            $aux['date'] = $month . " " . $year;
            $aux['datetime'] = $fila['fecha_public'];
            $aux['link'] = obtener_url_amigable($lang, "noticias", $fila['id']);
            $aux['title'] = obtener_valor($lang, "noticias", "titular", $fila['id']);
            $aux['desc'] = extrae_cadena(strip_tags(obtener_valor($lang, "noticias", "contenido", $fila['id'])), 150);
            $aux["imagen"] = "";
            if($fila['imagen'] > 0){
                $aux["imagen"] = $siteUrl."lib/timThumb/timThumb.php?src=".pintar_imagen(obtnener_imagen($fila['imagen']), true)."&amp;w=720&amp;zc=2";
            }
            if ($parent == "ultimas-noticias") {
                $aux["categoria"] = obtener_valor($lang, "noticias_categoria", "descripcion", $fila['parent']);
            }

            $princ[] = $aux;
        }
    }

    return $princ;
}

function genera_paginacion($noticias_cargadas, $pagina_actual, $noticias_fu) {

    $paginacion = "";
    $paginas_vistas = $noticias_cargadas['paginas_vistas'];
    $num_paginas = $noticias_cargadas['num_paginas'];

    if ($num_paginas > 1) {
        $pagina_inicio = ($pagina_actual - $paginas_vistas) + 1;
        if ($pagina_inicio < 1) {
            $pagina_inicio = 1;
        }

        $a = $pagina_inicio;
        if ($pagina_inicio > 1) {
            $paginacion = '<a href="' . $noticias_fu . '?page=' . ($pagina_inicio - 1) . '"> << </a>';
        }

        while ($a <= $pagina_inicio + $paginas_vistas - 1 && $num_paginas >= $a) {
            if ($pagina_actual == $a) {
                $paginacion .= '<a class="selected" href="' . $noticias_fu . '?page=' . $a . '"> ' . $a . ' </a>';
            } else {
                if ($a == 1) {
                    $paginacion .= '<a href="' . $noticias_fu . '">' . $a . ' </a>';
                } else {
                    $paginacion .= '<a href="' . $noticias_fu . '?page=' . $a . '"> ' . $a . ' </a>';
                }
            }
            $a++;
        }

        if ($pagina_inicio + $paginas_vistas - 1 < $num_paginas && $num_paginas > $pagina_inicio + $paginas_vistas - 1) {
            $paginacion .= '<a href="' . $noticias_fu . '?page=' . $a . '"> >> </a>';
        }
    }

    return $paginacion;
}

function noticias_categorias() {
    global $link;
    global $lang;
    $query = "SELECT * FROM noticias_categoria WHERE activo = 1 ORDER BY orden ASC";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $princ = array();
        while ($row = mysqli_fetch_array($result)) {
            $aux = array();
            if ($row["imagen"] > 0) {
                $aux["imagen"] = obtnener_imagen($row["imagen"]);
            }
            $aux["id"] = $row["id"];
            $aux["title"] = obtener_valor($lang, "noticias_categoria", "descripcion", $row["id"]);
            $aux["link"] = obtener_url_amigable($lang, "noticias_categoria", $row["id"]);

            $princ[] = $aux;
        }
        return $princ;
    } else {
        return false;
    }
}

$pagina_actual = 1;
if (!empty($parametros) && isset($parametros["page"])) {
    $pagina_actual = $parametros["page"];
}
$parent = $id; // sols per tindre el concepte de "parent"

$noticias_categorias = noticias_categorias();
if (!empty($noticias_categorias)) {
    if ($parent == 0) { // si estic en la landing bosse últimas noticias
        $parent = "ultimas-noticias";
        $noticias_title = lang("ultimas_noticias");
    } else {
        $noticias_title = obtener_valor($lang, "noticias_categoria", "descripcion", $parent);
    }
    $migas = array(
        array(
            "url" => $noticias_fu,
            "title" => obtener_valor($lang, "paginas", "nombre_menu", $id_pagina_noticias)
        )
    );
} else {
    $migas = "";
    $noticias_title = obtener_valor($lang, "paginas", "nombre", $id_pagina_noticias);
}

$noticias_cargadas = preload_noticias($pagina_actual, $parent);
$paginator = genera_paginacion($noticias_cargadas, $pagina_actual, $noticias_fu);
$noticias = noticias_listado_paginacion($noticias_cargadas['consulta'], $parent, $lang);
