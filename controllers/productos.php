<?php
function producto_data($id_producto) {
    global $link;
    global $lang;
    $query = "SELECT id, imagen, referencia, categoria FROM productos WHERE id = " . $id_producto;
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $row = mysqli_fetch_array($result);
        $princ = array();
        $main_image = array(
            "imagen" => obtnener_imagen($row["imagen"]),
            "alt" => obtener_valor($lang, "productos", "nombre", $id_producto)
        );
        $princ["imagen"] = $main_image;
        $princ["ref"] = $row["referencia"];
        $princ["categoria"] = $row["categoria"];
        $princ["categoria_nombre"] = obtener_valor($lang, "productos_categoria", "descripcion", $row["categoria"]);
        $princ["nombre"] = obtener_valor($lang, "productos", "nombre", $id_producto);
        $princ["descripcion"] = obtener_valor($lang, "productos", "descripcion", $id_producto);
        $princ["caracteristicas"] = obtener_valor($lang, "productos", "caracteristicas", $id_producto);
        $princ["galeria"] = obtener_galeria_multi_imagen($id_producto, "productos-multi");
        return $princ;
    } else {
        return false;
    }
}

$content_width = "100"; // contingut al 100% de ample si NO hi ha menu de categories en llista
if(!$product_folder_style){
    include("productos_categoria.php");
    $content_width = "75"; // contingut al 75% si hi ha menu de categories en llista
}
$item = producto_data($id);

$products_title = obtener_valor($lang, "paginas", "nombre_menu", $id_pagina_productos);
$id_categoria = basic_query("categoria", "productos", $id);
$categoria = obtener_valor($lang, "productos_categoria", "descripcion", $id_categoria);
$product_cat_fu = obtener_url_amigable($lang, "productos_categoria", $id_categoria);


$migas = migas_de_pan_productos_public($id_categoria, $lang);

if (!empty($migas)) {
    $miga_inicio = array(
        "url" => obtener_url_amigable($lang, "productos_categoria", $id_pagina_productos),
        "title" => $products_title
    );
    array_unshift($migas, $miga_inicio);
}