<?
$noticias_listado = noticias_listado("",3);
$noticiasTitle = obtener_valor($lang, "paginas", "nombre", $id_pagina_noticias);

$categoria_imagenes_listado = categoria_imagenes_listado(4);
$imagenesTitle = obtener_valor($lang, "paginas", "nombre", $id_pagina_galeria_imagenes);

$videos_listado = videos_listado(2);
$videosTitle = obtener_valor($lang, "paginas", "nombre", $id_pagina_videos);

$banners_listado = banners_listado(1);

$homeTitle = obtener_valor($lang, "paginas", "nombre", 1);
$homeDesc = obtener_valor($lang, "paginas", "descripcion", 1);
$homeImgId = basic_query("imagen", "paginas", 1);
($homeImgId > 0) ? ($homeImg = pintar_imagen(obtnener_imagen($homeImgId))) : ($homeImg = "");
$homeImgAlt = $homeTitle." | ".$siteName;