<?php

function genera_product_nav($id_product_page, $lang)
{
    global $link;
    global $id_pagina_productos;
    $query_cats = "SELECT * FROM productos_categoria WHERE parent = 0 ORDER BY orden ASC";
    $result_cats = mysqli_query($link, $query_cats);
    $nItems = mysqli_num_rows($result_cats);
    $product_nav = "";
    if ($nItems > 0) {
        $url_all = obtener_url_amigable($lang, "paginas", $id_pagina_productos);
        $product_nav = '<a href="' . $url_all . '">' . obtener_valor($lang, "paginas", "nombre_menu", $id_product_page) . '</a>';
        $product_nav .= "<ul>";
        while ($row_cats = mysqli_fetch_array($result_cats)) {
            $nombre = obtener_valor($lang, "productos_categoria", "descripcion", $row_cats['id']);
            $url = obtener_url_amigable($lang, "productos_categoria", $row_cats['id']);
            $product_nav .= "<li><a href='$url' rel='subsection'>$nombre</a></li>";
        }
        $product_nav .= "</ul>";
        return $product_nav;
    }
}

function pinta_link_pagina($seccion_valor, $id_valor, $seccion_fu, $id_fu, $lang)
{
    return '<a href="' . obtener_url_amigable($lang, $seccion_fu, $id_fu) . '" rel="subsection">' . obtener_valor($lang, $seccion_valor, "nombre_menu", $id_valor) . '</a>';
}

function genera_html_menu($row, $lang)
{
    global $product_cats_on_nav;
    global $id_pagina_productos;
    $menu = "";
    if ($row['external_link']) {
        $menu = '<a href="' . obtener_valor($lang, "paginas", "external_link_url", $row['id']) . '" target="_blank">' . obtener_valor($lang, "paginas", "nombre_menu", $row['id']) . '</a>';
    } else {
        if ($product_cats_on_nav && $row['id'] == $id_pagina_productos) { //PRODUCTOS
            $menu = genera_product_nav($row['id'], $lang);
        } else {
            $seccion_valor = $seccion_fu = "paginas";
            $id_valor = $id_fu = $row["id"];
            if ($row['estatica'] == 1) {
                $seccion_fu = $row['estatica_sec'];
                $id_fu = 0;
            }
            if (tiene_paginas_subcats($row["id"])) {
                if ($row['nodo']) {
                    $menu = '<p>' . obtener_valor($lang, $seccion_valor, "nombre_menu", $id_valor) . '</p>';
                } else {
                    $menu = pinta_link_pagina($seccion_valor, $id_valor, $seccion_fu, $id_fu, $lang);
                }
            } else {
                $menu = pinta_link_pagina($seccion_valor, $id_valor, $seccion_fu, $id_fu, $lang);
            }
        }
    }

    return $menu;
}

function check_modul_contractat($estatica, $sec_public)
{
    if ((!$estatica) || ($estatica && modul_contractat($sec_public))) {
        return true;
    } else {
        return false;
    }
}

function selectar_item($idsel, $id, $seccion)
{
    global $id_pagina_productos;
    global $id_pagina_noticias;
    global $id_pagina_galeria_imagenes;
    global $id_pagina_videos;
    switch ($seccion) {
        case "paginas":
            if ($id == $idsel) {
                return true;
            }
            break;
        case "productos_categoria":
        case "productos":
            if ($id == $id_pagina_productos) {
                return true;
            }
            break;
        case "noticias_categoria":
        case "noticias":
            if ($id == $id_pagina_noticias) {
                return true;
            }
            break;
        case "galeria_categoria":
            if ($id == $id_pagina_galeria_imagenes) {
                return true;
            }
            break;
        case "videos_categoria":
            if ($id == $id_pagina_videos) {
                return true;
            }
            break;
    }
    return false;
}

function crea_menu($id, $seccion, $idsel, $lang)
{
    global $link;
    $menu = "";

    $sql_nav = "SELECT * FROM paginas WHERE parent = $id AND activo = 1 AND oculto = 0 AND visible_menu = 1 ORDER BY orden ASC";
//    echo $sql_nav."<br/>"; 
    $consulta_nav = mysqli_query($link, $sql_nav);
    $totals_nav = mysqli_num_rows($consulta_nav);

    if ($totals_nav > 0) {
        if ($id > 0) {
            $menu = "<ul>";
        }

        while ($row_nav = mysqli_fetch_array($consulta_nav)) {
            $selectar_item = "";
            if (check_modul_contractat($row_nav['estatica'], $row_nav['sec_public'])) {
                if (selectar_item($idsel, $row_nav['id'], $seccion)) {
                    $selectar_item = 'class="selected"';
                }
                /* if (tiene_paginas_subcats($row_nav['id'])){
                  $launcher = "launcher";
                  } */

                $menu .= "<li $selectar_item>";
                $menu .= genera_html_menu($row_nav, $lang);
                $menu .= crea_menu($row_nav['id'], $seccion, $idsel, $lang);
                $menu .= "</li>";
            }
        }

        if ($id > 0) {
            $menu .= "</ul>";
        }
    }

    return $menu;
}

function slide_show_portada($lang)
{
    global $link;
    global $lang;
    $princ = array();

    $sql = "SELECT id FROM showroom WHERE activo = 1 ORDER BY orden ASC";
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $aux = array();

        $imagen = obtnener_imagen_idioma("showroom", $fila["id"], $lang);
        $aux['link'] = obtener_valor($lang, "showroom", "enlace", $fila['id']);
        $aux['image'] = pintar_imagen($imagen["fichero"]);
        $aux['text'] = obtener_valor($lang, "showroom", "alt", $fila['id']);

        $princ[] = $aux;
    }

    return $princ;
}

if (count($idiomas["activos"]) > 1) {
    $home_fu = $siteUrl . $lang . "/";
} else {
    $home_fu = $siteUrl;
}
$home_txt = obtener_valor($lang, "paginas", "nombre_menu", 1);

$productos_fu = obtener_url_amigable($lang, "productos_categoria", 0);
$noticias_fu = obtener_url_amigable($lang, "noticias_categoria", 0);
$imagenes_fu = obtener_url_amigable($lang, "galeria_categoria", 0);
$videos_fu = obtener_url_amigable($lang, "videos_categoria", 0);

$error_fu = obtener_url_amigable($lang, "error", 0);
$contacto_fu = obtener_url_amigable($lang, "contacto", 0);
$aviso_legal_fu = obtener_url_amigable($lang, "aviso-legal", 0);
$sitemap_fu = obtener_url_amigable($lang, "sitemap", 0);
$menu = crea_menu(0, $seccion, $id_seccion, $lang);

$slidexouPortada = slide_show_portada($lang);
