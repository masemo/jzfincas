<?php

function obtener_galeria($id, $lang) {
    global $link;
    $princ = array();

    $sql = "SELECT * FROM videos_categoria WHERE parent = $id AND activo = 1";
//    echo $sql;
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $aux = array();

        $aux['title'] = obtener_valor($lang, "videos_categoria", "descripcion", $fila['id']);
        $aux['image'] = url_miniatura(obtener_ultimo_video($fila['id']));
        $aux['link'] = obtener_url_amigable($lang, "videos_categoria", $fila['id']);

        $princ[] = $aux;
    }

    return $princ;
}

function galeria_videos($id, $lang) {
    global $link;
    $princ = array();

    $sql = "SELECT * FROM videos WHERE categoria = $id"; //primera pasada
    $consulta = mysqli_query($link, $sql);

    while ($fila = mysqli_fetch_array($consulta)) {
        $aux = array();

        $aux['title'] = obtener_valor($lang, "videos", "titulo", $fila['id']);
        $aux['image'] = url_miniatura($fila['video']);
        $aux['link'] = $fila['video'];

        $princ[] = $aux;
    }

    return $princ;
}

$tipo = "categorias";
$categoria = $id; // sols per tindre el concepte de "categoria"
$inicio_title = $videos_title = obtener_valor($lang, "paginas", "nombre_menu", $id_pagina_videos);

if (tiene_videos($categoria)) {
    $galeria = galeria_videos($categoria, $lang);
    $tipo = "videos";
    $videos_title = obtener_valor($lang, $seccion, "descripcion", $categoria);
} else {
    $galeria = obtener_galeria($categoria, $lang);
}

$migas = migas_de_pan_videos($categoria, $lang, "");
$miga_inicio = array(
    "url" => $videos_fu,
    "title" => $inicio_title
);
array_unshift($migas, $miga_inicio);
array_pop($migas);
