<?php
header('Content-Type: text/html; charset=ISO-8859-1');
include("../admin/system/vars.php");
include("../admin/system/config-public.php");

include("../admin/common/functions.php");

include("../admin/common/mail/class.phpmailer.php");
include("../admin/common/mail/class.smtp.php");
include("../admin/common/mail/mail.php");

parse_str($_POST['formData'], $data);
//print_r($data);

if (!empty($data)) {
    $emailCreator = getHTML($siteUrl . "themes/", $siteTheme . "/template/mails/mail-contacto.php");
    $emailCreator = str_replace("#logo", $themeUrl . $logo, $emailCreator);
    $emailCreator = str_replace("#siteUrl", $siteUrl, $emailCreator);
    $emailCreator = str_replace("#siteName", $siteName, $emailCreator);
    foreach ($data as $key => $field) {
        $emailCreator = str_replace("#" . $key, $field, $emailCreator);
    }

    $envio_mail = sendmailAUTH($contact_to, $toname, $sender, $sendername, $subject, $emailCreator, $smtpHost, $smtpUser, $smtpPass);
    if ($envio_mail == "ok") {
        echo "ok";
    } else {
        echo $envio_mail;
    }
} else {
    echo "notok";
}
