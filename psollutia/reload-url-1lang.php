<?

// elimina subcarpeta idioma www.taca.com/es/cosas.html => www.taca.com/cosas.html
include("../admin/system/vars.php");
include("../admin/system/conexiondb.php");

//ini_set("display_errors", 2);

function recargar_idiomas($lang, $tabla) {
    global $link;

    echo "<h1>".strtoupper($tabla) . ":</h1>";

    $sql = "DELETE FROM $tabla WHERE idioma <> '$lang'";
    if (mysqli_query($link, $sql)) {
        echo "Borrando idiomas adicionales<br/><br/>";
    }

    $sql = "SELECT id, url_friendly_complet FROM $tabla WHERE idioma = '$lang' AND url_friendly_complet <> ''";
    $consulta = mysqli_query($link, $sql);
    $numero_filas = mysqli_num_rows($consulta);

    if ($numero_filas) {
        while ($fila = mysqli_fetch_array($consulta)) {
        $aux = "";
            echo $fila['url_friendly_complet'] . " -> ";

            $prefix = $lang . "/";
            $str = $fila['url_friendly_complet'];
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $aux = substr($str, strlen($prefix));
            }else{
                $aux = $str;
            }

            echo $aux;

            $sql = "UPDATE $tabla SET url_friendly_complet='$aux' WHERE id = " . $fila['id'];
            echo "<br/>" . $sql;
            if (mysqli_query($link, $sql)) {
                echo " - Actualizado";
            }

            echo "<br/><br/>";
        }
    }

    return 1;
}

if (!isset($_GET["lang"]))
    exit("FIN DE PROCESO: error 1");
$lang = $_GET["lang"];
if ($lang == "")
    exit("FIN DE PROCESO: error 2");

//test existe el lenguaje activo y por defecto
$sql = "SELECT id FROM config_idiomas WHERE idioma = '$lang' AND activo = 1 AND defecto = 1";
$consulta = mysqli_query($link, $sql);
$numero_filas = mysqli_num_rows($consulta);

if (!$numero_filas)
    exit("FIN DE PROCESO: error 3");

recargar_idiomas($lang, "idiomas");
recargar_idiomas($lang, "metas");

exit("El proceso ha finalizado correctemente.");
