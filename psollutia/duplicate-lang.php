<?
include("system/vars.php");
include("system/conexiondb.php");

function str_replace_first($search, $replace, $subject) {
    $pos = strpos($subject, $search);
    if ($pos !== false) {
        return substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

function recargar_idiomas($lang1, $lang2, $tabla){
    global $link;
    
    echo strtoupper($tabla).":<br/><br/>";

    $sql = "SELECT id, url_friendly_complet, campo FROM $tabla WHERE idioma = '$lang1' AND url_friendly_complet <> ''";
    $consulta = mysqli_query($link,$sql);
    $numero_filas = mysqli_num_rows($consulta);

    if ($numero_filas) {
         while ($fila = mysqli_fetch_array($consulta)) {
             
            $campo2 = str_replace("_".$lang1, "_".$lang2, $fila['campo']); 
             
            echo $fila['url_friendly_complet']." -> ";
            
            $url_friendly_complet2 = str_replace_first($lang1."/",$lang2."/",$fila['url_friendly_complet']);
			
            echo $url_friendly_complet2;
            
            $sql_insert = "INSERT INTO $tabla (idioma, seccion, seccion_public, id_seccion, campo, texto, url_friendly, url_friendly_complet, noborrar) SELECT idioma, seccion, seccion_public, id_seccion, campo, texto, url_friendly, url_friendly_complet, noborrar FROM $tabla WHERE id = ".$fila['id'];
            echo "<br/>".$sql_insert;
            mysqli_query($link,$sql_insert);
            $id_new = mysqli_insert_id($link);
			
            $sql_update = "UPDATE $tabla SET idioma='$lang2', url_friendly_complet='$url_friendly_complet2', campo='$campo2' WHERE id = ".$id_new;
            echo "<br/>".$sql_update;
            if (mysqli_query($link,$sql_update)){
                echo " - Actualizado";
            }

            echo "<br/><br/>";
         }
    }
    
    return 1;
}

if (!isset($_GET["lang1"])) exit("FIN DE PROCESO: error 1");
$lang1 = $_GET["lang1"];
if ($lang1 == "") exit("FIN DE PROCESO: error 2");

if (!isset($_GET["lang2"])) exit("FIN DE PROCESO: error 3");
$lang2 = $_GET["lang2"];
if ($lang2 == "") exit("FIN DE PROCESO: error 4");

//test existe el lenguaje para copiar
$sql = "SELECT id FROM idiomas WHERE idioma = '$lang1'";
//echo $sql;
$consulta = mysqli_query($link,$sql);
$numero_filas = mysqli_num_rows($consulta);

if ($numero_filas < 1) exit("FIN DE PROCESO: error 5");

recargar_idiomas($lang1, $lang2, "idiomas");
recargar_idiomas($lang1, $lang2, "metas");

exit("El proceso ha finalizado correctemente.");