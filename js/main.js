/*------------------------------------------------------------------- forms --*/
function toggle_enable_submit(checkbox) {
    var form = $(checkbox).parent().parent();
    var submit_btn = $("input[type='submit']", form);
    var checked = checkbox.prop("checked");
    if (checked) {
        submit_btn.removeClass("disabled").prop("disabled", false);
    } else {
        submit_btn.addClass("disabled").prop("disabled", true);
    }
}
function contact_loading(formulari) {
    var parent = $(formulari).parent();
    var alerts = $(".alert_group .alert", parent);
    alerts.hide(200);
    $("input, textarea, select", formulari).prop("disabled", true);
    $("input[type='submit']", formulari).prop("disabled", true).addClass("disabled loading");
}
function contact_loaded(status, formulari) {
    $("input[type='submit']", formulari).removeClass("loading");
    $("input:not([type='submit']), textarea, select", formulari).prop("disabled", false).val("");
    $("input[type='checkbox']").prop("checked", false);
    if (status === "notok") {
        $("input[type='submit']", formulari).prop("disabled", false).removeClass("disabled loading");
    }
}
function highlightFields(fields, action) {
//    console.log(fields);
    if (action === "add") {
        fields.each(function () {
            $(this).parent().addClass("highlight");
        });
    } else {
        fields.each(function () {
            $(this).parent().removeClass("highlight");
        });
    }
}
function valida_form_contacto(form) {
    var emptyRequired = false;
    var formulari = $(form);
    var parent = formulari.parent();
    var required = $(".required", formulari);
    var alerts = $(".alert_group .alert", parent);
    var email = $(".email", formulari);
    required.each(function () {
        if ($(this).val() === "" || ($(this).hasClass("checkbox") && !$(this).is(':checked'))) {
            emptyRequired = true;
        }
    });
    highlightFields(required, "remove");

    if (!emptyRequired) {
        alerts.hide();
        highlightFields(required, "remove");
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val()))) {
            $(".alert.email", parent).fadeIn(300);
            highlightFields(email, "add");
        } else {
            var formSerialized = formulari.serialize();
//            console.log(formSerialized);
            contact_loading(formulari);
            $.post(arrail + "ajax/contact-send.php", {formData: formSerialized}).done(function (data) {
                if (data === "ok") {
                    $(".alert.ok", parent).fadeIn(300);
                    contact_loaded("ok", formulari);
                } else {
//                    console.log("1-" + data);
                    $(".alert.catastrofe:not(.email)", parent).fadeIn(300);
                    contact_loaded("notok", parent);
                }
            }).error(function (data) {
//                console.log("2-" + data);
                $(".alert.catastrofe:not(.email)", parent).fadeIn(300);
                contact_loaded("notok", parent);
            });
        }
    } else {
//        console.log("rellenar");
        alerts.hide();
        required.each(function () {
            if ($(this).val() === "") {
                highlightFields($(this), "add");
            }
        });
        $(".alert.rellenar", parent).fadeIn(300);
    }

}

function setup_set_scroll() {
    var width_sidebar = {}, container_height = {}, startPoint = {}, rockBottom = {}, bottomPos = {}, inclass = {};

    $(".out, .in").attr("style", "");
    if (is_responsive()) {

    } else {
        $(".set_scroll").each(function (index) {
            width_sidebar[index] = $(".out", this).width();
            container_height[index] = $(this).parent().height();

            $(".out", this).height(container_height[index]);
            if ($(".in", this).hasClass("space")) {
                $(".out", this).height(container_height[index]);
                startPoint[index] = $(".in", this).offset().top - 64;
            } else {
                startPoint[index] = $(".in", this).offset().top;
            }

            rockBottom[index] = startPoint[index] + (container_height[index] - $(".in", this).height());
            bottomPos[index] = container_height[index] - $(".in", this).height();
            inclass[index] = $(".in", this);

            scroll(startPoint, rockBottom, bottomPos, inclass, width_sidebar, index);
            $(window).scroll(function () {
                scroll(startPoint, rockBottom, bottomPos, inclass, width_sidebar, index);
            });
        });
    }
}
function scroll(startPoint, rockBottom, bottomPos, inclass, width_sidebar, index) {
    if (is_responsive()) {
        inclass[index].removeClass("fixed");
        inclass[index].attr("style", "");
    } else {
        var sTop = $(this).scrollTop();
        if (sTop > startPoint[index] && sTop < rockBottom[index]) {

            inclass[index].attr("style", "").width(width_sidebar[index]).addClass("fixed");
        } else {
            if (sTop < startPoint[index]) {
                inclass[index].removeClass("fixed");
            }
            if (sTop > rockBottom[index]) {
                inclass[index].removeClass("fixed").css({
                    top: bottomPos[index]
                });
            }
        }
    }
}

/*-------------------------------------------------------------- responsive --*/
function is_responsive() {
    if ($("#main_launcher").css("display") === "block") {
        return true;
    }
    return false;
}
function check_responsive() {
    if (is_responsive()) {
        $(".subemu > ul").removeAttr("style");
        $(".producto-articulo h1, .producto-listado h1").appendTo(".main-launcher .text");
        responsive_tables("set");
    } else {
        $("#main_nav ul").removeAttr("style");
        $(".producto-listado .main-launcher .text h1").prependTo(".breadcrumber");
        $(".producto-articulo .main-launcher .text h1").prependTo(".content");
        responsive_tables("unset");
    }
}
function responsive_tables(action) {
    var table_scroll_container = $("<div class='table_scroller_container'><span class='icon scroll left'></span><span class='icon scroll right active'></span></div>");
    var fake_scroller = $("<div class='fake_scroller'><div></div></div>");
    var table_scroller = $("<div class='table_scroller'></div>");
    switch (action) {
        case "set":
            if (!exists(".table_scroller_container")) {
                $(".editor_content table").each(function () {
                    var tabla = $(this);
                    table_scroller.insertAfter(tabla).append(tabla);
                    table_scroll_container.insertAfter(table_scroller).append(table_scroller);
                    fake_scroller.insertBefore(table_scroller).find("div").css({
                        width: tabla.width()
                    });
                    var container = $(".table_scroller_container");
                    var scroll = $(".table_scroller");
                    var fake_scroll = $(".fake_scroller");
                    scroll.scroll(function () {
                        var scrollLeft = scroll.scrollLeft();
                        var maxScroll = tabla.width() - scroll.width();
                        fake_scroll.scrollLeft(scrollLeft);
                        update_scroll_arrow(scrollLeft, maxScroll, container);
                    });
                    fake_scroll.scroll(function () {
                        var scrollLeft = fake_scroll.scrollLeft();
                        var maxScroll = tabla.width() - scroll.width();
                        scroll.scrollLeft(scrollLeft);
                        update_scroll_arrow(scrollLeft, maxScroll, container);
                    });
                });
            }
            break;
        case "unset":
            // crec que no val la pena desfer tot el tinglao.
            break;
    }
}
function update_scroll_arrow(scrollLeft, maxScroll, container) {
    if (scrollLeft > 0 && scrollLeft < maxScroll) {
        $(".icon.scroll", container).addClass("active");
    }
    if (scrollLeft === maxScroll) {
        $(".scroll.right", container).removeClass("active");
    }
    if (scrollLeft === 0) {
        $(".scroll.left", container).removeClass("active");
    }
}

$(document).ready(function () {
    $("body").cookieAlert({
        domain: cookieAlertDomain,
        infoUrl: cookieAlertUrl,
        infoTxt: cookieAlertInfoTxt,
        aceptarTxt: cookieAlertAceptarTxt,
        masinfoTxt: cookieAlertMasinfoTxt,
        borderTopColor: "black",
        textColor: "#353535",
        btnBackgroundColor: "black",
        btnBackgroundColorHover: "#151515",
        linkColor: "black"
    });
    $("#main_nav li.selected").parents("li").addClass("selected");
    /*-- form --*/
    $("#acepto_politica").on("click", function () {
        toggle_enable_submit($(this));
    });

    $("#contact_form").submit(function (event) {
        event.preventDefault();
        valida_form_contacto($(this));
    })

    $('form.fran6validate input[type=submit]').on("click", function (event) {
        event.preventDefault();
        $().fran6validate("#" + $(this).parents("form.fran6validate").attr("id"), "submit");
    });

    $("#slidexou_portada, #slidexou, #slidexou_pagina").fran6gallery({
        duracio: 4000, //delay entre fades
        velocitat: 1000, //velocitat fade
        paginacio: 0, //redolinets(0) numeros(1)  no res(2)
        timer: true,
        bulletBackground: "#CCCCCC",
        bulletBackground_selected: "#CC9900"
    });
    $("#slidexou_producto").fran6gallery({
        duracio: 4000, //delay entre fades
        velocitat: 1000, //velocitat fade
        paginacio: 0, //redolinets(0) numeros(1)  no res(2)
        timer: false,
        fletxes: true,
        autoplay: false,
        bulletBackground: "#CCCCCC",
        bulletBackground_selected: "#CC9900"
    });
    $(".swipebox").swipebox();
    /*------------------------------------- videos --*/
    $("a.vimeo").click(function () {
        $.fancybox({
            'href': this.href.replace("vimeo.com", 'player.vimeo.com/video'),
            'type': 'iframe',
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });
        return false;
    });
    $("a.youtube").click(function () {
        $.fancybox({
            'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type': 'swf',
            'swf': {
                'wmode': 'transparent',
                'allowfullscreen': 'true'
            },
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });
        return false;
    });

    /*-- main nav / subemu --*/
    $(".subemu a").next("button").on("click", function () {
        $(this).next("ul").slideToggle(150);
    });
    $(".subemu p").on("click", function () {
        $(this).next("ul").slideToggle(150);
    });
    $("#main_nav .launcher, .main-launcher").on("click", function () {
        $(this).next("ul").slideToggle(200);
    });
    if (exists(".subemu")) {
        var selected = $(".subemu .selected");
        var parents = selected.parents("li");
        selected.parents("ul").show();
        $(" > ul", selected).show();
        $(parents.last()).addClass("selected");
        if (is_responsive()) {
            $(".subemu > ul").hide();
        }
    }
    $(".category_list button").on("click", function (event) {
        event.preventDefault();
        $(" + ul", this).slideToggle(250);
    });

    if (exists(".set_scroll")) {
        $(".set_scroll img").imagesLoaded()
            .always(function (instance) {
                //console.log('all images loaded');
            })
            .done(function (instance) {
                //console.log('all images successfully loaded');
                setup_set_scroll();
            })
            .fail(function () {
                //console.log('all images loaded, at least one is broken');
            })
            .progress(function (instance, image) {
                //var result = image.isLoaded ? 'loaded' : 'broken';
                //console.log('image is ' + result + ' for ' + image.img.src);
            });
    }

    check_responsive();
});
$(window).resize(function () {
    check_responsive();
    if (exists(".set_scroll")) {
        setup_set_scroll();
    }
});