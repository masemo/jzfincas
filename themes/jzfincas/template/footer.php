<footer id="main_footer">
    <section class="info">
        <div class="wrap">
            <header>
                <h3><?php echo lang("footer_datos_contacto"); ?></h3>
            </header>
            <p>
                <span class="icon icon-marker"></span> <?php echo lang("footer_direccion") ?><br/>
                <span class="icon icon-phone"></span> <?php echo lang("footer_phone") ?><br/>
                <span class="icon icon-email"></span> <a href="mailto: <?php echo lang("footer_mail") ?>"><?php echo lang("footer_mail") ?></a>
            </p>
            <br/>
            <header>
                <h3><?php echo lang("footer_horario"); ?></h3>
            </header>
            <p>
                <strong><?php echo lang("footer_horario_1") ?></strong><br/>
                <span class="icon icon-clock"></span> <?php echo lang("footer_horario_2") ?>
            </p>
            <p>
                <strong><?php echo lang("footer_horario_3") ?></strong><br/>
                <span class="icon icon-clock"></span> <?php echo lang("footer_horario_4") ?>
            </p>
        </div>
    </section>
    <section class="copy">
        <div class="wrap">
            <div class="row">
                <div class="col-50">
                    <p>&COPY;<? echo copyright_year($siteCopyright) . " " . $siteName; ?></p>
                </div>
                <div class="col-50 aRight">
                    <p>
                        <a href="<? echo $aviso_legal_fu; ?>">
                            <span><? echo lang("aviso_legal"); ?></span>
                        </a> |
                        <a href="<? echo $sitemap_fu; ?>">
                            <span><? echo lang("sitemap"); ?></span>
                        </a>
                        <? if ($sec == "home") { ?>|
                            <a href="<? echo $sollutia; ?>" rel="author">
                                <span><? echo lang("diseno_web"); ?></span>
                            </a>
                        <? } ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
</footer>