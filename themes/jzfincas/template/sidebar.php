<div id="sidebar">
    <div class="administracion-fincas">
        <div class="box">
            <a href="<?php echo obtener_url_amigable($lang, "paginas", 76); ?>" class="table">
                <div class="row">
                    <span class="icon round icon-house"></span>
                </div>
                <div class="row">
                    <h3><?php echo lang("home_administrador_de_fincas"); ?></h3>
                </div>
            </a>
        </div>
    </div>
    <div class="gestionamos-seguro">
        <div class="box">
            <a href="<?php echo obtener_url_amigable($lang, "paginas", 77); ?>" class="table">
                <div class="row width">
                    <span class="icon round icon-paper"></span>
                </div>
                <div class="row">
                    <h3><?php echo lang("home_gestionamos_seguros"); ?></h3>
                </div>
            </a>
        </div>
    </div>
    <div class="facebook">
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.7";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <div class="fb-page" data-href="https://www.facebook.com/jzfincas/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/jzfincas/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/jzfincas/">Jorge Zornoza Administración de Fincas</a></blockquote>
        </div>
    </div>
</div>