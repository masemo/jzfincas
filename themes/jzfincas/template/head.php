<link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet">
<link rel="stylesheet" href="<? echo $siteUrl; ?>lib/swipebox/css/<? echo minify("swipebox", "css", $development) ?>" />
<link rel="stylesheet" href="<? echo $siteUrl; ?>lib/fancybox/<? echo minify("jquery.fancybox", "css", $development) ?>" type="text/css" />