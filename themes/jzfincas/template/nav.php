<nav id="main_nav">
    <div class="wrap">
        <p id="main_launcher" class="launcher"><? echo lang("menu"); ?><span></span></p>
        <ul>
            <li <? if ($sec == "home" || $sec == "") { ?>class="selected"<? } ?>>
                <a href="<? echo $home_fu; ?>" rel="home">
                    <? echo $home_txt; ?>
                </a>
            </li>
            <? echo $menu; ?>
        </ul>
    </div>
</nav>