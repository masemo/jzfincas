<header id="main_header">
    <div class="wrap">
        <div class="logo">
            <a href="<? echo $home_fu; ?>" rel="home">
                <img src="<? echo $themeUrl; ?>images/logo.png" alt="<? echo $siteName; ?>"/>
            </a>
        </div>
        <div class="lang">
            <?
            if (count($idiomas) > 1) {
                generaLangNav($idiomas, $lang);
            }
            ?>
        </div>
        <p class="social">
            <? if ($facebook != "") { ?>
                <a href="<? echo $facebook; ?>">
                    <span class="icon icon-facebook"></span>
                </a>
            <? } ?>
            <? if ($twitter != "") { ?>
                <a href="<? echo $twitter; ?>">
                    <span class="icon icon-twitter"></span>
                </a>
            <? } ?>
            <? if ($googleplus != "") { ?>
                <a href="<? echo $googleplus; ?>">
                    <span class="icon icon-gplus"></span>
                </a>
            <? } ?>
        </p>
    </div>
</header>
<? include("nav.php"); ?>