<body>
<table cellpadding='10' cellspacing='0' style='font-family:arial;font-size:14px;width:600px;border:solid 1px #0097A3;background:white;'>
    <tr>
        <td colspan='2' style='text-align:center;'>
            <a href='#siteUrl'>
                <img src='#logo' border='0' alt='#siteName' width="320" style="margin: 0 auto 0 auto; display: block; position: relative;"/>
            </a>
            <br/><br/>
        </td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Id</td>
        <td>#idpublic</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Finca</td>
        <td>#finca</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Descripci&oacute;n de la incidencia</td>
        <td>#incidencia</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Nombre</td>
        <td>#nombre</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Tel&eacute;fono</td>
        <td>#telefono</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Correo electr&oacute;nico</td>
        <td>#email</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Piso</td>
        <td>#piso</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Sector</td>
        <td>#sector</td>
    </tr>
    <tr>
        <td style='text-align:right;font-weight:bold;margin-right:15px;width:150px;'>Acceso al admin</td>
        <td>#link</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
</table>
</body>
