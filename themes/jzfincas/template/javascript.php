<script type="text/javascript" src="<? echo $siteUrl; ?>lib/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>lib/swipebox/js/<? echo minify("jquery.swipebox", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>lib/fran6gallery/<? echo minify("fran6gallery", "js", $development); ?>"></script>
<script type="text/javascript" src="<? echo $siteUrl; ?>lib/fran6validate/<? echo minify("fran6validate", "js", $development); ?>"></script>
<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
            Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a
        ,m)

    })(window,document,'script','https://www.google-analytics.com/analytics.js',
        'ga');

    ga('create', 'UA-85292647-1', 'auto');
    ga('send', 'pageview');

</script>
