<section>
    <div class="wrap">
        <header class="breadcrumber">
            <?
            if (!empty($migas)) {
                include("includes/breadcrumbs.php");
            }
            ?> 
            <h1><? echo $videos_title; ?></h1>
        </header>
        <div class="content">
            <?
            if (!empty($galeria)) {
                if ($tipo == "categorias") {
                    ?>
                    <div class="gallery_cats">
                        <ul class="col-33">
                            <? foreach ($galeria as $id => $data) {
                                ?><li>
                                    <figure>
                                        <img src="<? echo $data["image"]; ?>" alt="<? echo $data["title"]; ?>" />
                                    </figure>
                                    <h3><? echo $data["title"]; ?></h3>
                                    <a href="<? echo $data["link"]; ?>" class="cover"><? echo $data["title"]; ?></a>
                                </li><? } ?>
                        </ul>
                    </div>
                <? } else { ?>
                    <div class="gallery_video">
                        <ul class="col-25">
                            <? foreach ($galeria as $id => $data) {
                                ?><li>
                                    <figure>
                                        <img src="<? echo $data["image"]; ?>" alt="<? echo $data["title"]; ?>" />
                                    </figure>
                                    <h3><? echo $data["title"]; ?></h3>
                                    <a href="<? echo $data["link"]; ?>" class="cover <? echo check_domain($data['link']); ?> fancybox.iframe" ><? echo $data["title"]; ?></a>
                                </li><? } ?>
                        </ul>
                    </div>
                    <?
                }
            } else {
                ?>           
                <p class="alert notok"><? echo lang("sin_videos"); ?></p>
            <? } ?>
            <footer class="share">
                <? include("includes/share.php"); ?>
            </footer>
        </div>
    </div>
</section>
