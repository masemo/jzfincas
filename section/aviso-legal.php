<section>
    <div class="wrap">
        <div class="row">
            <div class="col-70">
                <div class="content">
                    <header>
                        <h1><? echo lang("aviso_legal"); ?></h1>
                    </header>
                    <div class="editor_content">
                        <p><? echo lang("text_aviso_legal_1") . " " . $titular_NL; ?>, <? echo lang("text_aviso_legal_2") . " " . $direccion_NL . " " . $nif_NL; ?>.</p>
                        <p><? echo $web_NL . " " . lang("text_aviso_legal_3") . " " . $titular_NL . " " . lang("text_aviso_legal_4"); ?></p>
                        <p><? echo $titular_NL . " " . lang("text_aviso_legal_5") . " " . $titular_NL . " " . lang("text_aviso_legal_6"); ?></p>
                        <h3><? echo lang("text_aviso_legal_7"); ?></h3>
                        <p><? echo lang("text_aviso_legal_8") . " " . $titular_NL . " " . lang("text_aviso_legal_9"); ?></p>
                        <h3><? echo lang("text_aviso_legal_10"); ?></h3>
                        <p><? echo lang("text_aviso_legal_11") . " " . $titular_NL . " " . lang("text_aviso_legal_12"); ?></p>
                        <h3><? echo lang("text_aviso_legal_13"); ?></h3>
                        <p><? echo lang("text_aviso_legal_14"); ?></p>
                        <h3><? echo lang("text_aviso_legal_15"); ?></h3>
                        <p><? echo lang("text_aviso_legal_16") . " " . $titular_NL; ?><? echo lang("text_aviso_legal_17") . " " . $titular_NL . " " . lang("text_aviso_legal_18"); ?></p>
                        <p><? echo lang("text_aviso_legal_19") . " " . $titular_NL; ?><? echo lang("text_aviso_legal_2") . " " . $direccion_NL; ?></p>

                        <h3><? echo lang("politica_cookies_01"); ?></h3>
                        <p><? echo lang("politica_cookies_02"); ?></p>

                        <h4><? echo lang("politica_cookies_03"); ?></h4>
                        <h5><? echo lang("politica_cookies_04"); ?></h5>
                        <p><? echo lang("politica_cookies_05"); ?></p>

                        <h5><? echo lang("politica_cookies_06"); ?></h5>
                        <p><? echo lang("politica_cookies_07"); ?></p>

                        <h4><? echo lang("politica_cookies_08"); ?></h4>
                        <p><? echo lang("politica_cookies_09"); ?></p>
                        <p><? echo lang("politica_cookies_10"); ?></p>
                        <p><? echo lang("politica_cookies_11"); ?></p>
                        <p>
                            <? echo lang("politica_cookies_12"); ?>
                        </p>
                        <p><? echo lang("politica_cookies_13"); ?></p>

                        <h3><? echo lang("text_aviso_legal_21"); ?></h3>
                        <p><? echo lang("text_aviso_legal_22"); ?> <a href="http://www.sollutia.com">Sollutia S.L.</a></p>
                    </div>
                </div>
            </div>
            <div class="col-30"><? include($themeIncludeUrl . "template/sidebar.php"); ?></div>
        </div>
    </div>
</section>