<section>
    <div class="wrap">
        <header>
            <h1><?php echo obtener_valor($lang, $sec, "nombre", $id_seccion); ?></h1>
        </header>
        <div class="content formStyle">
            <form id="login_form" action="<? echo obtener_url_amigable($lang, $sec, $id_seccion); ?>" method="post">
                <fieldset>
                    <? create_form($fields); ?>
                    <?php if (isset($error) && $error != "") { ?>
                        <div class="alert_group">
                            <div class="alert catastrofe email"><? echo $error; ?></div>
                        </div>
                    <?php } ?>
                </fieldset>
            </form>
        </div>
    </div>
</section>