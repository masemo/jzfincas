<?php
$fields = array(
    "nombre" => array(
        "type" => "text",
        "class" => "required"
    ),
    "telefono" => array(
        "type" => "text",
        "class" => "mig"
    ),
    "email" => array(
        "type" => "text",
        "class" => "required email"
    ),
    "asunto" => array(
        "type" => "textarea",
        "class" => "required"
    ),
    "acepto_politica" => array(
        "type" => "checkbox",
        "class" => "required checkbox"
    ),
    "enviar" => array(
        "type" => "submit",
        "class" => ""
    )
);
?>
<section>
    <div class="wrap">
        <header>
            <h1><? echo $contacto_title; ?></h1>
        </header>
        <div class="row">
            <div class="col-50">
                <div class="content formStyle">
                    <div class="alert_group">
                        <div class="alert ok"><? echo lang("contacto_ok"); ?></div>
                        <div class="alert catastrofe"><? echo lang("contacto_ko"); ?></div>
                        <div class="alert notok rellenar"><? echo lang("contacto_rellenar"); ?></div>
                        <div class="alert catastrofe email"><? echo lang("contacto_email_ko"); ?></div>
                    </div>
                    <form id="contact_form" action="<? echo $siteUrl; ?>" method="post">
                        <fieldset>
                            <? create_form($fields); ?>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="col-50">
                <header>
                    <h3><?php echo lang("footer_datos_contacto"); ?></h3>
                </header>
                <p>
                    <span class="icon icon-marker"></span> <?php echo lang("footer_direccion") ?><br/>
                    <span class="icon icon-phone"></span> <?php echo lang("footer_phone") ?><br/>
                    <span class="icon icon-email"></span> <a href="mailto: <?php echo lang("footer_mail") ?>"><?php echo lang("footer_mail") ?></a>
                </p>
                <br/>
                <header>
                    <h3><?php echo lang("footer_horario"); ?></h3>
                </header>
                <p>
                    <strong><?php echo lang("footer_horario_1") ?></strong><br/>
                    <span class="icon icon-clock"></span> <?php echo lang("footer_horario_2") ?>
                </p>
                <p>
                    <strong><?php echo lang("footer_horario_3") ?></strong><br/>
                    <span class="icon icon-clock"></span> <?php echo lang("footer_horario_4") ?>
                </p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3113.9081098915667!2d-0.4872968846555338!3d38.69695497960197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6186545094ef73%3A0x8cba1ad9bbad7e9f!2sJorge+Zornoza+-+Administraci%C3%B3n+de+fincas!5e0!3m2!1ses!2ses!4v1480532770126" width="100%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>