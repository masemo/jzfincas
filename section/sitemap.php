<section>
    <div class="wrap">
        <div class="content">
            <header>
                <h1><? echo lang("sitemap"); ?></h1>
            </header>
            <div class="content">
                <p><? echo lang("mapa_contenidos_txt"); ?></p>
                <? include("includes/sitemap.php"); ?>
            </div>
        </div>
    </div>
</section>