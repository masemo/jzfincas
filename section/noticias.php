<section class="noticias noticias_data">
    <div class="wrap">
        <div class="row">
            <div class="col-70">
                <header class="hgroup">
                    <h1><? echo $noticia["title"]; ?></h1>
                    <? if (!empty($migas)) {
                        include("includes/breadcrumbs.php");
                    } ?>
                </header>
                <div class="row">
                    <? if (!empty($noticias_categorias)) { ?>
                        <div class="col-40">
                            <h3><? echo lang("categorias"); ?></h3>
                            <ul>
                                <? foreach ($noticias_categorias as $categoria) { ?>
                                    <li>
                                        <a href="<? echo $categoria["link"]; ?>" rel="subsection"><? echo $categoria["title"]; ?></a>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                    <? } ?>
                    <article class="col-60">
                        <? if (!empty($noticia["galeria"]) || !empty($noticia["imagen"])) { ?>
                            <figure>
                                <div id="slidexou">
                                    <ul class="gal6">
                                        <? if (!empty($noticia["imagen"])) { ?>
                                            <li>
                                                <a href="<? echo $noticia["imagen"]; ?>" class="swipebox">
                                                    <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo $noticia["thumbnail"]; ?>&amp;w=640&amp;h=360" alt="<? echo $noticia["imagen"]["alt"]; ?>"/>
                                                </a>
                                            </li>
                                            <?
                                        }
                                        if (!empty($noticia["galeria"])) {
                                            foreach ($noticia["galeria"] as $imagen) {
                                                ?>
                                                <li>
                                                    <a href="<? echo pintar_imagen($imagen["imagen"]); ?>" class="swipebox">
                                                        <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen["imagen"], true); ?>&amp;w=640&amp;h=360" alt="<? echo $noticia["nombre"]; ?>"/>
                                                    </a>
                                                </li>
                                                <?
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </figure>
                        <? } ?>
                        <div class="content">
                            <time datetime="<? echo $noticia["datetime"]; ?>"><? echo $noticia["date"]; ?></time>
                            <div class="editor_content">
                                <? echo $noticia["contenido"]; ?>
                            </div>
                        </div>
                        <footer class="share">
                            <? include("includes/share.php"); ?>
                        </footer>
                    </article>
                </div>
            </div>
            <div class="col-30"><? include($themeIncludeUrl . "template/sidebar.php"); ?></div>
        </div>
    </div>
</section>