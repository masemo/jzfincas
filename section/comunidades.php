<section>
    <div class="wrap">
        <header>
            <h1><?php echo obtener_valor($lang, $sec, "nombre", $id_seccion); ?></h1>
            <p class="logout"><a href="<?php echo $fu_logout ?>"><?php echo lang("cerrar_sesion"); ?></a></p>
        </header>
        <div>
            <h2><?php echo $comunidad["nombre"] ?></h2>
        </div>
        <section class="menu-oficina">
            <div class="row">
                <div class=col-50>
                    <div class="box">
                        <div>
                            <div class="circle red">
                                <span class="icon-bullhorn"></span>
                            </div>
                        </div>
                        <div>
                            <h4><?php echo lang("comunidad_t_anuncios_title") ?></h4>
                            <p><?php echo lang("comunidad_t_anuncios_descripcion") ?></p>
                            <a class="cover" href="<? echo obtener_url_amigable($lang, "comunidades-anuncios", 1); ?>"></a>
                        </div>
                    </div>
                    <div class="boxer">
                        <div class="box big">
                            <div>
                                <div class="circle orange">
                                    <span class="icon-wrench"></span>
                                </div>
                            </div>
                            <div>
                                <h4><?php echo lang("comunidad_t_incidencias_title") ?></h4>
                                <p><?php echo lang("comunidad_t_incidencias_descripcion") ?></p>
                                <a class="cover" href="<? echo obtener_url_amigable($lang, "comunidades-incidencias", 1); ?>"></a>
                            </div>
                        </div>
                        <div class="box small">

                            <a href="<? echo obtener_url_amigable($lang, "comunidades-incidencias-add", 1); ?>" class="button primary">
                                <span class="icon-add"></span>
                                <?php echo lang("comunidad_t_incidencias_add") ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class=col-50>
                    <div class="datos-comunidad">
                        <header>
                            <h3><?php echo lang("comunidad_datos_title"); ?></h3>
                        </header>
                        <div>
                            <table border="0" cellpadding="5" cellspacing="0" style="width: 500px;">
                                <tbody>
                                <tr>
                                    <td><p><?php echo lang("comunidades_nombre"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["nombre"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_n_propietarios"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["n_propietarios"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_cif"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["cif"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_n_cuenta"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["n_cuenta"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_direccion"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["direccion"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_ano_construcion"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["ano_construcion"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_presidente"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["presidente"] ?></strong></p></td>
                                </tr>
                                <tr>
                                    <td><p><?php echo lang("comunidades_vicepresidente"); ?></p></td>
                                    <td><p><strong><?php echo $comunidad["vicepresidente"] ?></strong></p></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="proveedores">
                        <header>
                            <h3><?php echo lang("comunidad_proveedores_title"); ?></h3>
                            <p><?php echo lang("comunidad_proveedores_descripcion"); ?></p>
                        </header>
                        <div>
                            <?php echo $comunidad["proveedores"]; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>