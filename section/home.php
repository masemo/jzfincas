<section class="welcome">
    <div class="wrap">
        <header>
            <h1><? echo $homeTitle; ?></h1>
        </header>
        <div class="editor_content">
            <? echo $homeDesc; ?>
        </div>
        <section class="home_sec">
            <div class="row">
                <div class="col-50">
                    <div class="administracion-fincas">
                        <div class="box">
                            <a href="<?php echo obtener_url_amigable($lang, "paginas", 76); ?>">
                                <h2><?php echo lang("home_administrador_de_fincas"); ?></h2>
                                <p><?php echo lang("home_administrador_de_fincas_p"); ?></p>
                                <span class="icon round icon-house"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-50">
                    <div class="gestionamos-seguro">
                        <div class="box">
                            <a href="<?php echo obtener_url_amigable($lang, "paginas", 77); ?>">
                                <h2><?php echo lang("home_gestionamos_seguros"); ?></h2>
                                <p><?php echo lang("home_gestionamos_seguros_p"); ?></p>
                                <span class="icon round icon-paper"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<section class="home">
    <div class="wrap">
        <div class="row">
            <div class="col-40">
                <section class="noticias">
                    <header>
                        <h2><? echo $noticiasTitle; ?></h2>
                    </header>
                    <? if (!empty($noticias_listado)) { ?>
                        <ul class="listado">
                            <? foreach ($noticias_listado as $id => $data) { ?>
                                <li>
                                    <div>
                                        <p>
                                            <a href="<? echo $data["link"]; ?>">
                                                <h3><? echo $data["title"]; ?></h3>
                                                <time datetime="<? echo $data["datetime"]; ?>"><span class="icon icon-calendar"></span> <? echo $data["date"]; ?></time>
                                            </a>
                                        </p>
                                        <span class="icon"></span>
                                    </div>
                                </li>
                            <? } ?>
                        </ul>
                        <a href="<? echo $noticias_fu; ?>" class="primary_btn" rel="section"><span class="icon icon-newspaper"></span> <? echo lang("noticias_btn"); ?></a>
                    <? } else { ?>
                        <p class="alert notok"><? echo lang("sin_noticias"); ?></p>
                    <? } ?>
                </section>
            </div>
            <div class="col-60">
                <div class="content"></div>
                <div class="slides">
                    <div class="slide" style="background-image: url(<? echo $homeImg; ?>);"></div>
                </div>
            </div>
        </div>
    </div>
</section>