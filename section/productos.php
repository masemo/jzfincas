<section>
    <div class="wrap">
        <div class="product_data">
            <div class="row">
                <? if (!empty($categorias)) { ?>
                    <div class="col-25">
                        <div class="category_list">
                            <p class="title"><? echo lang("categoria_productos"); ?></p>
                            <?
                            include("includes/product-nav-list-style.php");
                            ?>
                        </div>
                    </div>
                <? } ?>
                <? if (!empty($item)) { ?>
                    <div class="col-<? echo $content_width; ?>"
                         <div class="row">
                            <div class="col-60">
                                <header class="breadcrumber hgroup product">
                                    <?
                                    if (!empty($migas)) {
                                        include("includes/breadcrumbs.php");
                                    }
                                    ?>
                                    <h1><? echo $item["nombre"]; ?></h1>
                                </header>
                                <div class="descripcion editor_content">
                                    <? echo $item["descripcion"]; ?>
                                </div>
                                <div class="caracteristicas editor_content">
                                    <? echo $item["caracteristicas"]; ?>
                                </div>
                                <div class="share">
                                    <? include("includes/share.php"); ?>
                                </div>
                            </div>
                            <? if (!empty($item["galeria"]) || !empty($item["imagen"])) { ?>
                                <div class="col-40 product_gallery" id="slidexou_producto">
                                    <ul class="gal6">
                                        <? if (!empty($item["imagen"])) { ?>
                                            <li>
                                                <a href="<? echo pintar_imagen($item["imagen"]["imagen"]); ?>" class="swipebox">
                                                    <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo pintar_imagen($item["imagen"]["imagen"], true); ?>&amp;w=480&amp;h=720&amp;zc=1&amp;q=90" alt="<? echo $item["imagen"]["alt"]; ?>" />
                                                </a>
                                            </li>
                                            <?
                                        }
                                        if (!empty($item["galeria"])) {
                                            foreach ($item["galeria"] as $imagen) {
                                                ?>
                                                <li>
                                                    <a href="<? echo pintar_imagen($imagen["imagen"]); ?>" class="swipebox">
                                                        <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo pintar_imagen($imagen["imagen"], true); ?>&amp;w=480&amp;h=720&amp;zc=1&amp;q=90" alt="<? echo $item["nombre"]; ?>" />
                                                    </a>
                                                </li>
                                                <?
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                <? } else { ?>
                    <p class="alert notok"><? echo lang("sin_productos"); ?></p>
                <? } ?>
            </div>
        </div>
    </div>
</section>
