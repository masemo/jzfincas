<section class="galeria_imagenes">
    <div class="wrap">
        <header class="breadcrumber">
            <?
            if (!empty($migas)) {
                include("includes/breadcrumbs.php");
            }
            ?> 
            <h1><? echo $title; ?></h1>
        </header>
        <div class="content">
            <? if ($tipo == "categorias") { ?>
                <div class="gallery_cats">
                    <ul class="col-33">
                        <? foreach ($galeria as $id => $data) {
                            ?><li>
                                <figure>
                                    <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo $data["imageRelative"]; ?>&amp;w=640&amp;h=480" alt="<? echo $data["title"]; ?>" />
                                </figure>
                                <h3><? echo $data["title"]; ?></h3>
                                <a href="<? echo $data["link"]; ?>" class="cover"><? echo $data["title"]; ?></a>
                            </li><? } ?>
                    </ul>
                </div>
            <? } else { ?>
                <div class="gallery_img">
                    <ul class="col-25">
                        <? foreach ($galeria as $id => $data) {
                            ?><li>
                                <a href="<? echo $data["link"]; ?>" class="swipebox">
                                    <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo $data["imageRelative"]; ?>&amp;w=640&amp;h=480" alt="<? echo $data["title"]; ?>" />
                                </a>
                            </li><? } ?>
                    </ul>
                </div>
            <? } ?>
            <footer class="share">
                <? include("includes/share.php"); ?>
            </footer>
        </div>
    </div>
</section>
