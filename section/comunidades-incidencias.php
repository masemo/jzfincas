<section>
    <div class="wrap">
        <header>
            <h1><?php echo obtener_valor($lang, $sec, "nombre", $id_seccion); ?></h1>
            <p class="breadcrumbs"><a href="<? echo obtener_url_amigable($lang, "comunidades", 1); ?>"><?php echo obtener_valor($lang, "comunidades", "nombre", 1); ?></a></p>
        </header>
        <table class="list">
            <thead>
            <tr>
                <th><p>#</p></th>
                <th style="width: 100px;"><p><?php echo lang("incidencias_idpublic") ?></p></th>
                <th><p><?php echo lang("incidencias_siniestro") ?></p></th>
                <th style="width: 200px;"><p><?php echo lang("incidencias_f_apertura") ?></p></th>
                <th style="width: 200px;"><p><?php echo lang("incidencias_f_finalizacion") ?></p></th>
                <th style="width: 100px;"><p><?php echo lang("incidencias_sector") ?></p></th>
                <th style="width: 100px;"><p><?php echo lang("incidencias_estado") ?></p></th>
                <th><p></p></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            while ($incidencia = mysqli_fetch_array($incidencias, MYSQLI_ASSOC)) { ?>
                <tr>
                    <td><p><?php echo $i++ ?></p></td>
                    <td><p><strong><?php echo $incidencia["idpublic"] ?></strong></p></td>
                    <td><p><?php echo $incidencia["siniestro"]; ?></p></td>
                    <td><p><?php echo f_date($incidencia["f_apertura"], "d-m-Y", "sin_fecha"); ?></p></td>
                    <td><p><?php echo f_date($incidencia["f_finalizacion"], "d-m-Y", "sin_fecha"); ?></p></td>
                    <td><p><?php echo $_sectores[$incidencia["sector"]]["nombre"]; ?></p></td>
                    <td><p class="estado <?php echo $_estados[$incidencia["estado"]]["color"] ?>"><?php echo $_estados[$incidencia["estado"]]["nombre"]; ?></p></td>
                    <td></td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot></tfoot>
        </table>
    </div>
</section>