<section>
    <div class="wrap">
        <div class="content">
            <header>
                <h1><? echo lang("error"); ?></h1>
            </header>
            <p><? echo lang("error_text"); ?></p>
            <p><? echo lang("error_text_options"); ?></p>
            <ul>
                <li><p><? echo lang("error_text_options_01"); ?></p></li>
                <li><p><? echo lang("error_text_options_02_01"); ?> <a href="<? echo $siteUrl; ?>"><? echo lang("error_text_options_02_02"); ?></a> <? echo lang("error_text_options_02_03"); ?></p></li>
                <li><p><? echo lang("error_text_options_03"); ?></p></li>
                <li><p><? echo lang("error_text_options_04_01"); ?> <a href="<? echo $contacto_fu; ?>"><? echo lang("error_text_options_04_02"); ?></a> <? echo lang("error_text_options_04_03"); ?></p></li>
            </ul>

            <h2><? echo lang("sitemap_txt"); ?></h2>
            <? include("includes/sitemap.php"); ?>
        </div>
    </div>
</section>