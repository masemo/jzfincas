<section>
    <div class="wrap">
        <header>
            <h1><?php echo obtener_valor($lang, $sec, "nombre", $id_seccion); ?></h1>
            <p class="breadcrumbs"><a href="<? echo obtener_url_amigable($lang, "comunidades", 1); ?>"><?php echo obtener_valor($lang, "comunidades", "nombre", 1); ?></a></p>
        </header>
        <?php if (isset($idpublic)) { ?>
            <div class="alert ok email"><? echo lang("incidencia_add_numero_de_incidencia") . " <strong>" . $idpublic . "</strong>. " . lang("incidencia_add_gracias") ?></div>
        <?php } else { ?>
            <div class="content formStyle">
                <?php if (isset($error) && $error != "") { ?>
                    <div class="alert_group">
                        <div class="alert catastrofe email"><? echo $error; ?></div>
                    </div>
                <?php } ?>
                <form id="incidencia_add_form" action="<? echo obtener_url_amigable($lang, $sec, $id_seccion); ?>" method="post" class="fran6validate">
                    <fieldset>
                        <? create_form($fields); ?>
                    </fieldset>
                </form>
            </div>
        <?php } ?>
    </div>
</section>