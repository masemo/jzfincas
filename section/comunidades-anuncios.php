<section>
    <div class="wrap">
        <header>
            <h1><?php echo obtener_valor($lang, $sec, "nombre", $id_seccion); ?></h1>
            <p class="breadcrumbs"><a href="<? echo obtener_url_amigable($lang, "comunidades", 1); ?>"><?php echo obtener_valor($lang, "comunidades", "nombre", 1); ?></a></p>
        </header>
        <table class="list">
            <thead>
            <tr>
                <th><p>#</p></th>
                <th><p><?php echo lang("anuncios_nombre") ?></p></th>
                <th><p><?php echo lang("anuncios_f_subida") ?></p></th>
                <th><p><?php echo lang("anuncios_f_reunion") ?></p></th>
                <th><p><?php echo lang("anuncios_documento") ?></p></th>
                <th><p></p></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            while ($anuncio = mysqli_fetch_array($anuncios, MYSQLI_ASSOC)) { ?>
                <tr>
                    <td><p><?php echo $i++ ?></p></td>
                    <td><p><?php echo $anuncio["nombre"] ?></p></td>
                    <td><p><?php echo f_date($anuncio["f_subida"], "d-m-Y", "sin_fecha"); ?></p></td>
                    <td><p><?php echo f_date($anuncio["f_reunion"], "d-m-Y", "sin_fecha"); ?></p></td>
                    <td><p><a href="<?php echo pintar_imagen(obtnener_imagen($anuncio['doc'])); ?>" target="_blank"><span class="icon-eye"></span> <?php echo lang("ver_documento") ?></a></p></td>
                    <td></td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot></tfoot>
        </table>
    </div>
</section>