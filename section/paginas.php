<section class="pagina">
    <div class="wrap">
        <div class="row">
            <div class="col-70">
                <header class="breadcrumber">
                    <?
                    if (!empty($migas)) {
                        include("includes/breadcrumbs.php");
                    }
                    ?>
                    <h1><? echo $title; ?></h1>
                </header>
                <div class="content editor_content">
                    <?
                    if ($desc != "") {
                        echo $desc;
                    } else {
                        ?>
                        <p class="alert notok"><? echo lang("trabajando_contenido"); ?></p>
                    <? } ?>
                </div>
                <? if (!empty($galeria)) { ?>
                    <div class="gallery">
                        <ul class="col-20">
                            <? foreach ($galeria as $item) {
                                ?>
                                <li>
                                <a href="<? echo pintar_imagen($item["image"]); ?>" class="swipebox" title="<? echo $item["desc"]; ?>">
                                    <img src="<? echo $siteUrl; ?>lib/timThumb/timThumb.php?src=<? echo pintar_imagen($item["image"], true); ?>&amp;w=320&amp;h=320&amp;zc=2" alt="<? echo $item["desc"]; ?>"/>
                                </a>
                                </li><? } ?>
                        </ul>
                    </div>
                <? } ?>
                <footer class="share">
                    <? include("includes/share.php"); ?>
                </footer>
            </div>
            <div class="col-30">
                <? include($themeIncludeUrl . "template/sidebar.php"); ?>
            </div>
        </div>
    </div>
</section>
