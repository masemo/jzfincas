<section>
    <div class="wrap">
        <div class="row">
            <div class="col-25">
                <? if (!empty($categorias)) { ?>
                    <div class="category_list">
                        <p class="title"><? echo lang("categoria_productos"); ?></p>
                        <?
                        if ($product_folder_style) {
                            include("includes/product-nav-folder-style.php");
                        } else {
                            include("includes/product-nav-list-style.php");
                        }
                        ?>
                    </div>
                    <?
                } else {
                    if (!$product_folder_style) {
                        ?>
                        <p class="alert notok"><? echo lang("sin_productos"); ?></p>
                        <?
                    }
                }
                ?>
            </div>
            <div class="col-75">
                <header class="breadcrumber hgroup">
                    <?
                    if (!empty($migas)) {
                        include("includes/breadcrumbs.php");
                    }
                    ?>
                    <h1>
                        <? echo $productos_title; ?>
                        <span class="launcher">&#9662;</span>
                    </h1>
                </header>
                <? if (!empty($descripcion)) { ?>
                    <div class="cat_description">
                        <? echo $descripcion; ?>
                    </div>
                <? } ?>
                <? if (!empty($productos)) { ?>
                    <div class="product_list">
                        <ul class="col-33">
                            <? foreach ($productos as $item) {
                                ?><li>
                                    <div class="frame">
                                        <figure>
                                            <img src="<? echo $item["imagen"]; ?>" alt="<? echo $item["nombre"]; ?>" />
                                        </figure>
                                        <div class="content">
                                            <div>
                                                <p class="title">
                                                    <? echo $item["nombre"]; ?>
                                                </p>
                                            </div>
                                            <? if (!empty($item["ref"])) { ?>
                                                <div>
                                                    <p class="ref">
                                                        REF: <? echo $item["ref"]; ?>
                                                    </p>
                                                </div>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <a href="<? echo $item["url"]; ?>" class="cover"><? echo $item["nombre"]; ?></a>
                                </li><? } ?>
                        </ul>
                    </div>
                    <?
                } else {
                    if (!$product_folder_style && $id_categoria > 0) {
                        ?>
                        <p class="alert notok"><? echo lang("sin_productos"); ?></p>
                        <?
                    }
                }
                ?>
            </div>
        </div>

        <? if (empty($productos) && empty($categorias) && $product_folder_style) { ?>
            <p class="alert notok"><? echo lang("sin_productos"); ?></p>
        <? } ?>
    </div>
</section>
