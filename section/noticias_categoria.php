<section class="noticias">
    <div class="wrap">
        <div class="row">
            <div class="col-70">
                <header>
                    <? if (!empty($migas)) {
                        include("includes/breadcrumbs.php");
                    } ?>
                </header>
                <div class="noticias-list row">
                    <? if (!empty($noticias_categorias)) { ?>
                        <div class="col-30">
                            <header>
                                <h3><? echo lang("categorias"); ?></h3>
                            </header>
                            <ul>
                                <? foreach ($noticias_categorias as $categoria) { ?>
                                    <li>
                                        <a href="<? echo $categoria["link"]; ?>" rel="subsection"><? echo $categoria["title"]; ?></a>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                    <? } ?>
                    <div class="col-70">
                        <header>
                            <h1><? echo $noticias_title; ?></h1>
                        </header>
                        <? if (!empty($noticias)) { ?>
                            <ul class="col-50 listado">
                                <? foreach ($noticias as $item) { ?>
                                    <li>
                                        <div>
                                            <a href="<? echo $item["link"]; ?>">
                                                <span class="icon"></span>
                                                <h3><? echo $item["title"]; ?></h3>
                                                <!--<time datetime="<? echo $item["datetime"]; ?>"><? echo $item["date"]; ?></time>-->
                                                <? if (isset($item["categoria"])) { ?>
                                                    <span class="categoria"><? echo $item["categoria"]; ?></span><br/>
                                                <? } ?>
                                            </a>
                                            <div class="content">
                                                <p><? echo $item["desc"]; ?> <a href="<? echo $item["link"]; ?>"><? echo lang("leer_mas"); ?></a></p>
                                            </div>
                                        </div>
                                    </li>
                                <? } ?>
                            </ul>
                        <? } else { ?>
                            <p class="alert notok"><? echo lang("sin_noticias"); ?></p>
                        <? } ?>
                    </div>
                </div>
                <? if (!empty($paginator)) { ?>
                    <footer class="pager">
                        <? echo $paginator; ?>
                    </footer>
                <? } ?>
            </div>
            <div class="col-30">
                <? include($themeIncludeUrl . "template/sidebar.php"); ?>
            </div>
        </div>
    </div>
</section>